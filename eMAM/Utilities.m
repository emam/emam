//
//  Utilities.m
//  eMAM
//
//  Created by APPLE on 22/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities


#pragma mark - Get File Extensions

+ (NSArray *)getMovieFileExtensions{
    
    return [NSArray arrayWithObjects:@"mov",@"mp4",@"3gp", nil];
    
}
+ (NSArray *)getAudioFileExtensions{
    
    return [NSArray arrayWithObjects:@"mp3", nil];
    
}
+ (NSArray *)getImageFileExtensions{
    
    return [NSArray arrayWithObjects:@"png",@"jpg",@"jpeg", nil];
    
}
+ (NSArray *)getDocumentFileExtensions{
    
    return [NSArray arrayWithObjects:@"doc",@"docx",@"xls",@"xlsx", nil];
    
}


#pragma mark - UI Elements

+ (UIButton *)checkBox  {
    UIButton *checkbox = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    [checkbox setImage:[UIImage imageNamed:@"checkbox-normal.png"] forState:UIControlStateNormal];
    [checkbox setImage:[UIImage imageNamed:@"checkbox-pressed.png"] forState:UIControlStateHighlighted];
    [checkbox setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateSelected];
    [checkbox setBackgroundColor:[UIColor clearColor]];
    
    return checkbox;
}

+ (UIButton *)radioButton  {
    UIButton *radioButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    [radioButton setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
    [radioButton setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateSelected];
    [radioButton setBackgroundColor:[UIColor clearColor]];
    
    return radioButton;
}

+ (UIImage *)buttonBackgroundImage {
    UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bg_button" ofType:@"png"]];
    image= [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6) resizingMode:UIImageResizingModeStretch];
    return image;
}



#pragma mark -

+ (NSString *)typeImagePath:(NSString *)fileType    {
    
    NSString *filePath = nil;
    
    BEGIN_BLOCK
    
    if ([[self getMovieFileExtensions] containsObject:fileType]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"videoIcon" ofType:@"png"];
        break;
    }
    
    if ([[self getAudioFileExtensions] containsObject:fileType]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"soundIcon" ofType:@"png"];
        break;
    }
    
    if ([[self getImageFileExtensions] containsObject:fileType]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"imgIcon" ofType:@"png"];
        break;
    }
    
    if ([[self getDocumentFileExtensions] containsObject:fileType]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"documentIcon" ofType:@"png"];
        break;
    }
    
    END_BLOCK
    
    return filePath;
}

+ (NSString *)orderByOptionValue    {
    
    NSString *orderby = @"ingested_on";
    NSString *sortOption = [Datastore sharedStore].sortOption;
    
    BEGIN_BLOCK
    
    if ([sortOption isEqualToString:@"Most Recent"]) {
        orderby = @"lastmodified_on";
        break;
    }
    
    if ([sortOption isEqualToString:@"Size"]) {
        orderby = @"size";
        break;
    }
    
    if ([sortOption isEqualToString:@"Name"]) {
        orderby = @"title";
        break;
    }
    
    if ([sortOption isEqualToString:@"Type"]) {
        orderby = @"type_name";
        break;
    }
    
    if ([sortOption isEqualToString:@"Rating"]) {
        orderby = @"rating";
        break;
    }
    
    if ([sortOption isEqualToString:@"Description"]) {
        orderby = @"asset_desc";
        break;
    }
    
    if ([sortOption isEqualToString:@"Category"]) {
        orderby = @"category_name";
        break;
    }
    
    if ([sortOption isEqualToString:@"Ingested On"]) {
        orderby = @"ingested_on";
        break;
    }
    
    if ([sortOption isEqualToString:@"Ingested By"]) {
        orderby = @"ingested_by";
        break;
    }
    
    END_BLOCK
    
    return orderby;
}

- (int)assetTypeIDForAsset:(NSString *)asset    {
    
    int assetType = 0;
    BEGIN_BLOCK
    
    if ([asset isEqualToString:@"Video"]) {
        assetType = 1;
        break;
    }
    
    if ([asset isEqualToString:@"Audio"]) {
        assetType = 2;
        break;
    }
    
    if ([asset isEqualToString:@"Image"]) {
        assetType = 3;
        break;
    }
    
    if ([asset isEqualToString:@"Other Files"]) {
        assetType = 4;
        break;
    }
    
    END_BLOCK
    
    return assetType;
}



#pragma mark - Get Gateways & User

+(NSString *)rootServer{
    
    NSString *rootServer = [[NSUserDefaults standardUserDefaults] objectForKey:KROOTSERVER];
    return rootServer;
    
}
+(NSString *)serviceGateway{
    
    NSString *serviceGateway = [[NSUserDefaults standardUserDefaults] objectForKey:KSERVICEGATEWAY];
    return serviceGateway;
    
}
+(NSString *)analyticsGateway{
    
    NSString *analyticsGateway = [[NSUserDefaults standardUserDefaults] objectForKey:KANALYTICSGATEWAY];
    return analyticsGateway;
    
}
+(NSString *)loginedUserName{
    
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:KUSERNAME];
    return username;
    
}

#pragma mark - Image Lazy Loading

+ (void)imageForURL:(NSString *)strURL completionBlock:(void(^)(UIImage *image))block{
    
    NSString *folderPath = [LIBRARY_FOLDER stringByAppendingPathComponent:@"Preview"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *error = nil;
        BOOL sucesss = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (!sucesss) {
            NSLog(@"Utilities Exception : fail to create intermediate directory : %@",[error description]);
        }
    }
    
    UIImage *image = nil;
    NSString *filePath = [folderPath stringByAppendingPathComponent:[strURL lastPathComponent]];
    if (filePath) {
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    
    if (image) {
        block(image);
    }
    else {
        NSURL *imageURL = [NSURL URLWithString:strURL];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                       ^{
                           NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                           UIImage *image = [UIImage imageWithData:imageData];
                           if(image) {
                               [imageData writeToFile:filePath atomically:YES];
                               block(image);
                           }
                       });
        imageURL = nil;
    }

    
}

#pragma mark - UDID & Date Services

+ (NSString *)getUUID{
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
    
}
+ (NSString *)dateString{
  
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
	[formatter setDateFormat:@"MM-dd-yyyy"];
    NSString *formatedDateString = [formatter stringFromDate:[NSDate date]];
    formatter = nil;
	return formatedDateString;
    
    
}
+ (NSString *)getUTCFormateDate:(NSDate *)localDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}
+ (NSString *)stringFromDate:(NSDate *)date formatString:(NSString*)dateFormatterString{
    
    if(!date) return nil;
	
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
	[formatter setDateFormat:dateFormatterString];
    NSString *formatedDateString = [formatter stringFromDate:date];
    formatter = nil;
    
	return formatedDateString;
    
}


#pragma mark - To Take Top View controller

+ (UIViewController *)getTopViewController  {
    
    @try {
        // Try to find the root view controller programmically
        UIViewController *topViewController;
        
        // Find the top window (that is not an alert view or other window)
        UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
        if (topWindow.windowLevel != UIWindowLevelNormal)   {
            NSArray *windows = [[UIApplication sharedApplication] windows];
            for(topWindow in windows)   {
                if (topWindow.windowLevel == UIWindowLevelNormal)
                    break;
            }
        }
        
        UIView *rootView = ([[topWindow subviews] count] > 0)?[[topWindow subviews] objectAtIndex:0]:nil;
        //to avoid catching up with sync overlay.
       // if ([rootView isKindOfClass:[MBProgressHUD class]]) {
        //    rootView = [[topWindow subviews] objectAtIndex:1];
       // }
        
        id nextResponder = [rootView nextResponder];
        
        if ([nextResponder isKindOfClass:[UINavigationController class]]) {
            topViewController = nextResponder;
        }
        else if ([nextResponder isKindOfClass:[UIViewController class]])  {
            topViewController = nextResponder;
        }
        else    {
            //NSAssert(NO, @"getTopViewController: Could not find a root view controller. May have some un-wanted subviews in application window");
            topViewController = nil;
            //            NSLog( @"Utilities :  Error : getTopViewController: Could not find a root view controller. May have some un-wanted subviews in application window ***");
        }
        
        return topViewController;
    }
    @catch (NSException *exception) {
        NSLog(@"Utilities Exception : in getTopViewController : %@ --Handled",[exception description]);
    }
    
    return nil;
    
}

#pragma mark - Responder Services

+ (UIView *)findFirstResponderBeneathView:(UIView *)view{
    
    for ( UIView *childView in view.subviews ) {
        if ([childView respondsToSelector:@selector(isFirstResponder)] &&
            [childView isFirstResponder])
            return childView;
        UIView *result = [self findFirstResponderBeneathView:childView];
        if (result)
            return result;
    }
    return nil;
    
}
+ (void)resignFirstResponder{
    
    UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
    if (topWindow.windowLevel != UIWindowLevelNormal)   {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(topWindow in windows)   {
            if (topWindow.windowLevel == UIWindowLevelNormal)
                break;
        }
    }
    
    UIView *currentResponderView = [self findFirstResponderBeneathView:topWindow];
    if (currentResponderView) {
        [currentResponderView resignFirstResponder];
    }
    
}

#pragma mark - Alert View Methods

+ (void)showAlert:(NSString *)strAlertMessage    {
    [[self class] showAlert:APPNAME message:strAlertMessage delegateObject:nil viewTag:0];
}

+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate    {
    [[self class] showAlert:alertTitle message:strAlertMessage delegateObject:delegate viewTag:0];
}

+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag  {
    [[self class] showAlert:alertTitle message:strAlertMessage delegateObject:delegate viewTag:iTag showCancel:NO];
}

+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag showCancel:(BOOL)bShow   {
    
    UIAlertView *alertView;
    
    @try {
        
        if (bShow) {
			alertView=[[UIAlertView alloc] initWithTitle:alertTitle message:strAlertMessage delegate:delegate cancelButtonTitle:nil otherButtonTitles:@"OK",@"Cancel",nil];
		}
		else {
			
			alertView=[[UIAlertView alloc] initWithTitle:alertTitle message:strAlertMessage delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil];
			
		}
		
		alertView.tag=iTag;
        
		[alertView show];
		alertView   =   nil;
	}
	@catch (NSException * e) {
		
	}
	@finally {
        
	}
}

+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag otherButtonTitle:(NSString*)strTitle {
    [[self class] showAlert:alertTitle message:strAlertMessage delegateObject:delegate viewTag:iTag otherButtonTitle:strTitle secondButtonTitle:@"Cancel"];
}

+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag otherButtonTitle:(NSString*)strTitle secondButtonTitle:(NSString *)strSecondBtnTitle {
    UIAlertView *alertView;
    
    @try {
		
		alertView=[[UIAlertView alloc] initWithTitle:alertTitle message:strAlertMessage delegate:delegate cancelButtonTitle:nil otherButtonTitles:strTitle,strSecondBtnTitle,nil];
        
		alertView.tag=iTag;
		
		[alertView show];
		alertView   =   nil;
	}
	@catch (NSException * e) {
		
	}
	@finally {
		
	}
}

@end

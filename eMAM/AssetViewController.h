//
//  AssetViewController.h
//  eMAM
//
//  Created by APPLE on 24/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewView.h"
#import "MPMoviePlayer.h"
#import "AVVideoPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

#import "TPKeyboardAvoidingScrollView.h"

typedef enum    {
    LeftScrollViewTag               = 4550,
    RightScrollViewTag              = 4551,
    CustomPlayerSizeInputViewTag    = 4552,
    CustomPlayerWidthInputViewTag   = 4553,
    CustomPlayerHeightInputViewTag  = 4554,
    IncludeThumbnailCheckBoxTag     = 4555,
    SkinAutoHideCheckBoxTag         = 4556,
    AutoPlayCheckBoxTag             = 4557,
    ClosedCaptionCheckBoxTag        = 4558,
    FullScreenCheckBoxTag           = 4559,
    FinalTextViewTag                = 4560,
} EmbedViewSubViewTag;

typedef enum    {
    Preview      = 1,
    MetaData     = 2,
    Annotation   = 3,
    Subclips      = 4,
    Markers      = 5,
    ESend        = 6,
    Approval     = 7,
    Comment      = 8,
    History      = 9,
    Embed        = 10
} AssetViewType;

typedef enum    {
    Video       = 1,
    Audio       = 2,
    Image       = 3,
    OtherFiles  = 4
} AssetType;

typedef enum    {
    TopTitleViewTag      = 2623,
    BottomBarTag         = 2624,
} AssetViewerSubViewTag;


typedef enum    {
    EmailTextFieldTag                   = 4625,
    SubjectTextFieldTag                 = 4626,
    DescriptionTextViewTag              = 4627,
    SendButtonTag                       = 4628,
    ShowAdvanceEsendButtonTag           = 4629,
} eSendViewSubViewTag;


typedef enum    {
    AssetInfoTitleTag       = 3070,
    AssetInfoDescriptionTag = 3071,
    AssetInfoAuthorTag      = 3072,
    AssetInfoUploadOnTag    = 3073,
    AssetInfoUploadByTag    = 3074,
    TagTextViewTag          = 3075,
    TagPrivateRBTag         = 3076,
    TagPublicRBTag          = 3077,
    TagAddButtonTag         = 3078,
} MetadataViewSubViewTag;




@interface AssetViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIPopoverControllerDelegate>{
    UIDatePicker *approvalDatePicker;
    UIPopoverController *popoverControllerDownloads;
}

@property (nonatomic,strong) MPMoviePlayerController* moviePlayer;

@property (nonatomic, retain) NSDictionary *asset;
@property (nonatomic, assign) AssetViewType currentViewType;
@property (nonatomic, assign) AssetType currentAssetType;
@property (nonatomic, strong) NSMutableArray *possibleAssetOptions;


@property (nonatomic, strong) NSArray *previewInfo;
@property (nonatomic, assign) UIBarButtonItem *versionTitleBarItem;
@property (nonatomic, assign) int currentVersionIndex;



@property(nonatomic, strong) UIButton *selectedButton;
@property(nonatomic, strong) UIView *currentView;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

@property (weak, nonatomic) IBOutlet UIView *PreviewView;
@property (weak, nonatomic) IBOutlet UIView *movieView;
@property (weak, nonatomic) IBOutlet UILabel *previewTitle;
@property (weak, nonatomic) IBOutlet UILabel *previewDescription;
@property (weak, nonatomic) IBOutlet UILabel *assetDateAdded;
@property (weak, nonatomic) IBOutlet UILabel *assetTags;
@property (weak, nonatomic) IBOutlet UILabel *assetCategories;
@property (weak, nonatomic) IBOutlet UILabel *assetProjects;

- (IBAction)doneTapped:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *prevDetView;
@property (weak, nonatomic) IBOutlet UIScrollView *prevDetScroll;

@property (weak, nonatomic) IBOutlet UIView *embedView;
@property (weak, nonatomic) IBOutlet UIScrollView *embedDetScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *embedDetSkinScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *embedDetBtnsScroll;


@property (weak, nonatomic) IBOutlet UIView *nonMovieView;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;

@property (weak, nonatomic) IBOutlet UIView *historyView;
@property (weak, nonatomic) IBOutlet UIScrollView *historyScrollView;
@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *commentsScrollView;

@property (weak, nonatomic) IBOutlet UIView *esendView;
@property (weak, nonatomic) IBOutlet UIScrollView *esendScrollView;
@property (weak, nonatomic) IBOutlet UITextField *esentEmail;
@property (weak, nonatomic) IBOutlet UITextField *esendSubject;
@property (weak, nonatomic) IBOutlet UITextView *esendDescri;

@property (weak, nonatomic) IBOutlet UIView *approvalView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *approvalScrollView;

@property (weak, nonatomic) IBOutlet UITextField *internalApprovalUserTextField;
@property (weak, nonatomic) IBOutlet UITextField *externalApprovalEmail;
@property (weak, nonatomic) IBOutlet UITextField *externalApprovalUsername;
@property (weak, nonatomic) IBOutlet UITextField *externalApprovalPassword;
@property (weak, nonatomic) IBOutlet UITextField *externalApprovalExpiryDate;
@property (weak, nonatomic) IBOutlet UIButton *internalApprovalRB;
@property (weak, nonatomic) IBOutlet UIButton *externalApprovalRB;


@property (weak, nonatomic) IBOutlet UIView *metadataView;
@property (weak, nonatomic) IBOutlet UIView *meta1View;
@property (weak, nonatomic) IBOutlet UIView *meta2View;
@property (weak, nonatomic) IBOutlet UIView *meta3View;
@property (weak, nonatomic) IBOutlet UIView *meta4View;
@property (weak, nonatomic) IBOutlet UISegmentedControl *metaDataSegment;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *meta1ScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *meta2ScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *meta3ScrollView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *meta4ScrollView;
@property (weak, nonatomic) IBOutlet UITextField *meta1Title;
@property (weak, nonatomic) IBOutlet UITextView *meta1Descri;
@property (weak, nonatomic) IBOutlet UITextField *meta1Autor;
@property (weak, nonatomic) IBOutlet UILabel *meta1UploadOn;
@property (weak, nonatomic) IBOutlet UILabel *meta1UploadBy;
@property (weak, nonatomic) IBOutlet UITableView *meta2TableView;
@property (weak, nonatomic) IBOutlet UITableView *meta3TableView;
@property (weak, nonatomic) IBOutlet UITableView *meta4TableView;
@property (weak, nonatomic) IBOutlet UIButton *meta4PrivateBtn;
@property (weak, nonatomic) IBOutlet UIButton *meta4PublicBtn;
@property (weak, nonatomic) IBOutlet UITextView *meta4TagTxtView;
@property (weak, nonatomic) IBOutlet UIButton *meta4Add;


@property (weak, nonatomic) IBOutlet UIView *AVContainerView;




@property (nonatomic, strong) NSArray *embedInfo;
@property (nonatomic, strong) UIButton *selectRadioButton;
@property (nonatomic, strong) NSMutableDictionary *selectedValues;
- (void)setResponse:(NSString *)response;

- (void)shareVideo:(NSDictionary *)dictionay;
- (IBAction)getEmbedTapped:(id)sender;
- (IBAction)embedShareTapped:(id)sender;
- (IBAction)fullScreenTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *fullScrnBtn;
@property (weak, nonatomic) IBOutlet UILabel *historyAssetIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *historyTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *historyVerdionIdLbl;

@property (weak, nonatomic) IBOutlet UITableView *historyTable;

@property (weak, nonatomic) IBOutlet UITableView *commentsTable;
@property (weak, nonatomic) IBOutlet UITextView *commentTxtView;

@property (weak, nonatomic) IBOutlet UIButton *showDownloadsBtn;
@property (weak, nonatomic) IBOutlet UIButton *fetchForDownloadBtn;
@property (weak, nonatomic) IBOutlet UIProgressView *dwnldProgressView;

@property (weak, nonatomic) IBOutlet UILabel *progressLbl;




- (IBAction)addCommentTapped:(id)sender;

- (IBAction)esendSendTapped:(id)sender;
- (IBAction)esendAdvancedTapped:(id)sender;

- (IBAction)internalApprovalRadioButtonClicked:(id)sender;
- (IBAction)externalApprovalRadioButtonClicked:(id)sender;
- (IBAction)sendApprovalButtonClicked:(id)sender;
- (IBAction)selectUserButtonClicked:(id)sender;

- (IBAction)metadataSegmentTapped:(id)sender;
- (IBAction)updateAssetMetadataInfoTapped:(id)sender;
- (IBAction)meta4ClearTapped:(id)sender;
- (IBAction)meta4EditTapped:(id)sender;
- (IBAction)meta4PrivateTapped:(id)sender;
- (IBAction)meta4PublicTapped:(id)sender;

- (IBAction)showDownloadsTapped:(id)sender;
- (IBAction)fetchForOfflineTapped:(id)sender;





@end

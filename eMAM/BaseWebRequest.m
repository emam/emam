//
//  BaseWebRequest.m
//  eMAM
//
//  Created by APPLE on 06/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "BaseWebRequest.h"
#import "AppDelegate.h"

@implementation BaseWebRequest

#pragma mark - Host Name From URL

+ (NSString *)hostNameFromURL:(NSString *)urlString    {
    
    NSString    *hostName   =   nil;
    @try    {
        
        if (urlString != nil && [urlString length] > 0) {
            
            if ([urlString rangeOfString:@"https://"].location != NSNotFound)   {
                NSString    *hostString =   [urlString stringByReplacingOccurrencesOfString:@"https://" withString:@""];
                NSArray *hostParts  =   [hostString componentsSeparatedByString:@"/"];
                hostName    =   [hostParts objectAtIndex:0];
                hostParts = nil;
            }
            else if ([urlString rangeOfString:@"http://"].location != NSNotFound)   {
                NSString    *hostString =   [urlString stringByReplacingOccurrencesOfString:@"http://" withString:@""];
                NSArray *hostParts  =   [hostString componentsSeparatedByString:@"/"];
                hostName    =   [hostParts objectAtIndex:0];
                hostParts = nil;
                hostString = nil;
            }
            else    {
                NSArray *hostParts  =   [urlString componentsSeparatedByString:@"/"];
                hostName    =   [hostParts objectAtIndex:0];
                hostParts = nil;
            }
        }
        
    }
    @catch (NSException *exception) {
        
        hostName =   nil;
        NSLog(@"Exception on hostNameFromURL - %@ : %@",urlString,[exception description]);
    }
    @finally    {
        //NSLog(@"HostName:%@",hostName);
        return hostName;
    }
    //NSLog(@"HostName:%@",hostName);
    return hostName;
}


#pragma mark - Domain Name From URL

+ (NSString *)domainNameFromURL:(NSString *)urlString  {
    
    NSString    *domainName =   nil;
    @try    {
        
        NSString    *hostName   =   [[self class] hostNameFromURL:urlString];
        if (hostName != nil && [hostName length] > 0)   {
            NSArray *hostNameParts  =   [hostName componentsSeparatedByString:@"."];
            int noOfParts   =   [hostNameParts count];
            domainName  =   [hostNameParts objectAtIndex:noOfParts-2];
            hostNameParts = nil;
        }
    }
    @catch (NSException *exception) {
        
        domainName =   nil;
        NSLog(@"Exception on domainNameFromURL - %@ : %@",urlString,[exception description]);
    }
    @finally    {
         //NSLog(@"DomainName:%@",domainName);
        return domainName;
    }
    //NSLog(@"DomainName:%@",domainName);
    return domainName;
}

#pragma mark - Soap Header Remove

+ (NSDictionary *)truncateSoapHeaders:(NSDictionary *)dictionary    {
    if ([dictionary objectForKey:@"soap:Envelope"]) {
        dictionary = [dictionary objectForKey:@"soap:Envelope"];
        if ([dictionary objectForKey:@"soap:Body"]) {
            dictionary = [dictionary objectForKey:@"soap:Body"];
        }
    }
    return dictionary;
}



#pragma mark - Base Soap Request

+ (NSMutableURLRequest *)requestWithUrlStr:(NSString *)urlStr{
    
    NSURL *baseURL = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:baseURL];
    //[request setTimeoutInterval:600];
    [request setHTTPMethod:@"GET"];
	return request;
}


+ (NSMutableURLRequest *)soapPostRequestWithSoapAction:(NSString *)soapAction soapBody:(NSString *)soapBody parameters:(id)parameters{
    
    NSString *completePostBody = nil;
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:baseURL];
    
    
    if (soapBody != nil) {
        completePostBody  =   [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                               "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                               "<soap:Body>\n"
                               "%@\n"
                               "</soap:Body>\n"
                               "</soap:Envelope>\n",soapBody];
    }
    else    {
        completePostBody  =   [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                               "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                               "<soap:Body>\n"
                               "</soap:Body>\n"
                               "</soap:Envelope>\n"];
    }
    
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:600];
    [request setHTTPBody:[completePostBody dataUsingEncoding:NSUTF8StringEncoding]];
    [request addValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [request setValue:[self hostNameFromURL:[baseURL absoluteString]] forHTTPHeaderField:@"Host"];
    [request setValue:[self domainNameFromURL:[baseURL absoluteString]] forHTTPHeaderField:@"Domain"];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSLog(@"request:%@",request);

    return request;
    
    
}

+ (void)sendRequestASI:(NSURLRequest *)request completionHandler:(void (^)(NSDictionary*, NSError*)) handler {
    
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
   
    
   
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // NSLog(@"sendRequestASI Response: %@", responseObject);
        
        if ([responseObject isKindOfClass:[NSData class]]) {
            NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSError *error = nil;
            NSDictionary *dict = [self truncateSoapHeaders:[XMLReader dictionaryForXMLString:string  error:&error]];
           // NSLog(@"sendRequestASI Response:%@", dict);
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(dict,error);
            });
            return;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"sendRequestASI Response Error: %@", error);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(nil,error);
        });
        return;
        
    }];
    [[NSOperationQueue mainQueue] addOperation:operation];
    
}



@end

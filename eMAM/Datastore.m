//
//  Datastore.m
//  eMAM
//
//  Created by APPLE on 08/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "Datastore.h"

static Datastore *datastore = nil;

@implementation Datastore

@synthesize userRoleID;
@synthesize userFeatures = _userFeatures;

@synthesize totalAssets;
@synthesize assetsPerPage;
@synthesize sortOption;
@synthesize sortOrder;

@synthesize assets;
@synthesize basketAssets;
@synthesize projects;
@synthesize categories;
@synthesize assetTypes;
@synthesize assetSizes;
@synthesize frameDuration;

@synthesize parentCategoryId;
@synthesize subCategoryId;
@synthesize projectId;
@synthesize subProjectId;

@synthesize assetTypeId;
@synthesize assetMinSize;
@synthesize assetMaxSize;

// for search filtering
@synthesize assetTypeSearchFilter;
@synthesize categorySearchFilter;
@synthesize projectSearchFilter;
@synthesize assetSizeSearchFilter;

// for advance search
@synthesize isInSearch;
@synthesize isInAdvancedSearch;
@synthesize searchString;

@synthesize advancedSearchOption;
@synthesize needFullTextSearch;

@synthesize advancedSearchInOptions = _advancedSearchInOptions;
@synthesize advancedRefineSearchOptions = _advancedRefineSearchOptions;
@synthesize advancedSearchApprovalStates = _advancedSearchApprovalStates;
@synthesize advancedSearchAssetStates = _advancedSearchAssetStates;

@synthesize advancedSearchCategoryId;
@synthesize includeSubCategoriesInSearch;

@synthesize advancedSearchRating;

@synthesize searchCategories;

// for Advanced Asset Service
@synthesize currentPreviewInfo = _currentPreviewInfo;
@synthesize videoFrameRates = _videoFrameRates;



#pragma mark - Data access & Clear

+ (Datastore *)sharedStore  {
    if (!datastore) {
        datastore = [[Datastore alloc] init];
    }
    return datastore;
}

- (void)clearAllFilters{
    
    self.assets = nil;
    self.basketAssets = nil;
    self.projects = nil;
    self.categories = nil;
    self.assetTypes = nil;
    self.assetSizes = nil;
    self.frameDuration = nil;
    
    
    
    self.projectId = 0;
    self.subCategoryId = 0;
    self.parentCategoryId = 0;
    self.assetMinSize = 0;
    self.assetMaxSize = 0;
    self.assetTypeId = 0;
    
    self.assetTypeSearchFilter = nil;
    self.categorySearchFilter = nil;
    self.projectSearchFilter = nil;
    self.assetSizeSearchFilter = nil;
    
    self.isInSearch = NO;
    self.isInAdvancedSearch = NO;
    self.searchString = nil;
    self.needFullTextSearch = NO;
    self.advancedSearchInOptions = nil;
    self.advancedRefineSearchOptions = nil;
    self.advancedSearchApprovalStates = nil;
    self.advancedSearchAssetStates = nil;
    self.advancedSearchCategoryId = 0;
    self.includeSubCategoriesInSearch = NO;
    self.advancedSearchRating = 0;
    self.searchCategories = nil;
    
    self.currentPreviewInfo = nil;
    self.videoFrameRates = nil;
    
}


#pragma mark - User Features Methods

- (NSMutableArray *)userFeatures    {
    if (!_userFeatures) {
        _userFeatures = [[NSMutableArray alloc] initWithCapacity:53];
    }
    return _userFeatures;
}

- (BOOL)setUserFeature:(NSString *)userFeature  {
    BOOL success = ([userFeature length] <= 57);
    if (success) {
        int strLen = [userFeature length];
        for (int i = 0; i < strLen; i++) {
            NSString *singleCharSubstring = [userFeature substringWithRange:NSMakeRange(i, 1)];
            NSInteger result = [singleCharSubstring integerValue];
            
            [self.userFeatures insertObject:[NSNumber numberWithInt:result] atIndex:i];
        }
        
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:userFeature forKey:@"UserFeature"];
    }
    return success;
}

- (BOOL)allowUserFeature:(UserFeature)userFeature {
    return [[self.userFeatures objectAtIndex:userFeature] boolValue];
}

#pragma mark

- (NSArray *)categoriesWithID:(int)categoryId   {
    NSMutableArray *result = nil;
    if ([self.categories count] > 0) {
        result = [NSMutableArray array];
        for (NSDictionary *dictionary in self.categories) {
            if ([[[dictionary objectForKey:@"CAT_ID"] objectForKey:@"text"] intValue] == categoryId) {
                [result addObject:dictionary];
            }
        }
    }
    return result;
}

- (NSMutableArray *)subCategoriesWithParentID:(int)categoryId   {
    NSMutableArray *result = nil;
    if ([self.categories count] > 0) {
        result = [NSMutableArray array];
        for (NSDictionary *dictionary in self.categories) {
            if ([[[dictionary objectForKey:@"PARENT_ID"] objectForKey:@"text"] intValue] == categoryId) {
                [result addObject:dictionary];
            }
        }
    }
    return result;
}

#pragma mark

- (NSArray *)searchCategoriesWithID:(int)categoryId   {
    NSMutableArray *result = nil;
    if ([self.searchCategories count] > 0) {
        result = [NSMutableArray array];
        for (NSDictionary *dictionary in self.searchCategories) {
            if ([[[dictionary objectForKey:@"ID"] objectForKey:@"text"] intValue] == categoryId) {
                [result addObject:dictionary];
            }
        }
    }
    return result;
}

- (NSMutableArray *)subSearchCategoriesWithParentID:(int)categoryId   {
    NSMutableArray *result = nil;
    if ([self.searchCategories count] > 0) {
        result = [NSMutableArray array];
        for (NSDictionary *dictionary in self.searchCategories) {
            if ([[[dictionary objectForKey:@"parent_id"] objectForKey:@"text"] intValue] == categoryId) {
                [result addObject:dictionary];
            }
        }
    }
    return result;
}

#pragma mark

- (NSMutableArray *)advancedSearchInOptions    {
    if (!_advancedSearchInOptions) {
        _advancedSearchInOptions = [[NSMutableArray alloc] init];
    }
    return _advancedSearchInOptions;
}

- (NSMutableArray *)advancedRefineSearchOptions    {
    if (!_advancedRefineSearchOptions) {
        _advancedRefineSearchOptions = [[NSMutableArray alloc] init];
    }
    return _advancedRefineSearchOptions;
}

- (NSMutableArray *)advancedSearchAssetStates    {
    if (!_advancedSearchAssetStates) {
        _advancedSearchAssetStates = [[NSMutableArray alloc] init];
    }
    return _advancedSearchAssetStates;
}

- (NSMutableArray *)advancedSearchApprovalStates    {
    if (!_advancedSearchApprovalStates) {
        _advancedSearchApprovalStates = [[NSMutableArray alloc] init];
    }
    return _advancedSearchApprovalStates;
}




@end

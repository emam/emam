//
//  AVVideoPlayer.m
//  AVPlayerSample
//
//  Created by Naveen Shan on 11/3/12.
//  Copyright (c) 2012 Naveen Shan. All rights reserved.
//

#import "AVVideoPlayer.h"

#import "Marker.h"

#import <QuartzCore/QuartzCore.h>

/* Asset keys */
NSString * const kTracksKey         = @"tracks";
NSString * const kPlayableKey		= @"playable";

/* PlayerItem keys */
NSString * const kStatusKey         = @"status";

/* AVPlayer keys */
NSString * const kRateKey			= @"rate";
NSString * const kCurrentItemKey	= @"currentItem";

static void *AVPlayerRateObservationContext = &AVPlayerRateObservationContext;
static void *AVPlayerStatusObservationContext = &AVPlayerStatusObservationContext;
static void *AVPlayerCurrentItemObservationContext = &AVPlayerCurrentItemObservationContext;

static float nominalFrameRate;

@interface AVVideoPlayer  ()  {
    
    BOOL seekToZeroBeforePlay;
    
    id mTimeObserver;
    float mRestoreAfterScrubbingRate;
}

@property (nonatomic, strong) AVPlayer      *player;
@property (nonatomic, strong) AVPlayerItem  *playerItem;
@property (nonatomic, strong) AVPlayerLayer *playerView;

@property (nonatomic, strong) UIView        *videoControlPanel;
@property (nonatomic, strong) UISlider      *videoSlider;
@property (nonatomic, strong) UIButton      *playPauseButton;
@property (nonatomic, strong) UILabel       *timeLabel;
@property (nonatomic, strong) UIButton      *fullScreenButton;

@end

@implementation AVVideoPlayer

@synthesize currentOrientation;

@synthesize URL = mURL;

@synthesize player = mPlayer;
@synthesize timeLabel = mtimeLabel;
@synthesize frameRate = mframeRate;
@synthesize playerItem = mPlayerItem;
@synthesize videoSlider = mVideoSlider;
@synthesize videoMarkers = mVideoMarkers;
@synthesize videoSubclips = mVideoSubclips;
@synthesize playPauseButton = mPlayPauseButton;
@synthesize fullScreenButton = mFullScreenButton;
@synthesize videoControlPanel = _videoControlPanel;

@synthesize needShowMarker = mNeedShowMarker;
@synthesize needShowSubclip = mNeedShowSubclip;



#pragma mark - Initialization

+ (AVVideoPlayer *)sharedInstance {
    static AVVideoPlayer *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+ (void)destruct {
    AVVideoPlayer *instance = [[self class] sharedInstance];
    instance = nil;
}

- (id)initWithFrame:(CGRect)frame   {
    frame = [[UIScreen mainScreen] bounds];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor blackColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
        
        [self addVideoPlayerView];
        [self showScrubberAndButtons];
    }
    return self;
}

- (void)setFrame:(CGRect)frame  {
    [super setFrame:frame];
    
    frame.size.height -= 45;
    [self.playerView setFrame:frame];
    self.videoControlPanel.frame = CGRectMake(0, (self.frame.size.height - 45), (self.frame.size.width), 45);
    
}


- (void)orientationChanged:(NSNotification *)note
{
    self.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame = CGRectMake(0, 0, 768, 500);
    } else {
        
        self.frame = CGRectMake(0, 0, 1024, 500);
    }
    
    [self arrangeViews];

}

#pragma mark - Custom Actions

- (UIImage *)imageWithColor:(UIColor *)color  {
    // Create Transparent bg
    CGRect rect = CGRectMake(0, 0, 15, 15);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)arrangeViews    {
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame = CGRectMake(0, 0, 768, 500);
    } else {
        
        self.frame = CGRectMake(0, 0, 1024, 500);
    }
    
}

- (void)showScrubberAndButtons    {
    self.videoControlPanel = [[UIView alloc] init];
    self.videoControlPanel.frame = CGRectMake(0, (self.frame.size.height - 45), (self.frame.size.width), 45);
    self.videoControlPanel.backgroundColor = [UIColor grayColor];
    [self addSubview:self.videoControlPanel];
    
    self.playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playPauseButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin);
    self.playPauseButton.frame = CGRectMake(20, 10, 50, 25);
    [self.playPauseButton setImage:[UIImage imageNamed:@"vc_play.png"] forState:UIControlStateNormal];
    [self.playPauseButton setImage:[UIImage imageNamed:@"vc_pause.png"] forState:UIControlStateSelected];
    [self.playPauseButton addTarget:self action:@selector(playPauseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.videoControlPanel addSubview:self.playPauseButton];
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.playPauseButton.frame) + 30), 10, (150), 20)];
    self.timeLabel.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin);
    [self.timeLabel setFont:[UIFont fontWithName:APPBOLDFONTNAME size:12.0]];
    [self.timeLabel setBackgroundColor:[UIColor clearColor]];
    [self.timeLabel setTextColor:[UIColor whiteColor]];
    [self updateTimeLabel];
    
    [self.videoControlPanel addSubview:self.timeLabel];
    
    self.videoSlider = [[UISlider alloc] initWithFrame:CGRectMake((CGRectGetMaxX(self.timeLabel.frame) + 10), 12, (self.frame.size.width - 300), 15)];
    self.videoSlider.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin);
    UIImage *thumbImage = [UIImage imageNamed:@"vc_sliderthumb.png"]; //[self imageWithColor:[UIColor whiteColor]];
    [self.videoSlider setThumbImage:thumbImage forState:UIControlStateNormal];
    UIImage *minimumImage = [self imageWithColor:[UIColor lightGrayColor]];
    [self.videoSlider setMinimumTrackImage:minimumImage forState:UIControlStateNormal];
    UIImage *maximumImage = [self imageWithColor:[UIColor darkGrayColor]];
    [self.videoSlider setMaximumTrackImage:maximumImage forState:UIControlStateNormal];
    [self.videoSlider.layer setMasksToBounds:YES];
    [self.videoSlider.layer setCornerRadius:16.0];
    
    [self.videoSlider addTarget:self action:@selector(beginScrubbing:) forControlEvents:UIControlEventTouchDown];
    [self.videoSlider addTarget:self action:@selector(endScrubbing:) forControlEvents:UIControlEventTouchUpInside];
    [self.videoSlider addTarget:self action:@selector(endScrubbing:) forControlEvents:UIControlEventTouchUpOutside];
    [self.videoSlider addTarget:self action:@selector(scrub:) forControlEvents:UIControlEventValueChanged];
    
    [self.videoControlPanel addSubview:self.videoSlider];
    
    self.fullScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.fullScreenButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin);
    self.fullScreenButton.frame = CGRectMake((CGRectGetMaxX(self.videoSlider.frame) + 12), 9, 23, 23);
    [self.fullScreenButton setImage:[UIImage imageNamed:@"vc_fullscreen.png"] forState:UIControlStateNormal];
    [self.fullScreenButton setImage:[UIImage imageNamed:@"vc_normalscreen.png"] forState:UIControlStateSelected];
    [self.fullScreenButton addTarget:self action:@selector(fullScreenButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[self.videoControlPanel addSubview:self.fullScreenButton];
}

+ (NSString *)formattedStringForDuration:(CMTime)time    {
    if (CMTIME_IS_INVALID(time)) {
        return @"00:00:00:00";
    }
    double timeInSeconds = CMTimeGetSeconds(time);
    double duration = timeInSeconds;
    NSInteger hour = floor(duration/(60 * 60));
    duration = (duration - hour * (60 * 60));
    NSInteger minutes = floor(duration/60);
    duration = (duration - (minutes * 60));
    NSInteger seconds = round(duration);
    duration = fmodf(timeInSeconds,1);

    float videoFrameRate = ([[self class] sharedInstance].frameRate ? [[self class] sharedInstance].frameRate : nominalFrameRate);
    NSInteger currentFrame = roundf(duration * videoFrameRate);

    return [NSString stringWithFormat:@"%02d:%02d:%02d:%02d",hour, minutes, seconds,currentFrame];
}



+ (NSString *)formattedStringForDurationSubClip:(CMTime)time    {
    if (CMTIME_IS_INVALID(time)) {
        return @"00000000";
    }
    double timeInSeconds = CMTimeGetSeconds(time);
    double duration = timeInSeconds;
    NSInteger hour = floor(duration/(60 * 60));
    duration = (duration - hour * (60 * 60));
    NSInteger minutes = floor(duration/60);
    duration = (duration - (minutes * 60));
    NSInteger seconds = round(duration);
    duration = fmodf(timeInSeconds,1);
    
    float videoFrameRate = ([[self class] sharedInstance].frameRate ? [[self class] sharedInstance].frameRate : nominalFrameRate);
    NSInteger currentFrame = roundf(duration * videoFrameRate);
    
    return [NSString stringWithFormat:@"%02d%02d%02d%02d",hour, minutes, seconds,currentFrame];
}



- (void)updateTimeLabel {
    
    
    NSString *playerDuration = [[self class] formattedStringForDuration:[self playerItemDuration]];
    CMTime currentTime = [self.player currentTime];

    NSString *playerTime = [[self class] formattedStringForDuration:currentTime];
    
    [self.timeLabel setText:[NSString stringWithFormat:@"%@ / %@",playerTime,playerDuration]];
}

- (void)showFullScreenOption:(BOOL)show {
    self.fullScreenButton.hidden = !show;
}

////Dony////

- (void)fullScreenReset:(id)sender{
    
    [self.superview bringSubviewToFront:self];
    CGRect frame;
    frame = CGRectMake(0, 0, self.frame.size.width, 500);
    [self setFrame:frame];
    
    
    
    if (mFullScreenButton.selected) {
       mFullScreenButton.selected = !mFullScreenButton.selected; 
    }else{
        mFullScreenButton.selected = mFullScreenButton.selected; 
    }
    

}

////Dony////

- (void)fullScreenButtonClicked:(UIButton *)sender  {
    [self.superview bringSubviewToFront:self];
    self.playerView.hidden = YES;
    [UIView animateWithDuration:0.4 animations:^{
        CGRect frame;
        
        if (sender.selected) {
            frame = CGRectMake(0, 0, self.frame.size.width, 500);
            [self setFrame:frame];
        } else {
            frame  = [[UIScreen mainScreen] bounds];
            frame.size.height -= 64;
            [self setFrame:frame];
        }
    }completion:^(BOOL finished){
        sender.selected = !sender.selected;
        
        ////Dony////
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NavHide" object:nil];
        
        
        ///Dony/////
        
        self.playerView.hidden = NO;
    }];
}

#pragma mark Marker Control

- (NSMutableArray *)videoMarkers    {
    if (!mVideoMarkers) {
        mVideoMarkers = [[NSMutableArray alloc] init];
    }
    return mVideoMarkers;
}



- (BOOL) checkDuplicateMark:(Marker *)marker {
    
    
    for (Marker *markToCompare in self.videoMarkers) {
        
        
        if ([markToCompare.name isEqualToString:marker.name]) {
            return TRUE;
        }
        
    }
    
    return FALSE;
    
    
}



- (void)addNewMarker {
    
    if (!self.needShowMarker) {
        return;
    }
    [self beginScrubbing:nil];
    
    CMTime currentTime = [self.player currentTime];
    NSString *timeCode = [[self class] formattedStringForDuration:[self.player currentTime]];
    NSLog(@"AVVideoPlayer : TimeCode : %@ - Time %f",timeCode,CMTimeGetSeconds(currentTime));
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.timeCode = %@",timeCode];
    Marker *foundMarker = [[self.videoMarkers filteredArrayUsingPredicate:predicate] lastObject];

    if (!foundMarker) {
        Marker *marker = [[Marker alloc] init];
        marker.markerId = @"-1";
        
        marker.description = @"";
        marker.timeCode = timeCode;
//        marker.duration = [[self class] formattedStringForDuration:[self playerItemDuration]];
        marker.duration = @"00:00:00:00";
        marker.markerTime = currentTime;
        
       
        int totMark=[self.videoMarkers count];
        
        NSLog(@" totMark:%d", totMark);
        
        if (totMark) {

            for (int i=0; i<totMark;i++) {
                marker.name = [NSString stringWithFormat:@"Marker %d",(i + 2)];
                
                if ((i==0)&& ([marker.name isEqualToString:@"Marker 2"]) && ([self checkDuplicateMark:marker])){
                    marker.name = [NSString stringWithFormat:@"Marker %d",(i + 1)];
                }
                
                //NSLog(@" marker.name1:%@", marker.name);
                
                if (![self checkDuplicateMark:marker]) {
                    
                    //NSLog(@" marker.name2:%@", marker.name);
                    
                    if ([self addMarkerForMarker:marker]) {
                        [self.videoMarkers addObject:marker];
                    } else {
                        [Utilities showAlert:@"Fail to Add Marker on Video."];
                    }
                    break;
                }                
                
            }
            
        }else{
            
            marker.name = [NSString stringWithFormat:@"Marker %d",(0+ 1)];
            if ([self addMarkerForMarker:marker]) {
                [self.videoMarkers addObject:marker];
            } else {
                [Utilities showAlert:@"Fail to Add Marker on Video."];
            }

        }
        
        
    } else {
        [Utilities showAlert:@"A Marker exists with current time frame."];
    }
    
    NSLog(@"Markers Final:%@",[[AVVideoPlayer sharedInstance] videoMarkers]);
    
    [self endScrubbing:nil];
}

- (BOOL)addMarkerForMarker:(Marker *)marker {
    if (!self.needShowMarker) {
        return NO;
    }
    
     NSLog(@"addMarkerForMarker marker: %@",marker.name);
    NSLog(@"addMarkerForMarker marker: %@",marker.markerId);
//    NSLog(@"addMarkerForMarker marker: %@",marker);
//    NSLog(@"addMarkerForMarker marker: %@",marker);
//    NSLog(@"addMarkerForMarker marker: %@",marker);
    
    
    CMTime time = marker.markerTime;
    CMTime playerDuration = [self playerItemDuration];
    
     NSLog(@"time: %f",CMTimeGetSeconds(time));
    NSLog(@"playerDuration: %f",CMTimeGetSeconds(playerDuration));
    
    
	if (CMTIME_IS_VALID(playerDuration))  {
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration)) {
            
            CGRect frame = self.videoSlider.frame;
            
            CGFloat width = CGRectGetWidth(frame);
            CGFloat interval =  width / duration;
            
            duration = CMTimeGetSeconds(time);
            if (isfinite(duration)) {
                CGFloat xLocation = duration * interval;
                frame.origin.x += xLocation;
                
                frame.origin.x -= 7; // to correct the arrow head
                frame.origin.y += 15; // to move slit down
                frame.size.width = 14;
                frame.size.height = 13;
                
                UIButton *button = marker.markerControl;
                if (!button) {
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    //button.backgroundColor = [UIColor redColor];
                    [button setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_mark" ofType:@"png"]] forState:UIControlStateNormal];
                } else {
                    [button removeFromSuperview];
                }
                
                [button setFrame:frame];
                [self.videoControlPanel addSubview:button];
                
                marker.markerControl = button;
                return YES;
            }
            return NO;
        }
		return NO;
	}
    return NO;
}

- (void)addAllMarkers  {
    if (!self.needShowMarker) {
        return;
    }
    
     NSLog(@"addAllMarkers self.videoMarkers: %@",self.videoMarkers);
    
    for (Marker *marker in self.videoMarkers) {
        BOOL addMarker = [self addMarkerForMarker:marker];
        if (!addMarker) {
            NSLog(@"AVVideoPlayer : Fail to Add Marker : %@",marker.name);
        }
    }
}

- (void)removeAllMarkers    {
    for (Marker *marker in self.videoMarkers) {
        [marker.markerControl removeFromSuperview];
    }
}

#pragma mark Subclip Control

- (NSMutableArray *)videoSubclips    {
    if (!mVideoSubclips) {
        mVideoSubclips = [[NSMutableArray alloc] init];
    }
    return mVideoSubclips;
}

- (CMTime)newSetInTime   {
    
    return [self.player currentTime];
}


- (CMTime)fileSetOutTime   {
    
    // Get the total time.
    CMTime time = self.playerItem.duration;
    
    // Convert it to seconds.
    float seconds = CMTimeGetSeconds(time);
    
    NSLog(@"Duration: %.2f", seconds);
    
    return time;
}



- (BOOL) checkDuplicateSubclip:(Subclip *)subclip {
    
    
    for (Subclip *subclipToCompare in self.videoSubclips) {
        
        if ([subclipToCompare.clipName isEqualToString:subclip.clipName]) {
            return TRUE;
        }
        
    }
    
    return FALSE;
    
    
}



- (void)editSubClipWithSetInTime:(CMTime )setInTime  index:(int)indexToReplace   {
    
    
    if (!self.needShowSubclip) {
        return;
    } 
    [self beginScrubbing:nil];
    
    if (CMTIME_IS_VALID(setInTime))  {
        CMTime setOutTime = [self.player currentTime];
        
        double setInSecond = CMTimeGetSeconds(setInTime);
        double setOutSecond = CMTimeGetSeconds(setOutTime);
        if (setInSecond < setOutSecond) {
            NSString *setInTimeString = [[self class] formattedStringForDurationSubClip:setInTime];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.setInTime = %@",setInTimeString];
            NSArray *sameSetInSubClips = [self.videoSubclips filteredArrayUsingPredicate:predicate];
            
            NSString *setOutTimeString = [[self class] formattedStringForDurationSubClip:setOutTime];
            predicate = [NSPredicate predicateWithFormat:@"SELF.setOutTime = %@",setOutTimeString];
            NSArray *sameSetOutSubClips = [sameSetInSubClips filteredArrayUsingPredicate:predicate];
            
            
            
            
            
            
            if ([sameSetOutSubClips count] == 0) {
                
                Subclip *subclip = [[[AVVideoPlayer sharedInstance] videoSubclips] objectAtIndex:indexToReplace];

                NSLog(@"subclip.setInTimeString1:%@",subclip.setInTimeString);
                NSLog(@"subclip.setOutTimeString1:%@",subclip.setOutTimeString);
                NSLog(@"subclip.description1:%@",subclip.description);
                NSLog(@"subclip.subclipId1:%@",subclip.subclipId);
                
                subclip.setInTimeString = setInTimeString;
                subclip.setOutTimeString = setOutTimeString;
                subclip.description = @"";
               
                subclip.setInTime = setInTime;
                subclip.setOutTime = setOutTime;
                
                NSLog(@"subclip.setInTimeString2:%@",subclip.setInTimeString);
                NSLog(@"subclip.setOutTimeString2:%@",subclip.setOutTimeString);
                NSLog(@"subclip.description2:%@",subclip.description);
                NSLog(@"subclip.subclipId2:%@",subclip.subclipId);
               
                
                
                
                if ([self addSubclipForSubclip:subclip]) {
                    [self.videoSubclips replaceObjectAtIndex:indexToReplace withObject:subclip];
                } else {
                    [Utilities showAlert:@"Fail to Add Subclip on Video."];
                }
                
                
            } else {
                [Utilities showAlert:@"A Subclip exists with similiar time frame duration."];
            }
        } else {
           // [Utilities showAlert:@"SetOut Time of subclip is greater than SetIn Time,."];
            [Utilities showAlert:@"Set out time must be less than video duration and greater than set in time"];
            
            
        }
    } else {
        [Utilities showAlert:@"Fail to Add Subclip on Video Invalid Set-In Time."];
    }
    
    [self endScrubbing:nil];
}

- (void)addNewSubClipWithSetInDefaultTime:(CMTime )setInTime   {
    if (!self.needShowSubclip) {
        return;
    }
    [self beginScrubbing:nil];
    
    if (CMTIME_IS_VALID(setInTime))  {
        CMTime setOutTime = [self.playerItem duration];
        
        double setInSecond = CMTimeGetSeconds(setInTime);
        double setOutSecond = CMTimeGetSeconds(setOutTime);
        if (setInSecond < setOutSecond) {
            NSString *setInTimeString = [[self class] formattedStringForDurationSubClip:setInTime];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.setInTime = %@",setInTimeString];
            NSArray *sameSetInSubClips = [self.videoSubclips filteredArrayUsingPredicate:predicate];
            
            NSString *setOutTimeString = [[self class] formattedStringForDurationSubClip:setOutTime];
            predicate = [NSPredicate predicateWithFormat:@"SELF.setOutTime = %@",setOutTimeString];
            NSArray *sameSetOutSubClips = [sameSetInSubClips filteredArrayUsingPredicate:predicate];
            
            if ([sameSetOutSubClips count] == 0) {
                Subclip *subclip = [[Subclip alloc] init];
                subclip.subclipId = @"-1";
                
                subclip.setInTimeString = setInTimeString;
                subclip.setOutTimeString = setOutTimeString;
                subclip.description = @"";
                
                subclip.setInTime = setInTime;
                subclip.setOutTime = setOutTime;
                
                NSLog(@"subclip:%@",subclip);
                
                NSLog(@"subclip0:%@",subclip.setInTimeString);
                NSLog(@"subclip0:%@",subclip.setOutTimeString);
                NSLog(@"subclip0:%@",subclip.description);
                NSLog(@"subclip0:%@",subclip.subclipId);
                
                
                
                int totClips=[self.videoSubclips count];
                
                if (totClips) {
                    
                    
                    for (int i=0; i<totClips;i++) {
                        
                        subclip.clipName = [NSString stringWithFormat:@"Subclip %d",(i + 2)];
                        
                        if (![self checkDuplicateSubclip:subclip]) {
                            
                            //NSLog(@" marker.name2:%@", marker.name);
                            
                            if ([self addSubclipForSubclip:subclip]) {
                                [self.videoSubclips addObject:subclip];
                            } else {
                                [Utilities showAlert:@"Fail to Add Subclip on Video."];
                            }
                            break;
                        }
                        
                    }
                }else {
                    
                    subclip.clipName = [NSString stringWithFormat:@"Subclip %d",(0+ 1)];
                    if ([self addSubclipForSubclip:subclip]) {
                        [self.videoSubclips addObject:subclip];
                    } else {
                        [Utilities showAlert:@"Fail to Add Subclip on Video."];
                    }
                    
                }
                
                
            } else {
                [Utilities showAlert:@"A Subclip exists with similiar time frame duration."];
            }
        } else {
            [Utilities showAlert:@"Set out time must be less than video duration and greater than set in time"];
        }
    } else {
        [Utilities showAlert:@"Fail to Add Subclip on Video Invalid Set-In Time."];
    }
    
    [self endScrubbing:nil];
}




- (void)addNewSubClipWithSetInTime:(CMTime )setInTime   {
    if (!self.needShowSubclip) {
        return;
    }
    [self beginScrubbing:nil];
    
    if (CMTIME_IS_VALID(setInTime))  {
        CMTime setOutTime = [self.player currentTime];
        
        double setInSecond = CMTimeGetSeconds(setInTime);
        double setOutSecond = CMTimeGetSeconds(setOutTime);
        if (setInSecond < setOutSecond) {
            NSString *setInTimeString = [[self class] formattedStringForDurationSubClip:setInTime];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.setInTime = %@",setInTimeString];
            NSArray *sameSetInSubClips = [self.videoSubclips filteredArrayUsingPredicate:predicate];
            
            NSString *setOutTimeString = [[self class] formattedStringForDurationSubClip:setOutTime];
            predicate = [NSPredicate predicateWithFormat:@"SELF.setOutTime = %@",setOutTimeString];
            NSArray *sameSetOutSubClips = [sameSetInSubClips filteredArrayUsingPredicate:predicate];
            
            if ([sameSetOutSubClips count] == 0) {
                Subclip *subclip = [[Subclip alloc] init];
                subclip.subclipId = @"-1";
                
                subclip.setInTimeString = setInTimeString;
                subclip.setOutTimeString = setOutTimeString;
                subclip.description = @"";
                
                subclip.setInTime = setInTime;
                subclip.setOutTime = setOutTime;
                
                NSLog(@"subclip:%@",subclip);
                
                NSLog(@"subclip0:%@",subclip.setInTimeString);
                NSLog(@"subclip0:%@",subclip.setOutTimeString);
                NSLog(@"subclip0:%@",subclip.description);
                NSLog(@"subclip0:%@",subclip.subclipId);

                
                
                int totClips=[self.videoSubclips count];
                
                if (totClips) {
                    
                    
                    for (int i=0; i<totClips;i++) {
                        
                        subclip.clipName = [NSString stringWithFormat:@"Subclip %d",(i + 2)];
                        
                        if (![self checkDuplicateSubclip:subclip]) {
                            
                            //NSLog(@" marker.name2:%@", marker.name);
                            
                            if ([self addSubclipForSubclip:subclip]) {
                                [self.videoSubclips addObject:subclip];
                            } else {
                                [Utilities showAlert:@"Fail to Add Subclip on Video."];
                            }
                            break;
                        }                
                        
                    }
                }else {
                    
                    subclip.clipName = [NSString stringWithFormat:@"Subclip %d",(0+ 1)];
                    if ([self addSubclipForSubclip:subclip]) {
                        [self.videoSubclips addObject:subclip];
                    } else {
                        [Utilities showAlert:@"Fail to Add Subclip on Video."];
                    }
                    
                }
                
                
            } else {
                [Utilities showAlert:@"A Subclip exists with similiar time frame duration."];
            }
        } else {
            [Utilities showAlert:@"Set out time must be less than video duration and greater than set in time"];
        }
    } else {
        [Utilities showAlert:@"Fail to Add Subclip on Video Invalid Set-In Time."];
    }
    
    [self endScrubbing:nil];
}

- (BOOL)addSubclipForSubclip:(Subclip *)subclip   {
    if (!self.needShowSubclip) {
        return NO;
    }
    CMTime playerDuration = [self playerItemDuration];
    
	if (CMTIME_IS_VALID(playerDuration))  {
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration)) {
            
            CGRect frame = self.videoSlider.frame;
            
            CGFloat width = CGRectGetWidth(frame);
            CGFloat interval =  width / duration;
            
            CMTime setInTime = subclip.setInTime;
            CMTime setOutTime = subclip.setOutTime;
            
            duration = CMTimeGetSeconds(setInTime);
            if (isfinite(duration)) {
                CGFloat xStartLocation = duration * interval;
                
                duration = CMTimeGetSeconds(setOutTime);
                if (isfinite(duration)) {
                    CGFloat xEndLocation = duration * interval;
                    
                    frame.origin.x += xStartLocation;
                    
                    frame.origin.y += 20; // to move slit down
                    frame.size.width = ((xEndLocation - xStartLocation) > 0 ) ? (xEndLocation - xStartLocation) : 0;
                    frame.size.height = 15;
                    
                    UIView *subclipView = subclip.subclipView;
                    if (!subclipView) {
                        subclipView = [[UIView alloc] init];
                        subclipView.backgroundColor = [UIColor greenColor];
                    } else {
                        [subclipView removeFromSuperview];
                    }
                     
                    [self removeAllSubclips];
                    
                    [subclipView setFrame:frame];
                    [self.videoControlPanel addSubview:subclipView];
                    
                    subclip.subclipView = subclipView;
                    return YES;
                }
                return NO;
            }
            return NO;
        }
		return NO;
	}
    return NO;
}

- (void)addSubclip:(Subclip *)subclip   {
    if (!self.needShowSubclip) {
        return;
    }
    BOOL addSubclip = [self addSubclipForSubclip:subclip];
    if (!addSubclip) {
        NSLog(@"AVVideoPlayer : Fail to Add Subclip : %@",subclip.clipName);
    }
}

- (void)removeAllSubclips    {
    for (Subclip *subclip in self.videoSubclips) {
        [subclip.subclipView removeFromSuperview];
    }
}


#pragma mark - Movie controller methods

- (void)addVideoPlayerView  {
    CGRect frame = self.frame;
    frame.size.height -= 45;
    
    self.playerView = [AVPlayerLayer layer];
    [self.playerView setFrame:frame];
    [self.playerView setBackgroundColor:[UIColor blackColor].CGColor];
    [[self layer] addSublayer:self.playerView];
}

+ (Class)layerClass {
	return [AVPlayerLayer class];
}

- (AVPlayer*)player {
	return [(AVPlayerLayer*)self.playerView player];
}

- (void)setPlayer:(AVPlayer*)player {
	[(AVPlayerLayer*)self.playerView setPlayer:player];
}

/* Specifies how the video is displayed within a player layer’s bounds.
 (AVLayerVideoGravityResizeAspect is default) */
- (void)setVideoFillMode:(NSString *)fillMode   {
	AVPlayerLayer *playerLayer = (AVPlayerLayer*)self.playerView;
	playerLayer.videoGravity = fillMode;
}

#pragma mark
#pragma mark Button Actions

- (void)play:(id)sender {
    
    if (self.playPauseButton.enabled) {
        /* If we are at the end of the movie, we must seek to the beginning first
         before starting playback. */
        if (seekToZeroBeforePlay)   {
            seekToZeroBeforePlay = NO;
            [self.player seekToTime:kCMTimeZero];
        }
        
        [self.player play];
        
        [self showStopButton];
    }
}

- (void)pause:(id)sender    {
    
    if (self.playPauseButton.enabled) {
        [self.player pause];
        
        [self showPlayButton];
    }
}

- (void)playPauseButtonClicked:(UIButton *)sender    {
    
    if (sender.selected) {
        [self pause:nil];
    } else  {
        [self play:nil];
    }
}

/* Display AVMetadataCommonKeyTitle and AVMetadataCommonKeyCopyrights metadata. */
- (void)showMetadata:(id)sender {
	NSLog(@"AVVideoPlayer : Asset Metadata : %@",[[[self.player currentItem] asset] commonMetadata]);
}

#pragma mark Play, Stop buttons

/* Show the stop button in the movie player controller. */
-(void)showStopButton   {
    [self.playPauseButton setSelected:YES];
}

/* Show the play button in the movie player controller. */
-(void)showPlayButton   {
    [self.playPauseButton setSelected:NO];
}

/* If the media is playing, show the stop button; otherwise, show the play button. */
- (void)syncPlayPauseButtons    {
	if ([self isPlaying])   {
        [self showStopButton];
	}
	else    {
        [self showPlayButton];
	}
}

-(void)enablePlayerButtons  {
    self.playPauseButton.enabled = YES;
}

-(void)disablePlayerButtons {
    self.playPauseButton.enabled = NO;
}

#pragma mark Movie scrubber control

/* ---------------------------------------------------------
 **  Methods to handle manipulation of the movie scrubber control
 ** ------------------------------------------------------- */

/* Requests invocation of a given block during media playback to update the movie scrubber control. */
-(void)initScrubberTimer    {
    
	double interval = .1f;
	
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration))
	{
		return;
	}
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		CGFloat width = CGRectGetWidth([self.videoSlider bounds]);
		interval = 0.5f * duration / width;
	}
    
	/* Update the scrubber during normal playback. */
	mTimeObserver = [[self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                           queue:NULL /* If you pass NULL, the main queue is used. */
                                                      usingBlock:^(CMTime time)
                      {
                          [self syncScrubber];
                      }] retain];
    
}

/* Set the scrubber based on the player current time. */
- (void)syncScrubber    {
    
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration))
	{
		self.videoSlider.minimumValue = 0.0;
		return;
	}
    
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		float minValue = [self.videoSlider minimumValue];
		float maxValue = [self.videoSlider maximumValue];
		double time = CMTimeGetSeconds([self.player currentTime]);
		
		[self.videoSlider setValue:(maxValue - minValue) * time / duration + minValue];
	}
    
    [self updateTimeLabel];
}

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (void)beginScrubbing:(id)sender   {
	mRestoreAfterScrubbingRate = [self.player rate];
	[self.player setRate:0.f];
	
	/* Remove previous timer. */
	[self removePlayerTimeObserver];
}

/* Set the player current time to match the scrubber position. */
- (void)scrub:(id)sender    {
    
	if ([sender isKindOfClass:[UISlider class]])
	{
		UISlider* slider = sender;
		
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration)) {
			return;
		}
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			float minValue = [slider minimumValue];
			float maxValue = [slider maximumValue];
			float value = [slider value];
            
            NSLog(@"SlierValue:%f",[slider value]);
			
			double time = duration * (value - minValue) / (maxValue - minValue);
            NSLog(@"durationValue:%f",duration);
            NSLog(@"TimeValue:%f",time);
			
            
            int32_t timeScale = self.player.currentItem.asset.duration.timescale;
            CMTime timeCM = CMTimeMakeWithSeconds(time, timeScale);
            [self.player seekToTime:timeCM toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];

            [self updateTimeLabelWhileScrubbing:timeCM];
		}
        
        //[self updateTimeLabel];
	}
}


- (void)updateTimeLabelWhileScrubbing:(CMTime)time {
    
    
    NSString *playerDuration = [[self class] formattedStringForDuration:[self playerItemDuration]];
    
    NSString *playerTime = [[self class] formattedStringForDuration:time];
    
    [self.timeLabel setText:[NSString stringWithFormat:@"%@ / %@",playerTime,playerDuration]];
}


/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (void)endScrubbing:(id)sender {
    
    
	if (!mTimeObserver)
	{
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration))
		{
			return;
		}
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			CGFloat width = CGRectGetWidth([self.videoSlider bounds]);
			double tolerance = 0.5f * duration / width;
            int32_t timeScale = self.player.currentItem.asset.duration.timescale;
			mTimeObserver = [[self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, timeScale) queue:NULL usingBlock:
                              ^(CMTime time)
                              {
                                  [self syncScrubber];
                              }] retain];
		}
	}
    
	if (mRestoreAfterScrubbingRate) {
		[self.player setRate:mRestoreAfterScrubbingRate];
		mRestoreAfterScrubbingRate = 0.f;
	}
}

- (BOOL)isScrubbing {
	return mRestoreAfterScrubbingRate != 0.f;
}

-(void)enableScrubber   {
    self.videoSlider.enabled = YES;
}

-(void)disableScrubber  {
    self.videoSlider.enabled = NO;    
}

#pragma mark Player Item

- (BOOL)isPlaying   {
//	return mRestoreAfterScrubbingRate != 0.f || [self.player rate] != 0.f;
    return [self.player rate] != 0.f;
}

/* Called when the player item has played to its end time. */
- (void)playerItemDidReachEnd:(NSNotification *)notification    {
	/* After the movie has played to its end time, seek back to time zero
     to play it again. */
	seekToZeroBeforePlay = YES;
}

/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem.
 ** ------------------------------------------------------- */

- (CMTime)playerItemDuration    {
    
	AVPlayerItem *playerItem = [self.player currentItem];
	if (playerItem.status == AVPlayerItemStatusReadyToPlay) {
        
		return([playerItem duration]);
	}
	
	return(kCMTimeInvalid);
}

/* Cancels the previously registered time observer. */
-(void)removePlayerTimeObserver {
    
	if (mTimeObserver)  {
		[self.player removeTimeObserver:mTimeObserver];
//		[mTimeObserver release];
		mTimeObserver = nil;
	}
}

#pragma mark Asset URL

- (void)setURL:(NSURL*)URL  {
	if (mURL != URL)    {

		mURL = [URL copy];
        /*
         Create an asset for inspection of a resource referenced by a given URL.
         Load the values for the asset keys "tracks", "playable".
         */
        NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:mURL options:optionsDictionary];
        
        NSArray *requestedKeys = [NSArray arrayWithObjects:kTracksKey, kPlayableKey, nil];
        
        /* Tells the asset to load the values of any of the specified keys that are not already loaded. */
        [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{
             dispatch_async( dispatch_get_main_queue(),
                            ^{
                                /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
                                [self prepareToPlayAsset:asset withKeys:requestedKeys];
                            });
         }];
	}
}

#pragma mark Error Handling - Preparing Assets for Playback Failed

/* --------------------------------------------------------------
 **  Called when an asset fails to prepare for playback for any of
 **  the following reasons:
 **
 **  1) values of asset keys did not load successfully,
 **  2) the asset keys did load successfully, but the asset is not
 **     playable
 **  3) the item did not become ready to play.
 ** ----------------------------------------------------------- */

-(void)assetFailedToPrepareForPlayback:(NSError *)error {
    
    [self removePlayerTimeObserver];
    [self syncScrubber];
    [self disableScrubber];
    [self disablePlayerButtons];
    
    /* Display the error. */
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
														message:[error localizedFailureReason]
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	//
    
    //Dony Changes
   // [alertView show];
    
    
	alertView = nil;
}

#pragma mark Prepare to play Asset

- (void)initializeNewPlayerItemForAsset:(AVURLAsset *)asset {
    
    UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
    AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
                             sizeof(sessionCategory), &sessionCategory);
    
    /* Stop observing our prior AVPlayerItem, if we have one. */
    if (self.playerItem)    {
        /* Remove existing player item key value observers and notifications. */
        [self.playerItem removeObserver:self forKeyPath:kStatusKey];
		
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.playerItem];
    }
	
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    if ([[asset tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
		AVAssetTrack *playerTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
        nominalFrameRate = playerTrack.nominalFrameRate;
	}
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [self.playerItem addObserver:self
                       forKeyPath:kStatusKey
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:AVPlayerStatusObservationContext];
	
    /* When the player item has played to its end time we'll toggle
     the movie controller Pause button to be the Play button */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
}

/*
 Invoked at the completion of the loading of the values for all keys on the asset that we require.
 Checks whether loading was successfull and whether the asset is playable.
 If so, sets up an AVPlayerItem and an AVPlayer to play the asset.
 */
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys    {
    /* Make sure that the value of each key has loaded successfully. */
	for (NSString *thisKey in requestedKeys)    {
		NSError *error = nil;
		AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
		if (keyStatus == AVKeyValueStatusFailed)    {
			[self assetFailedToPrepareForPlayback:error];
			return;
		}
		/* If you are also implementing -[AVAsset cancelLoading], add your code here to bail out properly in the case of cancellation. */
	}
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable)    {
        
        /* Generate an error describing the failure. */
		NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
		NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
		NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
								   localizedDescription, NSLocalizedDescriptionKey,
								   localizedFailureReason, NSLocalizedFailureReasonErrorKey,
								   nil];
		NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"AVPlayer" code:0 userInfo:errorDict];
        
        /* Display the error to the user. */
        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];
        
        return;
    }
	
	/* At this point we're ready to set up for playback of the asset. */
    
    [self initializeNewPlayerItemForAsset:asset];
	
    seekToZeroBeforePlay = NO;
	
    /* Create new player, if we don't already have one. */
    if (![self player]) {
        /* Get a new AVPlayer initialized to play the specified player item. */
        [self setPlayer:[AVPlayer playerWithPlayerItem:self.playerItem]];
		
        /* Observe the AVPlayer "currentItem" property to find out when any
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
         occur.*/
        [self.player addObserver:self
                      forKeyPath:kCurrentItemKey
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:AVPlayerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.player addObserver:self
                      forKeyPath:kRateKey
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:AVPlayerRateObservationContext];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (self.player.currentItem != self.playerItem) {
        /* Replace the player item with a new player item. The item replacement occurs
         asynchronously; observe the currentItem property to find out when the
         replacement will/did occur*/
        [[self player] replaceCurrentItemWithPlayerItem:self.playerItem];
        
        [self syncPlayPauseButtons];
    }
	
    [self.videoSlider setValue:0.0];
}

#pragma mark -
#pragma mark Asset Key Value Observing
#pragma mark

#pragma mark Key Value Observer for player rate, currentItem, player item status

/* ---------------------------------------------------------
 **  Called when the value at the specified key path relative
 **  to the given object has changed.
 **  Adjust the movie play and pause button controls when the
 **  player item "status" value changes. Update the movie
 **  scrubber control when the player item is ready to play.
 **  Adjust the movie scrubber control when the player item
 **  "rate" value changes. For updates of the player
 **  "currentItem" property, set the AVPlayer for which the
 **  player layer displays visual output.
 **  NOTE: this method is invoked on the main queue.
 ** ------------------------------------------------------- */

- (void)observeValueForKeyPath:(NSString*) path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
	/* AVPlayerItem "status" property value observer. */
	if (context == AVPlayerStatusObservationContext)
	{
		[self syncPlayPauseButtons];
        
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerStatusUnknown:
            {
                [self removeAllMarkers];
                [self removePlayerTimeObserver];
                [self syncScrubber];
                
                [self disableScrubber];
                [self disablePlayerButtons];
            }
                break;
                
            case AVPlayerStatusReadyToPlay:
            {
                /* Once the AVPlayerItem becomes ready to play, i.e.
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 its duration can be fetched from the item. */
                
                [self initScrubberTimer];
                
                [self enableScrubber];
                [self enablePlayerButtons];
                [self addAllMarkers];
            }
                break;
                
            case AVPlayerStatusFailed:
            {
                [self removeAllMarkers];
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];
            }
                break;
        }
	}
	/* AVPlayer "rate" property value observer. */
	else if (context == AVPlayerRateObservationContext)
	{
        [self syncPlayPauseButtons];
	}
	/* AVPlayer "currentItem" property observer.
     Called when the AVPlayer replaceCurrentItemWithPlayerItem:
     replacement will/did occur. */
	else if (context == AVPlayerCurrentItemObservationContext)
	{
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            [self removeAllMarkers];
            [self disablePlayerButtons];
            [self disableScrubber];
        }
        else /* Replacement of player currentItem has occurred */
        {
            /* Set the AVPlayer for which the player layer displays visual output. */
//            [mPlaybackView setPlayer:self.player];
//            
//            [self setViewDisplayName];
//            
//            /* Specifies that the player should preserve the video’s aspect ratio and
//             fit the video within the layer’s bounds. */
//            [mPlaybackView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            
            [self syncPlayPauseButtons];
        }
	}
	else
	{
		[super observeValueForKeyPath:path ofObject:object change:change context:context];
	}
}


@end

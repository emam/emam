//
//  ShareViewController.h
//  eMAM Ingest
//
//  Created by Abdul Malik Aman on 26/06/16.
//  Copyright © 2016 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ShareViewController : SLComposeServiceViewController <UITextViewDelegate> {
    NSUserDefaults *sharedUserDefaults;
    NSExtensionItem *inputItem;
    NSMutableArray * IngestIdArray;
    NSString * ingestId;
    int isPlay;
    NSURL *ContentUrl;
    int is_Video;
    int isPickerShown;
    int percentageUploaded;
    int is_UploadCompleted;
    NSString * encodedIngestImage;
    NSString * UploadImagename;
    NSString * uploadimageFormat;
    UITapGestureRecognizer *ingestRecognizer;
    NSData *uploadImageData;
    NSString *IngestIdTitle;
    NSOperationQueue *eMAMqueue;
}

@property BOOL is_LoggedIn;
@property (weak, nonatomic) NSDictionary *imageDictionary;
@property (weak, nonatomic) IBOutlet UIView *ingestContainer;
@property (weak, nonatomic) IBOutlet UIImageView *ingestImageView;
@property (weak, nonatomic) IBOutlet UITextField *authorText;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UITextField *titleText;
@property (weak, nonatomic) IBOutlet UITextField *ingestIdText;
@property (weak, nonatomic) IBOutlet UIView *moviewPlayer;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet UITableView *IngestPicker;
@property (weak, nonatomic) IBOutlet UIButton *SubmitButton;
@property (weak, nonatomic) IBOutlet UIButton *CancelButton;
@property (weak, nonatomic) IBOutlet UIButton *showPickerButton;
@property (weak, nonatomic) IBOutlet UIScrollView *containerScroll;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadingLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *uploadingActivity;

@end

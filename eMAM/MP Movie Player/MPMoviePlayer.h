/**************************************************************************************
 *  File Name      : MPMoviePlayer.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 07-10-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>

@interface MPMoviePlayer : UIView

- (id)initWithContentURL:(NSURL *)movieURL AndFrame:(CGRect)frame;
- (void)setURL:(NSURL *)url;

- (void)start;
- (void)stop;

- (void)remove;

- (void)pause;
- (void)play;

@end

//
//  AppDelegate.h
//  eMAM
//
//  Created by APPLE on 24/07/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWRDownloadManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    NSMutableArray *failedMarkerRequests;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navigationController;
@property(nonatomic,retain) NSMutableArray *failedMarkerRequests;
@property (copy) void (^backgroundSessionCompletionHandler)();

@end

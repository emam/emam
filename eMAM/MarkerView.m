/**************************************************************************************
 *  File Name      : MarkerView.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "MarkerView.h"

#import "MarkerCell.h"
#import "AVVideoPlayer.h"
#import "TPKeyboardAvoidingTableView.h"

@interface MarkerView ()

@property (nonatomic, assign) UITableView *tableView;

@end

@implementation MarkerView

@synthesize tableView = _tableView;
@synthesize currentOrientation = _currentOrientation;

#pragma mark -

- (id)initWithFrame:(CGRect)frame   {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

- (void) initView {
    self.autoresizesSubviews = YES;
   // self.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    self.backgroundColor=UIColorFromRGB(0x3B3B3B);
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    UIButton *saveButton = [[UIButton alloc] init];
    saveButton.userInteractionEnabled = YES;
    saveButton.tag = 1122;
    saveButton.backgroundColor = [UIColor clearColor];
    [self addSubview:saveButton];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton release];
    saveButton = nil;
    
    UIButton *markButton = [[UIButton alloc] init];
    markButton.userInteractionEnabled = YES;
    markButton.tag = 1123;
    markButton.backgroundColor = [UIColor clearColor];
    [self addSubview:markButton];
    [markButton setTitle:@"Mark" forState:UIControlStateNormal];
    markButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [markButton addTarget:self action:@selector(addMarker:) forControlEvents:UIControlEventTouchUpInside];
    
    [markButton release];
    markButton = nil;
    
    UITableView *tableView = [[TPKeyboardAvoidingTableView alloc] init];
    tableView.delegate = (id)self;
    tableView.dataSource = (id)self;
    
    [self addSubview:tableView];
    [tableView setBackgroundColor:[UIColor clearColor]];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView = tableView;
    
    
    [self layoutOrientation];
}

- (void)orientationChanged:(NSNotification *)note
{
   self.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    [self refreshView]; 
}

- (void)layoutOrientation   {
    
    
    NSLog(@"Orient:%d",self.currentOrientation);
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame=CGRectMake(0, 500, 768, 416);
        UIView *saveButton = [self viewWithTag:1122];
        saveButton.frame = CGRectMake(300,10, 55, 26);
        UIView *markButton = [self viewWithTag:1123];
        markButton.frame = CGRectMake(400,10, 55, 26);
        self.tableView.frame = CGRectMake(16, 52, 736, 360);
    } else {
        
        self.frame=CGRectMake(0, 500, 1024, 160);
        UIView *saveButton = [self viewWithTag:1122];
        saveButton.frame = CGRectMake(440,10, 55, 26);
        UIView *markButton = [self viewWithTag:1123];
        markButton.frame = CGRectMake(540,10, 55, 26);
        self.tableView.frame = CGRectMake(150, 50, 736, 107);
    }
    
    [self.tableView reloadData];
}


- (void)refreshView {
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame=CGRectMake(0, 500, 768, 416);
        UIView *saveButton = [self viewWithTag:1122];
        saveButton.frame = CGRectMake(300,10, 55, 26);
        UIView *markButton = [self viewWithTag:1123];
        markButton.frame = CGRectMake(400,10, 55, 26);
        self.tableView.frame = CGRectMake(16, 52, 736, 360);
    } else {
        
        self.frame=CGRectMake(0, 500, 1024, 160);
        UIView *saveButton = [self viewWithTag:1122];
        saveButton.frame = CGRectMake(440,10, 55, 26);
        UIView *markButton = [self viewWithTag:1123];
        markButton.frame = CGRectMake(540,10, 55, 26);
        self.tableView.frame = CGRectMake(150, 50, 736, 107);
    }
    
    [self.tableView reloadData];
}

#pragma mark -

- (void)addMarker:(id)sender    {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer addNewMarker];
    
    [self refreshView];
}

- (void)saveButtonClicked:(id)sender    {
    
    if ([[[AVVideoPlayer sharedInstance] videoMarkers] count] > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(saveMarkers:)]) {
            [self.delegate saveMarkers:[[AVVideoPlayer sharedInstance] videoMarkers]];
        }
    }
}

#pragma mark - Override Setters

- (void)setMarkerInfo:(NSMutableArray *)markerInfo  {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer setVideoMarkers:markerInfo];
    [avVideoPlayer addAllMarkers];
    [self refreshView];
}

- (void)setCurrentOrientation:(UIInterfaceOrientation)currentOrientation    {
    _currentOrientation = currentOrientation;
    [self layoutOrientation];
}

#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
	int icount = [[AVVideoPlayer sharedInstance].videoMarkers count];
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	int icount = 1;
    return icount;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
    return view;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
	//	Return the height of cells in tableView
    CGFloat height = 60;
	return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    MarkerCell * cell;
    
    @try    {
        static NSString *CellIdentifier = @"MarkerCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[MarkerCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.delegate = self.delegate;
            cell.textLabel.font = [UIFont fontWithName:APPFONTNAME size:12.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        // Configure the cell...
        [cell setCurrentOrientation:self.currentOrientation];
        [cell setMarker:[[AVVideoPlayer sharedInstance].videoMarkers objectAtIndex:indexPath.section]];
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM Marker View : Exception for cellForAtIndexPath in SearchFilterViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    
}

@end

/**************************************************************************************
 *  File Name      : SubclipCell.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 02-01-2013
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/


#import <UIKit/UIKit.h>

#import "Subclip.h"
#import "SubclipView.h"

@interface SubclipCell : UITableViewCell

@property (nonatomic, strong) Subclip *subclip;

@property (nonatomic, strong) NSString *subclipTotalTimeString;
@property (nonatomic, strong) id target;
@property (nonatomic, strong) id <SubclipDelegate> delegate;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

-(UIButton *)updateCheckmark;
@end

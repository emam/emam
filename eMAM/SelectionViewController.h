/**************************************************************************************
 *  File Name      : SelectionViewController.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 05-15-2014
 *  Copyright (C) 2014 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

typedef void (^SelectionDismissalBlock)(NSMutableDictionary *selection);

@interface SelectionViewController : UIViewController

@property (nonatomic, strong) NSArray *selectionOptions;
@property (nonatomic, copy) SelectionDismissalBlock selectionBlock;

@end

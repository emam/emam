//
//  OfflineDownloads.m
//  eMAM
//
//  Created by Dony George on 20/03/15.
//  Copyright (c) 2015 Empress Cybernetic Systems. All rights reserved.
//

#import "OfflineDownloads.h"

@interface OfflineDownloads ()

@end

@implementation OfflineDownloads

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentDownloads=[[NSMutableArray alloc] initWithArray:[[TWRDownloadManager sharedManager] currentDownloads]];
    NSLog(@"Downloads:%@",currentDownloads);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [currentDownloads count];
}


- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"DownloadsCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:APPFONTNAME size:15.0];
        }
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = [currentDownloads objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }
    @catch (NSException *exception) {
       
    }
    @finally    {
        return cell;
    }
    
    return cell;
}


@end

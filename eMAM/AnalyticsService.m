//
//  AnalyticsService.m
//  eMAM
//
//  Created by APPLE on 22/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AnalyticsService.h"

@implementation AnalyticsService

+ (AnalyticsService *)sharedInstance {
    static AnalyticsService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


#pragma mark - Analytics

- (void)updateHitCountForFeature:(NSString *)featureCode performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<UpdateHitCount xmlns=\"http://www.empressmam.com//\">\
    <featureCode>%@</featureCode>\
    </UpdateHitCount>";
    NSString *soapAction = @"http://tempuri.org/UpdateHitCount";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)updateLoggedInFlag:(int)loggedInFlag forUser:(NSString *)userEmail performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<UpdateLoggedInFlag xmlns=\"http://tempuri.org/\">\
    <EmailId>%@</EmailId>\
    <IsLoggedIn>%d</IsLoggedIn>\
    </UpdateLoggedInFlag>";
    NSString *soapAction = @"http://tempuri.org/UpdateLoggedInFlag";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

@end

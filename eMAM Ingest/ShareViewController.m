//
//  ShareViewController.m
//  eMAM Ingest
//
//  Created by Abdul Malik Aman on 26/06/16.
//  Copyright © 2016 Empress Cybernetic Systems. All rights reserved.
//

#import "ShareViewController.h"
#import "Constants.h"

@import MobileCoreServices;

static NSString *const AppGroupId = @"group.com.empress.emamipad";

@interface ShareViewController ()

@end

@implementation ShareViewController

-(void)viewDidLoad {
    
    self.title = @"eMAM";
    
    eMAMqueue = [[NSOperationQueue alloc]init];
    self.titleText.delegate = self;
    self.view.backgroundColor = [UIColor clearColor];
    self.ingestContainer.hidden = true;
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
    //    self.navigationController.navigationBarHidden = true;
    
    id value = [sharedUserDefaults valueForKey:@"eMAMLoggedIn"];
    if ([value isEqualToString:@"True"]) {
        self.dismissButton.hidden = true;
        self.loginLabel.hidden = true;
        //        self.ingestContainer.hidden = false;
        [self.titleText becomeFirstResponder];
    }else {
        self.dismissButton.hidden = false;
        self.loginLabel.hidden = false;
        self.ingestContainer.hidden = true;
    }
    
    NSLog(@"Login : %@",value);
    
    self.SubmitButton.layer.cornerRadius = 5;
    self.SubmitButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.SubmitButton.layer.borderWidth = 1;
    self.CancelButton.layer.cornerRadius = 5;
    self.CancelButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.CancelButton.layer.borderWidth = 1;
    self.dismissButton.layer.cornerRadius = 5;
    self.dismissButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.dismissButton.layer.borderWidth = 1;
    self.containerScroll.contentSize = CGSizeMake(0, 1000);
    
    [self.IngestPicker registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ingest"];
    
    isPlay = NO;
    is_Video = NO;
    self.moviewPlayer.hidden = true;
    self.ingestImageView.hidden = true;
    
    ingestRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [ingestRecognizer setNumberOfTapsRequired:1];
    [ingestRecognizer setNumberOfTouchesRequired:1];
    
    percentageUploaded = 0;
    self.ingestImageView.layer.borderWidth = 1;
    self.ingestImageView.layer.borderColor = [[UIColor blackColor]CGColor];
    
    NSString *userName = [sharedUserDefaults objectForKey:KUSERNAME];
    self.authorText.text = userName;
    
    NSMutableArray *IngestIDArray = [sharedUserDefaults valueForKey:@"eMAMIngestArray"];
    if (IngestIDArray != nil) {
        IngestIdArray = IngestIDArray;
        [_IngestPicker reloadData];
        //        NSLog(@"IngestIDArray : %@",IngestIDArray);
    }
    
    for (NSInteger index = (IngestIdArray.count - 1); index >= 0; index--) {
        NSLog(@"IngestIdArray %@ :", IngestIdArray[index][@"profile_name1"][@"text"]);
        if ([IngestIdArray[index][@"Is_Default_Profile"][@"text"] isEqualToString: @"true"]) {
            ingestId = IngestIdArray[index][@"ProfileID"][@"text"];
            [self.ingestIdText setText: IngestIdArray[index][@"profile_name1"][@"text"]];
            IngestIdTitle = self.ingestIdText.text;
        }
    }
    
    if (ingestId == nil) {
        ingestId = IngestIdArray[0][@"ProfileID"][@"text"];
        [self.ingestIdText setText: IngestIdArray[0][@"profile_name1"][@"text"]];
        IngestIdTitle = self.ingestIdText.text;
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    
}

// MARK: - Actions
- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
}
- (IBAction)DismissContainer:(id)sender {
    [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
}

- (IBAction)ShowPicker:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = YES;
        self.pickerContainer.hidden = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 200);
        
    }];
    [self.IngestPicker reloadData];
}

-(void) dismissKeyboard {
    [self.view endEditing:true];
}
- (IBAction)DismissPicker:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
        self.pickerContainer.hidden = YES;
    }];
}

- (IBAction)cancelClicked:(id)sender {
    [self.view endEditing:YES];
    if (is_Video == YES) {
        isPlay = NO;
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
    }
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
    self.authorText.text = userName;
    self.descriptionText.text = @"";
    self.titleText.text = @"";
    self.ingestContainer.hidden = true;
    self.ingestImageView.image = NULL;
    ContentUrl = [NSURL URLWithString:@""];
    [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
}

- (IBAction)uploadIngestImages:(id)sender {
    [self.view endEditing:YES];
    if (is_Video == YES) {
        isPlay = NO;
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
    }
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
    NSString *message;
    if ([[self.titleText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Title Cannot be Empty";
    }else if ([[self.authorText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Author Cannot be Empty";
    }else if ([[self.ingestIdText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Ingest id Cannot be Empty";
    }else {
        [self postIngest];
    }
    
    //    if (message != nil) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
    //                                                        message: message
    //                                                       delegate:self
    //                                              cancelButtonTitle:@"OK"
    //                                              otherButtonTitles:nil];
    //        [alert show];
    //    }else {
    //        [self postIngest];
    //    }
}
- (IBAction)Play:(id)sender {
    if (isPlay == NO) {
        if (ContentUrl.absoluteString.length != 0) {
            [self.playButton setBackgroundImage: [UIImage imageNamed:@"Pause.png"] forState: UIControlStateNormal];
            isPlay = YES;
        }
    }else {
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
        isPlay = NO;
    }
    
}
- (IBAction)Stop:(id)sender {
    isPlay = NO;
    [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
}
- (void)showStatus {
    if (percentageUploaded < 70) {
        percentageUploaded = percentageUploaded + 20;
        //        [self.view addSubview:hud];
        //        [hud show:YES];
        //        hud.labelText = [NSString stringWithFormat:@"%d%@ Completed..", percentageUploaded, @"%"];
    }else {
        if (is_UploadCompleted != 2) {
            if (is_UploadCompleted == 1) {
                //                hud.labelText = [NSString stringWithFormat:@"Video Upload Sucess.."];
            }else {
                //                hud.labelText = [NSString stringWithFormat:@"Video Upload Failed.."];
            }
            
            NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
            self.authorText.text = userName;
            self.descriptionText.text = @"";
            self.titleText.text = @"";
            self.ingestContainer.hidden = true;
            self.ingestImageView.image = NULL;
            ContentUrl = [NSURL URLWithString:@""];
            percentageUploaded = 0;
//            [UploadTimer invalidate];
        }else {
            //            hud.labelText = [NSString stringWithFormat:@"95%@ Completed..", @"%"];
        }
    }
}


- (void)postIngest {
    dispatch_sync(dispatch_get_main_queue(), ^{
        self.uploadingLabel.hidden = false;
        self.uploadingActivity.hidden = false;
        is_UploadCompleted = 2;
        
        NSString *boundary = @"eMAMFormBoundary";
        NSData *imageData;
        
        if (is_Video == YES) {
            imageData = [NSData dataWithContentsOfURL:ContentUrl];
//            NSLog(@"video data : %@",imageData);
        }else {
            imageData = UIImageJPEGRepresentation(self.ingestImageView.image, 1.0);
        }
        
        NSLog(@"Image Name %@", uploadimageFormat);
  NSString *imageName = [[UploadImagename stringByAppendingString:uploadimageFormat] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSString *hostAddress;
        NSString *userName = [sharedUserDefaults valueForKey:KUSERNAME];
        NSString *licenceKey = [sharedUserDefaults valueForKey:KSERVERLICENSEKEY];
        NSString *RealRoot = [sharedUserDefaults objectForKey:KROOTSERVER];
        NSString *root = [RealRoot substringToIndex:[RealRoot length]-1];
        NSString *port = [sharedUserDefaults objectForKey:KSERVERPORT];
        NSString *password = [sharedUserDefaults objectForKey:KENCRYPTEDPASSWORD];
        NSString *contentFormat;
        
        if ([uploadimageFormat.lowercaseString isEqualToString:@".png"]) {
            contentFormat = @"image/png";
        }else if ([uploadimageFormat.lowercaseString isEqualToString:@".jpg"]) {
            contentFormat = @"image/jpg";
        }else if ([uploadimageFormat.lowercaseString isEqualToString:@"jpeg"]) {
            contentFormat = @"image/jpeg";
        }else {
            contentFormat = @"application/octet-stream";
        }
        
        if ([port isEqualToString:@""]) {
            hostAddress = [NSString stringWithFormat:@"%@", RealRoot];
        }else {
            hostAddress = [NSString stringWithFormat:@"%@:%@", root, port];
        }
        
        NSString * urlString = [NSString stringWithFormat:@"%@/eMAMUploadManager/EMAMUploader", hostAddress];
        
        NSString *jsonString = [NSString stringWithFormat: @"{\"FileName\":\"%@\",\"UserId\":\"%@\",\"Password\":\"%@\",\"LicenseKey\":\"%@\",\"CategoryIds\":\"\",\"Tags\":\"\",\"ProjectIds\":\"\",\"Title\":\"%@\",\"Description\":\"%@\",\"Author\":\"%@\",\"IngestProfileId\":\"%@\",\"MetadatasetId\":\"-1\",\"AppDetails\":\"12:appversion 3.7\",\"CustomMetaData\":[],\"GatewayURL\":\"%@/emamgateway/emamservice.asmx\",\"UnitId\":\"0\",\"UploadType\":\"asset\"}", imageName, userName, password, licenceKey, imageName, self.descriptionText.text, self.authorText.text, ingestId, root];
        
        NSLog(@"%@", jsonString);
        
        NSData  *responseData = nil;
        NSURL *url=[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        responseData = [NSMutableData data] ;
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"multipart/form-data;boundary=eMAMFormBoundary" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"Connection" forHTTPHeaderField:@"Keep-Alive"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        // add params (all params are strings)
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"metadata"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        // add image data
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=%@\r\n", UploadImagename, imageName] dataUsingEncoding:NSUTF8StringEncoding]];
            NSString *ImageContentType = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",contentFormat];
            [body appendData:[ImageContentType dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
//        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@""]];
//        
//        NSURLSessionUploadTask *task = [session uploadTaskWithStreamedRequest:request];
////        [task ];
        
        [NSURLConnection sendAsynchronousRequest:request queue: eMAMqueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSLog(@"%@", request);
            NSLog(@"%@", data);
            NSLog(@"%@", response);
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Server Response : %@", responseString);
            
            NSString * message;
            if ([responseString isEqualToString:@"1"]) {
                if (is_Video == YES) {
                    message = @"Video Upload Success";
                }else {
                    message = @"Image Upload Success";
                }
            }else if ([responseString isEqualToString:@""]){
                message = @"Upload Failed";
            }else {
                message = responseString;
            }
            
            if(data.length > 0) {
                NSLog(@"%@", message);
                is_UploadCompleted = 1;
                if (is_Video == false) {
                    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
                    self.authorText.text = userName;
//                    self.descriptionText.text = @"";
                    self.titleText.text = @"";
                    self.ingestContainer.hidden = true;
                    self.ingestImageView.image = NULL;
                    ContentUrl = [NSURL URLWithString:@""];
                    percentageUploaded = 0;
//                    [UploadTimer invalidate];
                }
                self.uploadingLabel.hidden = true;
                self.uploadingActivity.hidden = true;
                [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
            }else {
                NSLog(@"%@", message);
                is_UploadCompleted = 0;
                if (is_Video == false) {
                    
                }
                self.uploadingLabel.hidden = true;
                self.uploadingActivity.hidden = true;
                [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
            }
            
        }];
    });
    
    //    [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
}

// MARK: - TextView Delegates
-(void)didReceiveMemoryWarning {
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString: @"\n"]) {
        [textView resignFirstResponder];
        [self.containerScroll setContentOffset:CGPointMake(0, 0)];
        return false;
    }
    return true;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self.view addGestureRecognizer:ingestRecognizer];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer:recognizer];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.containerScroll setContentOffset:CGPointMake(0, 0)];
    
    return true;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view addGestureRecognizer:ingestRecognizer];
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    //    [self.view removeGestureRecognizer:ingestRecognizer];
    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer:recognizer];
    }
}

// MARK: - TableView Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return IngestIdArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ingest";
    UITableViewCell *cell = [self.IngestPicker dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font = [cell.textLabel.font fontWithSize:14];
    cell.textLabel.text = IngestIdArray[indexPath.row][@"profile_name1"][@"text"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    ingestId = IngestIdArray[indexPath.row][@"ProfileID"][@"text"];
    [self.ingestIdText setText: IngestIdArray[indexPath.row][@"profile_name1"][@"text"]];
    
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
        self.pickerContainer.hidden = YES;
    }];
    
}

- (BOOL)isContentValid {
    //    NSLog(@"%@", self.contentText);
    self.descriptionText.text = self.contentText;
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

- (void)didSelectPost {
    self.view.backgroundColor = [UIColor clearColor];
    self.ingestContainer.hidden = true;
    NSExtensionItem *itemProvider = [[NSExtensionItem alloc]init];
    itemProvider = self.extensionContext.inputItems[0];
    
    
    for (NSItemProvider* itemProvider in ((NSExtensionItem*)self.extensionContext.inputItems[0]).attachments ) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeImage]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage options:nil completionHandler:^(NSURL *url, NSError *error) {
                    NSLog(@"%@", url);
                    NSLog(@"itemProvider = %@", itemProvider);
                    
                    NSString *imageName = url.absoluteString;
                    NSArray *getImageFormat = [imageName componentsSeparatedByString: @"/"];
                    UploadImagename = getImageFormat[getImageFormat.count - 1];
                    NSArray * format = [UploadImagename componentsSeparatedByString:@"."];
                    uploadimageFormat = [@"." stringByAppendingString:format[1]];
                    UploadImagename = format[0];
                    self.titleText.text = UploadImagename;
                    NSLog(@"Image name : %@", UploadImagename);
                    NSLog(@"Image Format : %@", uploadimageFormat);
                    
                }];
            }
            
            [itemProvider loadPreviewImageWithOptions:nil completionHandler:^(UIImage *image, NSError *error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(image){
                        self.moviewPlayer.hidden = true;
                        self.ingestImageView.hidden = false;
                        self.ingestImageView.image = image;
                        is_Video = false;
                    }else {
                        self.ingestImageView.hidden = true;
                        self.moviewPlayer.hidden = false;
                        self.moviewPlayer.backgroundColor = [UIColor blackColor];
                        is_Video = true;
                    }
                });
            }];
            NSExtensionItem *item = self.extensionContext.inputItems.firstObject;
            NSItemProvider *itemProvider = item.attachments.firstObject;
            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeImage]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage
                                                options:nil
                  completionHandler:^(NSURL *url, NSError *error) {
                      // send the image
                      
                      [itemProvider loadPreviewImageWithOptions:nil completionHandler:^(UIImage *image, NSError *error){
                          NSData *imageData;
                          UIImage *sharedImage = nil;
                          is_Video = false;
                          NSLog(@"%@", image);
                          if(!image){
                              if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                                  sharedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:(NSURL*)item]];
                                  
                                  imageData = [NSData dataWithContentsOfURL:(NSURL*) item];
                                  uploadImageData = imageData;
                                  //                     self.ingestImageView.image = sharedImage;
                                  is_Video = true;
                              }
                          }else {
                              if([(NSObject*)item isKindOfClass:[UIImage class]]) {
                                  sharedImage = (UIImage*)item;
                                  imageData = UIImagePNGRepresentation((UIImage *)item);
                                  uploadImageData = imageData;
                                  //                      self.ingestImageView.image = sharedImage;
                              }
                              is_Video = false;
                          }
                          [self postIngest];
                      }];
                      
                      
                  }];
            }else {
                
                [itemProvider loadItemForTypeIdentifier:@"public.movie" options:nil completionHandler:
                 ^(id<NSSecureCoding> item, NSError *error) {
                    NSData *imageData;
                    UIImage *sharedImage = nil;
                    is_Video = false;
                    
                    if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                        sharedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:(NSURL*)item]];
                        
                        is_Video = true;
                        ContentUrl = (NSURL *)item;
                        NSString *imageName = [[NSString alloc]initWithFormat:@"%@", item];
                        NSArray *getImageFormat = [imageName componentsSeparatedByString: @"/"];
                        UploadImagename = getImageFormat[getImageFormat.count - 1];
                        NSArray * format = [UploadImagename componentsSeparatedByString:@"."];
                        uploadimageFormat = [@"." stringByAppendingString:format[1]];
                        UploadImagename = format[0];
                        NSLog(@"Image name : %@", UploadImagename);
                        NSLog(@"Image Format : %@", uploadimageFormat);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.titleText.text = UploadImagename;
                        });
                        
                        imageData = [NSData dataWithContentsOfURL:(NSURL*) item];
                        uploadImageData = imageData;

                    }
                    [self postIngest];
                }];
                
            }
        });
        
        
    }
}
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (NSArray *)configurationItems {
    SLComposeSheetConfigurationItem *item;
    
    item = [[SLComposeSheetConfigurationItem alloc] init];
    [item setTitle:@"eMAM Ingest Id"];
    [item setValue:IngestIdTitle];
    
    // Handle what happens when a user taps your option.
    [item setTapHandler:^(void){
        
    }];
    
    // Return an array containing your item.
    
    return @[item];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

@end

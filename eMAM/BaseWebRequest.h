//
//  BaseWebRequest.h
//  eMAM
//
//  Created by APPLE on 06/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
#import "XMLReader.h"

@interface BaseWebRequest : NSObject


#pragma mark - URL Request Create

+ (NSMutableURLRequest *)requestWithUrlStr:(NSString *)urlStr;

+ (NSMutableURLRequest *)soapPostRequestWithSoapAction:(NSString *)soapAction soapBody:(NSString *)soapBody parameters:(id)parameters;

#pragma mark - Soap Header Remove

+ (NSDictionary *)truncateSoapHeaders:(NSDictionary *)dictionary;

#pragma mark - ASI Request Send

+ (void)sendRequestASI:(NSURLRequest *)request completionHandler:(void (^)(NSDictionary*, NSError*)) handler;

@end

/**************************************************************************************
 *  File Name      : Subclip.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 02-01-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <Foundation/Foundation.h>

#import "AVVideoPlayer.h"

@interface Subclip : NSObject

@property (nonatomic, strong) NSString *subclipId;
@property (nonatomic, strong) NSString *clipName;
@property (nonatomic, strong) NSString *setInTimeString;
@property (nonatomic, strong) NSString *setOutTimeString;
@property (nonatomic, strong) NSString *description;

@property (nonatomic, assign) CMTime setInTime;
@property (nonatomic, assign) CMTime setOutTime;
@property (nonatomic, strong) UIView *subclipView;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end

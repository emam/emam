//
//  UserService.m
//  eMAM
//
//  Created by APPLE on 06/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "UserService.h"
#import "Cryptor.h"

@implementation UserService


+ (UserService *)sharedInstance {
    static UserService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


#pragma mark - Pinig Server

- (void)pingToServer:(NSString *)urlString performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    [[self class] sendRequestASI:[[self class] requestWithUrlStr:urlString] completionHandler:handler];
    
}

#pragma mark - User Methods

- (void)authenticateUser:(NSString *)username password:(NSString *)password performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler  {
    
    NSString *licenseKey = [[NSUserDefaults standardUserDefaults] objectForKey:KSERVERLICENSEKEY];
    
    if (!licenseKey) {
        licenseKey = @"";
    }
    
    NSString *soapBody = [NSString stringWithFormat:@"<AuthenticateUser xmlns=\"http://tempuri.org/\">\
                          <authenticateObj>\
                          <UserName>%@</UserName>\
                          <Password>%@</Password>\
                          </authenticateObj>\
                          <LicenseKey>%@</LicenseKey>\
                          </AuthenticateUser>",username,password,licenseKey];
    NSString *soapAction = @"http://tempuri.org/AuthenticateUser";
    
    NSString *enecrypted = [Cryptor encryptString:password key:KEncryptionKey];
    
    [[NSUserDefaults standardUserDefaults] setValue:enecrypted forKey:KENCRYPTEDPASSWORD];
    [[NSUserDefaults standardUserDefaults] setValue:password forKey:KPASSWORD];
    
    NSLog(@"soapBody: %@", soapBody);
    
    //NSLog(@"parameters: %@", parameters);
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}


- (void)passwordReset:(NSString *)username performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler  {
    
    NSString *soapBody = [NSString stringWithFormat:@"<SubmitForgotPasswordRequest xmlns=\"http://tempuri.org/\">\
                          <EmailId>%@</EmailId>\
                          </SubmitForgotPasswordRequest>",username];
    NSString *soapAction = @"http://tempuri.org/SubmitForgotPasswordRequest";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}


- (void)getUserStatusPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<GetUserStatus xmlns=\"http://tempuri.org/\" />";
    NSString *soapAction = @"http://tempuri.org/GetUserStatus";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
}


- (void)getUserFeaturesperformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<GetDecryptedFeatureHashTable xmlns=\"http://tempuri.org/\" />";
    NSString *soapAction = @"http://tempuri.org/GetDecryptedFeatureHashTable";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getIngestProfileId:(int)userId unitId:(int)unitId ingestProfile:(int)profileID performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler {
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetUnitIngestProfiles xmlns=\"http://tempuri.org/\">\
                          <UnitID>%d</UnitID>\
                          <UserId>%d</UserId>\
                          <IngestProfileId>%d</IngestProfileId>\
                          </GetUnitIngestProfiles>", userId, unitId, profileID];
    
//    NSString *soapBody = [NSString stringWithFormat:@"<GetUnitIngestProfiles xmlns=\"http://tempuri.org/\" />\<UnitID>%d</UnitID><UserId>%d</UserId>\<IngestProfileId>%d</IngestProfileId>\</GetUnitIngestProfiles>", userId, unitId, profileID];
    NSString *soapAction = @"http://tempuri.org/GetUnitIngestProfiles";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
}

#pragma mark - Client Logo

- (void)getClientLogoPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<GetClientLogo xmlns=\"http://tempuri.org/\"></GetClientLogo>";
    NSString *soapAction = @"http://tempuri.org/GetClientLogo";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
}


@end

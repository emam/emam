/**************************************************************************************
 *  File Name      : DeliveryViewController.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 19-01-2013
 *  Copyright (C) 2013 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>


@interface DeliveryViewController : UIViewController

@property (nonatomic, assign) BOOL isEDL;
@property (nonatomic, strong) NSArray *deliveryContents;

@end

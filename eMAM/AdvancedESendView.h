//
//  AdvancedESendView.h
//  eMAM
//
//  Created by APPLE on 23/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^SelectionDismissalBlock)(NSMutableDictionary *selection);

@interface AdvancedESendView : UIViewController


@property (nonatomic, assign) BOOL showSendOption;
@property (nonatomic, strong) NSMutableDictionary *eSendInfo;

@property (nonatomic, copy) SelectionDismissalBlock dismissalBlock;

@property (nonatomic, assign) BOOL fromAssetEsend;
@property (nonatomic, strong) NSMutableDictionary *esentDet;


@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)sendTapped:(id)sender;

@end

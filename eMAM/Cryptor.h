//
//  Cryptor.h
//  NACoder
//
//  Created by Naveen Shan on 16 Feb 2013.
//  Copyright (c) 2013 Naveen Shan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>


@interface Cryptor : NSObject

+ (NSString *)md5:(NSString *)string;

+ (NSString *)encryptString:(NSString *)string key:(NSString *)key;
+ (NSString *)decryptString:(NSString *)string key:(NSString *)key;
+ (NSString *)doCipher:(NSString *)sTextIn key:(NSString *)sKey context:(CCOperation)encryptOrDecrypt;

#pragma mark Based64

+ (NSString *) encodeBase64WithString:(NSString *)strData;
+ (NSString *) encodeBase64WithData:(NSData *)objData;
+ (NSData *) decodeBase64WithString:(NSString *)strBase64;
@end

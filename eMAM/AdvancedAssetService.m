//
//  AdvancedAssetService.m
//  eMAM
//
//  Created by APPLE on 21/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AdvancedAssetService.h"
#import "Marker.h"
#import "AFNetworkReachabilityManager.h"


@implementation AdvancedAssetService

+ (AdvancedAssetService *)sharedInstance {
    static AdvancedAssetService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


#pragma mark - Permissions

- (void)getAssetPermissionsForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetPermissions xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          </GetAssetPermissions>",assetId];
    NSString *soapAction = @"http://tempuri.org/GetAssetPermissions";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - Preview

- (void)getVersionInfoForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetVersionInfo xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <PlatformId>%d</PlatformId>\
                          </GetAssetVersionInfo>",assetId,versionId,0];
    NSString *soapAction = @"http://tempuri.org/GetAssetVersionInfo";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - Metadata

- (void)getEmbededMetadataForAssetId:(int)assetId versionId:(int)versionId fieldName:(NSString *)fieldName performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetEmbededMetadata xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <FieldName>%@</FieldName>\
                          </GetEmbededMetadata>",assetId,versionId,fieldName];
    NSString *soapAction = @"http://tempuri.org/GetEmbededMetadata";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getCustomMetaDataForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetCustomMetaData xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          </GetCustomMetaData>",assetId];
    NSString *soapAction = @"http://tempuri.org/GetCustomMetaData";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getAssetTagsForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetTags xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          </GetAssetTags>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/GetAssetTags";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getAllMetadataForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    [self getEmbededMetadataForAssetId:assetId versionId:versionId fieldName:@""  performRequestWithHandler:^(NSDictionary *embedMetadata, NSError *error){
        if (!error) {
            [self getCustomMetaDataForAssetId:assetId  performRequestWithHandler:^(NSDictionary *customMetadata, NSError *error){
                if (!error) {
                    [self getAssetTagsForAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *assetTags, NSError *error){
                        if (!error) {
                            
                            id embedMetadataResponse = [[[[[embedMetadata objectForKey:@"GetEmbededMetadataResponse"] objectForKey:@"GetEmbededMetadataResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
                            id customMetadataResponse = [[[[[customMetadata objectForKey:@"GetCustomMetaDataResponse"] objectForKey:@"GetCustomMetaDataResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
                            id assetTagsResponse = [[[[[assetTags objectForKey:@"GetAssetTagsResponse"] objectForKey:@"GetAssetTagsResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
                            
                            NSArray *embedMetadataArray = nil;
                            if ([embedMetadataResponse isKindOfClass:[NSArray class]]) {
                                embedMetadataArray = embedMetadataResponse;
                            } else if ([embedMetadataResponse isKindOfClass:[NSDictionary class]]) {
                                embedMetadataArray = [NSArray arrayWithObjects:embedMetadataResponse, nil];
                            }
                            NSArray *customMetadataArray = nil;
                            if ([customMetadataResponse isKindOfClass:[NSArray class]]) {
                                customMetadataArray = customMetadataResponse;
                            } else if ([customMetadataResponse isKindOfClass:[NSDictionary class]]) {
                                customMetadataArray = [NSArray arrayWithObjects:customMetadataResponse, nil];
                            }
                            NSArray *assetTagsArray = nil;
                            if ([assetTagsResponse isKindOfClass:[NSArray class]]) {
                                assetTagsArray = assetTagsResponse;
                            } else if ([assetTagsResponse isKindOfClass:[NSDictionary class]]) {
                                assetTagsArray = [NSArray arrayWithObjects:assetTagsResponse, nil];
                            }
                            
                            NSLog(@"getAllMetadataForAssetId Asset Tag Array:%@",assetTagsArray);
                            
                            NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:(embedMetadataArray ? embedMetadataArray : @""),@"EmbedMetadata",(customMetadataArray ? customMetadataArray : @""),@"CustomMetadata",(assetTagsArray ? assetTagsArray : @""),@"AssetTags", nil];
                            handler(dictionary, nil);
                            return;
                        } else {
                            handler(nil, error);
                            return;
                        }
                    }];
                } else {
                    handler(nil, error);
                    return;
                }
            }];
        } else {
            handler(nil, error);
            return;
        }
    }];
}

- (void)updateFilePropertiesForAsset:(NSDictionary *)dictionary performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    int assetId = [[dictionary objectForKey:@"AssetId"] intValue];
    NSString *author = [dictionary objectForKey:@"Author"];
    NSString *title = [dictionary objectForKey:@"Title"];
    NSString *description = [dictionary objectForKey:@"Description"];
    
    NSString *soapBody = [NSString stringWithFormat:@"<UpdateFilePropertiesMetadata xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <Author>%@</Author>\
                          <Title>%@</Title>\
                          <Description>%@</Description>\
                          </UpdateFilePropertiesMetadata>",assetId,author,title,description];
    NSString *soapAction = @"http://tempuri.org/UpdateFilePropertiesMetadata";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)addAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagValue:(NSString *)tagValue tagType:(int)tagType performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<AddAssetsTags xmlns=\"http://tempuri.org/\">\
                          <TagValue>%@</TagValue>\
                          <AssetID>%d</AssetID>\
                          <VersionID>%d</VersionID>\
                          <TagType>%d</TagType>\
                          </AddAssetsTags>",tagValue,assetId,versionId,tagType];
    NSString *soapAction = @"http://tempuri.org/AddAssetsTags";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)updateAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagId:(int)tagId tagValue:(NSString *)tagValue performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<UpdateAssetsTags xmlns=\"http://tempuri.org/\">\
                          <TagID>%d</TagID>\
                          <TagValue>%@</TagValue>\
                          <AssetID>%d</AssetID>\
                          <VersionID>%d</VersionID>\
                          </UpdateAssetsTags>",tagId,tagValue,assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/UpdateAssetsTags";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)saveAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagInfo:(NSDictionary *)tagInfo performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    int tagId = [[tagInfo objectForKey:@"TagId"] intValue];
    int tagType = [[tagInfo objectForKey:@"TagType"] boolValue];
    NSString *tagValue = [tagInfo objectForKey:@"TagValue"];
    
    if ([tagInfo objectForKey:@"TagId"]) {
        [self updateAssetsTagsForAssetId:assetId versionId:versionId tagId:tagId tagValue:tagValue performRequestWithHandler:handler];
    } else {
        [self addAssetsTagsForAssetId:assetId versionId:versionId tagValue:tagValue tagType:tagType performRequestWithHandler:handler];
    }
    
}

- (void)deleteAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagId:(int)tagId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<DeleteAssetsTags xmlns=\"http://tempuri.org/\">\
                          <TagID>%d</TagID>\
                          <AssetID>%d</AssetID>\
                          <VersionID>%d</VersionID>\
                          </DeleteAssetsTags>",tagId,assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/DeleteAssetsTags";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - SubClips

- (void)getAllSubClipsForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<LoadAllMarkerTimeCodes xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <isMarker>false</isMarker>\
                          </LoadAllMarkerTimeCodes>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/LoadAllMarkerTimeCodes";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)saveAllSubclips:(NSArray *)subclips forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *subclipString = @"&lt;SubClip ID=\"%d\" Name=\"%@\"&gt;\
    &lt;SetInTime&gt;%@&lt;/SetInTime&gt;\
    &lt;SetOutTime&gt;%@&lt;/SetOutTime&gt;\
    &lt;Description&gt;%@&lt;/Description&gt;\
    &lt;/SubClip&gt;";
    
    NSString *newsubclipString = nil;
    NSMutableString *subclipDetails = [NSMutableString string];
    [subclipDetails appendString:@"&lt;SubClips&gt;"];
    for (Subclip *subclip in subclips) {
        
        
        NSLog(@"Subclip:%@",subclip.clipName);
        
        newsubclipString = [NSString stringWithFormat:subclipString,[subclip.subclipId intValue],subclip.clipName,subclip.setInTimeString,subclip.setOutTimeString,(((NSNull *)subclip.description == [NSNull null])? @"":subclip.description)];
        
        NSLog(@"newsubclipString:%@",newsubclipString);
        
        [subclipDetails appendString:newsubclipString];
        newsubclipString = nil;
    }
    [subclipDetails appendString:@"&lt;/SubClips&gt;"];
    
    NSString *strPostBody = [NSString stringWithFormat:@"<SaveMarkers xmlns=\"http://tempuri.org/\">\
                             <xmlMarkerDetails>%@</xmlMarkerDetails>\
                             <AssetId>%d</AssetId>\
                             <VersionId>%d</VersionId>\
                             <isMarker>false</isMarker>\
                             </SaveMarkers>",subclipDetails,assetId,versionId];

    
    
    NSString *soapBody = strPostBody;
    NSString *soapAction = @"http://tempuri.org/SaveMarkers";
    
    if  ([AFNetworkReachabilityManager sharedManager].reachable){
        
        [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
        
    }else{
        [MY_APP_DELEGATE.failedMarkerRequests addObject:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil]];
    }

    
    
    
}


- (void)deleteSubclips:(NSArray *)subclips forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *newsubclipString = nil;
    NSMutableString *subclipDetails = [NSMutableString string];
    [subclipDetails appendString:@"<DeleteMarkers xmlns=\"http://tempuri.org/\">\
     <arrMarkerID>"];
    for (Subclip *subclip in subclips) {
        newsubclipString = [NSString stringWithFormat:@"<int>%d</int>",[subclip.subclipId intValue]];
        [subclipDetails appendString:newsubclipString];
        newsubclipString = nil;
    }
    [subclipDetails appendString:@"</arrMarkerID>\
     </DeleteMarkers>"];
    
    NSLog(@"AdvancedAsset Service : Delete Subclips Post Body : %@",subclipDetails);
    
    NSString *soapBody = subclipDetails;
    NSString *soapAction = @"http://tempuri.org/DeleteMarkers";
    
    if  ([AFNetworkReachabilityManager sharedManager].reachable){
        
        [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
        
    }else{
        [MY_APP_DELEGATE.failedMarkerRequests addObject:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil]];
    }
    
}

- (void)getAllDeliveryProfilesForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAllDeliveryProfiles xmlns=\"http://tempuri.org/\">\
                          <IncludeEDL>true</IncludeEDL>\
                          <IncludeDuplicate>false</IncludeDuplicate>\
                          <LocationType>ALL</LocationType>\
                          <DeliveryQueueId>0</DeliveryQueueId>\
                          <CategoryId>0</CategoryId>\
                          <FolderId>0</FolderId>\
                          <IncludeSubCategoriesOrFolders>false</IncludeSubCategoriesOrFolders>\
                          <IsArchiveAllowed>true</IsArchiveAllowed>\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <StorageId>0</StorageId>\
                          <ClientId>0</ClientId>\
                          </GetAllDeliveryProfiles>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/GetAllDeliveryProfiles";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getAllSendEDLProfilesForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAllDeliveryProfiles xmlns=\"http://tempuri.org/\">\
                          <IsEdl>true</IsEdl>\
                          <IsDuplicate>false</IsDuplicate>\
                          <DeliveryQueueId>0</DeliveryQueueId>\
                          <CategoryId>0</CategoryId>\
                          <FolderId>0</FolderId>\
                          <IncludeSubCategoriesOrFolders>true</IncludeSubCategoriesOrFolders>\
                          <IsArchiveAllowed>true</IsArchiveAllowed>\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <StorageId>0</StorageId>\
                          <ClientId>0</ClientId>\
                          </GetAllDeliveryProfiles>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/GetAllDeliveryProfiles";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)insertDeliveryQueueDetailsForAssetId:(int)assetId versionId:(int)versionId addedDate:(NSString *)addedDate markers:(NSArray *)markers profiles:(NSArray *)profiles allowDuplicate:(BOOL)allowDuplicate isEDL:(BOOL)isEDL performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    
    NSString *strAllowDuplicated = (allowDuplicate) ? @"true" : @"false";
    
    NSString *newString = nil;
    NSMutableString *strProfileIds = [NSMutableString string];
    for (NSDictionary *profile in profiles) {
        newString = [NSString stringWithFormat:@"<int>%@</int>",[[profile objectForKey:@"DELIVERYPROFILEID"] objectForKey:@"text"]];
        [strProfileIds appendString:newString];
        newString = nil;
    }
    
    NSMutableString *strMarkerIds = [NSMutableString string];
  //  for (Subclip *subclip in markers) {
       // newString = [NSString stringWithFormat:@"<int>%@</int>",subclip.subclipId];
        [strMarkerIds appendString:newString];
        newString = nil;
    //}

    
    NSString *soapBody = [NSString stringWithFormat:@"<InsertDeliveryQueueDetails xmlns=\"http://tempuri.org/\">\
                          <intUserID>0</intUserID>\
                          <intAssetID>%d</intAssetID>\
                          <intVersionID>%d</intVersionID>\
                          <dtDateAdded>%@</dtDateAdded>\
                          <strStatus>Queued</strStatus>\
                          <MarkerIds>%@</MarkerIds>\
                          <IsDuplicateAllowed>%@</IsDuplicateAllowed>\
                          <ProfileIds>%@</ProfileIds>\
                          <IsArchiveAllowed>false</IsArchiveAllowed>\
                          </InsertDeliveryQueueDetails>",assetId,versionId,addedDate,strMarkerIds,strAllowDuplicated,strProfileIds];
    NSString *soapAction = @"http://tempuri.org/InsertDeliveryQueueDetails";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:^(NSDictionary *dictionary, NSError *error) {
        NSString *resultString = [self resultStringInResponse:dictionary isEDL:isEDL];
        int resultID = 0;
        NSDictionary *responseDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:resultID], @"ResultIdentifier",resultString,@"ResultString",nil];
        handler(responseDictionary,nil);
    }];
    
}


- (void)UpdateDeliveryQueueDetailsForAssetId:(int)assetId versionId:(int)versionId addedDate:(NSString *)addedDate markers:(NSArray *)markers profiles:(NSArray *)profiles allowDuplicate:(BOOL)allowDuplicate isEDL:(BOOL)isEDL performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *strAllowDuplicated = (allowDuplicate) ? @"true" : @"false";
    
    NSString *newString = nil;
    NSMutableString *strProfileIds = [NSMutableString string];
    for (NSDictionary *profile in profiles) {
        newString = [NSString stringWithFormat:@"%@",[[profile objectForKey:@"DELIVERYPROFILEID"] objectForKey:@"text"]];
        [strProfileIds appendString:newString];
        newString = nil;
    }
    
    
    NSMutableString *strMarkerIds = [NSMutableString string];
    for (Subclip *subclip in markers) {
        newString = [NSString stringWithFormat:@"<int>%@</int>",subclip.subclipId];
        [strMarkerIds appendString:newString];
        newString = nil;
    }
    NSString *strPostBody = [NSString stringWithFormat:@"<UpdateDeliveryQueueDetails xmlns=\"http://tempuri.org/\">\
                             <OperationType>Insert</OperationType>\
                             <ProfileId>%@</ProfileId>\
                             <QueueID>0</QueueID>\
                             <VersionID>%d</VersionID>\
                             <ClipIDs>%@</ClipIDs>\
                             <Status>Queued</Status>\
                             <Description>Added for delivery</Description>\
                             <IsRestoreAllowed>true</IsRestoreAllowed>\
                             <KeepFolderStructure>false</KeepFolderStructure>\
                             <CarbonResponseID></CarbonResponseID>\
                             <DeliveryFileName></DeliveryFileName>\
                             <StorageProfileId>0</StorageProfileId>\
                             </UpdateDeliveryQueueDetails>",strProfileIds,versionId,strMarkerIds];
    
    NSLog(@"AdvancedAsset Service : Insert Delivery Post Body : %@",strPostBody);
    
    NSString *soapAction = @"http://tempuri.org/UpdateDeliveryQueueDetails";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:strPostBody parameters:nil] completionHandler:^(NSDictionary *dictionary, NSError *error) {
        
        
        NSLog(@"dictionary:%@",dictionary);
        
        
        
        id response = [[dictionary objectForKey:@"UpdateDeliveryQueueDetailsResponse"] objectForKey:@"UpdateDeliveryQueueDetailsResult"];
        
        NSLog(@"response:%@",response);
        
        
        NSDictionary *responseDictionary=[NSDictionary dictionaryWithDictionary:response];
        
        NSLog(@"responseDictionary:%@",responseDictionary);
        
        //        NSString *resultString = [self resultStringInResponse:dictionary isEDL:isEDL];
        //        int resultID = 0;
        //        NSDictionary *responseDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:resultID], @"ResultIdentifier",resultString,@"ResultString",nil];
        
        
        
        
        
        handler(responseDictionary,nil);
    }];
    
}

- (NSString *)resultStringInResponse:(NSDictionary *)dictionary isEDL:(BOOL)isEDL {
    int result = 0;
    NSString * responseString = @"";
    switch (result) {
        case 1: {
            responseString = @"Asset added to delivery queue successfully.";
            break;
        }
        case 2: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is archived.":@"Delivery failed. This asset is archived.");
            break;
        }
        case 3: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is archived.":@"Delivery failed. This asset is archived.");
            break;
        }
        case 4: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is purged.":@"Delivery failed. This asset is purged.");
            break;
        }
        case 5: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is queued for Archive. Please try after sometime":@"Delivery failed. This asset is queued for Archive. Please try after sometime");
            break;
        }
        case 6: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is queued for Restore. Please try after sometime":@"Delivery failed. This asset is queued for Restore. Please try after sometime");
            break;
        }
        case 7: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is queued for Purge. Please try after sometime":@"Delivery failed. This asset is queued for Purge. Please try after sometime");
            break;
        }
        case 8: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is not completely archived.":@"Delivery failed. This asset is not completely archived.");
            break;
        }
        case 9: {
            responseString = ((isEDL)?@"EDL Delivery failed. This asset is queued for Archive. Please try after sometime":@"Delivery failed. This asset is queued for Archive. Please try after sometime");
            break;
        }
        case 10: {
            responseString = @"Asset already queued for delivery.Do you want to submit job again?";
            break;
        }
            
        default:
            break;
    }
    return responseString;
}



#pragma mark - Markers

- (void)getAllMarkersForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<LoadAllMarkerTimeCodes xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <VersionId>%d</VersionId>\
                          <IsMarker>true</IsMarker>\
                          </LoadAllMarkerTimeCodes>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/LoadAllMarkerTimeCodes";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)saveAllMarkers:(NSArray *)markers forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *markerString = @"&lt;Marker ID=\"%d\" Name=\"%@\"&gt;\
    &lt;TimeCode&gt;%@&lt;/TimeCode&gt;\
    &lt;Duration&gt;%@&lt;/Duration&gt;\
    &lt;Description&gt;%@&lt;/Description&gt;\
    &lt;/Marker&gt;";
    NSString *newMarkerString = nil;
    
    NSMutableString *markerDetails = [NSMutableString string];
    [markerDetails appendString:@"&lt;Markers&gt;"];
    for (Marker *marker in markers) {
        
        NSLog(@"Marker:%@",marker);
        
        newMarkerString = [NSString stringWithFormat:markerString,[marker.markerId intValue],marker.name,marker.timeCode,marker.duration,(((NSNull *)marker.description == [NSNull null])? @"":marker.description)];
        
        NSLog(@"newMarkerString:%@",newMarkerString);
        
        [markerDetails appendString:newMarkerString];
        
        newMarkerString = nil;
    }
    [markerDetails appendString:@"&lt;/Markers&gt;"];
    
    NSString *soapBody = [NSString stringWithFormat:@"<SaveMarkers xmlns=\"http://tempuri.org/\">\
                             <xmlMarkerDetails>%@</xmlMarkerDetails>\
                             <AssetId>%d</AssetId>\
                             <VersionId>%d</VersionId>\
                             <isMarker>true</isMarker>\
                             </SaveMarkers>",markerDetails,assetId,versionId];
    
    
    NSString *soapAction = @"http://tempuri.org/SaveMarkers";
    
    if  ([AFNetworkReachabilityManager sharedManager].reachable){
        
        [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
        
    }else{
        [MY_APP_DELEGATE.failedMarkerRequests addObject:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil]];
    }
    
//    NSLog(@"Req Fails:%@",[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil]);
//    NSLog(@"Req Fails array:%@",MY_APP_DELEGATE.failedMarkerRequests);
    
    
    
}

- (void)deleteMarkers:(NSArray *)markers forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *newMarkerString = nil;
    NSMutableString *markerDetails = [NSMutableString string];
    [markerDetails appendString:@"<DeleteMarkers xmlns=\"http://tempuri.org/\">\
     <arrMarkerID>"];
    for (Marker *marker in markers) {
        newMarkerString = [NSString stringWithFormat:@"<int>%d</int>",[marker.markerId intValue]];
        [markerDetails appendString:newMarkerString];
        newMarkerString = nil;
    }
    [markerDetails appendString:@"</arrMarkerID>\
     </DeleteMarkers>"];
    
    NSString *soapBody = markerDetails;
    NSString *soapAction = @"http://tempuri.org/DeleteMarkers";
    
    
    if  ([AFNetworkReachabilityManager sharedManager].reachable){
        
        [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
        
    }else{
        [MY_APP_DELEGATE.failedMarkerRequests addObject:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil]];
    }
    
    
    
}

#pragma mark - Approval

- (void)geUsersForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *permissionCode = @"";
//    switch ([Datastore sharedStore].userRoleID) {
//        case 2:
//            permissionCode = @"SIAPP"; // Admin user
//            break;
//        case 3:
//            permissionCode = @"APPREJ"; // eMAM user
//            break;
//            
//        default:
//            NSLog(@"AdvancedAsset Service Error : Fail to find user permission code - role id : %d ***",[Datastore sharedStore].userRoleID);
//            break;
//    }

    
NSString *soapBody = [NSString stringWithFormat:@"<GetUsersWithPermissionOnAsset xmlns=\"http://tempuri.org/\">\
                      <assetId>%d</assetId>\
                      <permissionCode>%@</permissionCode>\
                      </GetUsersWithPermissionOnAsset>",assetId,permissionCode];
    NSString *soapAction = @"http://tempuri.org/GetUsersWithPermissionOnAsset";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)submitApprovalForUser:(int)userId assetId:(int)asetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<SubmitApproval xmlns=\"http://tempuri.org/\">\
                          <ApproverID>%d</ApproverID>\
                          <AssetID>%d</AssetID>\
                          <Version_ID>%d</Version_ID>\
                          </SubmitApproval>",userId,asetId,versionId];
    NSString *soapAction = @"http://tempuri.org/SubmitApproval";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)submitExternalApprovalForUser:(NSDictionary *)userDetails assetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *emailIds = [userDetails objectForKey:@"EmailIds"];
    NSString *loginName = [userDetails objectForKey:@"LoginName"];
    NSString *password = [userDetails objectForKey:@"Password"];
    NSString *expiryDate = [userDetails objectForKey:@"ExpiryDate"];
    
    NSString *soapBody =[NSString stringWithFormat:@"<SubmitExternalApproval xmlns=\"http://tempuri.org/\">\
                         <AssetID>%d</AssetID>\
                         <Version_ID>%d</Version_ID>\
                         <Email_ID>%@</Email_ID>\
                         <LoginName>%@</LoginName>\
                         <Password>%@</Password>\
                         <ExpiryDate>%@</ExpiryDate>\
                         </SubmitExternalApproval>",assetId,versionId,emailIds,loginName,password,expiryDate];
    
    NSString *soapAction = @"http://tempuri.org/SubmitExternalApproval";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - eSend

- (void)sendeSendWithDetails:(NSDictionary *)eSendDetails performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *reelName = [eSendDetails objectForKey:@"ReelName"];
    NSString *expiryDate = [eSendDetails objectForKey:@"ExipryDate"];
    NSString *emailType = [eSendDetails objectForKey:@"EmailType"];
    NSString *downloadable = [[eSendDetails objectForKey:@"Downloadable"] boolValue] ? @"true" : @"false";
    NSString *confirmationEmail = @"false"; //[eSendDetails objectForKey:@"ConfirmationEmail"];
    NSString *reportTracking = [[eSendDetails objectForKey:@"ReportTracking"] boolValue] ? @"true" : @"false";
    NSString *forwardOption = [[eSendDetails objectForKey:@"ForwardOption"] boolValue] ? @"true" : @"false";
    NSString *sendCopy = [[eSendDetails objectForKey:@"SendCopy"] boolValue] ? @"true" : @"false";
    NSString *allowComments = [[eSendDetails objectForKey:@"AllowComments"] boolValue] ? @"true" : @"false";
    int emailThemeId = [[eSendDetails objectForKey:@"EmailThemeId"] intValue];
    int previewThemeId = [[eSendDetails objectForKey:@"PreviewThemeId"] intValue];
    int emailCustomThemeId = 0;
    int previewCustomThemeId = 0;
    NSString *thumbNailUrl = [eSendDetails objectForKey:@"ThumbNailUrl"];
    NSString *logoPath = [eSendDetails objectForKey:@"LogoPath"];
    NSString *description = [eSendDetails objectForKey:@"Description"];
    NSString *emailIds = [eSendDetails objectForKey:@"EmailIds"];
    NSString *assetIds = [eSendDetails objectForKey:@"AssetIds"];
    NSString *assetVersionIds = [eSendDetails objectForKey:@"AssetVersionIds"];
    NSString *subject = [eSendDetails objectForKey:@"Subject"];
    NSString *unsubscribeLink = [[eSendDetails objectForKey:@"UnsubscribeLink"] boolValue] ? @"true" : @"false";
    int accessLimit = [[eSendDetails objectForKey:@"AccessLimit"] intValue];
    NSString *emailTheme = @"Milky"; //[eSendDetails objectForKey:@"EmailTheme"];
    NSString *previewTheme = @"Milky"; //[eSendDetails objectForKey:@"PreviewTheme"];

    
    NSString *soapBody = [NSString stringWithFormat:@"<AddeBINSendInfo xmlns=\"http://tempuri.org/\">\
                          <objeBInSendInfo>\
                          <ReelName>%@</ReelName>\
                          <UserId>0</UserId>\
                          <ExpireDate>%@</ExpireDate>\
                          <EmailType>%@</EmailType>\
                          <Downloadable>%@</Downloadable>\
                          <ConfirmationEmail>%@</ConfirmationEmail>\
                          <ReportTracking>%@</ReportTracking>\
                          <ForwardOption>%@</ForwardOption>\
                          <SendCopy>%@</SendCopy>\
                          <AllowComments>%@</AllowComments>\
                          <EmailThemeId>%d</EmailThemeId>\
                          <PreviewThemeId>%d</PreviewThemeId>\
                          <EmailCustomThemeId>%d</EmailCustomThemeId>\
                          <PreviewCustomThemeId>%d</PreviewCustomThemeId>\
                          <ThumbNailUrl>%@</ThumbNailUrl>\
                          <LogoPath>%@</LogoPath>\
                          <Description>%@</Description>\
                          <EmailIds>%@</EmailIds>\
                          <AssetIds>%@</AssetIds>\
                          <AssetVersionIds>%@</AssetVersionIds>\
                          <Subject>%@</Subject>\
                          <ForwardUserId>0</ForwardUserId>\
                          <SendInfoId>0</SendInfoId>\
                          <UnsubscribeLink>%@</UnsubscribeLink>\
                          <MessageID>0</MessageID>\
                          <AccessLimit>%d</AccessLimit>\
                          <EmailTheme>%@</EmailTheme>\
                          <PreviewTheme>%@</PreviewTheme>\
                          <SendMail>true</SendMail>\
                          </objeBInSendInfo>\
                          </AddeBINSendInfo>",reelName,expiryDate,emailType,downloadable,confirmationEmail,reportTracking,forwardOption,sendCopy,allowComments,emailThemeId,previewThemeId,emailCustomThemeId,previewCustomThemeId,thumbNailUrl,logoPath,description,emailIds,assetIds,assetVersionIds,subject,unsubscribeLink,accessLimit,emailTheme,previewTheme];
    NSString *soapAction = @"http://tempuri.org/AddeBINSendInfo";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - Comments

- (void)getAssetCommentsForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetComments xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <IncludeAsset>%d</IncludeAsset>\
                          <IncludeApproval>%d</IncludeApproval>\
                          <IncludeEsend>%d</IncludeEsend>\
                          </GetAssetComments>",assetId,true,true,true];
                          
        NSString *soapAction = @"http://tempuri.org/GetAssetComments";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)addAssetCommentsForAssetId:(int)assetId comment:(NSString *)comment performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<AddAssetComments xmlns=\"http://tempuri.org/\">\
                          <Comment>%@</Comment>\
                          <AssetId>%d</AssetId>\
                          </AddAssetComments>",comment,assetId];
    NSString *soapAction = @"http://tempuri.org/AddAssetComments";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)deleteAssetCommentsForAssetId:(int)assetId commentId:(int)commentId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<DeleteAssetsComments xmlns=\"http://tempuri.org/\">\
                          <CommentID>%d</CommentID>\
                          <Type>%s</Type>\
                          </DeleteAssetsComments>",commentId,"asset"];
    NSString *soapAction = @"http://tempuri.org/DeleteAssetsComments";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - History

- (void)getAssetHistoryForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetHistroy xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          </GetAssetHistroy>",assetId];
    NSString *soapAction = @"http://tempuri.org/GetAssetHistroy";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

#pragma mark - Embed

- (void)geteMAMPlayeSkinsWithPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = @"<GeteMAMPlayerSkins xmlns=\"http://tempuri.org/\">\
    </GeteMAMPlayerSkins>";
    NSString *soapAction = @"http://tempuri.org/GeteMAMPlayerSkins";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)generatePlayerObject:(NSDictionary *)parameters performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GeneratePlayerObj xmlns=\"http://tempuri.org/\">\
                          <strFlvSource>%@</strFlvSource>\
                          <UUID>%@</UUID>\
                          <applicationId>embededObject</applicationId>\
                          <ColorCode>%@</ColorCode>\
                          <Width>%d</Width>\
                          <Height>%d</Height>\
                          <SkinAutoHide>%d</SkinAutoHide>\
                          <AutoPlay>%d</AutoPlay>\
                          <FullScreen>%d</FullScreen>\
                          <ClosedCaption>%d</ClosedCaption>\
                          <LowResVirtualPath>%@</LowResVirtualPath>\
                          </GeneratePlayerObj>",[parameters objectForKey:@"VirtualPath"],[parameters objectForKey:@"UUID"],[parameters objectForKey:@"ColorCode"],[[parameters objectForKey:@"PlayerWidth"] intValue],[[parameters objectForKey:@"PlayerHeight"] intValue],[[parameters objectForKey:@"SkinAutoHide"] intValue],[[parameters objectForKey:@"AutoPlay"] intValue],[[parameters objectForKey:@"FullScreen"] intValue],[[parameters objectForKey:@"ClosedCaption"] intValue],[parameters objectForKey:@"LowResPath"]];
    NSString *soapAction = @"http://tempuri.org/GeneratePlayerObj";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)inserteMAMAssetPlayer:(NSDictionary *)parameters performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<InserteMAMAssetPlayer xmlns=\"http://tempuri.org/\">\
                          <objeMAMPlayer>\
                          <SkinName>%@</SkinName>\
                          <ColorCode>%@</ColorCode>\
                          <UUID>%@</UUID>\
                          <Width>%d</Width>\
                          <Height>%d</Height>\
                          <AutoHide>%d</AutoHide>\
                          <AutoPlay>%d</AutoPlay>\
                          <ClosedCaption>%d</ClosedCaption>\
                          <FullScreen>%d</FullScreen>\
                          <IsDeleted>%d</IsDeleted>\
                          <SkinId>%d</SkinId>\
                          </objeMAMPlayer>\
                          </InserteMAMAssetPlayer>",[parameters objectForKey:@"SkinName"],[parameters objectForKey:@"ColorCode"],[parameters objectForKey:@"UUID"],[[parameters objectForKey:@"PlayerWidth"] intValue],[[parameters objectForKey:@"PlayerHeight"] intValue],[[parameters objectForKey:@"SkinAutoHide"] intValue],[[parameters objectForKey:@"AutoPlay"] intValue],[[parameters objectForKey:@"ClosedCaption"] intValue],[[parameters objectForKey:@"FullScreen"] intValue],0,[[parameters objectForKey:@"SkinId"] intValue]];
    NSString *soapAction = @"http://tempuri.org/InserteMAMAssetPlayer";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}



@end

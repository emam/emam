/**************************************************************************************
 *  File Name      : SubclipView.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 26-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

#import "Subclip.h"
@protocol SubclipDelegate;

@interface SubclipView : UIView

@property (nonatomic, strong) id <SubclipDelegate> delegate;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

- (void)refreshView;
- (void)setSubclipInfo:(NSMutableArray *)subclipInfo;

@end

@protocol SubclipDelegate <NSObject>

- (void)saveSubclips:(NSArray *)subclips;
- (void)deleteSubclips:(NSArray *)subclips;
- (void)showDeliveryProfilesForSubclips:(NSArray *)subclips;
- (void)showSendEDLProfilesForSubclips:(NSArray *)subclips;

@end
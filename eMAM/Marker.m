/**************************************************************************************
 *  File Name      : Marker.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 15-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "Marker.h"

@implementation Marker

@synthesize markerId = _markerId;
@synthesize name = _name;
@synthesize description = _description;
@synthesize timeCode = _timeCode;
@synthesize duration = _duration;

@synthesize markerControl = _markerControl;
@synthesize markerTime = _markerTime;

#pragma mark -

- (id)initWithDictionary:(NSDictionary *)dictionary  {
    self = [super init];
    if (self) {
        self.markerId = [[dictionary objectForKey:@"id"] objectForKey:@"text"];
        self.name = [[dictionary objectForKey:@"MarkerName"] objectForKey:@"text"];
        self.description = [[dictionary objectForKey:@"Description"] objectForKey:@"text"];
        self.timeCode = [[dictionary objectForKey:@"SetInTime"] objectForKey:@"text"];
        self.duration = [[dictionary objectForKey:@"Duration"] objectForKey:@"text"];
        [self setCMTime];
    }
    return self;
}

- (void)setCMTime   {
    NSLog(@"EMAM Marker View : TimeCode : %@",self.timeCode);
    
    NSArray *components = [self.timeCode componentsSeparatedByString:@":"];
    if ([components count] >= 4) {
        float hourInSeconds = [[components objectAtIndex:0] intValue] * 60 * 60;
        float minitueInSeconds = [[components objectAtIndex:1] intValue] * 60;
        float seconds = [[components objectAtIndex:2] intValue];
        
        float frame = [[components objectAtIndex:3] intValue];
        float frameSeconds = frame / [AVVideoPlayer sharedInstance].frameRate;
        
//        NSString *frameString = [NSString stringWithFormat:@".%@",[components objectAtIndex:3]];
//        float timeFrameInSeconds = [frameString floatValue] * [AVVideoPlayer sharedInstance].frameRate;
        float totalSeconds = hourInSeconds + minitueInSeconds + seconds + frameSeconds;
////        NSString *floatString = [NSString stringWithFormat:@"%.0f.%.0f",totalSeconds,timeFrameInSeconds];
////        totalSeconds = [floatString floatValue];
//        totalSeconds = totalSeconds + timeFrameInSeconds;
//        NSLog(@"EMAM Marker View :  FrameString : %@ - [frameString floatValue] : %f  - totalSeconds %f timeFrameInSeconds : %f -- %f",frameString,[frameString floatValue],totalSeconds,timeFrameInSeconds,fmodf(timeFrameInSeconds,1));
        
        CMTime cmTime = CMTimeMakeWithSeconds(totalSeconds, NSEC_PER_SEC);
        NSLog(@"EMAM Marker View : New TimeCode : %@ - Time %f",self.timeCode,CMTimeGetSeconds(cmTime));
        self.markerTime = cmTime;
        CMTimeShow(self.markerTime);
    }
}

@end

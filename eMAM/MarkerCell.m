/**************************************************************************************
 *  File Name      : MarkerCell.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "MarkerCell.h"
#import "AVVideoPlayer.h"

@implementation MarkerCell

@synthesize marker = _marker;
@synthesize delegate = _delegate;
@synthesize currentOrientation = _currentOrientation;

#pragma mark -

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

- (void)initView {
    self.contentView.backgroundColor = [UIColor darkGrayColor];
    

    UILabel *objLabelName = [[UILabel alloc] init];
    objLabelName.tag = 1001;
    objLabelName.backgroundColor = [UIColor clearColor];
    objLabelName.textColor=[UIColor whiteColor];
    objLabelName.numberOfLines = 1;
    objLabelName.lineBreakMode = UILineBreakModeTailTruncation;
    [objLabelName setText:@"Name"];
    objLabelName.font = [UIFont fontWithName:APPBOLDFONTNAME size:13.0];
    [self.contentView addSubview:objLabelName];
    [objLabelName release];
    objLabelName=nil;
    
    UILabel *objLabelDescription = [[UILabel alloc] init];
    objLabelDescription.tag = 1002;
    objLabelDescription.backgroundColor = [UIColor clearColor];
    objLabelDescription.textColor=[UIColor whiteColor];
    objLabelDescription.numberOfLines = 1;
    objLabelDescription.lineBreakMode = UILineBreakModeTailTruncation;
    [objLabelDescription setText:@"Description"];
    objLabelDescription.font = [UIFont fontWithName:APPBOLDFONTNAME size:13.0];
    [self.contentView addSubview:objLabelDescription];
    [objLabelDescription release];
    objLabelDescription=nil;
    
    UILabel *objLabelStart = [[UILabel alloc] init];
    objLabelStart.tag = 1003;
    objLabelStart.backgroundColor = [UIColor clearColor];
    objLabelStart.textColor=[UIColor whiteColor];
    objLabelStart .numberOfLines = 1;
    objLabelStart .lineBreakMode = UILineBreakModeTailTruncation;
    [objLabelStart  setText:@"Start"];
    objLabelStart .font = [UIFont fontWithName:APPBOLDFONTNAME size:13.0];
    [self.contentView addSubview:objLabelStart ];
    [objLabelStart  release];
    objLabelStart =nil;
    
    UILabel *objLabelDuration = [[UILabel alloc] init];
    objLabelDuration.tag = 1004;
    objLabelDuration.backgroundColor = [UIColor clearColor];
    objLabelDuration.textColor=[UIColor whiteColor];
    objLabelDuration.numberOfLines = 1;
    objLabelDuration.lineBreakMode = UILineBreakModeTailTruncation;
    [objLabelDuration setText:@"Duration"];
    objLabelDuration.font = [UIFont fontWithName:APPBOLDFONTNAME size:13.0];
    [self.contentView addSubview:objLabelDuration];
    [objLabelDuration release];
    objLabelDuration=nil;
    
    UITextField *nameTextField = [[UITextField alloc] init];
    nameTextField.delegate = (id)self;
    nameTextField.tag = 1005;
    nameTextField.borderStyle = UITextBorderStyleLine;
    nameTextField.returnKeyType = UIReturnKeyNext;
    nameTextField.backgroundColor = [UIColor clearColor];
    nameTextField.textColor = [UIColor whiteColor];
    nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    nameTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:nameTextField];
    [nameTextField release];
    nameTextField = nil;
    
    UITextField *descriptionTextField = [[UITextField alloc] init];
    descriptionTextField.delegate = (id)self;
    descriptionTextField.tag = 1006;
    descriptionTextField.borderStyle = UITextBorderStyleLine;
    descriptionTextField.returnKeyType = UIReturnKeyNext;
    descriptionTextField.backgroundColor = [UIColor clearColor];
    descriptionTextField.textColor = [UIColor whiteColor];
    descriptionTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    descriptionTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    descriptionTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:descriptionTextField];
    [descriptionTextField release];
    descriptionTextField = nil;
    
    UITextField *startTextField = [[UITextField alloc] init];
    startTextField.delegate = (id)self;
    startTextField.tag = 1007;
    startTextField.borderStyle = UITextBorderStyleLine;
    startTextField.returnKeyType = UIReturnKeyNext;
    startTextField.backgroundColor = [UIColor clearColor];
    startTextField.textColor = [UIColor whiteColor];
    startTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    startTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    startTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:startTextField];
    [startTextField release];
    startTextField = nil;
    
    UITextField *durationTextField = [[UITextField alloc] init];

    durationTextField.delegate = (id)self;
    durationTextField.tag = 1008;
    durationTextField.borderStyle = UITextBorderStyleLine;
    durationTextField.returnKeyType = UIReturnKeyNext;
    durationTextField.backgroundColor = [UIColor clearColor];
    durationTextField.textColor = [UIColor whiteColor];
    durationTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    durationTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    durationTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:durationTextField];
    [durationTextField release];
    durationTextField = nil;
    
    UIButton *closeButton = [[UIButton alloc] init];
    closeButton.tag = 1009;
    closeButton.backgroundColor = [UIColor clearColor];
    //[closeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_marker_close" ofType:@"png"]] forState:UIControlStateNormal];
    [closeButton setImage:[UIImage imageNamed:@"btn_comment_delete.png"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:closeButton];
    [closeButton release];
    closeButton = nil;
}

- (void)layoutSubviews   {
    [super layoutSubviews];
    UIView *objLabelName = [self viewWithTag:1001];
    UIView *objLabelDescription = [self viewWithTag:1002];
    UIView *objLabelStart = [self viewWithTag:1003];
    UIView *objLabelDuration = [self viewWithTag:1004];
    UIView *nameTextField = [self viewWithTag:1005];
    UIView *descriptionTextField = [self viewWithTag:1006];
    UIView *startTextField = [self viewWithTag:1007];
    UIView *durationTextField = [self viewWithTag:1008];
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        objLabelName.frame = CGRectMake(15,0, 150,20);
        objLabelDescription.frame = CGRectMake(210,0, 150,20);
        objLabelStart .frame = CGRectMake(475,0, 150,20);
        objLabelDuration.frame = CGRectMake(580,0, 150,20);
        
        nameTextField.frame = CGRectMake(15, 25, 180, 25);
        descriptionTextField.frame = CGRectMake(210, 25, 240, 25);
        startTextField.frame = CGRectMake(475, 25, 100, 25);
        durationTextField.frame = CGRectMake(580, 25, 100, 25);
    } else {
        objLabelName.frame = CGRectMake(15,0, 150,20);
        objLabelDescription.frame = CGRectMake(210,0, 150,20);
        objLabelStart .frame = CGRectMake(475,0, 150,20);
        objLabelDuration.frame = CGRectMake(580,0, 150,20);
        
        nameTextField.frame = CGRectMake(15, 25, 180, 25);
        descriptionTextField.frame = CGRectMake(210, 25, 240, 25);
        startTextField.frame = CGRectMake(475, 25, 100, 25);
        durationTextField.frame = CGRectMake(580, 25, 100, 25);
    }
    
    UIView *closeButton = [self viewWithTag:1009];
    closeButton.frame = CGRectMake(710.0, 2.0, 25.0, 25.0);
    
}

- (void)fillContent  {
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1005];
    UITextField *descriptionTextField = (UITextField *)[self viewWithTag:1006];
    UITextField *startTextField = (UITextField *)[self viewWithTag:1007];
    UITextField *durationTextField = (UITextField *)[self viewWithTag:1008];
    
    nameTextField.text = self.marker.name;
    if (self.marker.description && (NSNull *)self.marker.description != [NSNull null]&& (![self.marker.description isEqualToString:@"(null)"])) {
        descriptionTextField.text = self.marker.description;
    }else{
         descriptionTextField.text =@"";
    }
    startTextField.text = self.marker.timeCode;
    durationTextField.text = self.marker.duration;
}

- (void)saveContent {
    
    
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1005];
    UITextField *descriptionTextField = (UITextField *)[self viewWithTag:1006];
    UITextField *startTextField = (UITextField *)[self viewWithTag:1007];
    UITextField *durationTextField = (UITextField *)[self viewWithTag:1008];
    
    self.marker.name = nameTextField.text;
    
    if ([descriptionTextField.text length]>0) {
        
        self.marker.description = descriptionTextField.text;
        
    }else{
        self.marker.description = @"";
    }
    
    self.marker.timeCode = startTextField.text;
    self.marker.duration = durationTextField.text;
    
    // To Modify the Shared Store used while saving.
    NSUInteger index = [[[AVVideoPlayer sharedInstance] videoMarkers] indexOfObject:self.marker];
    if (index != NSNotFound) {
        [[[AVVideoPlayer sharedInstance] videoMarkers] replaceObjectAtIndex:index withObject:self.marker];
    }
}

- (void)deleteButtonClicked:(id)sender  {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteMarker:)]) {
        [self.delegate deleteMarker:self.marker];
    }
}

#pragma mark - Override Setters

- (void)setCurrentOrientation:(UIInterfaceOrientation)currentOrientation    {
    _currentOrientation = currentOrientation;
    [self layoutIfNeeded];
}

- (void)setMarker:(Marker *)marker  {
    _marker = marker;
    [self fillContent];
}

#pragma mark - UITextField Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField   {
    [self saveContent];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self saveContent];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Event handlers

- (void)goToNextResponder {
//    MarkerCell *nextCell = nil;
//    nextCell = (PropertyEditorCell *) [self.parentTable cellForRowAtIndexPath:[NSIndexPath
//                                                                               indexPathForRow:self.indexPath.row + 1 inSection:self.indexPath.section]];
//    if (!nextCell) {
//        // May the Section Completes
//        nextCell = (PropertyEditorCell *) [self.parentTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0
//                                                                                                     inSection:self.indexPath.section + 1]];
//    }
//
//    
//    [nextCell becomeFirstResponder];
}

@end

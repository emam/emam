//
//  AssetViewController.m
//  eMAM
//
//  Created by APPLE on 24/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AssetViewController.h"
#import "AdvancedAssetService.h"
#import "AdvancedESendView.h"
#import "SelectionViewController.h"
#import "DeliveryViewController.h"

#import "AVVideoPlayer.h"
#import "MPMoviePlayer.h"
#import "SubclipView.h"
#import "MarkerView.h"
#import "Marker.h"
#import "UIColor+HTMLColors.h"


@interface AssetViewController ()


@property (nonatomic, strong) NSArray *historyInfo;

@property (nonatomic, strong) NSArray *commentInfo;

@property (nonatomic, strong) NSMutableDictionary *eSendInfo;

@property (nonatomic, strong) NSMutableArray *approvalUserInfo;
@property (nonatomic, strong) NSDictionary *selectedInternalUser;


@property (nonatomic, strong) NSDictionary *metadataInfo;
@property (nonatomic, strong) NSMutableDictionary *currentAssetTag;


@property (nonatomic, assign) SubclipView *subclipView;
@property (nonatomic, strong) NSMutableArray *subclipInfo;

@property (nonatomic, assign) MarkerView *markerView;
@property (nonatomic, strong) NSMutableArray *markerInfo;


@property (assign, nonatomic) CGFloat progress;

@end

@implementation AssetViewController

@synthesize asset = _asset;
@synthesize currentViewType = _currentViewType;
@synthesize currentAssetType = _currentAssetType;
@synthesize possibleAssetOptions = _possibleAssetOptions;


@synthesize previewInfo = _previewInfo;
@synthesize versionTitleBarItem = _versionTitleBarItem;
@synthesize currentVersionIndex = _currentVersionIndex;

@synthesize selectedButton = _selectedButton;
@synthesize currentView = _currentView;
@synthesize currentOrientation = _currentOrientation;


@synthesize embedInfo = _embedInfo;
@synthesize selectedValues = _selectedValues;
@synthesize selectRadioButton = _selectRadioButton;



@synthesize historyInfo = _historyInfo;

@synthesize commentInfo = _commentInfo;

@synthesize eSendInfo = _eSendInfo;

@synthesize approvalUserInfo = _approvalUserInfo;
@synthesize selectedInternalUser = _selectedInternalUser;
@synthesize approvalScrollView;

@synthesize metadataInfo = _metadataInfo;
@synthesize currentAssetTag = _currentAssetTag;

@synthesize subclipView = _subclipView;
@synthesize subclipInfo = _subclipInfo;

@synthesize markerView = _markerView;
@synthesize markerInfo = _markerInfo;









int previewStartedFlag;


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.navigationController.navigationBar.tintColor=UIColorFromRGB(0xFF6666);
    self.navigationController.navigationBar.barTintColor=UIColorFromRGB(0xECECEC);
    _PreviewView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _prevDetView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _prevDetScroll.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _embedDetScroll.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _historyScrollView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _commentsView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _esendView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _approvalView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _metadataView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _meta1View.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _meta2View.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _meta3View.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _meta4View.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _AVContainerView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    
    _prevDetScroll.contentSize=CGSizeMake(_prevDetScroll.frame.size.width, _prevDetScroll.frame.size.height);
    _embedDetScroll.contentSize=CGSizeMake(_embedDetScroll.frame.size.width, _embedDetScroll.frame.size.height);
    _historyScrollView.contentSize=CGSizeMake(_historyScrollView.frame.size.width, _historyScrollView.frame.size.height);
    [self.commentsScrollView contentSizeToFit];
    _esendScrollView.contentSize=CGSizeMake(_esendScrollView.frame.size.width, _esendScrollView.frame.size.height);
    [self.approvalScrollView contentSizeToFit];
    [self.meta1ScrollView contentSizeToFit];
    [self.meta4ScrollView contentSizeToFit];
    
    
    
    UINib *libCellNib = [UINib nibWithNibName:@"HistoryCell" bundle:nil];
    [_historyTable registerNib:libCellNib forCellReuseIdentifier:@"HistoryCell"];
    _historyTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    UINib *commentCellNib = [UINib nibWithNibName:@"CommentCell" bundle:nil];
    [_commentsTable registerNib:commentCellNib forCellReuseIdentifier:@"CommentCell"];
    _commentsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UINib *EmbeddedMetadataCellNib = [UINib nibWithNibName:@"EmbeddedMetadataCell" bundle:nil];
    [_meta2TableView registerNib:EmbeddedMetadataCellNib forCellReuseIdentifier:@"EmbeddedMetadataCell"];
    _meta2TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [_meta3TableView registerNib:EmbeddedMetadataCellNib forCellReuseIdentifier:@"EmbeddedMetadataCell"];
    _meta3TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    UINib *TagsMetadataCellNib = [UINib nibWithNibName:@"TagsMetadataCell" bundle:nil];
    [_meta4TableView registerNib:TagsMetadataCellNib forCellReuseIdentifier:@"TagsMetadataCell"];
    _meta4TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    NSLog(@"EMAM AssetViewerController : Current asset : %@",self.asset);
    [Datastore sharedStore].currentPreviewInfo = nil;
    
    previewStartedFlag=0;
    
    [self setAssetType];
    [self setAssetOptions];
    [self createAndDisplayTopTitleView];
    
    
}

- (void)optionButtonClick:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (!button.isSelected) {
        [self selectButton:button];
    }
}


- (IBAction)showDownloadsTapped:(id)sender {
    
    UIButton *button=(UIButton *)sender;
    
    //    if (popoverControllerDownloads) {
    //        [popoverControllerDownloads dismissPopoverAnimated:YES];
    //        popoverControllerDownloads = nil;
    //    }
    //    //PendingDownloads *pendDown = [[PendingDownloads alloc] init];
    //
    //    UIStoryboard *downloadStoryboard = [UIStoryboard storyboardWithName:@"DownloadManager" bundle:nil];
    //    UITabBarController *downloadManager=[downloadStoryboard instantiateInitialViewController];
    //
    //    UINavigationController *pendDownNavController = [[UINavigationController alloc] initWithRootViewController:downloadManager];
    //
    //    [self.navigationController pushViewController:downloadManager animated:YES];
    ////    pendDownNavController.navigationBarHidden=YES;
    ////    popoverControllerDownloads = [[UIPopoverController alloc] initWithContentViewController:pendDownNavController];
    ////    popoverControllerDownloads.backgroundColor=[UIColor lightGrayColor];
    ////    [popoverControllerDownloads presentPopoverFromRect:button.frame inView:self.navigationController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
    
}

- (IBAction)fetchForOfflineTapped:(id)sender {
    
    if ([[TWRDownloadManager sharedManager] fileExistsForUrl:[NSString stringWithFormat:@"%@",[self videoURL]]]) {
        _dwnldProgressView.progress = 1.0;
        _progressLbl.text=@"File feched for offline";
        _fetchForDownloadBtn.hidden=YES;
        NSLog(@"File:%@",[self videoURL]);
        NSLog(@"File Downloa Path:%@",[[TWRDownloadManager sharedManager] localPathForFile:[NSString stringWithFormat:@"%@",[self videoURL]]]);
    }else{
        
        [[TWRDownloadManager sharedManager] downloadFileForURL:[NSString stringWithFormat:@"%@",[self videoURL]] progressBlock:^(CGFloat progress) {
            self.progress = progress;
            _dwnldProgressView.progress = progress;
        } remainingTime:^(NSUInteger seconds) {
            _progressLbl.text = [NSString stringWithFormat:@"Progress: %.0f%% - ETA: %lu sec.", self.progress*100, (unsigned long)seconds];
        } completionBlock:^(BOOL completed) {
            NSLog(@"Download completed!");
            NSLog(@"File:%@",[self videoURL]);
            NSLog(@"File Downloa Path:%@",[[TWRDownloadManager sharedManager] localPathForFile:[NSString stringWithFormat:@"%@",[self videoURL]]]);
            _progressLbl.text=@"File feched for offline";
            _fetchForDownloadBtn.hidden=YES;
            
        } enableBackgroundMode:YES];
    }
}




- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    popoverController=nil;
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (popoverControllerDownloads) {
        
        [popoverControllerDownloads dismissPopoverAnimated:YES];
        
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    
    
    
    
}





#pragma mark -

- (void)setCurrentViewType:(AssetViewType)type   {
    [self deSelectPreviousViewType:_currentViewType];
    NSLog(@"EMAM AssetViewerController : Select View : %d",type);
    _currentViewType = type;
    switch (_currentViewType) {
        case Preview:   {
            
            if (previewStartedFlag==0) {
                [self initPreview];
            }
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=NO;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            
            
            
            
            break;
        }
        case MetaData:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=NO;
            [self initMetadata];
            
            break;
        }
        case Annotation:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=YES;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            
            break;
        }
        case Subclips:   {
            [self refreshAVPlayer];
            _AVContainerView.hidden=NO;
            
            _PreviewView.hidden=YES;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            
            [self initSubclip];
            
            
            break;
        }
        case Markers:   {
            [self refreshAVPlayer];
            _AVContainerView.hidden=NO;
            
            _PreviewView.hidden=YES;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            
            [self initMarkers];
            
            
            break;
        }
        case ESend:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=NO;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            [self initESend];
            
            break;
        }
        case Approval:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=NO;
            _metadataView.hidden=YES;
            [self initApproval];
            
            break;
        }
        case Comment:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=NO;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            [self initComments];
            
            break;
        }
        case History:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _embedView.hidden=YES;
            _historyView.hidden=NO;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            [self initHistory];
            
            break;
        }
        case Embed:   {
            _AVContainerView.hidden=YES;
            
            _PreviewView.hidden=NO;
            _prevDetView.hidden=YES;
            _historyView.hidden=YES;
            _commentsView.hidden=YES;
            _esendView.hidden=YES;
            _approvalView.hidden=YES;
            _metadataView.hidden=YES;
            [self initEmbed];
            
            break;
        }
        default:
            break;
    }
    
}


- (void)deSelectPreviousViewType:(AssetViewType)viewType {
    NSLog(@"EMAM AssetViewerController : De-Select View : %d",viewType);
    switch (viewType) {
        case Preview:
            [self hidePreview];
            break;
        case MetaData:
            
            [self hideMetadata];
            
            break;
        case Annotation:
            
            break;
        case Subclips:
            [self hideSubclip];
            break;
        case Markers:
            [self hideMarkers];
            break;
        case ESend:
            [self hideESend];
            
            break;
        case Approval:
            [self hideApproval];
            
            break;
        case Comment:
            [self hideComments];
            
            break;
        case History:
            [self hideHistory];
            break;
        case Embed:
            [self hideEmbed];
            
            break;
            
        default:
            break;
    }
}


- (void)setCurrentAssetType:(AssetType)currentAssetType {
    NSLog(@"EMAM AssetViewerController : Asset Type : %d",currentAssetType);
    _currentAssetType = currentAssetType;
}

#pragma mark - - Preview - -

- (void)initPreview {
    _PreviewView.hidden=NO;
    _prevDetView.hidden=NO;
    [self getAllAssetVersions];
    [self setPreviewLabels:self.asset];
    
    
}

- (void)hidePreview {
    
    _PreviewView.hidden=YES;
    _prevDetView.hidden=YES;
    
}





#pragma mark - - Embed - -



- (UILabel *)labelWithFont:(UIFont *)font    {
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, 100, 20)];
    [label setFont:font];
    [label setNumberOfLines:1];
    [label setLineBreakMode:UILineBreakModeTailTruncation];
    label.textColor=[UIColor whiteColor];
    [label setBackgroundColor:[UIColor clearColor]];
    
    return label;
}



- (void)getEmbedCode:(NSDictionary *)dictionary {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading ";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
        NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
        
        NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [requestDictionary setValue:[NSString stringWithFormat:@"%@/%@_2.flv",virtualPath,uuid] forKey:@"VirtualPath"];
        [requestDictionary setValue:uuid forKey:@"UUID"];
        [requestDictionary setValue:[NSString stringWithFormat:@"%@/%@_2.flv",virtualPath,uuid] forKey:@"LowResPath"];
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService generatePlayerObject:dictionary  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSString *responseString = [[[dictionary objectForKey:@"GeneratePlayerObjResponse"] objectForKey:@"GeneratePlayerObjResult"] objectForKey:@"text"];
            NSLog(@"Get Embed Code Response : %@",dictionary);
            
            [self setResponse:responseString];
            
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}

- (void)getShareLink:(NSDictionary *)dictionary {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading ";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
        NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
        
        NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [requestDictionary setValue:[NSString stringWithFormat:@"%@/%@_2.flv",virtualPath,uuid] forKey:@"VirtualPath"];
        [requestDictionary setValue:uuid forKey:@"UUID"];
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService inserteMAMAssetPlayer:requestDictionary performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSString *responseString = [[[dictionary objectForKey:@"InserteMAMAssetPlayerResponse"] objectForKey:@"InserteMAMAssetPlayerResult"] objectForKey:@"text"];
            
            NSString *gatewayURL = [Utilities serviceGateway];
            gatewayURL = [gatewayURL stringByReplacingOccurrencesOfString:@"emamservice.asmx" withString:@"eMAMPlayer.htm?UrlIdentifier="];
            gatewayURL = [NSString stringWithFormat:@"%@%@",gatewayURL,responseString];
            
            NSLog(@"Get Share Link Response : %@",gatewayURL);
            [self setResponse:gatewayURL];
            
            
            
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}

- (void)setResponse:(NSString *)response    {
    
    UITextView *textView = (UITextView *)[_embedDetBtnsScroll viewWithTag:FinalTextViewTag];
    textView.text = response;
    textView.userInteractionEnabled=YES;
    textView.editable=NO;
}


- (void)skinRadioButtonClicked:(id)sender   {
    [self.selectRadioButton setSelected:NO];
    self.selectRadioButton = sender;
    [self.selectRadioButton setSelected:YES];
    
    NSDictionary *skin = [self.embedInfo objectAtIndex:self.selectRadioButton.tag];
    [self.selectedValues setValue:[[skin objectForKey:@"color_code"] objectForKey:@"text"] forKey:@"ColorCode"];
    [self.selectedValues setValue:[[skin objectForKey:@"name"] objectForKey:@"text"] forKey:@"SkinName"];
    [self.selectedValues setValue:[[skin objectForKey:@"id"] objectForKey:@"text"] forKey:@"SkinId"];
}

- (void)createSkinsView {
    
    CGFloat xPosition = 20;
    CGFloat yPosition = 20;
    
    UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"vc_skin" ofType:@"png"]];
    
    for (int i = 0; i < [self.embedInfo count]; i++) {
        NSDictionary *skin = [self.embedInfo objectAtIndex:i];
        
        xPosition = 20;
        
        UIButton *radioButton = [Utilities radioButton];
        radioButton.tag = i;
        [radioButton setSelected:NO];
        [radioButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
        [_embedDetSkinScroll addSubview:radioButton];
        [radioButton addTarget:self action:@selector(skinRadioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        xPosition += 40;
        
        UILabel *tag = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
        [tag setFrame:CGRectMake(xPosition, yPosition, 70, 20)];
        [tag setText:[[skin  objectForKey:@"name"] objectForKey:@"text"]];
        [_embedDetSkinScroll addSubview:tag];
        
        xPosition += 80;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        [imageView setFrame:CGRectMake(xPosition, yPosition, 150, 20)];
        NSString *colorCode = [[skin objectForKey:@"color_code"] objectForKey:@"text"];
        UIColor * myColor = [UIColor colorWithCSS :[@"#" stringByAppendingString:colorCode]];
        NSLog(@"%@",myColor);
        [imageView setBackgroundColor:myColor];
        //        [imageView setBackgroundColor:[UIColor colorWithHexValue:myColor]];
        [_embedDetSkinScroll addSubview:imageView];
        
        yPosition += 40;
    }
    
    [_embedDetSkinScroll setContentSize:CGSizeMake(_embedDetSkinScroll.frame.size.width, yPosition)];
    
    
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (void)getAllEmbeds {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading Embed Data";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService geteMAMPlayeSkinsWithPerformRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"GeteMAMPlayerSkinsResponse"] objectForKey:@"GeteMAMPlayerSkinsResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
            NSLog(@"GetPlayer Skins Response : %@",responseDictionary);
            
            self.embedInfo = [NSArray array];
            id embedInfo = [responseDictionary objectForKey:@"Table"];
            if (embedInfo) {
                if ([embedInfo isKindOfClass:[NSDictionary class]]) {
                    self.embedInfo = [NSArray arrayWithObjects:embedInfo, nil];
                } else {
                    self.embedInfo = embedInfo;
                }
            }
            
            [self createSkinsView];
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
}

- (UIViewController *)playerSizeOptionSelectController  {
    UIViewController *viewController = [[UIViewController alloc] init];
    
    int X = 8;
    int Y = 10;
    
    NSArray *array = [NSArray arrayWithObjects:@"SD(4:3)",@"HD(16:9)",@"Custom", nil];
    
    for(int i=0;i<[array count]; i++)   {
        UIButton *objButton = [[UIButton alloc] init];
        [objButton setTag:i];
        
        [objButton setFrame:CGRectMake(X, Y, 180, 30)];
        [objButton setBackgroundImage:[Utilities buttonBackgroundImage] forState:UIControlStateNormal];
        
        [objButton setTitle:[array objectAtIndex:i] forState:UIControlStateNormal];
        [objButton addTarget:self action:@selector(clickedPlayerSizeSelectButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewController.view addSubview:objButton];
        objButton = nil;
        
        Y = Y + 40;
    }
    
    viewController.preferredContentSize = CGSizeMake(200, 140);
    
    return viewController;
}


- (void)playerSizeButtonClicked:(id)sender  {
    UIButton *button = (UIButton *)sender;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:[self playerSizeOptionSelectController]];
    
    [popoverController presentPopoverFromRect:button.frame inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)clickedPlayerSizeSelectButton:(id)sender  {
    UIButton *button = (UIButton *)sender;
    
    NSArray *sub=[[sender superview] subviews];
    
    for (UIButton* btn in sub) {
        
        btn.backgroundColor=[UIColor clearColor];
        
        
    }
    
    
    button.backgroundColor=[UIColor redColor];
    
    
    NSLog(@"EMAM Embed View : Select Player Size Button : %d",button.tag);
    
    
    
    
    UIView *backgroundView = [_embedDetBtnsScroll viewWithTag:CustomPlayerSizeInputViewTag];
    switch (button.tag) {
        case 0: {
            backgroundView.hidden =  YES;
            [self.selectedValues setValue:[NSNumber numberWithInt:480] forKey:@"PlayerWidth"];
            [self.selectedValues setValue:[NSNumber numberWithInt:360] forKey:@"PlayerHeight"];
            break;
        }
        case 1: {
            backgroundView.hidden =  YES;
            [self.selectedValues setValue:[NSNumber numberWithInt:640] forKey:@"PlayerWidth"];
            [self.selectedValues setValue:[NSNumber numberWithInt:360] forKey:@"PlayerHeight"];
            break;
        }
        case 2: {
            backgroundView.hidden =  NO;
            
            break;
        }
            
        default:
            break;
    }
}

- (void)selectCheckBoxButtonClicked:(id)sender  {
    UIButton *button = (UIButton *)sender;
    [button setSelected:!button.selected];
    
    switch (button.tag) {
        case IncludeThumbnailCheckBoxTag:   {
            [self.selectedValues setValue:[NSNumber numberWithBool:button.selected] forKey:@"IncludeThumpnail"];
            break;
        }
        case SkinAutoHideCheckBoxTag:   {
            [self.selectedValues setValue:[NSNumber numberWithBool:button.selected] forKey:@"SkinAutoHide"];
            break;
        }
        case AutoPlayCheckBoxTag:   {
            [self.selectedValues setValue:[NSNumber numberWithBool:button.selected] forKey:@"AutoPlay"];
            break;
        }
        case ClosedCaptionCheckBoxTag:   {
            [self.selectedValues setValue:[NSNumber numberWithBool:button.selected] forKey:@"ClosedCaption"];
            break;
        }
        case FullScreenCheckBoxTag:   {
            [self.selectedValues setValue:[NSNumber numberWithBool:button.selected] forKey:@"FullScreen"];
            break;
        }
        default:
            break;
    }
}

- (void)createLayoutView    {
    
    
    CGFloat xPosition = 20;
    CGFloat yPosition = 20;
    
    
    UIButton *playerSizeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    playerSizeButton.tag = 999;
    
    [playerSizeButton setTitle:@"Select Player Size" forState:UIControlStateNormal];
    playerSizeButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [playerSizeButton addTarget:self action:@selector(playerSizeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [playerSizeButton setFrame:CGRectMake(xPosition + 60, yPosition - 4, 200, 30)];
    playerSizeButton.backgroundColor = [UIColor clearColor];
    [playerSizeButton setTitleColor:[UIColor whiteColor]
                           forState:UIControlStateNormal];
    [_embedDetBtnsScroll addSubview:playerSizeButton];
    
    
    yPosition += 40;
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(xPosition, yPosition, _embedDetBtnsScroll.frame.size.width, 20)];
    backgroundView.hidden = YES;
    [backgroundView setBackgroundColor:[UIColor clearColor]];
    backgroundView.tag  = CustomPlayerSizeInputViewTag;
    
    UILabel *widthLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [widthLabel setFrame:CGRectMake(10, 0, 50, 20)];
    [widthLabel setText:@"Width"];
    [backgroundView addSubview:widthLabel];
    
    UITextField *widthTextField = [[UITextField alloc] init];
    widthTextField.tag = CustomPlayerWidthInputViewTag;
    widthTextField.delegate = (id)self;
    [widthTextField setFont:[UIFont systemFontOfSize:12.0]];
    [widthTextField setFrame:CGRectMake(CGRectGetMaxX(widthLabel.frame), 0, 80, 20)];
    [widthTextField setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:widthTextField];
    widthTextField.layer.borderWidth = 1.0;
    widthTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    UILabel *heightLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [heightLabel setFrame:CGRectMake(CGRectGetMaxX(widthTextField.frame) + 20, 0, 50, 20)];
    [heightLabel setText:@"Height"];
    [backgroundView addSubview:heightLabel];
    
    UITextField *heightTextField = [[UITextField alloc] init];
    heightTextField.tag = CustomPlayerHeightInputViewTag;
    heightTextField.delegate = (id)self;
    [heightTextField setFont:[UIFont systemFontOfSize:12.0]];
    [heightTextField setFrame:CGRectMake(CGRectGetMaxX(heightLabel.frame), 0, 80, 20)];
    [heightTextField setBackgroundColor:[UIColor whiteColor]];
    [backgroundView addSubview:heightTextField];
    heightTextField.layer.borderWidth = 1.0;
    heightTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    [_embedDetBtnsScroll addSubview:backgroundView];
    
    yPosition += 40;
    
    UIButton *includeThumbnailSelectButton = [Utilities checkBox];
    includeThumbnailSelectButton.tag = IncludeThumbnailCheckBoxTag;
    [includeThumbnailSelectButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
    includeThumbnailSelectButton.backgroundColor = [UIColor clearColor];
    [includeThumbnailSelectButton addTarget:self action:@selector(selectCheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_embedDetBtnsScroll addSubview:includeThumbnailSelectButton];
    
    UILabel *includeThumbnailLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [includeThumbnailLabel setFrame:CGRectMake(xPosition + 40, yPosition, 150, 20)];
    [includeThumbnailLabel setText:@"Include Thumbnail"];
    //    [scrollView addSubview:includeThumbnailLabel];
    //
    //    yPosition += 30;
    
    UIButton *skinAutoHideSelectButton = [Utilities checkBox];
    skinAutoHideSelectButton.tag = SkinAutoHideCheckBoxTag;
    [skinAutoHideSelectButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
    skinAutoHideSelectButton.backgroundColor = [UIColor clearColor];
    [skinAutoHideSelectButton addTarget:self action:@selector(selectCheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_embedDetBtnsScroll addSubview:skinAutoHideSelectButton];
    
    UILabel *skinAutoHideLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [skinAutoHideLabel setFrame:CGRectMake(xPosition + 40, yPosition, 150, 20)];
    [skinAutoHideLabel setText:@"Skin AutoHide"];
    [_embedDetBtnsScroll addSubview:skinAutoHideLabel];
    
    yPosition += 30;
    
    UIButton *autoPlaySelectButton = [Utilities checkBox];
    autoPlaySelectButton.tag = AutoPlayCheckBoxTag;
    [autoPlaySelectButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
    autoPlaySelectButton.backgroundColor = [UIColor clearColor];
    [autoPlaySelectButton addTarget:self action:@selector(selectCheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_embedDetBtnsScroll addSubview:autoPlaySelectButton];
    
    UILabel *autoPlayLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [autoPlayLabel setFrame:CGRectMake(xPosition + 40, yPosition, 150, 20)];
    [autoPlayLabel setText:@"Auto Play"];
    [_embedDetBtnsScroll addSubview:autoPlayLabel];
    
    yPosition += 30;
    
    UIButton *closedCaptionSelectButton = [Utilities checkBox];
    closedCaptionSelectButton.tag = ClosedCaptionCheckBoxTag;
    [closedCaptionSelectButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
    closedCaptionSelectButton.backgroundColor = [UIColor clearColor];
    [closedCaptionSelectButton addTarget:self action:@selector(selectCheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_embedDetBtnsScroll addSubview:closedCaptionSelectButton];
    
    UILabel *closedCaptionLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [closedCaptionLabel setFrame:CGRectMake(xPosition + 40, yPosition, 150, 20)];
    [closedCaptionLabel setText:@"Closed Caption"];
    [_embedDetBtnsScroll addSubview:closedCaptionLabel];
    
    yPosition += 30;
    
    UIButton *fullScreenSelectButton = [Utilities checkBox];
    fullScreenSelectButton.tag = FullScreenCheckBoxTag;
    [fullScreenSelectButton setFrame:CGRectMake(xPosition, yPosition - 4, 30, 30)];
    fullScreenSelectButton.backgroundColor = [UIColor clearColor];
    [fullScreenSelectButton addTarget:self action:@selector(selectCheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_embedDetBtnsScroll addSubview:fullScreenSelectButton];
    
    UILabel *fullScreenLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [fullScreenLabel setFrame:CGRectMake(xPosition + 40, yPosition, 150, 20)];
    [fullScreenLabel setText:@"Full Screen"];
    [_embedDetBtnsScroll addSubview:fullScreenLabel];
    
    yPosition += 30;
    
    UITextView *textView = [[UITextView alloc] init];
    textView.editable=NO;
    textView.delegate = (id)self;
    [textView setFrame:CGRectMake(xPosition, yPosition, 290, 100)];
    textView.tag = FinalTextViewTag;
    textView.font = [UIFont systemFontOfSize:12.0];
    textView.backgroundColor = [UIColor whiteColor];
    textView.layer.borderWidth = 1.0;
    textView.layer.borderColor = [UIColor grayColor].CGColor;
    [_embedDetBtnsScroll addSubview:textView];
    
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    /*  limit to only numeric characters  */
    BOOL success = NO;
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            success = YES;
            break;
        }
    }
    
    if (!success && [string length] > 0) {
        return NO;
    }
    
    /*  limit the users input to only 9 characters  */
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    success = (newLength > 4) ? NO : YES;
    
    NSString *textFieldString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (success) {
        
        if (textField.tag == CustomPlayerWidthInputViewTag) {
            int width = [textFieldString intValue];
            UITextField *heightTextField = (UITextField *)[_embedDetBtnsScroll viewWithTag:CustomPlayerHeightInputViewTag];
            int height = width * 9 / 16;
            heightTextField.text = [NSString stringWithFormat:@"%.0f",round(height)];
            
            [self.selectedValues setValue:[NSNumber numberWithInt:width] forKey:@"PlayerWidth"];
            [self.selectedValues setValue:[NSNumber numberWithInt:[heightTextField.text intValue]] forKey:@"PlayerHeight"];
        } else {
            int height = [textFieldString intValue];
            UITextField *widthTextField = (UITextField *)[_embedDetBtnsScroll viewWithTag:CustomPlayerWidthInputViewTag];
            int width = height * 16 / 9;
            widthTextField.text = [NSString stringWithFormat:@"%.0f",round(width)];
            
            [self.selectedValues setValue:[NSNumber numberWithInt:[widthTextField.text intValue]] forKey:@"PlayerWidth"];
            [self.selectedValues setValue:[NSNumber numberWithInt:height] forKey:@"PlayerHeight"];
        }
    }
    
    return success;
}


- (void)initEmbed {
    
    _embedView.hidden=NO;
    
    if (self.currentAssetType != Video) {
        NSLog(@"EMAM AssetViewerController : Embed is possible in Video Assets...");
        return;
    }
    
    if (!self.embedInfo) {
        [self getAllEmbeds];
        
    }
    
    [self createLayoutView];
    
    
}

- (void)hideEmbed {
    
    _embedView.hidden=YES;
    
    
    
}

- (void) fromDatePickerAction:(id)sender{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    _externalApprovalExpiryDate.text = [format stringFromDate:approvalDatePicker.date];
    
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField==_externalApprovalExpiryDate) {
        
        approvalDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        approvalDatePicker.datePickerMode = UIDatePickerModeDate;
        [approvalDatePicker addTarget:self action:@selector(fromDatePickerAction:) forControlEvents:UIControlEventValueChanged];
        textField.inputView = approvalDatePicker;
        approvalDatePicker.minimumDate=[NSDate date];
        
        return YES;
        
    }
    
    return NO;
}

#pragma mark - AVPlayer -


- (void)initAVPlayer {
    
    
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        
        avVideoPlayer.frame = CGRectMake(0, 0, 768, 500);
    } else {
        
        avVideoPlayer.frame = CGRectMake(0, 0, 1024, 500);
    }
    
    avVideoPlayer.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    
    // to ensure addSubview once.
    if (!avVideoPlayer.superview) {
        NSURL *movieURL = [self videoURL];
        
        if (movieURL) {
            [avVideoPlayer setURL:movieURL];
        }
        avVideoPlayer.hidden = YES;
        [_AVContainerView addSubview:avVideoPlayer];
    }
    
}

- (void)hideAVPlayer {
    _PreviewView.hidden=NO;
    _prevDetView.hidden=NO;
    
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer pause:nil];
    [avVideoPlayer removeAllMarkers];
    [avVideoPlayer removeAllSubclips];
    avVideoPlayer.hidden = YES;
    
    
}
- (void)showAVPlayer    {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    avVideoPlayer.hidden = NO;
}

- (void)destructAVPlayer  {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer pause:nil];
    if (avVideoPlayer.superview) {
        [avVideoPlayer removeFromSuperview];
    }
    [AVVideoPlayer destruct];
}


- (void)refreshAVPlayer {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer removeAllMarkers];
    [avVideoPlayer removeAllSubclips];
    avVideoPlayer.videoMarkers = nil;
    avVideoPlayer.videoSubclips = nil;
    avVideoPlayer.needShowMarker = NO;
    avVideoPlayer.needShowSubclip = NO;
    
    NSURL *movieURL = [self videoURL];
    
    if (movieURL) {
        [avVideoPlayer setURL:movieURL];
    }
}


#pragma mark - - Subclips - -

- (void)initSubclip {
    if (self.subclipView) {
        AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
        [avVideoPlayer performSelector:@selector(fullScreenReset:) withObject:nil];
        [self showSubclip];
        return;
    }
    
    [self initAVPlayer];
    
    SubclipView *subclipView = [[SubclipView alloc] init];
    subclipView.delegate = (id)self;
    subclipView.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    self.subclipView = subclipView;
    [self showSubclip];
}

- (void)showSubclip {
    [[AVVideoPlayer sharedInstance] setNeedShowSubclip:YES];
    [self showAVPlayer];
    [_AVContainerView addSubview:self.subclipView];
    [self refreshSubclipView];
}

- (void)hideSubclip {
    
    if (self.subclipView) {
        self.subclipView = nil;
    }
    
    [[AVVideoPlayer sharedInstance] setNeedShowSubclip:NO];
    [self hideAVPlayer];
    
    [self.subclipView removeFromSuperview];
}

- (void)refreshSubclipView   {
    // empty marker info - call webservice.
    if (!self.subclipInfo) {
        [self getAllSubclips];
        return;
    }
    // setting subclip info internally calls refresh view.
    [self.subclipView setSubclipInfo:self.subclipInfo];
}


- (void)getAllSubclips {
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService getAllSubClipsForAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            
            
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSDictionary *responseDictionary = [[[[[dictionary objectForKey:@"LoadAllMarkerTimeCodesResponse"] objectForKey:@"LoadAllMarkerTimeCodesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
        NSLog(@"Get AllSubclips For Asset Response : %@",responseDictionary);
        
        
        if ([dictionary count] == 0) {
            [Utilities showAlert:APPNAME message:@"Authentication Expired" delegateObject:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Authentication Expired" object:nil];
        }else{
            
            [self processGetAllSubclipResponse:responseDictionary];
            [self refreshSubclipView];
        }
    }];
}

- (void)processGetAllSubclipResponse:(id)response    {
    [[AVVideoPlayer sharedInstance] removeAllSubclips];
    if ([response isKindOfClass:[NSDictionary class]]) {
        response = [NSArray arrayWithObjects:response, nil];
    }
    NSMutableArray *array = [NSMutableArray array];
    if ([response isKindOfClass:[NSArray class]]) {
        for (NSDictionary *dictionary in response) {
            Subclip *subclip = [[Subclip alloc] initWithDictionary:dictionary];
            [array addObject:subclip];
        }
    }
    
    self.subclipInfo = array;
}

- (void)saveAllSubclips:(NSArray *)subclips {
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService saveAllSubclips:subclips forAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        if (error) {
            
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id response = [[[[[dictionary objectForKey:@"SaveMarkersResponse"] objectForKey:@"SaveMarkersResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"DocumentElement"] objectForKey:@"UpdatedSubClips"];
        NSLog(@"Save AllSubclips For Asset Response : %@",response);
        
        if ([dictionary count] == 0) {
            [Utilities showAlert:APPNAME message:@"Authentication Expired" delegateObject:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Authentication Expired" object:nil];
        }else{
            
            [self getAllSubclips];
        }
    }];
}

- (void)deleteAllSubclips:(NSArray *)subclips {
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    NSLog(@"subclips : %@",subclips);
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService deleteSubclips:subclips forAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearCheckMarks" object:nil];
        //id response = [[[[dictionary objectForKey:@"LoadAllMarkerTimeCodesResponse"] objectForKey:@"LoadAllMarkerTimeCodesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
        //NSLog(@"Delete Subclips For Asset Response : %@",dictionary);
        [self getAllSubclips];
    }];
}

#pragma mark - SubclipDelegate

- (void)saveSubclips:(NSArray *)subclips    {
    
    [self saveAllSubclips:subclips];
}

- (void)deleteSubclips:(NSArray *)subclips    {
    NSMutableArray *deleteSubClips = [NSMutableArray array];
    for (Subclip *subclip in subclips) {
        [subclip.subclipView removeFromSuperview];
        if ([subclip.subclipId isEqualToString:@"-1"]) {
            [[[AVVideoPlayer sharedInstance] videoSubclips] removeObject:subclip];
            [self.subclipView refreshView];
        } else  {
            [deleteSubClips addObject:subclip];
        }
    }
    if ([deleteSubClips count] > 0) {
        [self deleteAllSubclips:deleteSubClips];
    }
}

- (void)showDeliveryProfilesForSubclips:(NSArray *)subclips {
    DeliveryViewController *deliveryViewController = [[DeliveryViewController alloc] init];
    deliveryViewController.isEDL = NO;
    deliveryViewController.deliveryContents = subclips;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:deliveryViewController];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentModalViewController:navigationController animated:YES];
    deliveryViewController = nil;
}

- (void)showSendEDLProfilesForSubclips:(NSArray *)subclips  {
    DeliveryViewController *deliveryViewController = [[DeliveryViewController alloc] init];
    deliveryViewController.isEDL = YES;
    deliveryViewController.deliveryContents = subclips;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:deliveryViewController];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentModalViewController:navigationController animated:YES];
    deliveryViewController = nil;
}


#pragma mark - - Markers - -



- (void)showMarker {
    
    [[AVVideoPlayer sharedInstance] setNeedShowMarker:YES];
    [self showAVPlayer];
    [_AVContainerView addSubview:self.markerView];
    [self refreshMarkerView];
}


- (void)initMarkers {
    
    [self.moviePlayer pause];
    
    
    if (self.markerView) {
        
        AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
        [avVideoPlayer performSelector:@selector(fullScreenReset:) withObject:nil];
        [self showMarker];
        return;
    }
    
    [self initAVPlayer];
    
    MarkerView *markerView = [[MarkerView alloc] init];
    markerView.delegate = (id)self;
    markerView.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    self.markerView = markerView;
    
    [self showMarker];
    
    
    
}

- (void)hideMarkers {
    
    if (self.markerView) {
        self.markerView = nil;
    }
    
    [[AVVideoPlayer sharedInstance] setNeedShowMarker:NO];
    [self hideAVPlayer];
    [self.markerView removeFromSuperview];
    
}

- (void)refreshMarkerView   {
    // empty marker info - call webservice.
    if (!self.markerInfo) {
        [self getAllMarkers];
        return;
    }
    // setting marker info internally calls refresh view.
    [self.markerView setMarkerInfo:self.markerInfo];
}


- (void)getAllMarkers {
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService getAllMarkersForAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id response = [[[[[dictionary objectForKey:@"LoadAllMarkerTimeCodesResponse"] objectForKey:@"LoadAllMarkerTimeCodesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
        //NSLog(@"Get AllMarkers For Asset Response : %@",response);
        
        [self processGetAllMarkerResponse:response];
        
        [self refreshMarkerView];
    }];
}


- (void)processGetAllMarkerResponse:(id)response    {
    [[AVVideoPlayer sharedInstance] removeAllMarkers];
    if ([response isKindOfClass:[NSDictionary class]]) {
        response = [NSArray arrayWithObjects:response, nil];
    }
    NSMutableArray *array = [NSMutableArray array];
    if ([response isKindOfClass:[NSArray class]]) {
        for (NSDictionary *dictionary in response) {
            Marker *marker = [[Marker alloc] initWithDictionary:dictionary];
            [array addObject:marker];
        }
    }
    
    self.markerInfo = array;
}

- (void)saveAllMarkers:(NSArray *)markers {
    
    
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService saveAllMarkers:markers forAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id response = [[[[[dictionary objectForKey:@"SaveMarkersResponse"] objectForKey:@"SaveMarkersResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"DocumentElement"] objectForKey:@"UpdatedMarkers"];
        NSLog(@"Save AllMarkers For Asset Response : %@",response);
        [self getAllMarkers];
    }];
}

- (void)deleteAllMarkers:(NSArray *)markers {
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = 0;
    if ([self.previewInfo count] > self.currentVersionIndex) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:self.currentVersionIndex];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService deleteMarkers:markers forAssetId:assetId versionId:versionId   performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        if (error) {
            //[Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            // return;
        }
        
        //        id response = [[[[dictionary objectForKey:@"LoadAllMarkerTimeCodesResponse"] objectForKey:@"LoadAllMarkerTimeCodesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
        //NSLog(@"Delete AllMarkers For Asset Response : %@",dictionary);
        [self getAllMarkers];
    }];
}

#pragma mark - MarkerDelegate

- (void)saveMarkers:(NSArray *)markers  {
    
    
    
    
    [self saveAllMarkers:markers];
}

- (void)deleteMarker:(Marker *)marker   {
    
    [marker.markerControl removeFromSuperview];
    if ([marker.markerId isEqualToString:@"-1"]) {
        [[[AVVideoPlayer sharedInstance] videoMarkers] removeObject:marker];
        [self.markerView refreshView];
    } else  {
        [self deleteAllMarkers:[NSArray arrayWithObjects:marker, nil]];
    }
}



#pragma mark - - Metadata - -

- (void)refreshMetadataView   {
    [self.metadataInfo setValue:[Datastore sharedStore].currentPreviewInfo forKey:@"AssetInfo"];
    
    NSDictionary *assetInfo = [self.metadataInfo objectForKey:@"AssetInfo"];
    
    [_meta4TableView reloadData];
    NSLog(@"assetInfo : %@",assetInfo);
    
    [_meta1Title setText:[[assetInfo objectForKey:@"TITLE"] objectForKey:@"text"]];
    [_meta1Descri setText:[[assetInfo objectForKey:@"ASSET_DESC"] objectForKey:@"text"]];
    [_meta1Autor setText:[[assetInfo objectForKey:@"AUTHOR"] objectForKey:@"text"]];
    [_meta1UploadOn setText:[[assetInfo objectForKey:@"INGESTED_ON"] objectForKey:@"text"]];
    //[_meta1UploadBy setText:[[assetInfo objectForKey:@"Ingested_By"] objectForKey:@"text"]];
    
    
    
}


- (IBAction)meta4ClearTapped:(id)sender {
    
    _meta4TagTxtView.text=@"";
    _meta4PrivateBtn.selected=YES;
    _meta4PublicBtn.selected=NO;
    [_meta4Add setTitle:@"Edit" forState:UIControlStateNormal];
    
}

- (void)resetAssetTagView   {
    _meta4TagTxtView.text=@"";
    _meta4PrivateBtn.selected=YES;
    _meta4PublicBtn.selected=NO;
    [_meta4Add setTitle:@"Add" forState:UIControlStateNormal];
}


- (IBAction)meta4EditTapped:(id)sender {
    
    
    
    BOOL success = YES;
    [self assertNonEmptyStringForTextField:_meta4TagTxtView success:&success];
    if (success) {
        
        if ([_meta4Add.titleLabel.text isEqualToString:@"Add"]) {
            self.currentAssetTag=nil;
        }
        
        if (!self.currentAssetTag) {
            self.currentAssetTag = [NSMutableDictionary dictionary];
        }
        [self.currentAssetTag setValue:_meta4TagTxtView.text forKey:@"TagValue"];
        
        [self.currentAssetTag setValue:[NSNumber numberWithBool:_meta4PublicBtn.selected] forKey:@"TagType"];
        
        
        
        [self resetAssetTagView];
        
        NSLog(@"CurrentAssetTags2:%@",self.currentAssetTag);
        
        [self saveAssetMetadataTag:self.currentAssetTag];
        
    }
    
}

- (IBAction)meta4PrivateTapped:(id)sender {
    
    _meta4PrivateBtn.selected=YES;
    _meta4PublicBtn.selected=NO;
}

- (IBAction)meta4PublicTapped:(id)sender {
    
    _meta4PrivateBtn.selected=NO;
    _meta4PublicBtn.selected=YES;
}


- (IBAction)deleteTag:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    NSLog(@"Del Tag Asset : %d",(btn.tag-1000));
    
    self.currentAssetTag = nil;
    NSArray *array = [self.metadataInfo objectForKey:@"AssetTags"];
    if (array && [array isKindOfClass:[NSArray class]]) {
        if ([array count] > (btn.tag-1000)) {
            NSDictionary *dictionary = [array objectAtIndex:(btn.tag-1000)];
            self.currentAssetTag = [NSMutableDictionary dictionary];
            [self.currentAssetTag setObject:[[dictionary objectForKey:@"Tag_id"] objectForKey:@"text"] forKey:@"TagId"];
            
            [self deleteAssetMetadataTag:self.currentAssetTag];
            
            
        }
    }
}

- (IBAction)editTag:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    NSLog(@"Edit Tag Asset : %d",(btn.tag));
    
    NSArray *array = [self.metadataInfo objectForKey:@"AssetTags"];
    if (array && [array isKindOfClass:[NSArray class]]) {
        if ([array count] > btn.tag) {
            NSDictionary *dictionary = [array objectAtIndex:btn.tag];
            self.currentAssetTag = [NSMutableDictionary dictionary];
            [self.currentAssetTag setObject:[[dictionary objectForKey:@"Tag_id"] objectForKey:@"text"] forKey:@"TagId"];
            
            [self fillEditTag:dictionary];
            
            
        }
    }
    
}


- (void)fillEditTag:(NSDictionary *)dictionary  {
    
    [_meta4Add setTitle:@"Edit" forState:UIControlStateNormal];
    BOOL isPublic = [[[dictionary objectForKey:@"tag_type"] objectForKey:@"text"] boolValue];
    _meta4PrivateBtn.selected = !isPublic;
    _meta4PublicBtn.selected = isPublic;
    _meta4TagTxtView.text = [[dictionary objectForKey:@"tag_value"] objectForKey:@"text"];
}





- (void)getAllMetadatas {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Fetch Asset Metadatas...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        int versionId = 0;
        if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
            NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
            versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
        }
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService getAllMetadataForAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSLog(@"Get AllMetadatas For Asset Response : %@",dictionary);
            
            self.metadataInfo = [NSMutableDictionary dictionaryWithDictionary:dictionary];
            if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
                NSDictionary *versionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
                [self.metadataInfo setValue:versionInfo forKey:@"AssetInfo"];
            }
            
            [self refreshMetadataView];
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
}

- (void)getAssetMetadataInfo {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Fetch Asset Metadata Info...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService getVersionInfoForAssetId:assetId versionId:0   performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"GetAssetVersionInfoResponse"] objectForKey:@"GetAssetVersionInfoResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
            
            id verionInfo = [responseDictionary objectForKey:@"Table"];
            if ([verionInfo isKindOfClass:[NSDictionary class]]) {
                self.previewInfo = [NSArray arrayWithObjects:verionInfo, nil];
            } else {
                self.previewInfo = verionInfo;
            }
            
            if (!self.metadataInfo) {
                self.metadataInfo = [NSMutableDictionary dictionary];
            }
            
            if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
                NSDictionary *versionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
                [[Datastore sharedStore] setCurrentPreviewInfo:versionInfo];
            }
            
            [self refreshMetadataView];
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}

- (void)getAssetMetadataTags {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Fetch Asset Metadata Tags...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        int versionId =[[[self.asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
        NSLog(@"self.asset : %@",self.asset);
        
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        
        [assetService getAssetTagsForAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *assetTags, NSError *error){
            
            
            id assetTagsResponse = [[[[[assetTags objectForKey:@"GetAssetTagsResponse"] objectForKey:@"GetAssetTagsResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
            
            
            NSArray *assetTagsArray = nil;
            if ([assetTagsResponse isKindOfClass:[NSArray class]]) {
                assetTagsArray = assetTagsResponse;
            } else if ([assetTagsResponse isKindOfClass:[NSDictionary class]]) {
                assetTagsArray = [NSArray arrayWithObjects:assetTagsResponse, nil];
            }
            
            NSLog(@"getAllMetadataForAssetId Asset Tag Array:%@",assetTagsArray);
            
            NSLog(@"getAssetMetadataTags Response : %@",assetTagsResponse);
            
            if (!self.metadataInfo) {
                self.metadataInfo = [NSMutableDictionary dictionary];
            }
            
            if ([assetTagsResponse isKindOfClass:[NSDictionary class]]) {
                [self.metadataInfo setValue:[NSArray arrayWithObjects:assetTagsResponse, nil] forKey:@"AssetTags"];
            } else {
                [self.metadataInfo setValue:assetTagsResponse forKey:@"AssetTags"];
            }
            
            [self refreshMetadataView];
            
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
}

- (void)updateAssetMetadata:(NSDictionary *)metadataInfo {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Update Metadata...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        NSMutableDictionary *dictionaryMetaInfo = [NSMutableDictionary dictionaryWithDictionary:metadataInfo];
        [dictionaryMetaInfo setObject:[[self.asset objectForKey:@"ID"] objectForKey:@"text"] forKey:@"AssetId"];
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService updateFilePropertiesForAsset:dictionaryMetaInfo  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            NSLog(@"Update Asset Metadata Tag Response dictionary: %@",dictionary);
            
            if (!dictionary) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSString *responseString = [[[dictionary objectForKey:@"UpdateFilePropertiesMetadataResponse"] objectForKey:@"UpdateFilePropertiesMetadataResult"] objectForKey:@"text"];
            NSLog(@"Update Asset Metadata Response : %@",dictionary);
            if ([responseString boolValue]) {
                [self getAssetMetadataInfo];
            } else{
                [Utilities showAlert:APPNAME message:@"Fail to Update Asset Metadata Info" delegateObject:nil];
                
            }
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}

- (void)saveAssetMetadataTag:(NSDictionary *)tagInfo {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Saving Metadata Tag...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        NSLog(@"Save Asset Metadata Tag tagInfo : %@",tagInfo);
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        
        int versionId =[[[self.asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService saveAssetsTagsForAssetId:assetId versionId:versionId tagInfo:tagInfo  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            NSLog(@"Save Asset Metadata Tag Response dictionary: %@",dictionary);
            
            if (!dictionary) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSString *responseString = [[[dictionary objectForKey:@"AddAssetsTagsResponse"] objectForKey:@"AddAssetsTagsResult"] objectForKey:@"text"];
            NSLog(@"Save Asset Metadata Tag Response : %@",responseString);
            if ([responseString boolValue]) {
                [self getAssetMetadataTags];
                self.previewInfo = nil;
            } else  {
                //[Utilities showAlert:APPNAME message:@"Fail to Save Metadata Tag" delegateObject:nil];
                [self getAssetMetadataTags];
            }
        }];
        
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
}

- (void)deleteAssetMetadataTag:(NSDictionary *)tagInfo {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Deleting Metadata Tag...";
    [hud showAnimated:YES whileExecutingBlock:^{
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    NSLog(@"deleteAssetMetadataTag Metadata Tag tagInfo : %@",tagInfo);
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId =[[[self.asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
    int tagId = [[tagInfo objectForKey:@"TagId"] intValue];
    
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService deleteAssetsTagsForAssetId:assetId versionId:versionId tagId:tagId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        NSLog(@"Delete Asset Metadata Tag Response dictionary: %@",dictionary);
        if (!dictionary) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"LoadAllMetadataTimeCodesResponse"] objectForKey:@"LoadAllMetadataTimeCodesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
        NSLog(@"Delete Asset Metadata Tag Response : %@",dictionary);
        
        [self getAssetMetadataTags];
    }];
}


- (void)initMetadata {
    
    _metadataView.hidden=NO;
    [_metaDataSegment setSelectedSegmentIndex:0];
    _meta1View.hidden=NO;
    _meta2View.hidden=YES;
    _meta3View.hidden=YES;
    _meta4View.hidden=YES;
    
    
    if (!self.metadataInfo) {
        [self getAllMetadatas];
    }
    
    [self refreshMetadataView];
    
    
    
}

- (void)hideMetadata {
    
    _metadataView.hidden=YES;
    
    
    
}


- (IBAction)metadataSegmentTapped:(id)sender {
    
    switch (_metaDataSegment.selectedSegmentIndex) {
        case 0: {
            
            _meta1View.hidden=NO;
            _meta2View.hidden=YES;
            _meta3View.hidden=YES;
            _meta4View.hidden=YES;
            
            if (!self.metadataInfo) {
                [self getAllMetadatas];
            }
            
            [self refreshMetadataView];
            
            break;
        }
        case 1: {
            
            _meta1View.hidden=YES;
            _meta2View.hidden=NO;
            _meta3View.hidden=YES;
            _meta4View.hidden=YES;
            
            [_meta2TableView reloadData];
            
            break;
        }
            
        case 2: {
            
            _meta1View.hidden=YES;
            _meta2View.hidden=YES;
            _meta3View.hidden=NO;
            _meta4View.hidden=YES;
            
            [_meta3TableView reloadData];
            
            break;
        }
        case 3: {
            
            _meta1View.hidden=YES;
            _meta2View.hidden=YES;
            _meta3View.hidden=YES;
            _meta4View.hidden=NO;
            
            _meta4PrivateBtn.selected=YES;
            _meta4PublicBtn.selected=NO;
            
            [_meta4TableView reloadData];
            
            break;
        }
            
        default:
            break;
    }
    
    
}

- (BOOL)validateMetaInfoInputs  {
    BOOL success = YES;
    
    [self assertNonEmptyStringForTextField:_meta1Title success:&success];
    [self assertNonEmptyStringForTextField:_meta1Descri success:&success];
    [self assertNonEmptyStringForTextField:_meta1Autor success:&success];
    
    return success;
}


- (IBAction)updateAssetMetadataInfoTapped:(id)sender {
    
    
    if ([self validateMetaInfoInputs]) {
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:_meta1Title.text,@"Title",
                                    _meta1Descri.text,@"Description",
                                    _meta1Autor.text,@"Author",
                                    _meta1UploadOn.text,@"UploadOn",
                                    _meta1UploadBy.text,@"UploadBy", nil];
        
        
        [self updateAssetMetadata:dictionary];
    }
}




#pragma mark - - Approval - -

- (void)refreshApprovalView   {
    // empty approval info - call webservice.
    if (!self.approvalUserInfo && [[Datastore sharedStore] allowUserFeature:Internal_Approval]) {
        [self getAllInternalUserForApproval];
        return;
    }
    
    
}

- (IBAction)internalApprovalRadioButtonClicked:(id)sender {
    
    _internalApprovalRB.selected = YES;
    _externalApprovalRB.selected = NO;
}

- (IBAction)externalApprovalRadioButtonClicked:(id)sender {
    _internalApprovalRB.selected = NO;
    _externalApprovalRB.selected = YES;
}


- (BOOL)validateApprovalInputs  {
    BOOL success = YES;
    
    [self assertNonEmptyStringForTextField:_externalApprovalEmail success:&success];
    [self assertNonEmptyStringForTextField:_externalApprovalUsername success:&success];
    [self assertNonEmptyStringForTextField:_externalApprovalPassword success:&success];
    [self assertNonEmptyStringForTextField:_externalApprovalExpiryDate success:&success];
    
    return success;
}


- (IBAction)sendApprovalButtonClicked:(id)sender {
    
    if (_internalApprovalRB.selected) {
        
        if (!self.selectedInternalUser) {
            BOOL success = YES;
            [self assertNonEmptyStringForTextField:_internalApprovalUserTextField success:&success];
            return;
        }
        
        int userId = [[[self.selectedInternalUser objectForKey:@"ID"] objectForKey:@"text"] intValue];
        
        [self submitInternalApprovalToUser:userId];
        
        
        
    } else {
        
        
        
        if ([self validateApprovalInputs]) {
            
            NSArray *emails = [self validateEmails:_externalApprovalEmail.text];
            if (emails && [emails count] > 0) {
                NSLog(@"EMAM Approval View : Valid Emails found : \n %@",emails);
                
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString *emailIDs = [emails componentsJoinedByString:@","];
                [dictionary setValue:emailIDs forKey:@"EmailIds"];
                [dictionary setValue:_externalApprovalUsername.text forKey:@"LoginName"];
                [dictionary setValue:_externalApprovalPassword.text forKey:@"Password"];
                [dictionary setValue:_externalApprovalExpiryDate.text forKey:@"ExpiryDate"];
                
                [self submitExternalApprovalToUser:dictionary];
                
            }
            else {
                NSLog(@"EMAM Approval View : NO Valid Emails...");
            }
        }
    }
    
    
}

- (IBAction)selectUserButtonClicked:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    SelectionViewController *selectionViewController = [[SelectionViewController alloc] init];
    selectionViewController.selectionOptions = self.approvalUserInfo;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:selectionViewController];
    popoverController.backgroundColor=[UIColor lightGrayColor];
    
    [popoverController presentPopoverFromRect:button.frame inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [selectionViewController setSelectionBlock:^(NSDictionary *selection) {
        
        self.selectedInternalUser = selection;
        
        [_internalApprovalUserTextField setText:[[selection objectForKey:@"FIRSTNAME"] objectForKey:@"text"]];
        
        [popoverController dismissPopoverAnimated:YES];
    }];
}




- (void)initApproval {
    
    _approvalView.hidden=NO;
    _internalApprovalRB.selected = YES;
    _externalApprovalRB.selected = NO;
    [self refreshApprovalView];
    
    
    
}

- (void)hideApproval {
    
    _approvalView.hidden=YES;
    
    
    
}

- (void)getAllInternalUserForApproval {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Fetch Internal Users...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        
        
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService geUsersForAssetId:assetId   performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            [hud removeFromSuperview];
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            id response = [[[[[dictionary objectForKey:@"GetUsersWithPermissionOnAssetResponse"] objectForKey:@"GetUsersWithPermissionOnAssetResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
            NSLog(@"Get All InternalUser For Approval Response : %@",response);
            
            if ([response isKindOfClass:[NSDictionary class]]) {
                self.approvalUserInfo = [NSMutableArray arrayWithObjects:response, nil];
            } else {
                self.approvalUserInfo = [NSMutableArray arrayWithArray:response];
            }
            
            NSLog(@"EMAM AssetViewerController : Total Internal User count for Approval Request : %d",self.approvalUserInfo.count);
            // to fetch current 'userId' and skip current user from list.
            NSString *emailId = [[NSUserDefaults standardUserDefaults] objectForKey:KUSEREMAILID];
            if (emailId) {
                emailId = [emailId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.EMAIL_ID.text = %@",emailId];
                if (predicate) {
                    NSDictionary *dictionary = [[self.approvalUserInfo filteredArrayUsingPredicate:predicate] lastObject];
                    if (dictionary) {
                        [self.approvalUserInfo removeObject:dictionary];
                    }
                }
            }
            NSLog(@"EMAM AssetViewerController : Total Internal User count after removing current user for Approval Request : %d",self.approvalUserInfo.count);
            
            
        }];
        
        
        
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
}

- (void)submitInternalApprovalToUser:(int)userId {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Submitting Approval Requests...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        int versionId = 0;
        if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
            NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
            versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
        }
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService submitApprovalForUser:userId assetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            [hud removeFromSuperview];
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            id response = [[dictionary objectForKey:@"SubmitApprovalResponse"] objectForKey:@"SubmitApprovalResult"];
            NSLog(@"Submit InternalApproval To User Response : %@",response);
            
            if (response) {
                NSDictionary *responseDic = (NSDictionary *)response;
                if ([responseDic objectForKey:@"text"]) {
                    int requestId = [[responseDic objectForKey:@"text"] intValue];
                    if (requestId > 0) {
                        [Utilities showAlert:APPNAME message:@"Approval Request sent successfully..." delegateObject:nil];
                    } else {
                        [Utilities showAlert:APPNAME message:@"Fail to send Approval Request..." delegateObject:nil];
                    }
                }
            }
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
    
}

- (void)submitExternalApprovalToUser:(NSDictionary *)userDetails {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Submitting Approval Requests...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        int versionId = 0;
        if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
            NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
            versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
        }
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService submitExternalApprovalForUser:userDetails assetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            [hud removeFromSuperview];
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            id response = [[dictionary objectForKey:@"SubmitExternalApprovalResponse"] objectForKey:@"SubmitExternalApprovalResult"];
            NSLog(@"Submit ExternalApproval For User Response : %@",response);
            if (response) {
                NSDictionary *responseDic = (NSDictionary *)response;
                if ([responseDic objectForKey:@"text"]) {
                    int requestId = [[responseDic objectForKey:@"text"] intValue];
                    if (requestId > 0) {
                        [Utilities showAlert:APPNAME message:@"Approval Request sent successfully..." delegateObject:nil];
                    } else {
                        [Utilities showAlert:APPNAME message:@"Fail to send Approval Request..." delegateObject:nil];
                    }
                }
            }
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
}




#pragma mark - - eSend - -

- (void)createLayoutViewEsend    {
    
    
    
}

- (NSMutableArray *)validateEmails:(NSString *)emails  {
    // For multiple email validation.
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray) {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email]) {
            [validEmails addObject:email];
        } else {
            // To highlight the error area inside textfield.
            
            [_esentEmail becomeFirstResponder];
            _esentEmail.layer.borderColor = [UIColor redColor].CGColor;
            NSRange selectedRange = [_esentEmail.text rangeOfString:email];
            
            UITextPosition *startPosition = [_esentEmail positionFromPosition:_esentEmail.beginningOfDocument offset:selectedRange.location];
            UITextPosition *endPosition = [_esentEmail positionFromPosition:_esentEmail.beginningOfDocument offset:(selectedRange.location + selectedRange.length)];
            UITextRange *selection = [_esentEmail textRangeFromPosition:startPosition toPosition:endPosition];
            _esentEmail.selectedTextRange = selection;
            
            
            validEmails = nil;
            break;
        }
    }
    return validEmails;
}



- (void)assertNonEmptyStringForTextField:(UITextField *)textfield success:(BOOL *)success   {
    UITextField *textField = (UITextField *)textfield;
    if (textField.superview) {
        NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([text length] <= 0) {
            *success = NO;
            textField.backgroundColor = [UIColor grayColor];
            textField.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
}

- (BOOL)validateInputs  {
    BOOL success = YES;
    
    [self assertNonEmptyStringForTextField:_esentEmail success:&success];
    [self assertNonEmptyStringForTextField:_esendSubject success:&success];
    
    
    return success;
}



- (void)showAdvanceEsendOptions {
    
    
    
    if ([self validateInputs]&&([_esendDescri.text length]>0)) {
        
        
        NSArray *emails = [self validateEmails:_esentEmail.text];
        if (emails && [emails count] > 0) {
            NSLog(@"EMAM AdvancedEsendViewController : Valid Emails found... \n %@",emails);
            
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            NSString *emailIDs = [emails componentsJoinedByString:@","];
            
            [dictionary setValue:emailIDs forKey:@"EmailIds"];
            [dictionary setValue:_esendSubject.text forKey:@"Subject"];
            [dictionary setValue:_esendDescri.text forKey:@"Description"];
            
            
            int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
            int versionId = 0;
            if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
                NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
                versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
            }
            [dictionary setValue:[NSString stringWithFormat:@"%d",assetId] forKey:@"AssetIds"];
            [dictionary setValue:[NSString stringWithFormat:@"%d",versionId] forKey:@"AssetVersionIds"];
            
            NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
            NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
            [dictionary setValue:[NSString stringWithFormat:@"%@/%@_1.jpg",virtualPath,uuid] forKey:@"ThumbNailUrl"];
            
            NSString *clientLogoImagePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"ClientLogoImagePath"];
            if (! clientLogoImagePath) {
                clientLogoImagePath = @"";
            }
            [dictionary setValue:clientLogoImagePath forKey:@"LogoPath"];
            NSLog(@"eSendDetails : %@",dictionary);
            
            AdvancedESendView *advancedEsendViewController = [[AdvancedESendView alloc] init];
            
            NSLog(@"info:%@",self.eSendInfo);
            advancedEsendViewController.fromAssetEsend=YES;
            advancedEsendViewController.esentDet=dictionary;
            advancedEsendViewController.eSendInfo = self.eSendInfo;
            advancedEsendViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:advancedEsendViewController];
            navigationController.navigationBarHidden=YES;
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentViewController:navigationController animated:YES completion:nil];
            [advancedEsendViewController setDismissalBlock:^(NSMutableDictionary *dictionary) {
                
                self.eSendInfo = dictionary;
            }];
            
            
            
            
        }
        else {
            NSLog(@"EMAM AdvancedEsendViewController : NO Valid Emails...");
        }
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                        message:@"All fields are mandatory"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
    
    
    
}



- (IBAction)esendSendTapped:(id)sender {
    
    if ([self validateInputs]&&([_esendDescri.text length]>0)) {
        
        
        NSArray *emails = [self validateEmails:_esentEmail.text];
        if (emails && [emails count] > 0) {
            NSLog(@"EMAM AdvancedEsendViewController : Valid Emails found... \n %@",emails);
            
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            NSString *emailIDs = [emails componentsJoinedByString:@","];
            
            [dictionary setValue:emailIDs forKey:@"EmailIds"];
            [dictionary setValue:_esendSubject.text forKey:@"Subject"];
            [dictionary setValue:_esendDescri.text forKey:@"Description"];
            
            
            [self sendEsendWithOptions:dictionary];
            
            
            
        }
        else {
            NSLog(@"EMAM AdvancedEsendViewController : NO Valid Emails...");
        }
    }
}

- (IBAction)esendAdvancedTapped:(id)sender {
    
    [self showAdvanceEsendOptions];
}



- (void)initESend {
    
    _esendView.hidden=NO;
    
    
    if (!self.eSendInfo) {
        [self setDefaultEsendValues];
        return;
    }
    
}

- (void)hideESend  {
    
    _esendView.hidden=YES;
    
}


- (void)setDefaultEsendValues {
    self.eSendInfo = [NSMutableDictionary dictionaryWithCapacity:20];
    // Set the default values.
    NSString *reelName = [NSString stringWithFormat:@"Message-%@",[Utilities getUUID]];
    [self.eSendInfo setValue:reelName forKey:@"ReelName"];
    [self.eSendInfo setValue:@"HTML" forKey:@"EmailType"];
    [self.eSendInfo setValue:[NSNumber numberWithInt:0] forKey:@"AccessLimit"];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    dateComponents = nil;
    
    [self.eSendInfo setValue:[Utilities stringFromDate:newDate formatString:@"yyyy-MM-ddThh:mm:ss"] forKey:@"ExipryDate"];
    
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"Downloadable"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ReportTracking"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ForwardOption"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"SendCopy"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"AllowComments"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ClearBasket"];
    [self.eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"UnsubscribeLink"];
    [self.eSendInfo setValue:[NSNumber numberWithInt:6] forKey:@"EmailThemeId"];
    [self.eSendInfo setValue:[NSNumber numberWithInt:6] forKey:@"PreviewThemeId"];
}

- (void)sendEsend:(NSMutableDictionary *)eSendDetails {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Submitting eSend Request...";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        int versionId = 0;
        if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
            NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
            versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
        }
        [eSendDetails setValue:[NSString stringWithFormat:@"%d",assetId] forKey:@"AssetIds"];
        [eSendDetails setValue:[NSString stringWithFormat:@"%d",versionId] forKey:@"AssetVersionIds"];
        
        NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
        NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
        [eSendDetails setValue:[NSString stringWithFormat:@"%@/%@_1.jpg",virtualPath,uuid] forKey:@"ThumbNailUrl"];
        
        NSString *clientLogoImagePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"ClientLogoImagePath"];
        if (! clientLogoImagePath) {
            clientLogoImagePath = @"";
        }
        [eSendDetails setValue:clientLogoImagePath forKey:@"LogoPath"];
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        
        NSLog(@"eSendDetails : %@",eSendDetails);
        
        [assetService sendeSendWithDetails:eSendDetails  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            [hud removeFromSuperview];
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            NSLog(@"Add eBin Send Info Response1 : %@",dictionary);
            
            id response = [[dictionary objectForKey:@"AddeBINSendInfoResponse"] objectForKey:@"AddeBINSendInfoResult"];
            
            NSLog(@"Add eBin Send Info Response2 : %@",response);
            
            if (response) {
                int responseStatus = [[response objectForKey:@"text"] intValue];
                
                if (responseStatus > 0) {
                    [Utilities showAlert:APPNAME message:@"Message Sent Successfully..." delegateObject:nil];
                    // to reset the details.
                    [self setDefaultEsendValues];
                }
                else if (responseStatus == -2) {
                    [Utilities showAlert:APPNAME message:@"Sending mail failed..." delegateObject:nil];
                } else if (responseStatus == -1) {
                    [Utilities showAlert:APPNAME message:@"The message name you have entered is already exists, please enter another name..." delegateObject:nil];
                } else {
                    [Utilities showAlert:APPNAME message:@"Sending failed..." delegateObject:nil];
                }
            }
        }];
        
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
}



- (void)sendEsendWithOptions:(NSMutableDictionary *)dictionary {
    // update the eSend info dictionary.
    [self.eSendInfo addEntriesFromDictionary:dictionary];
    [self sendEsend:self.eSendInfo];
}




#pragma mark - - Comments - -

- (void)initComments {
    
    _commentsView.hidden=NO;
    
    if (!self.commentInfo) {
        [self getAllComments];
        return;
    }
    
    [self refreshCommentView];
    
    
}

- (void)hideComments  {
    
    _commentsView.hidden=YES;
    
}


- (void)refreshCommentView  {
    
    [_commentsTable reloadData];
    
}

- (void)getAllComments {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading Comments";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService getAssetCommentsForAssetId:assetId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"GetAssetCommentsResponse"] objectForKey:@"GetAssetCommentsResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
            
            self.commentInfo = [NSArray array];
            id commentInfo = [responseDictionary objectForKey:@"Table"];
            if (commentInfo) {
                if ([commentInfo isKindOfClass:[NSDictionary class]]) {
                    self.commentInfo = [NSArray arrayWithObjects:commentInfo, nil];
                } else {
                    self.commentInfo = commentInfo;
                }
                
                NSLog(@"GetAsset Comments Response : %@",self.commentInfo);
                [_commentsTable reloadData];
            }
            
            [self refreshCommentView];
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}


- (IBAction)deleteComment:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    
    NSString *commentId = [[[self.commentInfo objectAtIndex:(btn.tag-3)] objectForKey:@"COMMENT_ID"] objectForKey:@"text"];
    NSLog(@"Del Tag : %d",(btn.tag-3));
    NSLog(@"commentId: %d",[commentId intValue]);
    
    [self deleteAssetCommentWithId:[commentId intValue]];
}
- (void)deleteAssetCommentWithId:(int)commentId {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Deleting comment";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService deleteAssetCommentsForAssetId:assetId commentId:commentId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            //        NSString *response = [[[dictionary objectForKey:@"AddAssetCommentsResponse"] objectForKey:@"AddAssetCommentsResult"] objectForKey:@"text"];
            //NSLog(@"Delete Asset Comment Response : %@",dictionary);
            
            [self getAllComments];
        }];
        
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
}



- (void)addAssetComment:(NSString *)comment {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Adding comment";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService addAssetCommentsForAssetId:assetId comment:comment  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            _commentTxtView.text=nil;
            //NSString *response = [[[dictionary objectForKey:@"AddAssetCommentsResponse"] objectForKey:@"AddAssetCommentsResult"] objectForKey:@"text"];
            //NSLog(@"Add Asset Comment Response : %@",([response boolValue]) ? @"Sucess" : @"Fails");
            
            [self getAllComments];
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}




#pragma mark - - History - -

- (void)initHistory {
    
    _historyView.hidden=NO;
    
    if (!self.historyInfo) {
        [self getAssetHistory];
        return;
    }
    
    
    [self refreshHistoryView];
    
}

- (void)hideHistory  {
    
    _historyView.hidden=YES;
    
}


- (void)getAssetHistory {
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading Asset Data";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService getAssetHistoryForAssetId:assetId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"GetAssetHistroyResponse"] objectForKey:@"GetAssetHistroyResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
            
            
            self.historyInfo = [NSArray array];
            id historyInfo = [responseDictionary objectForKey:@"Table"];
            if (historyInfo) {
                if ([historyInfo isKindOfClass:[NSDictionary class]]) {
                    self.historyInfo = [NSArray arrayWithObjects:historyInfo, nil];
                } else {
                    self.historyInfo = historyInfo;
                }
                
                [_historyTable reloadData];
                
                NSLog(@"GetAsset History Response : %@",_historyInfo);
            }
            
            [self refreshHistoryView];
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}


- (void)refreshHistoryView {
    
    
    
    NSString *assetId = [[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_ID"] objectForKey:@"text"];
    if (!assetId) {
        assetId = @"";
    }
    
    _historyAssetIdLbl.text=assetId;
    
    NSString *title = [[[Datastore sharedStore].currentPreviewInfo objectForKey:@"TITLE"] objectForKey:@"text"];
    if (!title) {
        title = @"";
    }
    
    _historyTitleLbl.text=title;
    
    NSString *versionId = [[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"];
    if (!versionId) {
        versionId = @"";
    }
    
    _historyVerdionIdLbl.text=versionId;
    
    
    
}

- (NSString *)formatDateString:(NSString *)currentDateString  {
    
    NSString* year  = [currentDateString substringWithRange: NSMakeRange( 0, 4)];
    NSString* month = [currentDateString substringWithRange: NSMakeRange( 5, 2)];
    NSString* day   = [currentDateString substringWithRange: NSMakeRange( 8, 2)];
    
    NSString* output = [NSString stringWithFormat:@"%@/%@/%@",day,month,year];
    
    return output;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_historyTable) {
        
        return [self.historyInfo count];
        
    }else if (tableView==_commentsTable) {
        
        return [self.commentInfo count];
    }else if ((tableView==_meta2TableView)||(tableView==_meta3TableView)||(tableView==_meta4TableView)) {
        
        int icount = 0;
        NSArray *array = nil;
        switch (_metaDataSegment.selectedSegmentIndex) {
            case 1:
                array = [self.metadataInfo objectForKey:@"EmbedMetadata"];
                break;
            case 2:
                array = [self.metadataInfo objectForKey:@"CustomMetadata"];
                break;
            case 3:
                array = [self.metadataInfo objectForKey:@"AssetTags"];
                break;
                
            default:
                break;
        }
        if (array && [array isKindOfClass:[NSArray class]]) {
            icount = [array count];
        }
        
        return icount;
    }else {
        return 0;
    }
    
    
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_historyTable) {
        static NSString *cellIdentifier = @"HistoryCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        UILabel *dateLbl = (UILabel *)[cell viewWithTag:1];
        UILabel *actionLbl = (UILabel *)[cell viewWithTag:2];
        UILabel *userNameLbl = (UILabel *)[cell viewWithTag:3];
        UILabel *userIdLbl = (UILabel *)[cell viewWithTag:4];
        
        
        dateLbl.text = [[[self.historyInfo objectAtIndex:indexPath.row] objectForKey:@"Date_of_Action"] objectForKey:@"text"];
        actionLbl.text = [[[self.historyInfo objectAtIndex:indexPath.row] objectForKey:@"Action_Performed"] objectForKey:@"text"];
        userNameLbl.text = [[[self.historyInfo objectAtIndex:indexPath.row] objectForKey:@"User_Name"] objectForKey:@"text"];
        userIdLbl.text = [[[self.historyInfo objectAtIndex:indexPath.row] objectForKey:@"User_Id"] objectForKey:@"text"];
        
        return cell;
    }else if (tableView==_commentsTable) {
        static NSString *cellIdentifier = @"CommentCell";
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
        UILabel *commentLbl = (UILabel *)[cell viewWithTag:2];
        
        
        
        
        
        NSDictionary *comment = [self.commentInfo objectAtIndex:indexPath.row];
        NSString *date = [self formatDateString:[[comment objectForKey:@"DATE"] objectForKey:@"text"]];
        NSString *title = [NSString stringWithFormat:@"%@ (%@)",[[comment objectForKey:@"USER"] objectForKey:@"text"],date];
        //NSString *commentId = [[comment objectForKey:@"COMMENT_ID"] objectForKey:@"text"];
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(deleteComment:) forControlEvents:UIControlEventTouchUpInside];
        button.tag=(indexPath.row+3);
        [button setImage:[UIImage imageNamed:@"btn_comment_delete.png"] forState:UIControlStateNormal];
        button.frame = CGRectMake(710.0, 2.0, 25.0, 25.0);
        [cell addSubview:button];
        
        
        nameLabel.text = title;
        commentLbl.text = [[comment objectForKey:@"COMMENT"] objectForKey:@"text"];
        
        return cell;
    }else if ((tableView==_meta2TableView)||(tableView==_meta3TableView)||(tableView==_meta4TableView)) {
        
        
        NSArray *array = nil;
        switch (_metaDataSegment.selectedSegmentIndex) {
            case 1: {
                static NSString *cellIdentifier = @"EmbeddedMetadataCell";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                
                UILabel *fieldLbl = (UILabel *)[cell viewWithTag:1];
                UILabel *valueLbl = (UILabel *)[cell viewWithTag:2];
                array = [self.metadataInfo objectForKey:@"EmbedMetadata"];
                if (array && [array isKindOfClass:[NSArray class]]) {
                    if ([array count] > indexPath.row) {
                        NSDictionary *dictionary = [array objectAtIndex:indexPath.row];
                        fieldLbl.text = [[dictionary objectForKey:@"field_name"] objectForKey:@"text"];
                        valueLbl.text = [[dictionary objectForKey:@"field_value"] objectForKey:@"text"];
                    }
                }
                return cell;
                break;
            }
            case 2: {
                static NSString *cellIdentifier = @"EmbeddedMetadataCell";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                
                UILabel *fieldLbl = (UILabel *)[cell viewWithTag:1];
                UILabel *valueLbl = (UILabel *)[cell viewWithTag:2];
                array = [self.metadataInfo objectForKey:@"CustomMetadata"];
                if (array && [array isKindOfClass:[NSArray class]]) {
                    if ([array count] > indexPath.row) {
                        NSDictionary *dictionary = [array objectAtIndex:indexPath.row];
                        fieldLbl.text = [[dictionary objectForKey:@"field_name"] objectForKey:@"text"];
                        valueLbl.text = [[dictionary objectForKey:@"field_value"] objectForKey:@"text"];
                    }
                }
                return cell;
                break;
            }
            case 3: {
                
                
                static NSString *cellIdentifier = @"TagsMetadataCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                
                UILabel *tagNameLbl = (UILabel *)[cell viewWithTag:1];
                UILabel *tagTypeLbl = (UILabel *)[cell viewWithTag:2];
                UIButton *tagEditBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                UIButton *tagDeleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [tagEditBtn setTitle:@"Edit" forState:UIControlStateNormal];
                [tagDeleteBtn setTitle:@"Delete" forState:UIControlStateNormal];
                tagEditBtn.titleLabel.font = [UIFont systemFontOfSize:13];
                tagDeleteBtn.titleLabel.font = [UIFont systemFontOfSize:13];
                
                [tagEditBtn addTarget:self action:@selector(editTag:) forControlEvents:UIControlEventTouchUpInside];
                tagEditBtn.tag=(indexPath.row);
                
                [tagDeleteBtn addTarget:self action:@selector(deleteTag:) forControlEvents:UIControlEventTouchUpInside];
                tagDeleteBtn.tag=(indexPath.row+1000);
                
                
                tagEditBtn.frame = CGRectMake(579.0, 7.0, 60.0, 30.0);
                [cell addSubview:tagEditBtn];
                
                tagDeleteBtn.frame = CGRectMake(647.0, 7.0, 60.0, 30.0);
                [cell addSubview:tagDeleteBtn];
                
                array = [self.metadataInfo objectForKey:@"AssetTags"];
                if (array && [array isKindOfClass:[NSArray class]]) {
                    if ([array count] > indexPath.row) {
                        NSDictionary *dictionary = [array objectAtIndex:indexPath.row];
                        tagNameLbl.text = [[dictionary objectForKey:@"tag_value"] objectForKey:@"text"];
                        tagTypeLbl.text = ([[[dictionary objectForKey:@"tag_type"] objectForKey:@"text"] intValue] == 0 ? @"Private" : @"Public");
                    }
                }
                return cell;
                
                break;
            }
                
            default:
                
                break;
        }
        
        return nil;
        
        
    }else {
        return nil;
    }
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


#pragma mark - - Orientation - -

- (void)orientationChanged:(NSNotification *)note
{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}


- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.moviePlayer.view.frame = CGRectMake(0, 0, 768, 500);
            
            
            
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.moviePlayer.view.frame = CGRectMake(0, 0, 1024, 500);
            
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}


-(void)doneButtonClick:(NSNotification*)aNotification{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(doneButtonClick:)       name:MPMoviePlayerWillExitFullscreenNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}


- (void)setPreviewLabels:(NSDictionary *)versionInfo  {
    
    
    NSString *text = nil;
    text = [[versionInfo objectForKey:@"TITLE"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        [_previewTitle setText:text];
    } else {
        [_previewTitle setText:@"Unknown Asset"];
    }
    
    
    text = [[versionInfo objectForKey:@"INGESTED_ON"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        [_assetDateAdded setText:text];
    }
    
    text = [[versionInfo objectForKey:@"ASSET_DESC"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        
        [_previewDescription setText:text];
    }
    
    text = [[versionInfo objectForKey:@"Asset_Tags"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        
        [_assetTags setText:text];
    }
    
    text = [[versionInfo objectForKey:@"Asset_Categories"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        
        [_assetCategories setText:text];
    }
    
    text = [[versionInfo objectForKey:@"Asset_In_Projects"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        
        [_assetProjects setText:text];
    }
    
}


- (void)initVideoPreview {
    
    _nonMovieView.hidden=YES;
    
    NSURL *movieURL = [self videoURL];
    
    self.moviePlayer = [[MPMoviePlayerController alloc]
                        initWithContentURL:movieURL];
    self.moviePlayer.view.frame = _movieView.bounds;
    [_movieView addSubview:self.moviePlayer.view];
    [self.moviePlayer play]; //Start playing
    
    previewStartedFlag=1;
    
    
}

- (void)refreshPreviewPlayer    {
    
    NSLog(@"EMAM AssetViewerController : Preview Player...");
    NSURL *movieURL = [self videoURL];
    
    
    AVURLAsset* videoAsset = [AVURLAsset URLAssetWithURL:movieURL options:nil];
    CMTime videoDuration = videoAsset.duration;
    //NSLog(@"videoDuration:%@",videoDuration);
    //NSLog(@"videoDuration:%@",[self formattedStringForDurationSubClip:videoDuration]);
    
    
    NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
    NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
    NSString *typeName = [[self.asset objectForKey:@"TYPE_NAME"] objectForKey:@"text"];
    NSString *originalExtention = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
    if ([typeName isEqualToString:@"Audio"]) {
        
        return;
        
    } else {
        
        [[Datastore sharedStore] setFrameDuration:[self formattedStringForDurationSubClip:videoDuration]];
        
    }
    
}


-(NSString *)formattedStringForDurationSubClip:(CMTime)time    {
    
    if (CMTIME_IS_INVALID(time)) {
        return @"00000000";
    }
    double timeInSeconds = CMTimeGetSeconds(time);
    double duration = timeInSeconds;
    NSInteger hour = floor(duration/(60 * 60));
    duration = (duration - hour * (60 * 60));
    NSInteger minutes = floor(duration/60);
    duration = (duration - (minutes * 60));
    NSInteger seconds = round(duration);
    duration = fmodf(timeInSeconds,1);
    
    
    //    NSURL *movieURL = [self videoURL];
    //
    //    AVURLAsset* videoAsset = [AVURLAsset URLAssetWithURL:movieURL options:nil];
    //
    //    AVAssetTrack *playerTrack = [videoAsset tracksWithMediaType:AVMediaTypeVideo][0];
    //
    //    float nominalFrameRate = playerTrack.nominalFrameRate;
    //
    //    float videoFrameRate = ([AVVideoPlayer sharedInstance].frameRate ? [AVVideoPlayer sharedInstance].frameRate : nominalFrameRate);
    NSInteger currentFrame = roundf(duration * [self getFrameRateFromAVPlayer]);
    
    return [NSString stringWithFormat:@"%02d%02d%02d%02d",hour, minutes, seconds,currentFrame];
}

/*
 {
 
 if (CMTIME_IS_INVALID(time)) {
 return @"00000000";
 }
 double timeInSeconds = CMTimeGetSeconds(time);
 double duration = timeInSeconds;
 NSInteger hour = floor(duration/(60 * 60));
 duration = (duration - hour * (60 * 60));
 NSInteger minutes = floor(duration/60);
 duration = (duration - (minutes * 60));
 NSInteger seconds = round(duration);
 duration = fmodf(timeInSeconds,1);
 
 
 NSURL *movieURL = [self videoURL];
 
 //    AVURLAsset* videoAsset = [AVURLAsset URLAssetWithURL:movieURL options:nil];
 
 AVAsset* videoAsset = [AVAsset assetWithURL:movieURL];
 
 AVMutableComposition *composition = [AVMutableComposition composition];
 [composition  addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
 //    NSLog([videoAsset tracksWithMediaType:AVMediaTypeVideo]);
 //    AVAssetTrack *playerTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] valueForKey:@"URL"];
 
 
 //    float nominalFrameRate = playerTrack.nominalFrameRate;
 
 //    float videoFrameRate = ([AVVideoPlayer sharedInstance].frameRate ? [AVVideoPlayer sharedInstance].frameRate : nominalFrameRate);
 NSInteger currentFrame = roundf(duration * [self getFrameRateFromAVPlayer]);
 
 return [NSString stringWithFormat:@"%02d%02d%02d%02d",hour, minutes, seconds,currentFrame];
 }
 */
-(float)getFrameRateFromAVPlayer
{
    float fps=0.00;
    NSURL *movieURL = [self videoURL];
    AVURLAsset* videoAsset = [AVURLAsset URLAssetWithURL:movieURL options:nil];
    AVAssetTrack * videoATrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] lastObject];
    if(videoATrack)
    {
        fps = videoATrack.nominalFrameRate;
    }
    return fps;
}
- (NSURL *)videoURL {
    
    
    BOOL success = NO;
    NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
    NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
    NSString *typeName = [[self.asset objectForKey:@"TYPE_NAME"] objectForKey:@"text"];
    NSString *originalExtention = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
    if (virtualPath && uuid && [virtualPath length] > 0 && [uuid length] > 0) {
        NSString *strURL = nil;
        if ([typeName isEqualToString:@"Audio"]) {
            strURL = [NSString stringWithFormat:@"%@/%@_2.%@",virtualPath,uuid,originalExtention];
            NSLog(@"EMAM AssetViewerController : Audio URL : %@",strURL);
        } else {
            strURL = [NSString stringWithFormat:@"%@/%@_1.mp4",virtualPath,uuid];
            NSLog(@"EMAM AssetViewerController : Video URL : %@",strURL);
        }
        
        if (strURL) {
            NSURL *movieURL = [NSURL URLWithString:strURL];
            
            if (movieURL) {
                success = YES;
                
                
                
                [[TWRDownloadManager sharedManager] isFileDownloadingForUrl:[NSString stringWithFormat:@"%@",movieURL] withProgressBlock:^(CGFloat progress) {
                    _fetchForDownloadBtn.hidden=YES;
                    self.progress = progress;
                    _dwnldProgressView.progress = progress;
                    _progressLbl.text = [NSString stringWithFormat:@"Download Progress: %.0f%% ", self.progress*100];
                } completionBlock:^(BOOL completed) {
                    NSLog(@"Download completed!");
                    NSLog(@"File:%@",[self videoURL]);
                    NSLog(@"File Downloa Path:%@",[[TWRDownloadManager sharedManager] localPathForFile:[NSString stringWithFormat:@"%@",[self videoURL]]]);
                    _progressLbl.text=@"File feched for offline";
                    _fetchForDownloadBtn.hidden=YES;
                    
                }];
                
                
                if ([[TWRDownloadManager sharedManager] fileExistsForUrl:[NSString stringWithFormat:@"%@",movieURL]]) {
                    _dwnldProgressView.progress = 1.0;
                    _progressLbl.text=@"File feched for offline";
                    _fetchForDownloadBtn.hidden=YES;
                    NSString *savedFilePath=[[TWRDownloadManager sharedManager] localPathForFile:[NSString stringWithFormat:@"%@",movieURL]];
                    return [NSURL fileURLWithPath:savedFilePath];
                }else{
                    _fetchForDownloadBtn.hidden=NO;
                    _dwnldProgressView.progress = 0.0;
                    _progressLbl.text=@"";
                    return movieURL;
                }
                
                
                
            }
        } else {
            NSLog(@"EMAM AssetViewerController : Un-able to find %@ URL : %@-%@",typeName,virtualPath,uuid);
        }
    }
    
    if (!success) {
        NSLog(@"EMAM AssetViewerController : Un-avle to find %@ URL : %@-%@-%@",typeName,virtualPath,uuid,originalExtention);
        // [Utilities showAlert:[NSString stringWithFormat:@"Unable to find %@ URL.",typeName]];
    }
    return nil;
}



- (void)initAudioPreview {
    
    _nonMovieView.hidden=YES;
    
    
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback
                                           error: nil];
    NSURL *movieURL = [self videoURL];
    
    self.moviePlayer = [[MPMoviePlayerController alloc]
                        initWithContentURL:movieURL];
    self.moviePlayer.view.frame = _movieView.bounds;
    
    
    UIImage *image = [UIImage imageNamed:@"bg_audio.png"];
    _moviePlayer.backgroundView.backgroundColor=[UIColor colorWithPatternImage:image];
    
    
    [_movieView addSubview:self.moviePlayer.view];
    
    [self.moviePlayer play]; //Start playing
    
    previewStartedFlag=1;
    
    
}

- (void)initImagePreview {
    
    _nonMovieView.hidden=NO;
    
    NSString *strPath = [[self.asset objectForKey:@"PROXY_FILENAME"] objectForKey:@"text"];
    if (strPath) {
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator setCenter:_previewImageView.center];
        [activityIndicator startAnimating];
        [_previewImageView addSubview:activityIndicator];
        [_fullScrnBtn setEnabled:NO];
        
        
        NSLog(@"EMAM AssetViewerController : Start Fetch Image in Path : %@",strPath);
        [Utilities imageForURL:strPath completionBlock:^(UIImage *image) {
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               NSLog(@"EMAM AssetViewerController : Image Download complete : %@",[strPath lastPathComponent]);
                               [_fullScrnBtn setEnabled:YES];
                               [activityIndicator removeFromSuperview];
                               [_previewImageView setImage:image];
                           });
        }];
        
    }else {
        
        NSString *fileType = [[self.asset objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
        strPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_asset",fileType] ofType:@"jpg"];
        UIImage *image = [UIImage imageWithContentsOfFile:strPath];
        if (image) {
            [_previewImageView setImage:image];
        }
        else {
            NSLog(@"EMAM AssetViewerController : No Preview Image Found For Type : %@",fileType);
            UILabel *objLabelFileType = [[UILabel alloc] init];
            objLabelFileType.numberOfLines = 1;
            objLabelFileType.lineBreakMode = UILineBreakModeTailTruncation;
            objLabelFileType.textAlignment = UITextAlignmentCenter;
            objLabelFileType.font = [UIFont fontWithName:APPBOLDFONTNAME size:18.0];
            
            [objLabelFileType setText:[[NSString stringWithFormat:@"%@ File",fileType] uppercaseString]];
            objLabelFileType.textColor = [UIColor blackColor];
            objLabelFileType.backgroundColor = [UIColor clearColor];
            objLabelFileType.frame = CGRectMake(0,((_previewImageView.frame.size.height/2) - 10), _previewImageView.frame.size.width,20);
            [_previewImageView addSubview:objLabelFileType];
            
        }
    }
    
    
    
    
}








#pragma mark UIDocumentInteractionController Delegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller   {
    return self;
}

- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller canPerformAction:(SEL)action{
    return false;
}


- (void)initNoPreviewAvailable {
    
    NSLog(@"EMAM AssetViewerController : Show No Preview Available View...");
    
    NSString *strPath = [[NSBundle mainBundle] pathForResource:@"bg_no_preview_available" ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:strPath];
    [_previewImageView setImage:image];
    
    
}



- (void)initOtherFilesPreview {
    _nonMovieView.hidden=NO;
    
    
    NSString *originalExtention = [[self.asset objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
    if (![[Utilities getDocumentFileExtensions] containsObject:originalExtention]) {
        [self initNoPreviewAvailable];
        return;
    }
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(30, 0, (_movieView.frame.size.width - 60), 460)];
    webview.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
    webview.scalesPageToFit = NO;
    webview.scrollView.bounces = NO;
    
    NSString *strGeneralPath = [[self.asset objectForKey:@"GENERAL_VIRTUAL_PATH"] objectForKey:@"text"];
    NSString *strUUID = [[self.asset objectForKey:@"UUID"] objectForKey:@"text"];
    NSString *strURL = [NSString stringWithFormat:@"%@/%@_1.pdf",strGeneralPath,strUUID];
    NSURL *documentURL = [NSURL URLWithString:strURL];
    NSLog(@"EMAM AssetViewerController : Show other file preview with url : %@",strURL);
    
    [webview loadRequest:[NSURLRequest requestWithURL:documentURL]];
    
}



#pragma mark -

- (void)getAllAssetVersions {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"Loading Asset Data";
    [hud showAnimated:YES whileExecutingBlock:^{
        
        int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService getVersionInfoForAssetId:assetId versionId:0  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
            
            if (error) {
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSDictionary *responseDictionary = [[[[dictionary objectForKey:@"GetAssetVersionInfoResponse"] objectForKey:@"GetAssetVersionInfoResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
            
            id verionInfo = [responseDictionary objectForKey:@"Table"];
            if ([verionInfo isKindOfClass:[NSDictionary class]]) {
                self.previewInfo = [NSArray arrayWithObjects:verionInfo, nil];
            } else {
                self.previewInfo = verionInfo;
            }
            
            NSLog(@"GetVersionInfo For Asset Response : %@",self.previewInfo);
            self.currentVersionIndex = 1;
            [self setValueToVersionTextField:self.currentVersionIndex];
            
            if (self.currentAssetType == Video) {
                [self getVideoFrameRate];
                [self refreshPreviewPlayer];
            }else{
                switch (self.currentAssetType) {
                    case Audio:
                        [self initAudioPreview];
                        [self refreshPreviewPlayer];
                        break;
                    case Image:
                        [self initImagePreview];
                        break;
                    case OtherFiles:
                        [self initOtherFilesPreview];
                        break;
                    default:
                        break;
                }
            }
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}



- (void) setValueToVersionTextField:(int)version  {
    dispatch_async(dispatch_get_main_queue(), ^{
        int totalVersions = [self.previewInfo count];
        totalVersions = (totalVersions == 0) ? 1 : totalVersions;
        [self updateVersionInfo];
    });
}

- (void)updateVersionInfo   {
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *versionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        [[Datastore sharedStore] setCurrentPreviewInfo:versionInfo];
        
    }
}



- (void)getVideoFrameRate   {
    
    
    int assetId = [[[self.asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    int versionId = 0;
    if ([self.previewInfo count] > (self.currentVersionIndex - 1)) {
        NSDictionary *currentVersionInfo = [self.previewInfo objectAtIndex:(self.currentVersionIndex - 1)];
        versionId = [[[currentVersionInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    }
    [assetService getEmbededMetadataForAssetId:assetId versionId:versionId fieldName:@"Frame rate" performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        //NSLog(@"Get Frame rate For Asset dictionary : %@",dictionary);
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id embedMetadata = [[[[[dictionary objectForKey:@"GetEmbededMetadataResponse"] objectForKey:@"GetEmbededMetadataResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
        NSArray *frameRateInfo = nil;
        if ([embedMetadata isKindOfClass:[NSDictionary class]]) {
            frameRateInfo = [NSArray arrayWithObjects:embedMetadata, nil];
        } else {
            frameRateInfo = embedMetadata;
        }
        [[Datastore sharedStore] setVideoFrameRates:frameRateInfo];
        NSLog(@"Get Frame rate For Asset Response : %@",frameRateInfo);
        if (frameRateInfo && [frameRateInfo count] > (self.currentVersionIndex - 1)) {
            NSDictionary *currentFrameRateInfo = [frameRateInfo objectAtIndex:(self.currentVersionIndex - 1)];
            float currentFrameRate = [[[currentFrameRateInfo objectForKey:@"field_value"] objectForKey:@"text"] floatValue];
            [AVVideoPlayer sharedInstance].frameRate = currentFrameRate;
            
            
            switch (self.currentAssetType) {
                case Video:
                    [self initVideoPreview];
                    break;
                default:
                    break;
            }
            
        } else if([[[self.asset valueForKey:@"ORIGINAL_EXTENTION"] valueForKey:@"text"] isEqualToString:@"dpx.zip"]){
            [AVVideoPlayer sharedInstance].frameRate = 20;
            
            
        }else {
            
            [AVVideoPlayer sharedInstance].frameRate = 20;
            [Utilities showAlert:APPNAME message:@"Authentication Expired" delegateObject:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Authentication Expired" object:nil];
            [self dismissModalViewControllerAnimated:YES];
        }
        
    }];
}




#pragma mark - Helpers

- (void)selectButton:(UIButton *)selectedButton  {
    [self.selectedButton setSelected:NO];
    self.selectedButton = selectedButton;
    [self.selectedButton setSelected:YES];
    [self setCurrentViewType:self.selectedButton.tag];
}





- (void)createAndDisplayTopTitleView    {
    int buttonWidth = 62;
    UIBarButtonItem *barButton;
    NSMutableArray *barButtons = [NSMutableArray array];
    
    UIBarButtonItem *flexibleBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [barButtons addObject:flexibleBarButton];
    
    if ([self allowAssetOption:Preview]) {
        UIButton *previewButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        previewButton.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
        previewButton.tag = Preview;
        
        //to enable preview while load.
        [self selectButton:previewButton];
        self.currentViewType = Preview;
        
        [previewButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_preview" ofType:@"png"]] forState:UIControlStateNormal];
        [previewButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_preview_s" ofType:@"png"]] forState:UIControlStateSelected];
        [previewButton setBackgroundColor:[UIColor clearColor]];
        [previewButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:previewButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        previewButton = nil;
    }
    
    if ([self allowAssetOption:MetaData]) {
        UIButton *metadataButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        metadataButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        metadataButton.tag = MetaData;
        metadataButton.userInteractionEnabled = YES;
        
        [metadataButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_metadata" ofType:@"png"]] forState:UIControlStateNormal];
        [metadataButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_metadata_s" ofType:@"png"]] forState:UIControlStateSelected];
        [metadataButton setBackgroundColor:[UIColor clearColor]];
        [metadataButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:metadataButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        metadataButton = nil;
    }
    
    if ([self allowAssetOption:Annotation]) {
        UIButton *annotationButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        annotationButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        annotationButton.tag = Annotation;
        annotationButton.userInteractionEnabled = YES;
        
        [annotationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_annotationNormal" ofType:@"png"]] forState:UIControlStateNormal];
        [annotationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"btn_annotationSelected" ofType:@"png"]] forState:UIControlStateSelected];
        [annotationButton setBackgroundColor:[UIColor clearColor]];
        [annotationButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:annotationButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        annotationButton = nil;
    }
    
    if ([self allowAssetOption:Subclips]) {
        UIButton *subclipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        subclipButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        subclipButton.tag = Subclips;
        subclipButton.userInteractionEnabled = YES;
        
        [subclipButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_subclip" ofType:@"png"]] forState:UIControlStateNormal];
        [subclipButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_subclip_s" ofType:@"png"]] forState:UIControlStateSelected];
        [subclipButton setBackgroundColor:[UIColor clearColor]];
        [subclipButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:subclipButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        subclipButton = nil;
    }
    
    if ([self allowAssetOption:Markers]) {
        UIButton *markersButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        markersButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        markersButton.tag =  Markers;
        markersButton.userInteractionEnabled = YES;
        
        [markersButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_markers" ofType:@"png"]] forState:UIControlStateNormal];
        [markersButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_markers_s" ofType:@"png"]] forState:UIControlStateSelected];
        [markersButton setBackgroundColor:[UIColor clearColor]];
        [markersButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:markersButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        markersButton = nil;
    }
    
    if ([self allowAssetOption:ESend]) {
        UIButton *esendButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        esendButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        esendButton.tag = ESend;
        esendButton.userInteractionEnabled = YES;
        
        [esendButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_esend" ofType:@"png"]] forState:UIControlStateNormal];
        [esendButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_esend_s" ofType:@"png"]] forState:UIControlStateSelected];
        [esendButton setBackgroundColor:[UIColor clearColor]];
        [esendButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:esendButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        
        esendButton = nil;
    }
    
    if ([self allowAssetOption:Approval]) {
        UIButton *approvalButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        approvalButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        approvalButton.tag = Approval;
        approvalButton.userInteractionEnabled = YES;
        
        [approvalButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_approval" ofType:@"png"]] forState:UIControlStateNormal];
        [approvalButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_approval_s" ofType:@"png"]] forState:UIControlStateSelected];
        [approvalButton setBackgroundColor:[UIColor clearColor]];
        [approvalButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:approvalButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        
        approvalButton = nil;
    }
    
    if ([self allowAssetOption:Comment]) {
        UIButton *commentButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        commentButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        commentButton.tag = Comment;
        commentButton.userInteractionEnabled = YES;
        
        [commentButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_comment" ofType:@"png"]] forState:UIControlStateNormal];
        [commentButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_comment_s" ofType:@"png"]] forState:UIControlStateSelected];
        [commentButton setBackgroundColor:[UIColor clearColor]];
        [commentButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:commentButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        
        commentButton = nil;
    }
    
    if ([self allowAssetOption:History]) {
        UIButton *historyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        historyButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        historyButton.tag = History;
        historyButton.userInteractionEnabled = YES;
        
        [historyButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_history" ofType:@"png"]] forState:UIControlStateNormal];
        [historyButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_history_s" ofType:@"png"]] forState:UIControlStateSelected];
        [historyButton setBackgroundColor:[UIColor clearColor]];
        [historyButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:historyButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        
        historyButton = nil;
    }
    
    if ([self allowAssetOption:Embed]) {
        UIButton *embedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, 40)];
        embedButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        embedButton.tag = Embed;
        embedButton.userInteractionEnabled = YES;
        
        [embedButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_embed" ofType:@"png"]] forState:UIControlStateNormal];
        [embedButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon_embed_s" ofType:@"png"]] forState:UIControlStateSelected];
        [embedButton setBackgroundColor:[UIColor clearColor]];
        [embedButton addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:embedButton];
        [barButtons addObject:barButton];
        [barButtons addObject:flexibleBarButton];
        
        embedButton = nil;
    }
    
    barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                              target:self
                                                              action:@selector(doneTapped:)];
    [barButtons addObject:flexibleBarButton];
    
    
    [barButtons addObject:barButton];
    self.navigationItem.leftBarButtonItems = barButtons;
    barButtons = nil;
    
    
    
}


- (void)setAssetType    {
    NSString *typeName = [[self.asset objectForKey:@"TYPE_NAME"] objectForKey:@"text"];
    _dwnldProgressView.hidden=YES;
    _fetchForDownloadBtn.hidden=YES;
    _dwnldProgressView.progress=0.0;
    _progressLbl.hidden=YES;
    if ([typeName isEqualToString:@"Video"]) {
        self.currentAssetType = Video;
        _dwnldProgressView.hidden=NO;
        _progressLbl.hidden=NO;
        _fetchForDownloadBtn.hidden=NO;
        
        
    }
    else if([typeName isEqualToString:@"Audio"]) {
        self.currentAssetType = Audio;
    }
    else if([typeName isEqualToString:@"Image"]) {
        self.currentAssetType = Image;
    }
    else if([typeName isEqualToString:@"Other Files"]) {
        self.currentAssetType = OtherFiles;
    }
}

- (void)setAssetOptions {
    self.possibleAssetOptions = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO],
                                 [NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],
                                 [NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],
                                 [NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],
                                 [NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],
                                 [NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO], nil];
    // Common Asset Options
    [self.possibleAssetOptions replaceObjectAtIndex:Preview withObject:[NSNumber numberWithBool:YES]];
    [self.possibleAssetOptions replaceObjectAtIndex:MetaData withObject:[NSNumber numberWithBool:YES]];
    
    [self.possibleAssetOptions replaceObjectAtIndex:ESend withObject:[NSNumber numberWithBool:YES]];
    [self.possibleAssetOptions replaceObjectAtIndex:Approval withObject:[NSNumber numberWithBool:YES]];
    [self.possibleAssetOptions replaceObjectAtIndex:Comment withObject:[NSNumber numberWithBool:YES]];
    [self.possibleAssetOptions replaceObjectAtIndex:History withObject:[NSNumber numberWithBool:YES]];
    
    [self.possibleAssetOptions replaceObjectAtIndex:Embed withObject:[NSNumber numberWithBool:NO]];
    
    switch (self.currentAssetType) {
        case Video: {
            [self.possibleAssetOptions replaceObjectAtIndex:Annotation withObject:[NSNumber numberWithBool:YES]];
            [self.possibleAssetOptions replaceObjectAtIndex:Subclips withObject:[NSNumber numberWithBool:YES]];
            [self.possibleAssetOptions replaceObjectAtIndex:Markers withObject:[NSNumber numberWithBool:YES]];
            [self.possibleAssetOptions replaceObjectAtIndex:Embed withObject:[NSNumber numberWithBool:YES]];
            break;
        }
        case Audio: {
            [self.possibleAssetOptions replaceObjectAtIndex:Annotation withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Subclips withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Markers withObject:[NSNumber numberWithBool:NO]];
            break;
        }
        case Image: {
            [self.possibleAssetOptions replaceObjectAtIndex:Annotation withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Subclips withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Markers withObject:[NSNumber numberWithBool:NO]];
            break;
        }
        case OtherFiles: {
            [self.possibleAssetOptions replaceObjectAtIndex:Annotation withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Subclips withObject:[NSNumber numberWithBool:NO]];
            [self.possibleAssetOptions replaceObjectAtIndex:Markers withObject:[NSNumber numberWithBool:NO]];
            break;
        }
            
        default:
            break;
    }
    // Lock for current version.
    [self.possibleAssetOptions replaceObjectAtIndex:Annotation withObject:[NSNumber numberWithBool:NO]];
}

- (BOOL)allowAssetOption:(AssetViewType)viewType {
    BOOL allow = NO;
    switch (viewType) {
        case Preview:
            allow = YES;
            break;
        case MetaData:
            allow = [[Datastore sharedStore] allowUserFeature:Custom_Metadata];
            break;
        case Annotation:
            allow = NO; // for current version - Annotations
            break;
        case Subclips:
            allow = (self.currentAssetType == Video)? [[Datastore sharedStore] allowUserFeature:Proxy_Editing] : NO;
            break;
        case Markers:
            allow = (self.currentAssetType == Video)? [[Datastore sharedStore] allowUserFeature:Marker_Feature] : NO;
            break;
        case ESend:
            allow = [[Datastore sharedStore] allowUserFeature:eSEND_Feature];
            break;
        case Approval:
            allow = ([[Datastore sharedStore] allowUserFeature:Internal_Approval] || [[Datastore sharedStore] allowUserFeature:External_Approval]);
            break;
        case Comment:
            allow = YES;
            break;
        case History:
            allow = YES;
            break;
        case Embed:
            allow = (self.currentAssetType == Video)? [[Datastore sharedStore] allowUserFeature:EMBED_CODE] : NO;
            break;
            
        default:
            break;
    }
    return allow;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)doneTapped:(id)sender {
    [self destructAVPlayer];
    [self hideMarkers];
    [self hideSubclip];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)getEmbedTapped:(id)sender {
    
    [self getEmbedCode:self.selectedValues];
}

- (IBAction)embedShareTapped:(id)sender {
    
    [self getShareLink:self.selectedValues];
}

- (IBAction)fullScreenTapped:(id)sender {
    
    NSString *strPath = [[self.asset objectForKey:@"PROXY_FILENAME"] objectForKey:@"text"];
    NSString *folderPath = [LIBRARY_FOLDER stringByAppendingPathComponent:@"Preview"];
    NSString *filePath = [folderPath stringByAppendingPathComponent:[strPath lastPathComponent]];
    NSURL *documentURL = nil;
    if (filePath) {
        documentURL = [NSURL fileURLWithPath:filePath];
    }
    
    if (documentURL) {
        
        UIDocumentInteractionController *docController = [UIDocumentInteractionController interactionControllerWithURL:documentURL];
        docController.name = @"Asset Preview";
        
        docController.delegate = (id)self;
        
        BOOL isValid = [docController presentPreviewAnimated:YES];
        
        //        BOOL isValid = [docController presentOpenInMenuFromRect:sender.frame inView:self.view animated:YES];
        //
        if (!isValid)   {
            NSLog(@"EMAM AssetViewerController : Full Screen Preview for this Asset is not possible. Document URL : %@",filePath);
            [Utilities showAlert:APPNAME  message:@"Full Screen Preview for this Asset is not possible." delegateObject:nil];
            
            docController = nil;
        }
    } else {
        NSLog(@"EMAM AssetViewerController : Full Screen Preview for this Asset is not possible. Document URL : %@",filePath);
        [Utilities showAlert:APPNAME  message:@"Full Screen Preview for this Asset is not possible." delegateObject:nil];
    }
    
}
- (IBAction)addCommentTapped:(id)sender {
    
    if ([_commentTxtView.text length]>0) {
        [self addAssetComment:_commentTxtView.text];
    }
}



@end

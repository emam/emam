/**************************************************************************************
 *  File Name      : PreviewView.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

@interface PreviewView : UIView

@property (nonatomic, strong) NSDictionary *versionInfo;

@end

//
//  AssetService.h
//  eMAM
//
//  Created by APPLE on 20/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseWebRequest.h"
#import "AFHTTPRequestOperation.h"
#import "XMLReader.h"

@interface AssetService : BaseWebRequest

+ (AssetService *)sharedInstance;


#pragma mark - Asset Services

- (void)getUserBasket:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAssetsAtPage:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAllCategoriesForSearchPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)addToUserBasketAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)removeFromUserBasketAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)removeAllItemFromUserBasketPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAssetsInBasketAtPage:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;



@end

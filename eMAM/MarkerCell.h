/**************************************************************************************
 *  File Name      : MarkerCell.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

#import "Marker.h"
#import "MarkerView.h"

@interface MarkerCell : UITableViewCell

@property (nonatomic, strong) Marker *marker;
@property (nonatomic, strong) id <MarkerDelegate> delegate;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

@end

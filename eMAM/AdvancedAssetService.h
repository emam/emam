//
//  AdvancedAssetService.h
//  eMAM
//
//  Created by APPLE on 21/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseWebRequest.h"
#import "AFHTTPRequestOperation.h"
#import "XMLReader.h"

@interface AdvancedAssetService : BaseWebRequest

+ (AdvancedAssetService *)sharedInstance;

#pragma mark - Permissions

- (void)getAssetPermissionsForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Preview

- (void)getVersionInfoForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Metadata

- (void)getEmbededMetadataForAssetId:(int)assetId versionId:(int)versionId fieldName:(NSString *)fieldName performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getCustomMetaDataForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAssetTagsForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAllMetadataForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)updateFilePropertiesForAsset:(NSDictionary *)dictionary performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)addAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagValue:(NSString *)tagValue tagType:(int)tagType performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)updateAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagId:(int)tagId tagValue:(NSString *)tagValue performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)saveAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagInfo:(NSDictionary *)tagInfo performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)deleteAssetsTagsForAssetId:(int)assetId versionId:(int)versionId tagId:(int)tagId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - SubClips

- (void)getAllSubClipsForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)saveAllSubclips:(NSArray *)subclips forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)deleteSubclips:(NSArray *)subclips forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAllDeliveryProfilesForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)getAllSendEDLProfilesForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)insertDeliveryQueueDetailsForAssetId:(int)assetId versionId:(int)versionId addedDate:(NSString *)addedDate markers:(NSArray *)markers profiles:(NSArray *)profiles allowDuplicate:(BOOL)allowDuplicate isEDL:(BOOL)isEDL performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;


- (void)UpdateDeliveryQueueDetailsForAssetId:(int)assetId versionId:(int)versionId addedDate:(NSString *)addedDate markers:(NSArray *)markers profiles:(NSArray *)profiles allowDuplicate:(BOOL)allowDuplicate isEDL:(BOOL)isEDL performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;


#pragma mark - Markers

- (void)getAllMarkersForAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)saveAllMarkers:(NSArray *)markers forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)deleteMarkers:(NSArray *)markers forAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Approval

- (void)geUsersForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)submitApprovalForUser:(int)userId assetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)submitExternalApprovalForUser:(NSDictionary *)userDetails assetId:(int)asetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - eSend

- (void)sendeSendWithDetails:(NSDictionary *)eSendDetails performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Comments

- (void)getAssetCommentsForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)addAssetCommentsForAssetId:(int)assetId comment:(NSString *)comment performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)deleteAssetCommentsForAssetId:(int)assetId commentId:(int)commentId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - History

- (void)getAssetHistoryForAssetId:(int)assetId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Embed

- (void)geteMAMPlayeSkinsWithPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)generatePlayerObject:(NSDictionary *)parameters performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)inserteMAMAssetPlayer:(NSDictionary *)parameters performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;



@end

//
//  Utilities.h
//  eMAM
//
//  Created by APPLE on 22/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Hex Color Extension

@interface UIColor (HexAdditions)
+ (UIColor *)colorWithHexValue:(NSInteger)rgbValue ;

@end


@interface Utilities : NSObject


#pragma mark - UI Elements

+ (UIButton *)checkBox;
+ (UIButton *)radioButton;
+ (UIImage *)buttonBackgroundImage;

#pragma mark - Get File Extensions

+ (NSArray *)getMovieFileExtensions;
+ (NSArray *)getAudioFileExtensions;
+ (NSArray *)getImageFileExtensions;
+ (NSArray *)getDocumentFileExtensions;

#pragma mark -
+ (NSString *)typeImagePath:(NSString *)fileType;
+ (NSString *)orderByOptionValue;
- (int)assetTypeIDForAsset:(NSString *)asset;


#pragma mark - Get Gateways & User

+(NSString *)rootServer;
+(NSString *)serviceGateway;
+(NSString *)analyticsGateway;
+(NSString *)loginedUserName;

#pragma mark - Image Lazy Loading

+ (void)imageForURL:(NSString *)strURL completionBlock:(void(^)(UIImage *image))block;

#pragma mark - UDID & Date Services

+ (NSString *)getUUID;
+ (NSString *)dateString;
+ (NSString *)getUTCFormateDate:(NSDate *)localDate;
+ (NSString *)stringFromDate:(NSDate *)date formatString:(NSString*)dateFormatterString;


#pragma mark - To Take Top View controller

+ (UIViewController *)getTopViewController;

#pragma mark - Responder Services

+ (UIView *)findFirstResponderBeneathView:(UIView *)view;
+ (void)resignFirstResponder;

#pragma mark - Alert View Methods

+ (void)showAlert:(NSString *)strAlertMessage;
+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate;
+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag;
+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag showCancel:(BOOL)bShow;
+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag otherButtonTitle:(NSString*)strTitle;
+ (void)showAlert:(NSString *)alertTitle message:(NSString *)strAlertMessage delegateObject:(id)delegate viewTag:(int)iTag otherButtonTitle:(NSString*)strTitle secondButtonTitle:(NSString *)strSecondBtnTitle;



@end

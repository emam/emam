//
//  SearchFilterView.h
//  eMAM
//
//  Created by APPLE on 18/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum    {
    AssetTypeFilter     = 1,
    CategoryFilter      = 2,
    ProjectFilter       = 3,
    AssetSizeFilter     = 4,
    InitialFilters      = 5,
} SearchFilterDisplayType;

@protocol SearchFilterViewDelegate <NSObject>

@optional
- (void)searchFilterAssetDidFiltered;

@end


@class SearchFilterViewDelegate;

@interface SearchFilterView : UIViewController{
    
}

@property(nonatomic, retain) id <SearchFilterViewDelegate> delegate;
@property(nonatomic, assign) SearchFilterDisplayType filterType;
@property(nonatomic, assign) int categoryId;
 @property (nonatomic, strong) NSArray *searchfilters;

+ (UIImageView *)filterView;
@property (weak, nonatomic) IBOutlet UITableView *searchFtrTable;


@end

//
//  AdvancedSearchView.h
//  eMAM
//
//  Created by APPLE on 22/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum    {
    Normal          = 1,
    SearchOptions   = 2,
    SearchIn        = 3,
    RefineSearch    = 4,
    SearchCategory  = 5,
    AssetState      = 6,
    ApprovalState   = 7,
    Rating          = 8,
    UnKnown         = 9
} SearchViewType;


@protocol AdvancedSearchViewControllerDelegate <NSObject>

@optional
- (void)clearSearchResults;
- (void)filteredTheDisplayWithSearchOptions;

@end

@interface AdvancedSearchView : UIViewController

@property (nonatomic, assign) id <AdvancedSearchViewControllerDelegate> delegate;
@property (nonatomic, assign) SearchViewType currentType;
@property(nonatomic, assign) int categoryId;
@property (weak, nonatomic) IBOutlet UITableView *advTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *advSearchBar;

@end

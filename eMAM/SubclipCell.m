/**************************************************************************************
 *  File Name      : SubclipCell.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 02-01-2013
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "SubclipCell.h"

#import "AVVideoPlayer.h"

@implementation SubclipCell



@synthesize target = mtarget;
@synthesize subclip = _subclip;
@synthesize subclipTotalTimeString = _subclipTotalTimeString;
@synthesize delegate = _delegate;
@synthesize currentOrientation = _currentOrientation;

#pragma mark -

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}


- (UIButton *)updateCheckmark{
    
     UIButton *selectButton = (UIButton *)[self viewWithTag:1000];
    return selectButton;
}


- (void)initView {
    
    self.contentView.backgroundColor = [UIColor darkGrayColor];
    
    UIButton *selectButton = [Utilities checkBox];
    selectButton.tag = 1000;
    selectButton.backgroundColor = [UIColor clearColor];
    [selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:selectButton];
    [selectButton release];
    selectButton = nil;
    
    UILabel *clipName = [[UILabel alloc] init];
    clipName.tag = 1001;
    clipName.backgroundColor = [UIColor clearColor];
    clipName.textColor=[UIColor whiteColor];
    clipName.numberOfLines = 1;
    clipName.lineBreakMode = UILineBreakModeTailTruncation;
    [clipName setText:@"Clip Name"];
    clipName.font = [UIFont fontWithName:APPBOLDFONTNAME size:12.0];
    [self.contentView addSubview:clipName];
    [clipName release];
    clipName = nil;
    
    UILabel *setIn = [[UILabel alloc] init];
    setIn.tag = 1002;
    setIn.backgroundColor = [UIColor clearColor];
    setIn.textColor=[UIColor whiteColor];
    setIn.numberOfLines = 1;
    setIn.lineBreakMode = UILineBreakModeTailTruncation;
    [setIn setText:@"Set In"];
    setIn.font = [UIFont fontWithName:APPBOLDFONTNAME size:12.0];
    [self.contentView addSubview:setIn];
    [setIn release];
    setIn = nil;
    
    UILabel *setOut = [[UILabel alloc] init];
    setOut.tag = 1003;
    setOut.backgroundColor = [UIColor clearColor];
    setOut.textColor=[UIColor whiteColor];
    setOut.numberOfLines = 1;
    setOut.lineBreakMode = UILineBreakModeTailTruncation;
    [setOut  setText:@"Set Out"];
    setOut.font = [UIFont fontWithName:APPBOLDFONTNAME size:12.0];
    [self.contentView addSubview:setOut];
    [setOut  release];
    setOut = nil;
    
    UITextField *nameTextField = [[UITextField alloc] init];
    nameTextField.delegate = (id)self;
    nameTextField.tag = 1005;
    nameTextField.borderStyle = UITextBorderStyleLine;
    nameTextField.returnKeyType = UIReturnKeyNext;
    nameTextField.backgroundColor = [UIColor whiteColor];
    nameTextField.textColor = [UIColor blackColor];
    nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    nameTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:nameTextField];
    [nameTextField release];
    nameTextField = nil;
    
    UITextField *setInTextField = [[UITextField alloc] init];
    setInTextField.delegate = (id)self;
    setInTextField.tag = 1006;
    setInTextField.borderStyle = UITextBorderStyleLine;
    setInTextField.returnKeyType = UIReturnKeyNext;
    setInTextField.backgroundColor = [UIColor whiteColor];
    setInTextField.textColor = [UIColor blackColor];
    setInTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    setInTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    setInTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:setInTextField];
    [setInTextField release];
    setInTextField = nil;
    
    UITextField *setOutTextField = [[UITextField alloc] init];
    setOutTextField.delegate = (id)self;
    setOutTextField.tag = 1007;
    setOutTextField.borderStyle = UITextBorderStyleLine;
    setOutTextField.returnKeyType = UIReturnKeyNext;
    setOutTextField.backgroundColor = [UIColor whiteColor];
    setOutTextField.textColor = [UIColor blackColor];
    setOutTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    setOutTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    setOutTextField.font = [UIFont systemFontOfSize:12.0];
    
    [self.contentView addSubview:setOutTextField];
    [setOutTextField release];
    setOutTextField = nil;
    
    UIButton *closeButton = [[UIButton alloc] init];
    closeButton.tag = 1009;
    closeButton.backgroundColor = [UIColor clearColor];
    [closeButton setImage:[UIImage imageNamed:@"btn_comment_delete.png"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:closeButton];
    [closeButton release];
    closeButton = nil;
}

- (void)layoutSubviews   {
    [super layoutSubviews];
    [self setBackgroundColor:[UIColor lightGrayColor]];
    
    UIView *clipName = [self viewWithTag:1001];
    UIView *setIn = [self viewWithTag:1002];
    UIView *setOut = [self viewWithTag:1003];
    UIView *nameTextField = [self viewWithTag:1005];
    UIView *setInTextField = [self viewWithTag:1006];
    UIView *setOutTextField = [self viewWithTag:1007];
    
    UIView *selectButton = [self viewWithTag:1000];
    selectButton.frame = CGRectMake(10, 25, 20, 20);
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        clipName.frame = CGRectMake(40,0, 300,20);
        setIn.frame = CGRectMake(360,0, 120,20);
        setOut .frame = CGRectMake(500,0, 120,20);
        
        nameTextField.frame = CGRectMake(40, 25, 300, 25);
        setInTextField.frame = CGRectMake(360, 25, 120, 25);
        setOutTextField.frame = CGRectMake(500, 25, 120, 25);
    } else {
        clipName.frame = CGRectMake(40,0, 300,20);
        setIn.frame = CGRectMake(360,0, 120,20);
        setOut .frame = CGRectMake(500,0, 120,20);
        
        nameTextField.frame = CGRectMake(40, 25, 300, 25);
        setInTextField.frame = CGRectMake(360, 25, 120, 25);
        setOutTextField.frame = CGRectMake(500, 25, 120, 25);
    }
    
    UIView *closeButton = [self viewWithTag:1009];
    closeButton.frame = CGRectMake(710.0, 2.0, 25.0, 25.0);
    
    
}


-(NSString *)modText:(NSString *)text{
    
    NSString *firstSection = [text substringWithRange:NSMakeRange(0,2)];
    NSString *secondSection = [text substringWithRange:NSMakeRange(2,2)];
    NSString *thirdSection = [text substringWithRange:NSMakeRange(4,2)];
    NSString *fourthSection = [text substringWithRange:NSMakeRange(6,2)];
    
    NSString *returnStr=[NSString stringWithFormat:@"%@:%@:%@:%@",firstSection,secondSection,thirdSection,fourthSection];
    return returnStr;
}

- (void)fillContent  {
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1005];
    UITextField *setInTextField = (UITextField *)[self viewWithTag:1006];
    UITextField *setOutTextField = (UITextField *)[self viewWithTag:1007];
    
    nameTextField.text = self.subclip.clipName;
    setInTextField.text = [self modText:self.subclip.setInTimeString];
    setOutTextField.text = [self modText:self.subclip.setOutTimeString];
}
- (void)saveContent {
    
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1005];
    UITextField *setInTextField = (UITextField *)[self viewWithTag:1006];
    UITextField *setOutTextField = (UITextField *)[self viewWithTag:1007];
     UIButton *selectButton = (UIButton *)[self viewWithTag:1000];
    
    self.subclip.clipName = nameTextField.text;
    self.subclip.setInTimeString = [setInTextField.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    self.subclip.setOutTimeString = [setOutTextField.text stringByReplacingOccurrencesOfString:@":" withString:@""];
     //[selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    // To Modify the Shared Store used while saving.
    NSUInteger index = [[[AVVideoPlayer sharedInstance] videoSubclips] indexOfObject:self.subclip];
    
    
    NSLog(@"Subclip IndeX:%d",index);
    
    if (index != NSNotFound) {
        [[[AVVideoPlayer sharedInstance] videoSubclips] replaceObjectAtIndex:index withObject:self.subclip];
    }
}


- (void)saveContentFinish {
    
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1005];
    UITextField *setInTextField = (UITextField *)[self viewWithTag:1006];
    UITextField *setOutTextField = (UITextField *)[self viewWithTag:1007];
    UIButton *selectButton = (UIButton *)[self viewWithTag:1000];
    
    self.subclip.clipName = nameTextField.text;
    self.subclip.setInTimeString = [setInTextField.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    self.subclip.setOutTimeString = [setOutTextField.text stringByReplacingOccurrencesOfString:@":" withString:@""];
    //[selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    // To Modify the Shared Store used while saving.
    NSUInteger index = [[[AVVideoPlayer sharedInstance] videoSubclips] indexOfObject:self.subclip];
    
    
    NSLog(@"Subclip IndeX:%d",index);
    
    NSLog(@"Subclip IndeX:%@",self.subclipTotalTimeString);
    
    int frameDuration=[self.subclipTotalTimeString intValue];
    NSLog(@"frameDuration:%d",frameDuration);
    NSLog(@"self.subclip.setOutTimeString:%d",[self.subclip.setOutTimeString intValue]);
    
    if ([self.subclip.setOutTimeString intValue]>frameDuration) {
        
        setOutTextField.text=[self modText:self.subclipTotalTimeString];
        self.subclip.setOutTimeString = [setOutTextField.text stringByReplacingOccurrencesOfString:@":" withString:@""];
        [Utilities showAlert:@"Set out time must be less than video duration and greater than set in time"];
        
    }else{
        
        if (index != NSNotFound) {
            [[[AVVideoPlayer sharedInstance] videoSubclips] replaceObjectAtIndex:index withObject:self.subclip];
        }
        
        
//        self.subclip.setOutTimeString = [NSString  stringWithFormat:@"%8d",frameDuration];
//        if (index != NSNotFound) {
//            [[[AVVideoPlayer sharedInstance] videoSubclips] replaceObjectAtIndex:index withObject:self.subclip];
//        }
    }
    
}


- (void)selectButtonClicked:(UIButton *)sender  {
    
    sender.selected = !sender.selected;
    if (self.target && [self.target respondsToSelector:@selector(selectSubClip:)]) {
        [self.target performSelector:@selector(selectSubClip:) withObject:self.subclip];
    }
}

- (void)deleteButtonClicked:(id)sender  {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteSubclips:)]) {
        [self.delegate deleteSubclips:[NSArray arrayWithObjects:self.subclip, nil]];
    }
}

#pragma mark - Override Setters

- (void)setCurrentOrientation:(UIInterfaceOrientation)currentOrientation    {
    _currentOrientation = currentOrientation;
    [self layoutIfNeeded];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor   {
    UIView *backGroundView = [self.contentView viewWithTag:1011];
    backGroundView.backgroundColor =  backgroundColor;
    self.contentView.backgroundColor = [UIColor darkGrayColor];
}

- (void)setSubclip:(Subclip *)subclip  {
    
    _subclip = subclip;
    [self fillContent];
}

#pragma mark - UITextField Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField   {
    [self saveContent];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self saveContentFinish];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    [textField resignFirstResponder];
    return NO;
}

@end

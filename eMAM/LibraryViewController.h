//
//  LibraryViewController.h
//  eMAM
//
//  Created by APPLE on 28/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchFilterView.h"
#import "SortOptionsView.h"
#import "AdvancedSearchView.h"
#import "EBinViewController.h"
#import "AssetViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
// Old
@interface LibraryViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,MBProgressHUDDelegate,UIPopoverControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    
    UIPopoverController *popoverController;
    UIPopoverController *popoverControllerAdvSearch;
    SearchFilterView *searchFilterController;
    AdvancedSearchView *advSearchFilterController;
    UIPopoverController *popoverControllerownloads;
    UINavigationController *advSearchNavigationController;
    UIImage * PickedImage;
    NSString * encodedIngestImage;
    NSString * UploadImagename;
    NSString * uploadimageFormat;
    NSString * UploadVideoURL;
    NSString * ingestId;
    int isPlay;
    NSURL *ContentUrl;
    int is_Video;
    NSMutableArray * IngestIdArray;
    NSMutableDictionary * IngestIdDict;
    int isPickerShown;
    NSTimer *UploadTimer;
    int percentageUploaded;
    int is_UploadCompleted;
    MBProgressHUD *hud;
    UITapGestureRecognizer *ingestRecognizer;
}
@property (weak, nonatomic) IBOutlet UICollectionView *libraryCollectionView;
@property (nonatomic, strong) NSMutableArray *assetsArray;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn;
@property (weak, nonatomic) IBOutlet UIButton *eBinBtn;
@property (weak, nonatomic) IBOutlet UISearchBar *assetSearchBar;
@property (weak, nonatomic) IBOutlet UIButton *advSearchBtn;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)logOutTapped:(id)sender;
- (IBAction)sortTapped:(id)sender;
- (IBAction)eBinTapped:(id)sender;
- (IBAction)advancedSearchedTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *pgNoLbl;
- (IBAction)downTapped:(id)sender;
- (IBAction)upTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *searchFilterBtn;
- (IBAction)searchFilterTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *down;
@property (weak, nonatomic) IBOutlet UIButton *up;
- (IBAction)downloadsPopUpTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *ingestContainer;
@property (weak, nonatomic) IBOutlet UIImageView *ingestImageView;

@property (weak, nonatomic) IBOutlet UITextField *authorText;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UITextField *titleText;
@property (weak, nonatomic) IBOutlet UITextField *ingestIdText;

@property (weak, nonatomic) IBOutlet UIView *moviewPlayer;
@property (strong, nonatomic) MPMoviePlayerController *videoController;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIView *pickerContainer;

@property (weak, nonatomic) IBOutlet UITableView *IngestPicker;

@property (weak, nonatomic) IBOutlet UIButton *SubmitButton;

@property (weak, nonatomic) IBOutlet UIButton *CancelButton;
@property (weak, nonatomic) IBOutlet UIButton *showPickerButton;
@property (weak, nonatomic) IBOutlet UIScrollView *containerScroll;

@end

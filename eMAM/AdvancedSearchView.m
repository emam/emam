//
//  AdvancedSearchView.m
//  eMAM
//
//  Created by APPLE on 22/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AdvancedSearchView.h"
#import "AssetService.h"

@interface AdvancedSearchView (){
    
    
}

@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (nonatomic, retain) NSMutableArray *options;

@end

@implementation AdvancedSearchView

@synthesize delegate;
@synthesize currentType;
@synthesize selectedIndexPath;
@synthesize options;
@synthesize categoryId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setViewTitle];
    
    [self loadDatas];
    
    [self showSearchBar];
    
    if (self.currentType == SearchCategory) {
        if (![Datastore sharedStore].searchCategories) {
            [self getAllCategories];
        }
    }
    
    self.preferredContentSize = self.view.frame.size;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self updateDatas];
    
}

#pragma mark - Data Handler

- (void)getAllCategories  {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:hud];
    hud.labelText = @"Fetching Categories...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        AssetService *assetService = [AssetService sharedInstance];
        [assetService getAllCategoriesForSearchPerformRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (dictionary) {
                dictionary = [dictionary objectForKey:@"GetCategoriesResponse"];
                NSDictionary *xmlResponse = [[[dictionary objectForKey:@"GetCategoriesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
                id categoryList = [xmlResponse objectForKey:@"Table"];
                if ([categoryList isKindOfClass:[NSArray class]]) {
                    [[Datastore sharedStore] setSearchCategories:categoryList];
                } else if ([categoryList isKindOfClass:[NSDictionary class]]) {
                    [[Datastore sharedStore] setSearchCategories:[NSArray arrayWithObject:categoryList]];
                }
            }else{
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
            NSLog(@"EMAM SearchViewController : Get All Search Categories : %d",[[[Datastore sharedStore] searchCategories] count]);
            [_advTableView reloadData];
        }];
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}


- (void)updateDatas {
    _advSearchBar.text = [Datastore sharedStore].searchString;
    
    for (NSDictionary *dictionary in self.options) {
        if ([[dictionary objectForKey:@"Title"] isEqualToString:@"Search Options:"]) {
            NSString *title = [Datastore sharedStore].advancedSearchOption;
            if (title) {
                [dictionary setValue:title forKey:@"RightTitle"];
            }
        } else if ([[dictionary objectForKey:@"Title"] isEqualToString:@"Rating:"]) {
            NSString *title = [self ratingStringAtIndex:[Datastore sharedStore].advancedSearchRating];
            if (title) {
                [dictionary setValue:title forKey:@"RightTitle"];
            }
        }
        else if ([[dictionary objectForKey:@"Title"] isEqualToString:@"Category:"]) {
            NSDictionary *category = [[[Datastore sharedStore] searchCategoriesWithID:[Datastore sharedStore].advancedSearchCategoryId] lastObject];
            NSString *title = [[category objectForKey:@"CATEGORY_NAME"] objectForKey:@"text"];
            if (title) {
                [dictionary setValue:title forKey:@"RightTitle"];
            }
        }
    }
    [_advTableView reloadData];
}



#pragma mark -

- (NSString *)searchOptionStringAtIndex:(int)index  {
    NSString *title = nil;
    switch (index) {
        case 0:
            title = @"Exact Match";
            break;
        case 1:
            title = @"Contains this word";
            break;
        case 2:
            title = @"Contain all these words";
            break;
        case 3:
            title = @"Contains any of these words";
            break;
            
        default:
            break;
    }
    return title;
}

- (NSString *)assetStateStringAtIndex:(int)index  {
    NSString *title = nil;
    switch (index) {
        case 0:
            title = @"Online Storage";
            break;
        case 1:
            title = @"Archived";
            break;
            
        default:
            break;
    }
    return title;
}

- (NSString *)ratingStringAtIndex:(int)index  {
    NSString *title = nil;
    switch (index) {
        case 0:
            title = @"All";
            break;
        case 1:
            title = @"1";
            break;
        case 2:
            title = @"2";
            break;
        case 3:
            title = @"3";
            break;
        case 4:
            title = @"4";
            break;
        case 5:
            title = @"5";
            break;
            
        default:
            break;
    }
    return title;
}







#pragma mark -

- (void)switchViewValueChanged:(UISwitch *)sender   {
    if (sender.tag == 101) {
        [Datastore sharedStore].needFullTextSearch = sender.isOn;
    } else if (sender.tag == 202) {
        [Datastore sharedStore].includeSubCategoriesInSearch = sender.isOn;
    }
}

- (SearchViewType)selectedIndexType:(NSIndexPath *)indexPath    {
    SearchViewType viewType = UnKnown;
    BEGIN_BLOCK
    if (indexPath.section == 0 && indexPath.row == 0) {
        viewType = SearchOptions;
        break;
    }
    if (indexPath.section == 1) {
        viewType = SearchIn;
        break;
    }
    if (indexPath.section == 2) {
        viewType = RefineSearch;
        break;
    }
    if (indexPath.section == 3 && indexPath.row == 0) {
        viewType = SearchCategory;
        break;
    }
    if (indexPath.section == 4) {
        viewType = AssetState;
        break;
    }
    if (indexPath.section == 5) {
        viewType = ApprovalState;
        break;
    }
    if (indexPath.section == 6) {
        viewType = Rating;
        break;
    }
    
    END_BLOCK
    
    return viewType;
}

#pragma mark

- (UIButton *) customDisclosureButton   {
    UIButton * button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    [button addTarget: self
               action: @selector(accessoryButtonTapped:withEvent:)
     forControlEvents: UIControlEventTouchUpInside];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateHighlighted];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateSelected];
    button.tintColor=[UIColor darkGrayColor];
    
    return (button);
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event   {
    NSIndexPath * indexPath = [_advTableView indexPathForRowAtPoint:[[[event touchesForView: button] anyObject] locationInView:_advTableView]];
    if ( indexPath == nil )
        return;
    
    [_advTableView.delegate tableView:_advTableView accessoryButtonTappedForRowWithIndexPath:indexPath];
}


#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
    int icount = 1;
    switch (self.currentType) {
        case Normal:    {
            icount = 7;
            break;
        }
        default:
            break;
    }
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    int icount = 1;
    
    switch (self.currentType) {
        case Normal:    {
            if (section == 0 || section == 3) {
                icount = 2;
            }
            break;
        }
        case SearchOptions:
        case RefineSearch:
        case ApprovalState:{
            icount = 4;
            break;
        }
        case SearchIn:    {
            icount = 11;
            break;
        }
        case SearchCategory:    {
            icount = [[[Datastore sharedStore] subSearchCategoriesWithParentID:self.categoryId] count];
            break;
        }
        case AssetState:    {
            icount = 2;
            break;
        }
        case Rating:    {
            icount = 6;
            break;
        }
        default:
            break;
    }
    
    return icount;
}




// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"SearchCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
            cell.detailTextLabel.font = [UIFont fontWithName:APPFONTNAME size:12.0];
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
        
        // Configure the cell...
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;
        cell.accessoryView = nil;
        
        int cellPosition = indexPath.section;
        if (self.currentType == Normal) {
            if (indexPath.section == 3) {
                if (indexPath.row > 0) {
                    cellPosition += 1;
                }
                cellPosition += 1;
            } else if (indexPath.section > 3) {
                cellPosition += 2;
            } else if (indexPath.section == 0) {
                if (indexPath.row > 0) {
                    cellPosition += 1;
                }
            } else if (indexPath.section >= 1) {
                cellPosition += 1;
            }
        }
        else {
            cellPosition = indexPath.row;
        }
        
        if (self.currentType != SearchCategory) {
            if ([self.options count] > cellPosition) {
                
                NSDictionary *dictionary = [self.options objectAtIndex:cellPosition];
                cell.textLabel.text = [dictionary objectForKey:@"Title"];
                
                if ([[dictionary objectForKey:@"RightViewType"] isEqualToString:@"Text"]) {
                    cell.detailTextLabel.text = [dictionary objectForKey:@"RightTitle"];
                }
                else if ([[dictionary objectForKey:@"RightViewType"] isEqualToString:@"Switch"]) {
                    UISwitch *switchView = [[UISwitch alloc] init];
                    [switchView setFrame:CGRectZero];
                    
                    BOOL setOn = NO;
                    if ([[dictionary objectForKey:@"Title"] isEqualToString:@"Fulltext Search:"]) {
                        switchView.tag = 101;
                        setOn = [Datastore sharedStore].needFullTextSearch;
                    } else if ([[dictionary objectForKey:@"Title"] isEqualToString:@"Include Subcategories:"]) {
                        switchView.tag = 202;
                        setOn = [Datastore sharedStore].includeSubCategoriesInSearch;
                    }
                    [switchView setOn:setOn animated:NO];
                    [switchView addTarget:self action:@selector(switchViewValueChanged:) forControlEvents:UIControlEventValueChanged];
                    cell.accessoryView = switchView;
                    switchView = nil;
                }
                
                if ([[dictionary objectForKey:@"AccessoryArrowNeeded"] boolValue]) {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                else if (self.currentType != Normal) {
                    NSString *selectedTitle = nil;
                    switch (self.currentType) {
                        case SearchOptions:
                            selectedTitle = [Datastore sharedStore].advancedSearchOption;
                            break;
                        case SearchIn:  {
                            if ([[Datastore sharedStore].advancedSearchInOptions containsObject:cell.textLabel.text]) {
                                selectedTitle = cell.textLabel.text;
                            }
                            break;
                        }
                        case RefineSearch:  {
                            if ([[Datastore sharedStore].advancedRefineSearchOptions containsObject:cell.textLabel.text]) {
                                selectedTitle = cell.textLabel.text;
                            }
                            break;
                        }
                        case AssetState:  {
                            if ([[Datastore sharedStore].advancedSearchAssetStates containsObject:cell.textLabel.text]) {
                                selectedTitle = cell.textLabel.text;
                            }
                            break;
                        }
                        case ApprovalState:  {
                            if ([[Datastore sharedStore].advancedSearchApprovalStates containsObject:cell.textLabel.text]) {
                                selectedTitle = cell.textLabel.text;
                            }
                            break;
                        }
                        case Rating:
                            selectedTitle = [self ratingStringAtIndex:[Datastore sharedStore].advancedSearchRating];
                            break;
                            
                        default:
                            break;
                    }
                    
                    if ([cell.textLabel.text isEqualToString:selectedTitle]) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        self.selectedIndexPath = indexPath;
                    }
                }
            }
        } else {
            
            NSDictionary *category = [[[Datastore sharedStore] subSearchCategoriesWithParentID:self.categoryId] objectAtIndex:indexPath.row];
            cell.textLabel.text = [[category objectForKey:@"CATEGORY_NAME"] objectForKey:@"text"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            int catId = [[[category objectForKey:@"ID"] objectForKey:@"text"] intValue];
            category = nil;
            
            NSArray *subCategory = [[Datastore sharedStore] subSearchCategoriesWithParentID:catId];
            if ([subCategory count] > 0) {
                cell.accessoryView = [self customDisclosureButton];
            }
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM SearchViewController : Exception for cellForAtIndexPath in SearchFilterViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    BOOL needPop = NO;
    int enableCheckMarkStatus = 0;
    
    switch (self.currentType) {
        case Normal:    {
            SearchViewType viewType = [self selectedIndexType:indexPath];
            if (viewType != UnKnown) {
                AdvancedSearchView *searchViewController = [[AdvancedSearchView alloc] init];
                searchViewController.currentType = viewType;
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                [self.navigationController pushViewController:searchViewController animated:YES];
                searchViewController = nil;
            }
            break;
        }
        case SearchOptions: {
            [[Datastore sharedStore] setAdvancedSearchOption:[self searchOptionStringAtIndex:indexPath.row]];
            enableCheckMarkStatus = 0;
            needPop = YES;
            break;
        }
        case SearchIn: {
            NSDictionary *dictionary = [self.options objectAtIndex:indexPath.row];
            NSString *searchInTitle = [dictionary objectForKey:@"Title"];
            if ([[Datastore sharedStore].advancedSearchInOptions containsObject:searchInTitle]) {
                [[Datastore sharedStore].advancedSearchInOptions removeObject:searchInTitle];
            } else {
                [[Datastore sharedStore].advancedSearchInOptions addObject:searchInTitle];
            }
            enableCheckMarkStatus = 1;
            break;
        }
        case RefineSearch: {
            NSDictionary *dictionary = [self.options objectAtIndex:indexPath.row];
            NSString *refineSearchTitle = [dictionary objectForKey:@"Title"];
            if ([[Datastore sharedStore].advancedRefineSearchOptions containsObject:refineSearchTitle]) {
                [[Datastore sharedStore].advancedRefineSearchOptions removeObject:refineSearchTitle];
            } else {
                [[Datastore sharedStore].advancedRefineSearchOptions addObject:refineSearchTitle];
            }
            enableCheckMarkStatus = 1;
            break;
        }
        case AssetState: {
            NSDictionary *dictionary = [self.options objectAtIndex:indexPath.row];
            NSString *assetStateTitle = [dictionary objectForKey:@"Title"];
            if ([[Datastore sharedStore].advancedSearchAssetStates containsObject:assetStateTitle]) {
                [[Datastore sharedStore].advancedSearchAssetStates removeObject:assetStateTitle];
            } else {
                [[Datastore sharedStore].advancedSearchAssetStates addObject:assetStateTitle];
            }
            enableCheckMarkStatus = 1;
            break;
        }
        case ApprovalState: {
            NSDictionary *dictionary = [self.options objectAtIndex:indexPath.row];
            // always remove all to ensure not select 'All' if any other selected.
            [[Datastore sharedStore].advancedSearchApprovalStates removeObject:@"All"];
            NSString *approvalStateTitle = [dictionary objectForKey:@"Title"];
            if ([[Datastore sharedStore].advancedSearchApprovalStates containsObject:approvalStateTitle]) {
                [[Datastore sharedStore].advancedSearchApprovalStates removeObject:approvalStateTitle];
            } else {
                [[Datastore sharedStore].advancedSearchApprovalStates addObject:approvalStateTitle];
            }
            
            
            if (indexPath.row == 0) { // first cell selection
                [[Datastore sharedStore] setAdvancedSearchApprovalStates:[NSMutableArray arrayWithObjects:@"All", nil]];
                
                for (int row = 1; row <= 3; row++) {
                    UITableViewCell *cell = [tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                
                enableCheckMarkStatus = 1;
            } else {
                self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0]; // First cell
                enableCheckMarkStatus = 0;
            }
            
            break;
        }
        case Rating:    {
            [[Datastore sharedStore] setAdvancedSearchRating:indexPath.row];
            enableCheckMarkStatus = 0;
            needPop = YES;
            break;
        }
        case SearchCategory:  {
            NSMutableArray *categories = [[Datastore sharedStore] subSearchCategoriesWithParentID:self.categoryId];
            if ([categories count] > indexPath.row) {
                NSDictionary *category = [categories objectAtIndex:indexPath.row];
                int catId = [[[category objectForKey:@"ID"] objectForKey:@"text"] intValue];
                category = nil;
                
                [[Datastore sharedStore] setAdvancedSearchCategoryId:catId];
                needPop = YES;
            }
            break;
        }
        default:
            break;
    }
    
    switch (enableCheckMarkStatus) {
        case 0: { // Means Unique Select.
            // Deselect old selected index
            if (self.selectedIndexPath) {
                UITableViewCell *cell = [tableview cellForRowAtIndexPath:self.selectedIndexPath];
                if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            // break;
        }
        case 1: {
            UITableViewCell *cell = [tableview cellForRowAtIndexPath:indexPath];
            if (cell.accessoryType == UITableViewCellAccessoryNone) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else  {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            break;
        }
        default:
            break;
    }
    self.selectedIndexPath = indexPath;
    
    if (needPop) {
        self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableview accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath    {
    switch (self.currentType) {
        case SearchCategory:    {
            NSMutableArray *categories = [[Datastore sharedStore] subSearchCategoriesWithParentID:self.categoryId];
            if ([categories count] > indexPath.row) {
                
                NSDictionary *category = [categories objectAtIndex:indexPath.row];
                int catId = [[[category objectForKey:@"ID"] objectForKey:@"text"] intValue];
                
                NSArray *subCategory = [[Datastore sharedStore] subSearchCategoriesWithParentID:catId];
                if ([subCategory count] > 0) {
                    AdvancedSearchView *searchfilter = [[AdvancedSearchView alloc] init];
                    searchfilter.currentType = SearchCategory;
                    searchfilter.categoryId = catId;
                    searchfilter.title = [[category objectForKey:@"CATEGORY_NAME"] objectForKey:@"text"];
                    self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                    [self.navigationController pushViewController:searchfilter animated:YES];
                }
                category = nil;
            }
            break;
        }
            
        default:
            break;
    }
}




- (void) showSearchBar {
    
    _advSearchBar.placeholder = @"Search Assets";
    
}

- (void)loadInitialSearchDatas   {
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    // 0
    [dictionary setValue:@"Search Options:" forKey:@"Title"];
    [dictionary setValue:@"Text" forKey:@"RightViewType"];
    [dictionary setValue:@"Contains any of these words" forKey:@"RightTitle"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 1
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Fulltext Search:" forKey:@"Title"];
    [dictionary setValue:@"Switch" forKey:@"RightViewType"];
    [self.options addObject:dictionary];
    // 2
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Search In:" forKey:@"Title"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 3
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Refine Search:" forKey:@"Title"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 4
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Category:" forKey:@"Title"];
    [dictionary setValue:@"Text" forKey:@"RightViewType"];
    [dictionary setValue:@"All" forKey:@"RightTitle"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 5
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Include Subcategories:" forKey:@"Title"];
    [dictionary setValue:@"Switch" forKey:@"RightViewType"];
    [self.options addObject:dictionary];
    // 6
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Asset State:" forKey:@"Title"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 7
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Approval State:" forKey:@"Title"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
    // 8
    dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:@"Rating:" forKey:@"Title"];
    [dictionary setValue:@"Text" forKey:@"RightViewType"];
    [dictionary setValue:@"All" forKey:@"RightTitle"];
    [dictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AccessoryArrowNeeded"];
    [self.options addObject:dictionary];
}



- (void)loadDatas   {
    self.options = [NSMutableArray array];
    
    switch (self.currentType) {
        case Normal:    {
            [self loadInitialSearchDatas];
            break;
        }
        case SearchOptions: {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"Exact Match" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Contains this word" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 2
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Contain all these words" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 3
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Contains any of these words" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        case SearchIn:    {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"Title" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Ingested by" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 2
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Closed Caption" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 3
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Asset ID" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 4
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Description" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 5
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Asset tags" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 6
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Embedded Metadata" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 7
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"UUID" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 8
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Author" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 9
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Comments" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 10
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Custom Metadata" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        case RefineSearch:{
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"Video" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Audio" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 2
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Image" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 3
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Other Files" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        case SearchCategory:    {
            
            break;
        }
        case AssetState:    {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"Online Storage" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Archived" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        case ApprovalState:{
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"All" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Approved" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 2
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Rejected" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 3
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"Pending for approval" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        case Rating:    {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            // 0
            [dictionary setValue:@"All" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 1
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"1" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 2
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"2" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 3
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"3" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 4
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"4" forKey:@"Title"];
            [self.options addObject:dictionary];
            // 5
            dictionary = [NSMutableDictionary dictionary];
            [dictionary setValue:@"5" forKey:@"Title"];
            [self.options addObject:dictionary];
            break;
        }
        default:
            break;
    }
}


#pragma mark - UISearchBar Delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar   {
    if (self.currentType != Normal) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return NO;
    }
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchbar {
    [searchbar resignFirstResponder];
    if ([searchbar.text length] != 0) {
        [Datastore sharedStore].searchString = searchbar.text;
        [Datastore sharedStore].isInAdvancedSearch = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(filteredTheDisplayWithSearchOptions)]) {
            [self.delegate filteredTheDisplayWithSearchOptions];
        }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Clear the current result when the user clear the query
    if ([searchText length] == 0) {
        [Datastore sharedStore].searchString = @"";
        [Datastore sharedStore].isInAdvancedSearch = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(clearSearchResults)]) {
            [self.delegate clearSearchResults];
        }
    } else {
        [Datastore sharedStore].searchString = searchText;
    }
}


- (void)setViewTitle  {
    switch (self.currentType) {
        case Normal:
            self.title = @"Advance Search";
            break;
        case SearchOptions:
            self.title = @"Search Options";
            break;
        case SearchIn:
            self.title = @"Search In Options";
            break;
        case RefineSearch:
            self.title = @"Refine Search";
            break;
        case SearchCategory:
            self.title = @"Category";
            break;
        case AssetState:
            self.title = @"Asset State";
            break;
        case ApprovalState:
            self.title = @"Approval State";
            break;
        case Rating:
            self.title = @"Rating";
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

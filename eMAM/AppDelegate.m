//
//  AppDelegate.m
//  eMAM
//
//  Created by APPLE on 24/07/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFNetworkReachabilityManager.h"
#import "AFHTTPRequestOperation.h"
#import "Login.h"


#pragma mark -

@implementation UIColor (HexAdditions)

// Create a color using a hex RGB value
// ex. [UIColor colorWithHexValue: 0x03047F]
+ (UIColor *)colorWithHexValue:(NSInteger)rgbValue {
    return [UIColor colorWithRed:((float) ((rgbValue & 0xFF0000) >> 16)) / 255.0
                           green:((float) ((rgbValue & 0xFF00) >> 8)) / 255.0
                            blue:((float) (rgbValue & 0xFF)) / 255.0
                           alpha:1.0];
    
}

@end

@implementation AppDelegate
@synthesize failedMarkerRequests;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSLog(@"eMAM AppDelegate didFinishLaunchingWithOptions");
    failedMarkerRequests=[[NSMutableArray alloc] init];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        if (!AFNetworkReachabilityStatusNotReachable) {
            [self performFailedRequests];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024 diskCapacity:20 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    [self registerDefaultsFromSettingsBundle];
    
    Login *loginViewController = [[Login alloc] initWithNibName:@"Login" bundle:nil];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    self.navigationController.navigationBarHidden=YES;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = UIColorFromRGB(0x585858);
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"%@", url);
    NSLog(@"%@", annotation);
    return true;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    //NSLog(@"eMAM AppDelegate applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //NSLog(@"eMAM AppDelegate applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    \
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //NSLog(@"eMAM AppDelegate applicationWillEnterForeground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //NSLog(@"eMAM AppDelegate applicationDidBecomeActive");
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self registerDefaultsFromSettingsBundle];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:KSHAREKEY];
    [shared setObject: @"False" forKey:@"eMAMLoggedIn"];
    [shared synchronize];
    //NSLog(@"eMAM AppDelegate applicationWillTerminate");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Backgrounding Methods -
-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    
    [TWRDownloadManager sharedManager].backgroundTransferCompletionHandler = completionHandler;
    
}
#pragma mark Settings Bundle


-(void)performFailedRequests{
    
   /*
    NSLog(@"performFailedRequests");
    NSLog(@"Failed Requests:%@",MY_APP_DELEGATE.failedMarkerRequests);
    NSLog(@"Failed Requests Count:%lu",(unsigned long)[MY_APP_DELEGATE.failedMarkerRequests count]);
    
    int requestCount=(int)[MY_APP_DELEGATE.failedMarkerRequests count];
    
    for (int i=0; i<requestCount; i++) {
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:[MY_APP_DELEGATE.failedMarkerRequests objectAtIndex:i]];
        
        

        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"Failure sendRequestASI Response: %@", responseObject);

            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Failure sendRequestASI Response Error: %@", error);
            
            
            
        }];
        [[NSOperationQueue mainQueue] addOperation:operation];
        
        if (i==(requestCount-1)) {
            [MY_APP_DELEGATE.failedMarkerRequests removeAllObjects];
        }
    }
    
    */
}

- (void)registerDefaultsFromSettingsBundle  {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"eMAM AppDelegate registerDefaultsFromSettingsBundle : Could not find Settings.bundle");
        return;
    }else{
        
        NSLog(@"eMAM AppDelegate registerDefaultsFromSettingsBundle : Found Settings.bundle");
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        NSString *defaultValue = [prefSpecification objectForKey:@"DefaultValue"];
        if(key && defaultValue) {
            [defaultsToRegister setObject:defaultValue forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    }
}



@end

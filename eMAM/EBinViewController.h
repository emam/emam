//
//  EBinViewController.h
//  eMAM
//
//  Created by APPLE on 22/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetViewController.h"

@interface EBinViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
- (IBAction)doneTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *eBinCollecnView;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *delAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIButton *downldBtn;
- (IBAction)deleteAllTapped:(id)sender;
- (IBAction)downldTapped:(id)sender;
- (IBAction)eSendTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *pgNoLbl;
- (IBAction)upTapped:(id)sender;
- (IBAction)downTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *up;
@property (weak, nonatomic) IBOutlet UIButton *down;

@end

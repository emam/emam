/**************************************************************************************
 *  File Name      : Marker.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 15-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <Foundation/Foundation.h>

#import "AVVideoPlayer.h"

@interface Marker : NSObject

@property (nonatomic, strong) NSString *markerId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *timeCode;
@property (nonatomic, strong) NSString *duration;

@property (nonatomic, strong) UIButton *markerControl;
@property (nonatomic, assign) CMTime   markerTime;

- (id)initWithDictionary:(NSDictionary *) dict;

@end

/**************************************************************************************
 *  File Name      : MPMoviePlayer.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 07-10-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "MPMoviePlayer.h"

@interface MPMoviePlayer ()

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

@end

#pragma mark -

@implementation MPMoviePlayer

@synthesize moviePlayerController;

#pragma mark  - Movie Notification Handlers

/* Register observers for the various movie object notifications. */
-(void)installMovieNotificationObservers    {
    MPMoviePlayerController *player = [self moviePlayerController];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieDurationAvailableNotification:)
                                                 name:MPMovieDurationAvailableNotification
                                               object:player];
}

/* Remove the movie notification observers from the movie object. */
-(void)removeMovieNotificationHandlers  {
    MPMoviePlayerController *player = [self moviePlayerController];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:player];
}

#pragma mark - Movie Settings

/* Apply user movie preference settings (these are set from the Settings: iPhone Settings->Movie Player)
 for scaling mode, control style, background color, repeat mode, application audio session, background
 image and AirPlay mode.
 */
-(void)applyDefaultUserSettingsToMoviePlayer    {
    MPMoviePlayerController *player = [self moviePlayerController];
    if (player) {
        
        player.view.frame = CGRectMake(0, 0, (self.frame.size.width), (self.frame.size.height - 10));
        //        player.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
        
        player.scalingMode = MPMovieScalingModeAspectFit;
        player.controlStyle = MPMediaTypeMusicVideo; //MPMovieControlStyleEmbedded;
        player.backgroundView.backgroundColor = [UIColor blackColor];
        player.repeatMode = MPMovieRepeatModeNone;
        player.useApplicationAudioSession = YES;
        
        /* Indicate the movie player allows AirPlay movie playback. */
        player.allowsAirPlay = YES;
    }
}

#pragma mark - Initialization

- (id)initWithContentURL:(NSURL *)movieURL AndFrame:(CGRect)frame  {
    self = [super init];
    if (self) {
        self.frame = frame;
        self.backgroundColor = [UIColor blackColor];
        //        self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
        // to avoid crashing.
        movieURL = (movieURL)? movieURL : [NSURL URLWithString:@""];
        
        /* Create a new movie player object. */
        MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
        player.shouldAutoplay = NO;
        [player prepareToPlay];
        self.moviePlayerController = player;
        player = nil;
        
        if (self.moviePlayerController) {
            /* Register the current object as an observer for the movie
             notifications. */
            [self installMovieNotificationObservers];
            
            /* Specify the URL that points to the movie file. */
            [self.moviePlayerController setContentURL:movieURL];
            
            /* If you specify the movie type before playing the movie it can result
             in faster load times. */
            MPMovieSourceType movieSourceType = MPMovieSourceTypeUnknown;
            if ([movieURL isFileURL]) {
                movieSourceType = MPMovieSourceTypeFile;
            }
            else { /* If we have a streaming url then specify the movie source type. */
                //                movieSourceType = MPMovieSourceTypeStreaming;
                movieSourceType = MPMovieSourceTypeUnknown;
            }
            [self.moviePlayerController setMovieSourceType:movieSourceType];
            
            /* Apply the default user movie preference settings to the movie player object. */
            [self applyDefaultUserSettingsToMoviePlayer];
            
            [self addSubview:self.moviePlayerController.view];
        }
    }
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor   {
    self.moviePlayerController.backgroundView.backgroundColor = backgroundColor;
}

- (void)setURL:(NSURL *)url {
    if (self.moviePlayerController) {
        [self.moviePlayerController pause];
        [self.moviePlayerController setContentURL:url];
        [self.moviePlayerController prepareToPlay];
    }
}

- (void)start   {
    if (self.moviePlayerController) {
        [self.moviePlayerController play];
    }
}

- (void)stop    {
    if (self.moviePlayerController) {
        [self.moviePlayerController pause];
        [self.moviePlayerController stop];
    }
}




- (void)pause    {
    if (self.moviePlayerController) {
        [self.moviePlayerController pause];
        }
}

- (void)play    {
    if (self.moviePlayerController) {
        [self.moviePlayerController play];
    }
}



- (void)remove {
    if (self.moviePlayerController) {
        [self.moviePlayerController stop];
        [self.moviePlayerController.view removeFromSuperview];
        self.moviePlayerController = nil;
    }
}

#pragma mark Movie Notification Handlers

/*  Notification called when the movie finished playing. */
- (void) moviePlayBackDidFinish:(NSNotification*)notification   {
    @try {
        NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
        switch ([reason integerValue])  {
                /* The end of the movie was reached. */
            case MPMovieFinishReasonPlaybackEnded:
                /*
                 Add your code here to handle MPMovieFinishReasonPlaybackEnded.
                 */
                break;
                
                /* An error was encountered during playback. */
            case MPMovieFinishReasonPlaybackError:
                NSLog(@"MPMoviePlayer : An error was encountered during playback");
                
                break;
                
                /* The user stopped playback. */
            case MPMovieFinishReasonUserExited:
                
                break;
                
            default:
                break;
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"MPMoviePlayer : Exception on moviePlayBackDidFinish : %@",[exception description]);
    }
    @finally {
        
    }
}

/* Handle movie load state changes. */
- (void)loadStateDidChange:(NSNotification *)notification   {
	MPMoviePlayerController *player = notification.object;
	MPMovieLoadState loadState = player.loadState;
    
	/* The load state is not known at this time. */
	if (loadState & MPMovieLoadStateUnknown)    {
        //        [self.overlayController setLoadStateDisplayString:@"n/a"];
        //
        //        [overlayController setLoadStateDisplayString:@"unknown"];
	}
	
	/* The buffer has enough data that playback can begin, but it
	 may run out of data before playback finishes. */
	if (loadState & MPMovieLoadStatePlayable)   {
        //        [overlayController setLoadStateDisplayString:@"playable"];
        
        UIButton *playButton = (UIButton *)[self viewWithTag:7878];
        [playButton setSelected:YES];
	}
	
	/* Enough data has been buffered for playback to continue uninterrupted. */
	if (loadState & MPMovieLoadStatePlaythroughOK)  {
        // Add an overlay view on top of the movie view
        //        [self addOverlayView];
        //
        //        [overlayController setLoadStateDisplayString:@"playthrough ok"];
	}
	
	/* The buffering of data has stalled. */
	if (loadState & MPMovieLoadStateStalled)    {
        //        [overlayController setLoadStateDisplayString:@"stalled"];
	}
}

/* Called when the movie playback state has changed. */
- (void) moviePlayBackStateDidChange:(NSNotification*)notification  {
	MPMoviePlayerController *player = notification.object;
    
    UIButton *playButton = (UIButton *)[self viewWithTag:7878];
    
	/* Playback is currently stopped. */
	if (player.playbackState == MPMoviePlaybackStateStopped)    {
        [playButton setSelected:NO];
	}
	/*  Playback is currently under way. */
	else if (player.playbackState == MPMoviePlaybackStatePlaying)   {
        [playButton setSelected:YES];
	}
	/* Playback is currently paused. */
	else if (player.playbackState == MPMoviePlaybackStatePaused)    {
        
	}
	/* Playback is temporarily interrupted, perhaps because the buffer
	 ran out of content. */
	else if (player.playbackState == MPMoviePlaybackStateInterrupted)   {
        
	}
}

/* Notifies observers of a change in the prepared-to-play state of an object
 conforming to the MPMediaPlayback protocol. */
- (void) mediaIsPreparedToPlayDidChange:(NSNotification*)notification   {
	// Add an overlay view on top of the movie view
    //    [self addOverlayView];
}

/* Notifies observers of about the duration of movie availabale. */
- (void) movieDurationAvailableNotification:(NSNotification*)notification   {
    self.moviePlayerController.currentPlaybackTime = 0;
    [self.moviePlayerController play];
}


#pragma mark -

- (void)layoutSubviews  {
    CGRect frame = self.bounds;
    self.moviePlayerController.view.frame = frame;
}

@end

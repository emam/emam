//
//  Login.h
//  eMAM
//
//  Created by APPLE on 24/07/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Login : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameFld;
@property (weak, nonatomic) IBOutlet UITextField *passwordFld;
@property (weak, nonatomic) IBOutlet UITextView *descriptionFld;
@property (weak, nonatomic) IBOutlet UILabel *frgtPassLbl;
@property (weak, nonatomic) IBOutlet UIButton *frgtPassBtn;
- (IBAction)forgetBtnTapped:(id)sender;
- (IBAction)loginTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *helpView;
@property (weak, nonatomic) IBOutlet UIScrollView *helpScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *helpPgCntrl;
@property (weak, nonatomic) IBOutlet UIButton *helpSkipBtn;
- (IBAction)skipTapped:(id)sender;

@end

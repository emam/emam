/**************************************************************************************
 *  File Name      : SelectionViewController.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 05-15-2014
 *  Copyright (C) 2014 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "SelectionViewController.h"

@interface SelectionViewController ()

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SelectionViewController

@synthesize tableView = _tableView;
@synthesize selectionBlock = _selectionBlock;
@synthesize selectionOptions = _selectionOptions;

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil    {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self performSelector:@selector(initView)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)initView    {
    self.title = @"Internal Users";
    
    self.contentSizeForViewInPopover = CGSizeMake(300, 500);
    
    //tableview for display filters
    CGRect  tableViewFrame = CGRectMake(0, 0, self.view.frame.size.width, (self.view.frame.size.height));
    self.tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    self.tableView.opaque = YES;
	self.tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.tableView setDataSource:(id)self];
	[self.tableView setDelegate:(id)self];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
}

- (void)setSelectionOptions:(NSArray *)selectionOptions {
    _selectionOptions = selectionOptions;
    [self refreshView];
}

- (void)refreshView {
    if ([self.selectionOptions count] == 0) {
//        [Utilities showAlert:@"No Internal User found."];
        UILabel *label = [[UILabel alloc] init];
        [label setText:@"No Internal Users found..."];
        [label sizeToFit];
        label.backgroundColor = [UIColor clearColor];
        label.center = CGPointMake(150, 230);
        
        [self.view addSubview:label] ;
    } else  {
        [self.tableView reloadData];
    }
}

- (void)dismissView {
    if (self.selectionBlock) {
        self.selectionBlock(nil);
    }
}

#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
	int icount = 1;
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	int icount = [self.selectionOptions count];
    return icount;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
	//	Return the height of cells in tableView
    CGFloat height = 44.0;
	return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"DeliveryCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
            cell.textLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        
        // Configure the cell...
        cell.accessoryType = UITableViewCellAccessoryNone;
        NSDictionary *selectionOption = [self.selectionOptions objectAtIndex:indexPath.row];
        cell.textLabel.text = [[selectionOption objectForKey:@"FIRSTNAME"] objectForKey:@"text"];
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM SelectionViewController : Exception for cellForAtIndexPath in SelectionViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    NSDictionary *selectionOption = [self.selectionOptions objectAtIndex:indexPath.row];
    if (self.selectionBlock) {
        self.selectionBlock(selectionOption);
    }
}


@end

//
//  LogHandler.m
//  EMAM
//
//  Created by Naveen Shan on 2/22/13.
//  Copyright (c) 2013 naveens@rapidvaluesolutions.com. All rights reserved.
//

#import "LogHandler.h"

#define LOG_FILE_PATH [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/console.log"]

@implementation LogHandler

+ (void) init   {
    NSError *error = nil;
    unsigned long long fileSize = 5000;
    NSString *logFile = LOG_FILE_PATH;
    
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:logFile error:&error];
    if (error) {
        NSLog(@"LogHandler Exception : Un-able to Fetch the 'console.log' file properties : %@",[error description]);
    } else {
        if([fileAttributes fileSize] > fileSize) {
            
            [[NSFileManager defaultManager] removeItemAtPath:logFile error:&error];
            if (error) {
                NSLog(@"LogHandler Exception : Un-able to Delete the 'console.log' file : %@",[error description]);
            } else {
                NSLog(@"LogHandler : Clear Log file : %@",[NSDate date]);
            }
        }
    }
    
    // to start log to a file.
    freopen([logFile cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

+ (NSString *) logFilePath   {
    return LOG_FILE_PATH;
}

@end

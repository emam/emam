//
//  AdvancedESendView.m
//  eMAM
//
//  Created by APPLE on 23/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AdvancedESendView.h"
#import <QuartzCore/QuartzCore.h>
#import "AdvancedAssetService.h"

typedef enum    {
    EmailIdTextFieldTag             = 7976,
    SubjectTextFieldTag             = 7977,
    DescriptionTextFieldTag         = 7978,
    MessageIdTextFieldTag           = 7979,
    EmailTypeTextFieldTag           = 7980,
    AccessLimitTextFieldTag         = 7981,
    ExpirayDateTextFieldTag         = 7982,
    DownloadableCheckBoxTag         = 7983,
    ReportTrackingCheckBoxTag       = 7984,
    ForwardCheckBoxTag              = 7985,
    SendCopyCheckBoxTag             = 7986,
    AllowCommentsCheckBoxTag        = 7987,
    ClearBasketCheckBoxTag          = 7988,
    UnsubsribeCheckBoxTag           = 7989,
    OKButtonTag                     = 7990,
} AdvancedSendViewSubViewTag;


@interface AdvancedESendView ()

@property (nonatomic, strong) UIScrollView *scrollView;

@end



@implementation AdvancedESendView

@synthesize showSendOption = _showSendOption;
@synthesize eSendInfo = _eSendInfo;
@synthesize dismissalBlock = _dismissalBlock;
@synthesize scrollView = _scrollView;
@synthesize fromAssetEsend = _fromAssetEsend;
@synthesize esentDet=_esentDet;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.view.backgroundColor=UIColorFromRGB(0x585858);
    _sendBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
     _navBar.tintColor=UIColorFromRGB(0x585858);
    
    [self initView];
}


- (void)initView {
    self.title = @"Advanced Send";
    self.view.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    
    
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:self.scrollView];
    
    CGFloat xPosition = 20;
    CGFloat yPosition = 30;
    
    CGFloat labelWidth = 180;
    CGFloat textFieldWidth = 300;
    
    if (self.showSendOption) {
        
        UILabel *emailLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
        [emailLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
        [emailLabel setText:@"Email Addresses"];
        [self.scrollView addSubview:emailLabel];
        
        UITextField *emailAddressTextField = [[UITextField alloc] init];
        emailAddressTextField.tag = EmailIdTextFieldTag;
        emailAddressTextField.delegate = (id)self;
        [emailAddressTextField setFont:[UIFont systemFontOfSize:12.0]];
        [emailAddressTextField setFrame:CGRectMake(CGRectGetMaxX(emailLabel.frame) + 20, yPosition, textFieldWidth, 20)];
        [emailAddressTextField setBackgroundColor:[UIColor whiteColor]];
        [self.scrollView addSubview:emailAddressTextField];
        emailAddressTextField.layer.borderWidth = 1.0;
        emailAddressTextField.layer.borderColor = [UIColor grayColor].CGColor;
        
        yPosition += 30;
        
        UILabel *subjectLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
        [subjectLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
        [subjectLabel setText:@"Subject"];
        [self.scrollView addSubview:subjectLabel];
        
        UITextField *subjectTextField = [[UITextField alloc] init];
        subjectTextField.tag = SubjectTextFieldTag;
        subjectTextField.delegate = (id)self;
        [subjectTextField setFont:[UIFont systemFontOfSize:12.0]];
        [subjectTextField setFrame:CGRectMake(CGRectGetMaxX(subjectLabel.frame) + 20, yPosition, textFieldWidth, 20)];
        [subjectTextField setBackgroundColor:[UIColor whiteColor]];
        [self.scrollView addSubview:subjectTextField];
        subjectTextField.layer.borderWidth = 1.0;
        subjectTextField.layer.borderColor = [UIColor grayColor].CGColor;
        
        yPosition += 30;
        
        UILabel *descriptionLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
        [descriptionLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
        [descriptionLabel setText:@"Description"];
        [self.scrollView addSubview:descriptionLabel];
        
        UITextField *descriptionTextField = [[UITextField alloc] init];
        descriptionTextField.tag = DescriptionTextFieldTag;
        descriptionTextField.delegate = (id)self;
        [descriptionTextField setFont:[UIFont systemFontOfSize:12.0]];
        [descriptionTextField setFrame:CGRectMake(CGRectGetMaxX(descriptionLabel.frame) + 20, yPosition, textFieldWidth, 20)];
        [descriptionTextField setBackgroundColor:[UIColor whiteColor]];
        [self.scrollView addSubview:descriptionTextField];
        descriptionTextField.layer.borderWidth = 1.0;
        descriptionTextField.layer.borderColor = [UIColor grayColor].CGColor;
        
        yPosition += 30;
    }
    
    UILabel *messageIdLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [messageIdLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
    [messageIdLabel setText:@"Message ID :"];
    [self.scrollView addSubview:messageIdLabel];
    
    UITextField *messageIdTextField = [[UITextField alloc] init];
    messageIdTextField.tag = MessageIdTextFieldTag;
    messageIdTextField.delegate = (id)self;
    [messageIdTextField setFont:[UIFont systemFontOfSize:12.0]];
    [messageIdTextField setFrame:CGRectMake(CGRectGetMaxX(messageIdLabel.frame) + 20, yPosition, textFieldWidth, 20)];
    [messageIdTextField setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView addSubview:messageIdTextField];
    messageIdTextField.layer.borderWidth = 1.0;
    messageIdTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    yPosition += 30;
    
    UILabel *emailTypeLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [emailTypeLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
    [emailTypeLabel setText:@"Email Type :"];
    [self.scrollView addSubview:emailTypeLabel];
    
    UITextField *emailTypeTextField = [[UITextField alloc] init];
    emailTypeTextField.tag = EmailTypeTextFieldTag;
    emailTypeTextField.delegate = (id)self;
    [emailTypeTextField setFont:[UIFont systemFontOfSize:12.0]];
    [emailTypeTextField setFrame:CGRectMake(CGRectGetMaxX(emailTypeLabel.frame) + 20, yPosition, textFieldWidth, 20)];
    [emailTypeTextField setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView addSubview:emailTypeTextField];
    emailTypeTextField.layer.borderWidth = 1.0;
    emailTypeTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    [pickerView setDataSource:(id)self];
    [pickerView setDelegate:(id)self];
    [pickerView setShowsSelectionIndicator:YES];
    
    [emailTypeTextField setInputView:pickerView];
    
    yPosition += 30;
    
    UILabel *messageAccessLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [messageAccessLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
    [messageAccessLabel setText:@"Message Access Limit :"];
    [self.scrollView addSubview:messageAccessLabel];
    
    UITextField *messageAccessTextField = [[UITextField alloc] init];
    messageAccessTextField.tag = AccessLimitTextFieldTag;
    messageAccessTextField.delegate = (id)self;
    messageAccessTextField.keyboardAppearance = UIKeyboardTypeNumberPad;
    [messageAccessTextField setFont:[UIFont systemFontOfSize:12.0]];
    [messageAccessTextField setFrame:CGRectMake(CGRectGetMaxX(messageAccessLabel.frame) + 20, yPosition, textFieldWidth, 20)];
    [messageAccessTextField setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView addSubview:messageAccessTextField];
    messageAccessTextField.layer.borderWidth = 1.0;
    messageAccessTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    yPosition += 30;
    
    UILabel *expirayDateLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [expirayDateLabel setFrame:CGRectMake(xPosition, yPosition, labelWidth, 20)];
    [expirayDateLabel setText:@"Expiray Date :"];
    
    [self.scrollView addSubview:expirayDateLabel];
    
    UITextField *expirayDateTextField = [[UITextField alloc] init];
    expirayDateTextField.tag = ExpirayDateTextFieldTag;
    expirayDateTextField.delegate = (id)self;
    [expirayDateTextField setFont:[UIFont systemFontOfSize:12.0]];
    [expirayDateTextField setFrame:CGRectMake(CGRectGetMaxX(messageAccessLabel.frame) + 20, yPosition, textFieldWidth, 20)];
    [expirayDateTextField setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView addSubview:expirayDateTextField];
    expirayDateTextField.layer.borderWidth = 1.0;
    expirayDateTextField.layer.borderColor = [UIColor grayColor].CGColor;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setMinimumDate:[NSDate date]];
    [datePicker setDate:[NSDate date]];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [expirayDateTextField setInputView:datePicker];
    
    yPosition += 50;
    labelWidth = 300;
    
    UIButton *downloadableCheckBoxButton = [Utilities checkBox];
    downloadableCheckBoxButton.tag = DownloadableCheckBoxTag;
    downloadableCheckBoxButton.backgroundColor = [UIColor clearColor];
    [downloadableCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [downloadableCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:downloadableCheckBoxButton];
    
    UILabel *downloadableLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [downloadableLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [downloadableLabel setText:@"Downloadable"];
    [self.scrollView addSubview:downloadableLabel];
    downloadableLabel = nil;
    
    yPosition += 30;
    
    UIButton *enableReportCheckBoxButton = [Utilities checkBox];
    enableReportCheckBoxButton.tag = ReportTrackingCheckBoxTag;
    enableReportCheckBoxButton.backgroundColor = [UIColor clearColor];
    [enableReportCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [enableReportCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:enableReportCheckBoxButton];
    enableReportCheckBoxButton = nil;
    
    UILabel *enableReportLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [enableReportLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [enableReportLabel setText:@"Enable report tracking"];
    [self.scrollView addSubview:enableReportLabel];
    enableReportLabel = nil;
    
    yPosition += 30;
    
    UIButton *forwardCheckBoxButton = [Utilities checkBox];
    forwardCheckBoxButton.tag = ForwardCheckBoxTag;
    forwardCheckBoxButton.backgroundColor = [UIColor clearColor];
    [forwardCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [forwardCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:forwardCheckBoxButton];
    forwardCheckBoxButton = nil;
    
    UILabel *forwardLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [forwardLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [forwardLabel setText:@"Forward"];
    [self.scrollView addSubview:forwardLabel];
    forwardLabel = nil;
    
    yPosition += 30;
    
    UIButton *sendCopyCheckBoxButton = [Utilities checkBox];
    sendCopyCheckBoxButton.tag = SendCopyCheckBoxTag;
    sendCopyCheckBoxButton.backgroundColor = [UIColor clearColor];
    [sendCopyCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [sendCopyCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:sendCopyCheckBoxButton];
    sendCopyCheckBoxButton = nil;
    
    UILabel *sendCopyLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [sendCopyLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [sendCopyLabel setText:@"Send me a copy of this"];
    [self.scrollView addSubview:sendCopyLabel];
    sendCopyLabel = nil;
    
    yPosition += 30;
    
    UIButton *allowCommentsCheckBoxButton = [Utilities checkBox];
    allowCommentsCheckBoxButton.tag = AllowCommentsCheckBoxTag;
    allowCommentsCheckBoxButton.backgroundColor = [UIColor clearColor];
    [allowCommentsCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [allowCommentsCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:allowCommentsCheckBoxButton];
    allowCommentsCheckBoxButton = nil;
    
    UILabel *allowCommentsLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [allowCommentsLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [allowCommentsLabel setText:@"Allow Comments"];
    [self.scrollView addSubview:allowCommentsLabel];
    allowCommentsLabel = nil;
    
    yPosition += 30;
    
    UIButton *clearBasketCheckBoxButton = [Utilities checkBox];
    clearBasketCheckBoxButton.tag = ClearBasketCheckBoxTag;
    clearBasketCheckBoxButton.backgroundColor = [UIColor clearColor];
    [clearBasketCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [clearBasketCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:clearBasketCheckBoxButton];
    clearBasketCheckBoxButton = nil;
    
    UILabel *clearBasketLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [clearBasketLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [clearBasketLabel setText:@"Clear your basket after send"];
    [self.scrollView addSubview:clearBasketLabel];
    clearBasketLabel = nil;
    
    yPosition += 30;
    
    UIButton *unsbsribeLinkCheckBoxButton = [Utilities checkBox];
    unsbsribeLinkCheckBoxButton.tag = UnsubsribeCheckBoxTag;
    unsbsribeLinkCheckBoxButton.backgroundColor = [UIColor clearColor];
    [unsbsribeLinkCheckBoxButton setFrame:CGRectMake(xPosition, yPosition, 20, 20)];
    [unsbsribeLinkCheckBoxButton addTarget:self action:@selector(selectCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:unsbsribeLinkCheckBoxButton];
    unsbsribeLinkCheckBoxButton = nil;
    
    UILabel *unsbsribeLinkLabel = [self labelWithFont:[UIFont systemFontOfSize:15.0]];
    [unsbsribeLinkLabel setFrame:CGRectMake(CGRectGetMaxX(downloadableCheckBoxButton.frame) + 20, yPosition, labelWidth, 20)];
    [unsbsribeLinkLabel setText:@"Unsubscribe link"];
    [self.scrollView addSubview:unsbsribeLinkLabel];
    unsbsribeLinkLabel = nil;
    
    yPosition += 30;
    [self setDefaultsValues];
}

- (UILabel *)labelWithFont:(UIFont *)font    {
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, 100, 20)];
    [label setFont:font];
    [label setNumberOfLines:1];
    [label setLineBreakMode:NSLineBreakByTruncatingTail];
    
    [label setBackgroundColor:[UIColor clearColor]];
    
    return label;
}

- (void)setDefaultsValues {
    UITextField *messageIDTextField = (UITextField *)[self.scrollView viewWithTag:MessageIdTextFieldTag];
    [messageIDTextField setUserInteractionEnabled:NO]; // not allow to edit.
    NSString *reelName = [self.eSendInfo objectForKey:@"ReelName"];
    [messageIDTextField setText:reelName];
    
    UITextField *emailTypeTextField = (UITextField *)[self.scrollView viewWithTag:EmailTypeTextFieldTag];
    NSString *emailType = [self.eSendInfo objectForKey:@"EmailType"];
    [emailTypeTextField setText:emailType];
    
    UITextField *accessLimitTextField = (UITextField *)[self.scrollView viewWithTag:AccessLimitTextFieldTag];
    NSString *accessLimit = [NSString stringWithFormat:@"%d",[[self.eSendInfo objectForKey:@"AccessLimit"] intValue]];
    [accessLimitTextField setText:accessLimit];
    
    UITextField *exipryDateTextField = (UITextField *)[self.scrollView viewWithTag:ExpirayDateTextFieldTag];
    NSString *exipryDate = [self.eSendInfo objectForKey:@"ExipryDate"];
    [exipryDateTextField setText:exipryDate];
    
    UIButton *downloadableCheckBox = (UIButton *)[self.scrollView viewWithTag:DownloadableCheckBoxTag];
    downloadableCheckBox.selected = [[self.eSendInfo objectForKey:@"Downloadable"] boolValue];
    
    UIButton *reportTrackingCheckBox = (UIButton *)[self.scrollView viewWithTag:ReportTrackingCheckBoxTag];
    reportTrackingCheckBox.selected = [[self.eSendInfo objectForKey:@"ReportTracking"] boolValue];
    
    UIButton *forwardOptionCheckBox = (UIButton *)[self.scrollView viewWithTag:ForwardCheckBoxTag];
    forwardOptionCheckBox.selected = [[self.eSendInfo objectForKey:@"ForwardOption"] boolValue];
    
    UIButton *sendCopyCheckBox = (UIButton *)[self.scrollView viewWithTag:SendCopyCheckBoxTag];
    sendCopyCheckBox.selected = [[self.eSendInfo objectForKey:@"SendCopy"] boolValue];
    
    UIButton *allowCommentsCheckBox = (UIButton *)[self.scrollView viewWithTag:AllowCommentsCheckBoxTag];
    allowCommentsCheckBox.selected = [[self.eSendInfo objectForKey:@"AllowComments"] boolValue];
    
    UIButton *clearBasketCheckBox = (UIButton *)[self.scrollView viewWithTag:ClearBasketCheckBoxTag];
    clearBasketCheckBox.selected = [[self.eSendInfo objectForKey:@"ClearBasket"] boolValue];
    
    UIButton *unsubscribeLinkCheckBox = (UIButton *)[self.scrollView viewWithTag:UnsubsribeCheckBoxTag];
    unsubscribeLinkCheckBox.selected = [[self.eSendInfo objectForKey:@"UnsubscribeLink"] boolValue];
}

- (void)selectCheckBox:(UIButton *)sender   {
    sender.selected = !sender.selected;
    
    switch (sender.tag) {
        case DownloadableCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"Downloadable"];
        }
            break;
        case ReportTrackingCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"ReportTracking"];
            if (!sender.selected) {
                UITextField *accessLimitTextField = (UITextField *)[self.scrollView viewWithTag:AccessLimitTextFieldTag];
                accessLimitTextField.text = @"0";
            }
        }
            break;
        case ForwardCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"ForwardOption"];
        }
            break;
        case SendCopyCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"SendCopy"];
        }
            break;
        case AllowCommentsCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"AllowComments"];
        }
            break;
        case ClearBasketCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"ClearBasket"];
        }
            break;
        case UnsubsribeCheckBoxTag: {
            [self.eSendInfo setValue:[NSNumber numberWithBool:sender.selected] forKey:@"UnsubscribeLink"];
        }
            break;
            
        default:
            break;
    }
}

- (void)updateTextField:(id)sender   {
    UITextField *expirayDateTextField = (UITextField *)[self.scrollView viewWithTag:ExpirayDateTextFieldTag];
    UIDatePicker *picker = (UIDatePicker*)expirayDateTextField.inputView;
    expirayDateTextField.text = [Utilities stringFromDate:picker.date formatString:@"yyyy-MM-ddThh:mm:ss"];
    [self.eSendInfo setValue:expirayDateTextField.text forKey:@"ExipryDate"];
}

- (NSMutableArray *)validateEmails:(NSString *)emails  {
    // For multiple email validation.
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray) {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email]) {
            [validEmails addObject:email];
        } else {
            // To highlight the error area inside textfield.
            UITextField *emailTextField = (UITextField *)[self.scrollView viewWithTag:EmailIdTextFieldTag];
            [emailTextField becomeFirstResponder];
            emailTextField.layer.borderColor = [UIColor redColor].CGColor;
            NSRange selectedRange = [emailTextField.text rangeOfString:email];
            
            UITextPosition *startPosition = [emailTextField positionFromPosition:emailTextField.beginningOfDocument offset:selectedRange.location];
            UITextPosition *endPosition = [emailTextField positionFromPosition:emailTextField.beginningOfDocument offset:(selectedRange.location + selectedRange.length)];
            UITextRange *selection = [emailTextField textRangeFromPosition:startPosition toPosition:endPosition];
            emailTextField.selectedTextRange = selection;
            
            
            validEmails = nil;
            break;
        }
    }
    return validEmails;
}

- (void)assertNonEmptyStringForTextFieldWithTag:(int)tag success:(BOOL *)success   {
    UITextField *textField = (UITextField *)[self.scrollView viewWithTag:tag];
    if (textField.superview) {
        NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([text length] <= 0) {
            *success = NO;
            textField.backgroundColor = [UIColor grayColor];
            textField.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
}

- (BOOL)validateInputs  {
    BOOL success = YES;
    
    [self assertNonEmptyStringForTextFieldWithTag:EmailIdTextFieldTag success:&success];
    [self assertNonEmptyStringForTextFieldWithTag:SubjectTextFieldTag success:&success];
    [self assertNonEmptyStringForTextFieldWithTag:DescriptionTextFieldTag success:&success];
    
    return success;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)sendTapped:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    if (_fromAssetEsend) {
        
        
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"EmailIds"] forKey:@"EmailIds"];
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"Subject"] forKey:@"Subject"];
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"Description"] forKey:@"Description"];
        
        
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"AssetIds"] forKey:@"AssetIds"];
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"AssetVersionIds"] forKey:@"AssetVersionIds"];
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"ThumbNailUrl"] forKey:@"ThumbNailUrl"];
        [self.eSendInfo setValue:[self.esentDet valueForKey:@"LogoPath"] forKey:@"LogoPath"];
        
        
        [self submitEsendRequests];
        
    }
    else{
    
    
    UIView *firstResponderView = [Utilities findFirstResponderBeneathView:self.scrollView];
    [firstResponderView resignFirstResponder];
    
    if ([self validateInputs]) {
        
        UITextField *emailTextField = (UITextField *)[self.scrollView viewWithTag:EmailIdTextFieldTag];
        NSArray *emails = [self validateEmails:emailTextField.text];
        if (emails && [emails count] > 0) {
            NSLog(@"EMAM AdvancedEsendViewController : Valid Emails found... \n %@",emails);
            
            UITextField *subjectTextField = (UITextField *)[self.scrollView viewWithTag:SubjectTextFieldTag];
            UITextField  *descriptionTextField = (UITextField *)[self.scrollView viewWithTag:DescriptionTextFieldTag];
            
            NSString *emailIDs = [emails componentsJoinedByString:@","];
            
            [self.eSendInfo setValue:emailIDs forKey:@"EmailIds"];
            [self.eSendInfo setValue:subjectTextField.text forKey:@"Subject"];
            [self.eSendInfo setValue:descriptionTextField.text forKey:@"Description"];
            
            UITextField *accessLimitTextField = (UITextField *)[self.scrollView viewWithTag:AccessLimitTextFieldTag];
            if ([accessLimitTextField.text length] > 0) {
                NSNumber *accessLimit = [NSNumber numberWithInt:[accessLimitTextField.text intValue]];
                [self.eSendInfo setValue:accessLimit forKey:@"AccessLimit"];
            }
            
            [self submitEsendRequests];
        }
        else {
            NSLog(@"EMAM AdvancedEsendViewController : NO Valid Emails...");
        }
    }
    }
    
}


- (void)submitEsendRequests {
    
    NSLog(@"EMAM AdvancedEsendViewController : Submitting eSend Request For all Assets in Basket...");
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        
        AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
        [assetService sendeSendWithDetails:self.eSendInfo performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            
            if (dictionary) {
                id response = [[dictionary objectForKey:@"AddeBINSendInfoResponse"] objectForKey:@"AddeBINSendInfoResult"];
                NSLog(@"EMAM AdvancedEsendViewController : Response for Submitting eSend Requests for Assets In Basket : %@",dictionary);
                
                if (response) {
                    int responseStatus = [[response objectForKey:@"text"] intValue];
                    
                    if (responseStatus > 0) {
                        [Utilities showAlert:APPNAME message:@"Message Sent Successfully..." delegateObject:nil];
                        // to dismiss the view.
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else if (responseStatus == -2) {
                        [Utilities showAlert:APPNAME message:@"Sending mail failed..." delegateObject:nil];
                    } else if (responseStatus == -1) {
                        [Utilities showAlert:APPNAME message:@"The message name you have entered is already exists, please enter another name..." delegateObject:nil];
                    } else {
                        [Utilities showAlert:APPNAME message:@"Sending failed..." delegateObject:nil];
                    }
                }
            }else{
                
                NSLog(@"EMAM AdvancedEsendViewController :  Submitting eSend Request For All Assets In Basket Failed : %@",[error description]);
                
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
                
            }
            
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
    
    
}


#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag != AccessLimitTextFieldTag) {
        return YES;
    }
    /*  limit to only numeric characters  */
    BOOL success = NO;
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            success = YES;
            break;
        }
    }
    
    if (!success && [string length] > 0) {
        return NO;
    }
    
    /*  limit the users input to only 9 characters  */
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    success = (newLength > 4) ? NO : YES;
    
    if (success) {
        UIButton *reportTrackingCheckBox = (UIButton *)[self.scrollView viewWithTag:ReportTrackingCheckBoxTag];
        reportTrackingCheckBox.selected = YES;
        [self.eSendInfo setValue:[NSNumber numberWithBool:reportTrackingCheckBox.selected] forKey:@"ReportTracking"];
    }
    
    return success;
}

#pragma mark - UIPickerView DataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

#pragma mark - UIPickerView Delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title = @"";
    switch (row) {
        case 0:
            title = @"HTML";
            break;
        case 1:
            title = @"TEXT";
            break;
            
        default:
            break;
    }
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    UITextField *emailTypeTextField = (UITextField *)[self.scrollView viewWithTag:EmailTypeTextFieldTag];
    emailTypeTextField.text = (row == 0) ? @"HTML" : @"TEXT";
    [self.eSendInfo setValue:emailTypeTextField.text forKey:@"EmailType"];
}



@end

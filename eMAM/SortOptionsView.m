//
//  SortOptionsView.m
//  eMAM
//
//  Created by APPLE on 18/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "SortOptionsView.h"

@interface SortOptionsView ()

@end

@implementation SortOptionsView

@synthesize viewType;
@synthesize options;
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    if([[Datastore sharedStore].sortOrder isEqualToString:@"ASC"])   {
        
        _sortSegCtrl.selectedSegmentIndex = 0;
    } else  {
        _sortSegCtrl.selectedSegmentIndex = 1;
    }
    
    
    switch (viewType) {
        case SortOptions:   {
            self.options = [NSArray arrayWithObjects:@"Most Recent",@"Size",@"Name",@"Type",@"Rating",@"Description",@"Category",@"Ingested On",@"Ingested By", nil];
            break;
        }
        case AssetOptions:  {
            self.options = [NSArray arrayWithObjects:@"10",@"20",@"50",@"100",@"250", nil];
            break;
        }
        default:
            break;
    }
    
    self.preferredContentSize = self.view.frame.size;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)segmentControlIndexChanged:(id)sender {
    
    BOOL needSync = NO;
    switch (_sortSegCtrl.selectedSegmentIndex) {
        case 0: {
            if(![[Datastore sharedStore].sortOrder isEqualToString:@"ASC"])   {
                
                [Datastore sharedStore].sortOrder = @"ASC";
                
                needSync = YES;
            }
            break;
        }
        case 1: {
            if(![[Datastore sharedStore].sortOrder isEqualToString:@"DESC"])   {
                
                [Datastore sharedStore].sortOrder = @"DESC";
                
                needSync = YES;
            }
            break;
        }
        default:
            break;
    }
    
    if(needSync)   {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(SortOptionsViewOptionChanged:ViewType:)]) {
            [self.delegate SortOptionsViewOptionChanged:[Datastore sharedStore].sortOrder ViewType:self.viewType];
        }
    }
    
}


#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    int icount = [self.options count];
    return icount;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
    //	Return the height of cells in tableView
    CGFloat height = 34.0;
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"RootCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:APPFONTNAME size:15.0];
        }
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        NSString *option = [self.options objectAtIndex:indexPath.row];
        cell.textLabel.text = option;
        
        if ([option isEqualToString:[[Datastore sharedStore] sortOption]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM OptionsViewController : Exception for cellForAtIndexPath in SortFilterViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    
    switch (viewType) {
        case SortOptions:   {
            
            NSString *sortOption = [self.options objectAtIndex:indexPath.row];
            NSString *oldOption = [Datastore sharedStore].sortOption;
            
            if (![oldOption isEqualToString:sortOption]) {
                
                [[Datastore sharedStore] setSortOption:sortOption];
                
                [tableview reloadData];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(SortOptionsViewOptionChanged:ViewType:)]) {
                    [self.delegate SortOptionsViewOptionChanged:sortOption ViewType:self.viewType];
                }
            }
            
            break;
        }
        case AssetOptions:   {
            NSString *assetPerPage = [self.options objectAtIndex:indexPath.row];
            int oldAssetPerPage = [Datastore sharedStore].assetsPerPage;
            int newAssetPerPage = [assetPerPage intValue];
            
            if (oldAssetPerPage != newAssetPerPage) {
                
                [[Datastore sharedStore] setAssetsPerPage:newAssetPerPage];
                
                [tableview reloadData];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(SortOptionsViewOptionChanged:ViewType:)]) {
                    [self.delegate SortOptionsViewOptionChanged:assetPerPage ViewType:self.viewType];
                }
            }
            
            break;
        }
            
        default:
            break;
    }
}



@end

/**************************************************************************************
 *  File Name      : MarkerView.h
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import <UIKit/UIKit.h>

#import "Marker.h"

@protocol MarkerDelegate;

@interface MarkerView : UIView

@property (nonatomic, strong) id <MarkerDelegate> delegate;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

- (void)refreshView;
- (void)setMarkerInfo:(NSMutableArray *)markerInfo;

@end

@protocol MarkerDelegate <NSObject>

- (void)saveMarkers:(NSArray *)markers;
- (void)deleteMarker:(Marker *)marker;

@end
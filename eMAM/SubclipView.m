/**************************************************************************************
 *  File Name      : SubclipView.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 26-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "SubclipView.h"

#import "SubclipCell.h"
#import "AVVideoPlayer.h"
#import "TPKeyboardAvoidingTableView.h"

#import <QuartzCore/QuartzCore.h>

@interface SubclipView ()

@property (nonatomic, assign) CMTime setInTime;
@property (nonatomic, assign) CMTime fileSetOutTime;
@property (nonatomic, assign) UITableView *tableView;
@property (nonatomic, strong) Subclip *selectedSubclip;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong) NSMutableArray *selectedSubclips;

@end

@implementation SubclipView

@synthesize delegate = _delegate;
@synthesize tableView = _tableView;
@synthesize setInTime = _setInTime;
@synthesize fileSetOutTime = _fileSetOutTime;
@synthesize selectedSubclip = _selectedSubclip;
@synthesize selectedSubclips = _selectedSubclips;
@synthesize selectedIndexPath = _selectedIndexPath;
int selectedIndex=1000;
@synthesize currentOrientation = _currentOrientation;


#pragma mark -

- (NSMutableArray *)selectedSubclips    {
    if (!_selectedSubclips) {
        _selectedSubclips = [[NSMutableArray alloc] init];
    }
    return _selectedSubclips;
}

#pragma mark -

- (id)initWithFrame:(CGRect)frame   {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}


-(void)DeleteCheckMarks{
    
     [self.selectedSubclips removeAllObjects];
    [self.tableView reloadData];
    
}

- (void) initView {
    self.autoresizesSubviews = YES;
    
    self.backgroundColor=UIColorFromRGB(0x3B3B3B);
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DeleteCheckMarks) name:@"ClearCheckMarks" object:nil];
    UIView *view = [[UIView alloc] init];
    view.tag = 1120;
    [view setBackgroundColor:[UIColor grayColor]];
    view.layer.cornerRadius = 10.0;
    [self addSubview:view];
    
    UITextField *subclipNameTextField = [[UITextField alloc] init];
    subclipNameTextField.frame = CGRectMake(10,15, 270, 30);  // 220
    subclipNameTextField.delegate = (id)self;
    subclipNameTextField.placeholder = @"Clip Name";
    subclipNameTextField.tag = 1121;
    subclipNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    subclipNameTextField.returnKeyType = UIReturnKeyNext;
    subclipNameTextField.backgroundColor = [UIColor whiteColor];
    subclipNameTextField.textColor = [UIColor blackColor];
    subclipNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    subclipNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    subclipNameTextField.layer.cornerRadius = 15.0;
    subclipNameTextField.layer.borderWidth = 1.0f; //To hide the square corners
    subclipNameTextField.layer.borderColor = [[UIColor grayColor] CGColor]; //assigning the default border color
    subclipNameTextField.userInteractionEnabled = NO;
    
    //[view addSubview:subclipNameTextField];
    [subclipNameTextField release];
    subclipNameTextField = nil;
    
    UIButton *setInButton = [[UIButton alloc] init];
    setInButton.frame = CGRectMake(15,0, 36, 40);
    setInButton.userInteractionEnabled = YES;
    setInButton.tag = 1122;
    setInButton.backgroundColor = [UIColor clearColor];
    [view addSubview:setInButton];
    [setInButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"subclipSetin" ofType:@"png"]] forState:UIControlStateNormal];
    [setInButton addTarget:self action:@selector(setInButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [setInButton release];
    setInButton = nil;
    
    UIButton *playButton = [[UIButton alloc] init];
    playButton.frame = CGRectMake(295,0, 36, 40);
    playButton.userInteractionEnabled = YES;
    playButton.tag = 1123;
    playButton.backgroundColor = [UIColor clearColor];
//    [view addSubview:playButton];
    [playButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"subclipPlay" ofType:@"png"]] forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [playButton release];
    playButton = nil;
    
    UIButton *setOutButton = [[UIButton alloc] init];
    setOutButton.frame = CGRectMake(140,0, 36, 40);
    setOutButton.userInteractionEnabled = YES;
    setOutButton.tag = 1124;
    setOutButton.backgroundColor = [UIColor clearColor];
    [view addSubview:setOutButton];
    [setOutButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"subclipSetout" ofType:@"png"]] forState:UIControlStateNormal];
    [setOutButton addTarget:self action:@selector(setOutButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [setOutButton release];
    setOutButton = nil;
    
    UITableView *tableView = [[TPKeyboardAvoidingTableView alloc] init];
    tableView.delegate = (id)self;
    tableView.dataSource = (id)self;
    
    [self addSubview:tableView];
    [tableView setBackgroundColor:[UIColor clearColor]];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView = tableView;
    
    UIButton *deleteButton = [[UIButton alloc] init];
    [deleteButton setFrame:CGRectMake(20, (view.frame.size.height - 35), 100, 30)];
    deleteButton.tag = 1125;
    deleteButton.backgroundColor = [UIColor clearColor];
    
    [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
    deleteButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
   // [deleteButton setBackgroundImage:[Utilities buttonBackgroundImage] forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:deleteButton];
    deleteButton = nil;
    
    UIButton *saveButton = [[UIButton alloc] init];
    [saveButton setFrame:CGRectMake(130, (view.frame.size.height - 35), 100, 30)];
    saveButton.tag = 1126;
    saveButton.backgroundColor = [UIColor clearColor];
    
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    //[saveButton setBackgroundImage:[Utilities buttonBackgroundImage] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:saveButton];
    saveButton = nil;
    
    UIButton *deliverButton = [[UIButton alloc] init];
    [deliverButton setFrame:CGRectMake((view.frame.size.width - 230), (view.frame.size.height - 35), 100, 30)];
    deliverButton.tag = 1127;
    deliverButton.backgroundColor = [UIColor clearColor];
    
    [deliverButton setTitle:@"Deliver" forState:UIControlStateNormal];
    deliverButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    //[deliverButton setBackgroundImage:[Utilities buttonBackgroundImage] forState:UIControlStateNormal];
    [deliverButton addTarget:self action:@selector(deliverButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:deliverButton];
    deliverButton = nil;
    
    UIButton *sendEDLButton = [[UIButton alloc] init];
    [sendEDLButton setFrame:CGRectMake((view.frame.size.width - 120), (view.frame.size.height - 35), 100, 30)];
    sendEDLButton.tag = 1128;
    sendEDLButton.backgroundColor = [UIColor clearColor];
    
    [sendEDLButton setTitle:@"Send EDL" forState:UIControlStateNormal];
    sendEDLButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
   // [sendEDLButton setBackgroundImage:[Utilities buttonBackgroundImage] forState:UIControlStateNormal];
    [sendEDLButton addTarget:self action:@selector(sendEDLButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //Dony
    //[self addSubview:sendEDLButton];
    
    //Dony
    
    
    sendEDLButton = nil;
    
    [self layoutOrientation];
}

- (void)orientationChanged:(NSNotification *)note
{
    self.currentOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    [self refreshView];
}

- (void)layoutOrientation   {
    
    
    
    
    NSLog(@"Orient:%d",self.currentOrientation);
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame=CGRectMake(0, 500, 768, 416);
        
        UIView *topView = [self viewWithTag:1120];
        topView.frame = CGRectMake(284,10, 200, 40);
        
        
        self.tableView.frame = CGRectMake(16, 60, 736, 300);
        [self.tableView reloadData];
        
        UIView *deleteButton = [self viewWithTag:1125];
        deleteButton.frame = CGRectMake(20, (self.frame.size.height - 40), 100, 30);
        UIView *saveButton = [self viewWithTag:1126];
        saveButton.frame = CGRectMake(130, (self.frame.size.height - 40), 100, 30);
        
        UIView *deliverButton = [self viewWithTag:1127];
        deliverButton.frame = CGRectMake((self.frame.size.width - 230), (self.frame.size.height - 40), 100, 30);
        UIView *sendEDLButton = [self viewWithTag:1128];
        sendEDLButton.frame = CGRectMake((self.frame.size.width - 120), (self.frame.size.height - 40), 100, 30);
        
        
    } else {
        
        self.frame=CGRectMake(0, 500, 1024, 160);
        
        UIView *topView = [self viewWithTag:1120];
        topView.frame = CGRectMake(412,10, 200, 40);
        
        self.tableView.frame = CGRectMake(150, 58, 736, 97);
        [self.tableView reloadData];
        
        UIView *deleteButton = [self viewWithTag:1125];
        deleteButton.frame = CGRectMake(130, 15, 100, 30);
        UIView *saveButton = [self viewWithTag:1126];
        saveButton.frame = CGRectMake(250, 15, 100, 30);
        
        UIView *deliverButton = [self viewWithTag:1127];
        deliverButton.frame = CGRectMake((self.frame.size.width - 230), 15, 100, 30);
        UIView *sendEDLButton = [self viewWithTag:1128];
        sendEDLButton.frame = CGRectMake((self.frame.size.width - 120), 15, 100, 30);
    }
    
    
}

- (void)refreshView {
    
    if (UIInterfaceOrientationIsPortrait(self.currentOrientation)) {
        
        self.frame=CGRectMake(0, 500, 768, 416);
        self.tableView.frame = CGRectMake(16, 60, 736, 300);
        [self.tableView reloadData];
        
        selectedIndex=1000;
        
        NSString *archived = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"IsArchived"] objectForKey:@"text"];
        BOOL isArchived = ([archived isEqualToString:@"true"]);
        
        UIView *deliverButton = [self viewWithTag:1127];
        deliverButton.hidden = isArchived;
        UIView *sendEDLButton = [self viewWithTag:1128];
        sendEDLButton.hidden = isArchived;
    } else {
        self.frame=CGRectMake(0, 500, 1024, 160);
        
        self.tableView.frame = CGRectMake(150, 58, 736, 97);
        [self.tableView reloadData];
        
        selectedIndex=1000;
        
        NSString *archived = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"IsArchived"] objectForKey:@"text"];
        BOOL isArchived = ([archived isEqualToString:@"true"]);
        
        UIView *deliverButton = [self viewWithTag:1127];
        deliverButton.hidden = isArchived;
        UIView *sendEDLButton = [self viewWithTag:1128];
        sendEDLButton.hidden = isArchived;
    }
    
}

#pragma mark -

- (void)selectSubClip:(Subclip *)subclip    {
    
    
    NSUInteger index = [self.selectedSubclips indexOfObject:subclip];
    if (index == NSNotFound) {
        [self.selectedSubclips addObject:subclip];
    } else {
        [self.selectedSubclips removeObject:subclip];
    }
    
    [self.tableView reloadData];
}

- (void)setInButtonClicked:(id)sender    {
    
    
   // UIButton *btn=(UIButton *)sender;
    //UIButton *btnOut=(UIButton *)[self viewWithTag:1124];
    self.setInTime = [[AVVideoPlayer sharedInstance] newSetInTime];
    self.fileSetOutTime = [[AVVideoPlayer sharedInstance] fileSetOutTime];
    
    // btn.backgroundColor=[UIColor redColor];
    //btnOut.backgroundColor=[UIColor clearColor];
    
    
    
//    NSString *setInTimeString = [[[AVVideoPlayer sharedInstance] class] formattedStringForDurationSubClip:self.setInTime];
//    NSString *fileSetOutTimeString = [[[AVVideoPlayer sharedInstance] class] formattedStringForDurationSubClip:self.fileSetOutTime];
//    NSLog(@"setInTimeString:%@",setInTimeString);
//    NSLog(@"fileSetOutTimeString:%@",fileSetOutTimeString);
    
    
    
    
    
    
//    if (selectedIndex==1000) {
//        [[AVVideoPlayer sharedInstance] addNewSubClipWithSetInDefaultTime:self.setInTime];
//    }else{
//        
//        NSLog(@"Edit");
//        
//        //[[AVVideoPlayer sharedInstance] addNewSubClipWithSetInTime:self.setInTime];
//        NSLog(@" Edit selectedIndex:%d",selectedIndex);
//        [[AVVideoPlayer sharedInstance] editSubClipWithSetInTime:self.setInTime index:selectedIndex];
//    }
//    
//    
//    [self refreshView];
    
    
}

- (void)setOutButtonClicked:(id)sender    {
    
   // UIButton *btn=(UIButton *)sender;
    // btn.backgroundColor=[UIColor redColor];
   // UIButton *btnIn=(UIButton *)[self viewWithTag:1122];
     // btnIn.backgroundColor=[UIColor redColor];
    
    if (selectedIndex==1000) {
        [[AVVideoPlayer sharedInstance] addNewSubClipWithSetInTime:self.setInTime];
    }else{
        
        NSLog(@"Edit");
        
       //[[AVVideoPlayer sharedInstance] addNewSubClipWithSetInTime:self.setInTime];
         NSLog(@" Edit selectedIndex:%d",selectedIndex);
       [[AVVideoPlayer sharedInstance] editSubClipWithSetInTime:self.setInTime index:selectedIndex];
    }
    
    
    [self refreshView];
   // btn.backgroundColor=[UIColor redColor];
}

- (void)playButtonClicked:(id)sender    {
    [[AVVideoPlayer sharedInstance] play:nil];
}

- (void)saveButtonClicked:(id)sender    {
    
    
    UIButton *btnOut=(UIButton *)[self viewWithTag:1124];
    
    btnOut.backgroundColor=[UIColor clearColor];
    UIButton *btnIn=(UIButton *)[self viewWithTag:1122];
    btnIn.backgroundColor=[UIColor clearColor];
    if([[[AVVideoPlayer sharedInstance] videoSubclips] count]>0){
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(saveSubclips:)]) {
        
        [self.delegate saveSubclips:[[AVVideoPlayer sharedInstance] videoSubclips]];
    }
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPNAME message:@"Mail account is not configured in the device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
       // [alert show];
       // [alert release];
        
    }
}

- (void)deleteButtonClicked:(id)sender    {
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1121];
    [nameTextField setText:@""];
    
    if([self.selectedSubclips count]>0){
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteSubclips:)]) {
        if ([self.selectedSubclips count] > 0) {
            [self.delegate deleteSubclips:self.selectedSubclips];
        } else  {
            [self.delegate deleteSubclips:[NSArray arrayWithObjects:self.selectedSubclip, nil]];
        }
    }
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPNAME message:@"No subclip selected to delete" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
}

- (void)deliverButtonClicked:(id)sender    {
    if ([self.selectedSubclips count] > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(showDeliveryProfilesForSubclips:)]) {
            [self.delegate showDeliveryProfilesForSubclips:self.selectedSubclips];
        }
    } else {
        [Utilities showAlert:@"Select Subclips to Delivery"];
    }
}

- (void)sendEDLButtonClicked:(id)sender    {
    if ([self.selectedSubclips count] > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(showSendEDLProfilesForSubclips:)]) {
            [self.delegate showSendEDLProfilesForSubclips:self.selectedSubclips];
        }
    } else {
        [Utilities showAlert:@"Select Subclips to SendEDL"];
    }
}

#pragma mark - Override Setters

- (void)setSubclipInfo:(NSMutableArray *)subclipInfo  {
    AVVideoPlayer *avVideoPlayer = [AVVideoPlayer sharedInstance];
    [avVideoPlayer setVideoSubclips:subclipInfo];
    [self refreshView];
}

- (void)setCurrentOrientation:(UIInterfaceOrientation)currentOrientation    {
    _currentOrientation = currentOrientation;
    [self layoutOrientation];
}

#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
	int icount = [[[AVVideoPlayer sharedInstance] videoSubclips] count];
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	int icount = 1;
    return icount;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
	//	Return the height of cells in tableView
    CGFloat height = 60;
   	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
    return view;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    SubclipCell * cell;
    
    @try    {
       // static NSString *CellIdentifier = @"SubclipCell";
        
        cell = [[SubclipCell alloc] init];
       // if (cell == nil) {
           // cell = [[[SubclipCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.target = self;
            cell.delegate = self.delegate;
            cell.textLabel.font = [UIFont fontWithName:APPFONTNAME size:15.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
        //}
        
        // Configure the cell...
        [cell setCurrentOrientation:self.currentOrientation];
        [cell setSubclip:[[[AVVideoPlayer sharedInstance] videoSubclips] objectAtIndex:indexPath.section]];
        
        
        
        
       
        [cell setSubclipTotalTimeString:[[Datastore sharedStore] frameDuration]];
        
        NSLog(@"subclipTotalTimeString:%@",[[Datastore sharedStore] frameDuration]);
        UIButton *selBtn=[cell updateCheckmark];
        Subclip *currSub=[[[AVVideoPlayer sharedInstance] videoSubclips] objectAtIndex:indexPath.section];
        
        NSUInteger index = [self.selectedSubclips indexOfObject:currSub];
        if (index == NSNotFound) {
            [selBtn setImage:[UIImage imageNamed:@"checkbox-normal.png"] forState:UIControlStateNormal];
            
        } else {
            [selBtn setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateNormal];
            
        }
        
      
        
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM Subclip View : Exception for cellForAtIndexPath in SearchFilterViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Navigation logic may go here. Create and push another view controller.
    Subclip *subclip = [[[AVVideoPlayer sharedInstance] videoSubclips] objectAtIndex:indexPath.section];
    
    NSUInteger index = [[[AVVideoPlayer sharedInstance] videoSubclips] indexOfObject:subclip];
    
    
    //NSLog(@"Subclip Inde:%d",index);
    
    
    [[AVVideoPlayer sharedInstance] addSubclip:subclip];
    self.selectedSubclip = subclip;
    
    SubclipCell *cell = (SubclipCell *)[tableView cellForRowAtIndexPath:self.selectedIndexPath];
    cell.backgroundColor = [UIColor lightGrayColor];
    self.selectedIndexPath = indexPath;
    selectedIndex=index;
    
    
    NSLog(@"selectedIndex:%d",selectedIndex);
    
    cell = (SubclipCell *)[tableView cellForRowAtIndexPath:self.selectedIndexPath];
    cell.backgroundColor = [UIColor darkGrayColor];
    
    UITextField *nameTextField = (UITextField *)[self viewWithTag:1121];
    [nameTextField setText:subclip.clipName];
}

@end

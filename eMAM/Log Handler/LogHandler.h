//
//  LogHandler.h
//  EMAM
//
//  Created by Naveen Shan on 2/22/13.
//  Copyright (c) 2013 naveens@rapidvaluesolutions.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogHandler : NSObject

+ (void) init;
+ (NSString *) logFilePath;

@end

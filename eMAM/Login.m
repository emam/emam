//
//  Login.m
//  eMAM
//
//  Created by APPLE on 24/07/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "Login.h"
#import "UserService.h"
#import "Cryptor.h"
#import "AnalyticsService.h"
#import "LibraryViewController.h"

@interface Login ()

@end

@implementation Login

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadHelpView{
    
    for(UIView *subview in [_helpScrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    for (int i=0; i<16; i++) {
        CGFloat xOrigin = i * self.view.frame.size.width;
        UIImageView *image = [[UIImageView alloc] initWithFrame:
                              CGRectMake(xOrigin, 0,
                                         768,
                                         1024)];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:
                                           @"%d.png", i]];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [_helpScrollView addSubview:image];
    }
    _helpScrollView.contentSize = CGSizeMake(768 *
                                             16,
                                             1024);
    _helpSkipBtn.hidden=NO;
}

-(void)constarintland{
    
    
    for (int i=0; i<16; i++) {
        CGFloat xOrigin = i * self.view.frame.size.width;
        UIImageView *image = [[UIImageView alloc] initWithFrame:
                              CGRectMake(xOrigin, 0,
                                         1024,
                                         768)];
        image.image = [UIImage imageNamed:[NSString stringWithFormat:
                                           @"l%d.png", i]];
        image.contentMode = UIViewContentModeScaleToFill;
        [_helpScrollView addSubview:image];
    }
    _helpScrollView.contentSize = CGSizeMake(1024 *
                                             16,
                                             768);
    _helpSkipBtn.hidden=NO;
}


-(void)updateLoginView{
    
    NSLog(@"eMAM Login updateLoginView : KROOTSERVER : %@",[[NSUserDefaults standardUserDefaults] valueForKey:KROOTSERVER]);
    
    NSLog(@"eMAM Login updateLoginView : KROOTSERVER2 : %@",ROOTSERVERNAME);
    NSLog(@"eMAM Login updateLoginView : KSERVERLICENSEKEY : %@",[[NSUserDefaults standardUserDefaults] valueForKey:KSERVERLICENSEKEY]);
}


- (void)authenticateUser{
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
   // hud.dimBackground = YES;
	hud.labelText = @"Authenticating";
    [hud show:YES];
	
        UserService *userService = [UserService sharedInstance];
        [userService authenticateUser:_userNameFld.text password:_passwordFld.text performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            if (dictionary) {
                NSLog(@"Login Response:%@", dictionary);
                NSString *result = [[[dictionary objectForKey:@"AuthenticateUserResponse"] objectForKey:@"AuthenticateUserResult"] objectForKey:@"text"];
                NSLog(@"Login : Authenticate Result : %@",result);
                if ([result boolValue]) {
                    
                    [hud removeFromSuperview];
                    [[NSUserDefaults standardUserDefaults] setValue:_userNameFld.text forKey:KUSERNAME];
                    [self getUserStatus];
                } else  {
                    
                    [hud removeFromSuperview];
                    [Utilities showAlert:APPNAME message:@"Fail to authenticate with given credentials" delegateObject:nil];
                }
                
                
            }
        }];
    
    
}


- (void)passwordReset{
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:hud];
    // hud.dimBackground = YES;
    hud.labelText = @"Loading";
    [hud show:YES];
    
    UserService *userService = [UserService sharedInstance];
    [userService passwordReset:_userNameFld.text performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        if (dictionary) {
            NSLog(@"Password reset Response:%@", dictionary);
            NSString *result = [[[dictionary objectForKey:@"SubmitForgotPasswordRequestResponse"] objectForKey:@"SubmitForgotPasswordRequestResult"] objectForKey:@"text"];
            NSLog(@"Reset password Result : %@",result);
            if ([result boolValue]) {
                
                [hud removeFromSuperview];
                [Utilities showAlert:APPNAME message:@"Password reset mail sent." delegateObject:nil];
            } else  {
                
                [hud removeFromSuperview];
                [Utilities showAlert:APPNAME message:@"Fail to reset the password. Please enter the correct email id." delegateObject:nil];
            }
            
            
        }
    }];
    
    
}


- (void)getUserStatus{
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
    //hud.dimBackground = YES;
	hud.labelText = @"Fetching User Status";
    [hud show:YES];
    
        UserService *userService = [UserService sharedInstance];
        [userService getUserStatusPerformRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (dictionary) {
                
                [hud removeFromSuperview];
                
                dictionary =[dictionary objectForKey:@"GetUserStatusResponse"];
                
                dictionary = [[[[dictionary objectForKey:@"GetUserStatusResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"DocumentElement"] objectForKey:@"UserInfo"];
                NSLog(@"GetUserStatus Response:%@", dictionary);
                BOOL success = YES;
                NSString *text;
                NSString *errorMessage;
                
                text = [[dictionary objectForKey:@"FirstName"] objectForKey:@"text"];
                if ([text length] > 0) {
                    [[NSUserDefaults standardUserDefaults] setValue:text forKey:KUSERFIRSTNAME];
                }
                
                text = [[dictionary objectForKey:@"EMAIL_ID"] objectForKey:@"text"];
                if ([text length] > 0) {
                    [[NSUserDefaults standardUserDefaults] setValue:text forKey:KUSEREMAILID];
                }
                
                BEGIN_BLOCK
                
                if (!dictionary) {
                    success = NO;
                    errorMessage = @"Un-able to fetch your account information. Please contact the developer.";
                    break;
                }
                
                text = [[dictionary objectForKey:@"Status"] objectForKey:@"text"];
                text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                int status = [text intValue];
                if (status != 1) {
                    success = NO;
                    errorMessage = @"Your account is disabled, Please contact system administrator.";
                    break;
                }
                
                text = [[dictionary objectForKey:@"IsExpired"] objectForKey:@"text"];
                text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                BOOL isExpired = [text boolValue];
                if (isExpired) {
                    success = NO;
                    errorMessage = @"Your account is expired, Please contact system administrator.";
                    break;
                }
                
                text = [[dictionary objectForKey:@"ACCOUNTNEVEREXPIRES"] objectForKey:@"text"];
                text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                int accountNeverExpires = [text intValue];
                if (accountNeverExpires == 0) {
                    NSString * accountExpiryDate = [[dictionary objectForKey:@"ACCOUNTEXPIRYDATE"] objectForKey:@"text"];
                    accountExpiryDate = [accountExpiryDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if (![accountExpiryDate isEqualToString:@""]) {
                        success = YES;
                        errorMessage = @"Your account is disabled, Please contact system administrator.";
                        break;
                    }
                }
                
                text = [[dictionary objectForKey:@"PROMPTCHANGEPASSWORD"] objectForKey:@"text"];
                text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                int promptChangePassword = [[[dictionary objectForKey:@"PROMPTCHANGEPASSWORD"] objectForKey:@"text"] intValue];
                if (promptChangePassword == 1) {
                    success = NO;
                    errorMessage = @"Your account is password is expired, Please change password.";
                    break;
                }
                
                END_BLOCK
                
                text = [[dictionary objectForKey:@"RoleId"] objectForKey:@"text"];
                text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (success) {
                    [self getUserFeatures];
                } else {
                    NSLog(@"Login : User Status Message : %@",errorMessage);
                    [Utilities showAlert:APPNAME message:errorMessage delegateObject:nil];
                }

            }else{
                
                [hud removeFromSuperview];
                
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
        }];
    
    
    
}

- (void)getUserFeatures   {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
    //hud.dimBackground = YES;
	hud.labelText = @"Fetching User Features";
    [hud show:YES];
    UserService *userService = [UserService sharedInstance];
    [userService getUserFeaturesperformRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        
        if (dictionary) {
            [hud removeFromSuperview];
            NSLog(@"GetUserFeatures Response:%@", dictionary);
            dictionary = [dictionary objectForKey:@"GetDecryptedFeatureHashTableResponse"];
            NSString *userHashValue = [[dictionary objectForKey:@"GetDecryptedFeatureHashTableResult"] objectForKey:@"text"];
            NSString *userFeature = [Cryptor decryptString:userHashValue key:KEncryptionKey];
            
            NSLog(@"Loginr : Feature Count : %d User Feature : %@",[userFeature length],userFeature);
            
            if (userFeature) {
                BOOL sucess = [[Datastore sharedStore] setUserFeature:userFeature];
                
                if (sucess) {
                    // if login success fetch the client logo.
                    [self fetchClientLogo];
                } else {
                    NSLog(@"Login : Un-able to Process User Feature : Count Mis-match");
                    [Utilities showAlert:APPNAME message:@"Un-able to parse user featues, Contact the administrator." delegateObject:nil];
                }
            } else {
                NSLog(@"Login : Un-able to find User HashValue : %@",dictionary);
                [Utilities showAlert:APPNAME message:@"Un-able to fetch user featues, Contact the administrator." delegateObject:nil];
            }
            
        }else{
            [hud removeFromSuperview];
            NSLog(@"Login : Fetching User Features Failed : %@",[error description]);
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
    }];
    
    
}

- (void)fetchClientLogo {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
    //hud.dimBackground = YES;
	hud.labelText = @"Fetching Client Logo";
    [hud show:YES];
        
    UserService *userService = [UserService sharedInstance];
    [userService getClientLogoPerformRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        
        if (dictionary) {
            [hud removeFromSuperview];
            
            NSLog(@"FetchClientLogo Response:%@", dictionary);
            id response = [[[[[dictionary objectForKey:@"GetClientLogoResponse"] objectForKey:@"GetClientLogoResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
            
            if (response) {
                NSString *clientId = [[response objectForKey:@"ClientId"] objectForKey:@"text"];
                if ([clientId length] > 0) {
                    [[NSUserDefaults standardUserDefaults] setValue:clientId forKey:@"ClientId"];
                }
                
                NSString *imagePath = [[response objectForKey:@"ImagePath"] objectForKey:@"text"];
                if ([imagePath length] > 0) {
                    [[NSUserDefaults standardUserDefaults] setValue:imagePath forKey:@"ClientLogoImagePath"];
                }
                
                NSLog(@"Login : Fetch Client Logo Sucess : Client Id : %@ ImagePath : %@",clientId,imagePath);
            } else {
                NSLog(@"Login : Fetch Client Logo Failed...");
            }
            
            // Send Analytics
            //[self updateHitCount];
           // [self updateLoggedInFlag];
            [self showLibrary:YES];
            
        }else{
            [hud removeFromSuperview];
            NSLog(@"EMAM LoginViewController : Fetching Client Logo Failed : %@",[error description]);
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
    }];
    
}


- (void)updateHitCount {
    AnalyticsService *analyticsService = [AnalyticsService sharedInstance];
    [analyticsService updateHitCountForFeature:@"LOGIN" performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        
    }];
}

- (void)updateLoggedInFlag {
    AnalyticsService *analyticsService = [AnalyticsService sharedInstance];
    [analyticsService updateLoggedInFlag:1 forUser:[Utilities loginedUserName] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        
    }];
}

- (void)showLibrary :(BOOL)sucess{
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    [userInfo setValue:[NSNumber numberWithBool:sucess] forKey:KOPERATIONSTATUS];
    [userInfo setValue:[Utilities loginedUserName] forKey:KUSERNAME];
    userInfo = nil;
    _passwordFld.text=@"";
    
    LibraryViewController *view=[[LibraryViewController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}



- (void)checkRootServer {
    
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
    hud.dimBackground = YES;
	hud.labelText = @"Checking Root Server";
	
    [hud show:YES];
		UserService *userService = [UserService sharedInstance];
        [userService pingToServer:[[NSUserDefaults standardUserDefaults] valueForKey:KROOTSERVER] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (dictionary) {
                [hud removeFromSuperview];
                NSLog(@"checkRootServer Response:%@", dictionary);
                
                [self checkDefaultServiceGateway];
                
            }else{
                [hud removeFromSuperview];
                //[Utilities showAlert:@"Invalid Root Server. Please go to settings app and correct the eMAM settings."];
            }
            
        }];
    
}

- (void)checkDefaultServiceGateway {
    
    
    
        UserService *userService = [UserService sharedInstance];
    
    NSString *defaultServiceGateway = [NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME];
    [[NSUserDefaults standardUserDefaults] setValue:defaultServiceGateway forKey:KSERVICEGATEWAY];
    
    
    NSLog(@"ServiceGateway:%@",[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME]);
        [userService pingToServer:[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (!error) {
                NSLog(@"checkDefaultServiceGateway success");
                
                [self checkDefaultAnalyticsGateway];
                
            }else{
                [Utilities showAlert:@"Invalid Default Service Gateway"];
            }
            
        }];


    
}

- (void)checkServiceGateway {
    
		UserService *userService = [UserService sharedInstance];
        [userService pingToServer:[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (!error) {
                NSLog(@"checkServiceGateway success");
                
            }else{
                [Utilities showAlert:@"Invalid Service Gateway"];
            }
            
        }];
    
}

- (void)checkDefaultAnalyticsGateway {
    
    
		UserService *userService = [UserService sharedInstance];
        [userService pingToServer:[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (!error) {
                NSLog(@"checkDefaultAnalyticsGateway success");
                
            }else{
                [Utilities showAlert:@"Invalid Default Analytics Service Gateway"];
            }
            
        }];

    
}

- (void)checkAnalyticsGateway {
    

		UserService *userService = [UserService sharedInstance];
        [userService pingToServer:[NSString stringWithFormat:@"%@emamgateway/emamservice.asmx",ROOTSERVERNAME] performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (!error) {
                NSLog(@"checkAnalyticsGateway success");
                
            }else{
                [Utilities showAlert:@"Invalid Analytics Service Gateway"];
            }
            
        }];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = _helpScrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = _helpScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    _helpPgCntrl.currentPage = page; // you need to have a **iVar** with getter for pageControl
    
    if (page==16) {
        _helpSkipBtn.hidden=NO;
    }else{
        _helpSkipBtn.hidden=NO;
    }
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.view.backgroundColor=UIColorFromRGB(0x585858);
    [_userNameFld setTextColor:UIColorFromRGB(0xCCCCCC)];
    [_passwordFld setTextColor:UIColorFromRGB(0xCCCCCC)];
//    [_descriptionFld setTextColor:UIColorFromRGB(0xA2A2A2)];
    [_frgtPassLbl setTextColor:UIColorFromRGB(0x999999)];
     NSLog(@"eMAM Login viewDidLoad");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLoginView) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self updateLoginView];
    [self checkRootServer];
    
    [self loadHelpView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)forgetBtnTapped:(id)sender {
    [_userNameFld resignFirstResponder];
    [_passwordFld resignFirstResponder];
    if ([_userNameFld.text length]>0) {
        [self passwordReset];
    }else{
        
        [Utilities showAlert:@"Please enter the username "];
    }

}



- (IBAction)loginTapped:(id)sender {
    
    
    [_userNameFld resignFirstResponder];
    [_passwordFld resignFirstResponder];
    if (([_userNameFld.text length]>0)&&([_passwordFld.text length]>0)) {
        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:KSHAREKEY];
        NSString *enecrypted = [Cryptor encryptString:_passwordFld.text key:KEncryptionKey];
        NSString *licenceKey = [[NSUserDefaults standardUserDefaults] valueForKey:KSERVERLICENSEKEY];
        NSString *RealRoot = [[NSUserDefaults standardUserDefaults] objectForKey:KROOTSERVER];
        NSString *port = [[NSUserDefaults standardUserDefaults] objectForKey:KSERVERPORT];
        
        [shared setValue: @"True" forKey:@"eMAMLoggedIn"];
        [shared setValue: _userNameFld.text forKey:@"eMAMUserName"];
        [shared setValue: _passwordFld.text forKey:@"eMAMPassword"];
        [shared setValue: enecrypted forKey:@"eMAMenCryptedPassword"];
        [shared setValue: licenceKey forKey:@"eMAMLicenceKey"];
        [shared setValue: RealRoot forKey:@"eMAMRoot"];
        [shared setValue: port forKey:@"eMAMPort"];
        
        
        [shared synchronize];
        [self authenticateUser];
    }else{
        
        [Utilities showAlert:@"Please fill all the required fields"];
    }

}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:true];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
    
}
- (IBAction)skipTapped:(id)sender {
    
    _helpView.hidden=YES;
}
@end

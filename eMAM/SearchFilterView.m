//
//  SearchFilterView.m
//  eMAM
//
//  Created by APPLE on 18/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "SearchFilterView.h"

static UIImageView *filterView;

@interface SearchFilterView ()

@end

@implementation SearchFilterView

@synthesize delegate;
@synthesize filterType;
@synthesize searchfilters;
@synthesize categoryId;



#pragma mark -

+ (UIImageView *)filterView {
    if (!filterView) {
        filterView = [[UIImageView alloc] init];
        [filterView setBackgroundColor:[UIColor clearColor]];
        [filterView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"filter" ofType:@"png"]]];
        [filterView sizeToFit];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"InSearchFilter"]) {
            filterView.hidden = NO;
        } else  {
            filterView.hidden = YES;
        }
    }
    return filterView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.searchfilters = [NSArray arrayWithObjects:@"Asset Type",@"Categories",@"Projects",@"Asset Size", nil];
    if (!self.title) {
        NSString *title = @"Search Filter";
        self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
        if ([self.searchfilters count] > (self.filterType - 1)) {
            title = [self.searchfilters objectAtIndex:(self.filterType - 1)];
        }
        self.title = title;
        title = nil;
    }
    
    [self enableSearchFilterIfExists];
    [self setContentSize];
}


- (void)enableSearchFilterIfExists  {
    UIView *filterView = [[self class] filterView];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"InSearchFilter"]) {
        NSLog(@"EMAM SearchFilterViewController : Search Filter Enabled...");
        UIBarButtonItem *clearFilterBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Clear Filters"
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:self
                                                                                action:@selector(clearFilterButtonClicked:)];
        
        self.navigationItem.rightBarButtonItem = clearFilterBarButton;
        clearFilterBarButton = nil;
        
        filterView.hidden = NO;
        
    } else  {
        NSLog(@"EMAM SearchFilterViewController : Search Filter Disabled...");
        filterView.hidden = YES;
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)setContentSize  {
    int icount = 0;
    
    switch (self.filterType) {
        case InitialFilters:
            icount  = [self.searchfilters count];
            break;
        case ProjectFilter:
            icount  = [[[Datastore sharedStore] projects] count];
            break;
        case CategoryFilter:
            icount  = [[[Datastore sharedStore] subCategoriesWithParentID:self.categoryId] count];
            break;
        case AssetTypeFilter:
            icount  = [[[Datastore sharedStore] assetTypes] count];
            break;
        case AssetSizeFilter:
            icount  = [[[Datastore sharedStore] assetSizes] count];
            break;
            
        default:
            break;
    }
    
    self.preferredContentSize = self.view.frame.size;
}


#pragma mark -

- (void) updateAssets   {
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchFilterAssetDidFiltered)]) {
        [self.delegate searchFilterAssetDidFiltered];
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark

- (NSString *)detailTextForCellAtIndex:(int)index   {
    NSString *text = @"";
    switch (index) {
        case 0:
            text = [Datastore sharedStore].assetTypeSearchFilter;
            break;
        case 1:
            text = [Datastore sharedStore].categorySearchFilter;
            break;
        case 2:
            text = [Datastore sharedStore].projectSearchFilter;
            break;
        case 3:
            text = [Datastore sharedStore].assetSizeSearchFilter;
            break;
            
        default:
            break;
    }
    return text;
}

- (UIButton *) customDisclosureButton   {
    UIButton * button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    [button addTarget: self
               action: @selector(accessoryButtonTapped:withEvent:)
     forControlEvents: UIControlEventTouchUpInside];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateHighlighted];
    [button setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:@"btn_accessoryarrow"
                                                       ofType:@"png"]]
            forState:UIControlStateSelected];
    
    return (button);
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event   {
    NSIndexPath * indexPath = [_searchFtrTable indexPathForRowAtPoint:[[[event touchesForView: button] anyObject] locationInView:_searchFtrTable]];
    if ( indexPath == nil )
        return;
    
    [_searchFtrTable.delegate tableView:_searchFtrTable accessoryButtonTappedForRowWithIndexPath:indexPath];
}

#pragma mark

- (void)clearFilterButtonClicked:(id)sender {
    NSLog(@"EMAM SearchFilterViewController : Clear Search Filter...");
    [Datastore sharedStore].projectId = 0;
    [Datastore sharedStore].subCategoryId = 0;
    [Datastore sharedStore].parentCategoryId = 0;
    [Datastore sharedStore].assetMinSize = 0;
    [Datastore sharedStore].assetMaxSize = 0;
    [Datastore sharedStore].assetTypeId = 0;
    
    [Datastore sharedStore].assetTypeSearchFilter = nil;
    [Datastore sharedStore].categorySearchFilter = nil;
    [Datastore sharedStore].projectSearchFilter = nil;
    [Datastore sharedStore].assetSizeSearchFilter = nil;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"InSearchFilter"];
    
    [self enableSearchFilterIfExists];
    
    [self updateAssets];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
    int icount = 1;
    
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    int icount = 0;
    
    switch (self.filterType) {
        case InitialFilters:
            icount  = [self.searchfilters count];
            break;
        case ProjectFilter:
            icount  = [[[Datastore sharedStore] projects] count];
            break;
        case CategoryFilter:
            icount  = [[[Datastore sharedStore] subCategoriesWithParentID:categoryId] count];
            break;
        case AssetTypeFilter:
            icount  = [[[Datastore sharedStore] assetTypes] count];
            break;
        case AssetSizeFilter:
            icount  = [[[Datastore sharedStore] assetSizes] count];
            break;
            
        default:
            break;
    }
    
    return icount;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
    //	Return the height of cells in tableView
    CGFloat height = 34.0;
    
    return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"RootCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:APPFONTNAME size:15.0];
           
        }
        // Configure the cell...
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        
        
        switch (self.filterType) {
            case InitialFilters:    {
                if ([self.searchfilters count] > indexPath.row) {
                    
                    NSString *title = [self.searchfilters objectAtIndex:indexPath.row];
                    cell.textLabel.text = title;
                    title = nil;
                    cell.detailTextLabel.text = [self detailTextForCellAtIndex:indexPath.row];
                    cell.accessoryView = [self customDisclosureButton];
                    
                }
                break;
            }
            case ProjectFilter: {
                if ([[[Datastore sharedStore] projects] count] > indexPath.row) {
                    
                    NSDictionary *projects = [[[Datastore sharedStore] projects] objectAtIndex:indexPath.row];
                    cell.textLabel.text = [[projects objectForKey:@"PRJ_NAME"] objectForKey:@"text"];
                    projects = nil;
                   
                }
                break;
            }
            case CategoryFilter:    {
                NSMutableArray *categories = [[Datastore sharedStore] subCategoriesWithParentID:self.categoryId];
                if ([categories count] > indexPath.row) {
                    
                    NSDictionary *category = [categories objectAtIndex:indexPath.row];
                    cell.textLabel.text = [[category objectForKey:@"CAT_NAME"] objectForKey:@"text"];
                    int catId = [[[category objectForKey:@"CAT_ID"] objectForKey:@"text"] intValue];
                    category = nil;
                    NSArray *subCategory = [[Datastore sharedStore] subCategoriesWithParentID:catId];
                    if ([subCategory count] > 0) {
                        cell.accessoryView = [self customDisclosureButton];
                    }
                }
                break;
            }
            case AssetTypeFilter:   {
                if ([[[Datastore sharedStore] assetTypes] count] > indexPath.row) {
                    
                    NSDictionary *assetTypes = [[[Datastore sharedStore] assetTypes] objectAtIndex:indexPath.row];
                    cell.textLabel.text = [[assetTypes objectForKey:@"TYP_NAME"] objectForKey:@"text"];
                    assetTypes = nil;
                    
                }
                break;
            }
            case AssetSizeFilter:   {
                if ([[[Datastore sharedStore] assetSizes] count] > indexPath.row) {
                    
                    NSDictionary *assetSizes = [[[Datastore sharedStore] assetSizes] objectAtIndex:indexPath.row];
                    cell.textLabel.text = [[assetSizes objectForKey:@"SIZ_TYPE"] objectForKey:@"text"];
                    assetSizes = nil;
                }
                break;
            }
            default:
                break;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM SearchFilterViewController : Exception for cellForAtIndexPath in SearchFilterViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    
    BOOL needSync = NO;
    switch (self.filterType) {
        case InitialFilters:    {
            break;
        }
        case ProjectFilter: {
            if ([[[Datastore sharedStore] projects] count] > indexPath.row) {
                
                NSDictionary *projects = [[[Datastore sharedStore] projects] objectAtIndex:indexPath.row];
                int projectId = [[[projects objectForKey:@"PRJT_ID"] objectForKey:@"text"] intValue];
                [Datastore sharedStore].projectId = projectId;
                
                [Datastore sharedStore].projectSearchFilter = [[projects objectForKey:@"PRJ_NAME"] objectForKey:@"text"];
                
                NSLog(@"EMAM SearchFilterViewController : Filter with Project : %@",[Datastore sharedStore].projectSearchFilter);
                
                needSync = YES;
            }
            break;
        }
        case CategoryFilter:    {
            if ([[[Datastore sharedStore] subCategoriesWithParentID:categoryId] count] > indexPath.row) {
                
                NSDictionary *category = [[[Datastore sharedStore] subCategoriesWithParentID:self.categoryId] objectAtIndex:indexPath.row];
                //int parentId = [[[category objectForKey:@"PARENT_ID"] objectForKey:@"text"] intValue];
                int catId = [[[category objectForKey:@"CAT_ID"] objectForKey:@"text"] intValue];
                [Datastore sharedStore].parentCategoryId = 0;
                [Datastore sharedStore].subCategoryId = catId;
                
                [Datastore sharedStore].categorySearchFilter = [[category objectForKey:@"CAT_NAME"] objectForKey:@"text"];
                
                NSLog(@"EMAM SearchFilterViewController : Filter with Category : %@",[Datastore sharedStore].categorySearchFilter);
                
                needSync = YES;
            }
            break;
        }
        case AssetTypeFilter:   {
            if ([[[Datastore sharedStore] assetTypes] count] > indexPath.row) {
                NSDictionary *assetTypes = [[[Datastore sharedStore] assetTypes] objectAtIndex:indexPath.row];
                int typeId = [[[assetTypes objectForKey:@"TYP_ID"] objectForKey:@"text"] intValue];
                [Datastore sharedStore].assetTypeId = typeId;
                
                [Datastore sharedStore].assetTypeSearchFilter = [[assetTypes objectForKey:@"TYP_NAME"] objectForKey:@"text"];
                
                NSLog(@"EMAM SearchFilterViewController : Filter with Asset Type : %@",[Datastore sharedStore].assetTypeSearchFilter);
                
                needSync = YES;
            }
            break;
        }
        case AssetSizeFilter:   {
            if ([[[Datastore sharedStore] assetSizes] count] > indexPath.row) {
                NSDictionary *assetSizes = [[[Datastore sharedStore] assetSizes] objectAtIndex:indexPath.row];
                
                NSString *minMaxSize = [[assetSizes objectForKey:@"MIN_MAX_SIZE"] objectForKey:@"text"];
                NSArray *components = [minMaxSize componentsSeparatedByString:@","];
                if ([components count] >= 2) {
                    int minValue = [[components objectAtIndex:0] intValue];
                    int maxValue = [[components objectAtIndex:1] intValue];
                    
                    [Datastore sharedStore].assetMinSize = minValue;
                    [Datastore sharedStore].assetMaxSize = maxValue;
                    [Datastore sharedStore].sortOption = @"Size";
                    
                    [Datastore sharedStore].assetSizeSearchFilter = [[assetSizes objectForKey:@"SIZ_TYPE"] objectForKey:@"text"];
                    
                    NSLog(@"EMAM SearchFilterViewController : Filter with Asset Size : %@",[Datastore sharedStore].assetSizeSearchFilter);
                    
                    needSync = YES;
                }
            }
            break;
        }
        default:
            break;
    }
    
    if (needSync) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"InSearchFilter"];
        [self updateAssets];
    }
}

- (void)tableView:(UITableView *)tableview accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath    {
    switch (self.filterType) {
        case InitialFilters:    {
            if ([self.searchfilters count] > indexPath.row) {
                
                SearchFilterView *searchfilter = [[SearchFilterView alloc] init];
                searchfilter.delegate = self.delegate;
                searchfilter.filterType = (indexPath.row + 1);
                searchfilter.categoryId = 0;
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                [self.navigationController pushViewController:searchfilter animated:YES];
            }
            break;
        }
        case ProjectFilter: {
            if ([[[Datastore sharedStore] projects] count] > indexPath.row && self.filterType != ProjectFilter) {
                
                NSDictionary *projects = [[[Datastore sharedStore] projects] objectAtIndex:indexPath.row];
                
                SearchFilterView *searchfilter = [[SearchFilterView alloc] init];
                searchfilter.delegate = self.delegate;
                searchfilter.filterType = ProjectFilter;
                searchfilter.title = [[projects objectForKey:@"PRJ_NAME"] objectForKey:@"text"];
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                [self.navigationController pushViewController:searchfilter animated:YES];
            }
            break;
        }
        case CategoryFilter:    {
            NSMutableArray *categories = [[Datastore sharedStore] subCategoriesWithParentID:self.categoryId];
            if ([categories count] > indexPath.row) {
                
                NSDictionary *category = [categories objectAtIndex:indexPath.row];
                int catId = [[[category objectForKey:@"CAT_ID"] objectForKey:@"text"] intValue];
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                NSArray *subCategory = [[Datastore sharedStore] subCategoriesWithParentID:catId];
                if ([subCategory count] > 0) {
                    SearchFilterView *searchfilter = [[SearchFilterView alloc] init];
                    searchfilter.delegate = self.delegate;
                    searchfilter.filterType = CategoryFilter;
                    searchfilter.categoryId = catId;
                    searchfilter.title = [[category objectForKey:@"CAT_NAME"] objectForKey:@"text"];
                    self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                    [self.navigationController pushViewController:searchfilter animated:YES];
                }
                category = nil;
            }
            break;
        }
        case AssetTypeFilter:   {
            if ([[[Datastore sharedStore] assetTypes] count] > indexPath.row && self.filterType != AssetTypeFilter) {
                
                SearchFilterView *searchfilter = [[SearchFilterView alloc] init];
                searchfilter.delegate = self.delegate;
                searchfilter.filterType = AssetTypeFilter;
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                [self.navigationController pushViewController:searchfilter animated:YES];
            }
            break;
        }
        case AssetSizeFilter:   {
            if ([[[Datastore sharedStore] assetSizes] count] > indexPath.row && self.filterType != AssetSizeFilter) {
                
                SearchFilterView *searchfilter = [[SearchFilterView alloc] init];
                searchfilter.delegate = self.delegate;
                searchfilter.filterType = AssetSizeFilter;
                self.navigationController.navigationBar.tintColor=[UIColor darkGrayColor];
                [self.navigationController pushViewController:searchfilter animated:YES];
            }
            break;
        }
        default:
            break;
    }
}


@end

//
//  EBinViewController.m
//  eMAM
//
//  Created by APPLE on 22/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "EBinViewController.h"
#import "AssetService.h"
#import "AdvancedESendView.h"

@interface EBinViewController (){
    int currentPage;
    int totalNumberOfAssets;
    int totalPage;
}

@property (nonatomic, strong) NSArray *eBinAssets;

@end

@implementation EBinViewController

@synthesize eBinAssets = _eBinAssets;

-(void)updatePageLbl{
    int totalResults = [self.eBinAssets count];
    totalPage = ((totalResults / 40) + 1);
    _pgNoLbl.text=[NSString stringWithFormat:@"Page %d of %d",currentPage,totalPage];
    _pgNoLbl.hidden=NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIColorFromRGB(0x585858);
    _eBinCollecnView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _doneBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _delAllBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _sendBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _downldBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _navBar.tintColor=UIColorFromRGB(0x585858);
    
    UINib *libCellNib = [UINib nibWithNibName:@"LibraryCollectionCell" bundle:nil];
    [_eBinCollecnView registerNib:libCellNib forCellWithReuseIdentifier:@"LibraryCell"];
    
    currentPage = 1;
    totalNumberOfAssets = 0;
    totalPage=1;
    if (totalNumberOfAssets <= 0) {
        
        _eBinCollecnView.hidden = YES;
    }
    
    [self getAssetsInBasketAtPage:currentPage];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Handlers

- (void)getAssetsInBasketAtPage:(int)page    {
   
    NSLog(@"EMAM EBinViewController : Exception on getAssetsInBasketAtPage : Current Page is less than 0");
    
    
    
    if (page>0) {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        
        AssetService *assetService = [AssetService sharedInstance];
        [assetService getAssetsInBasketAtPage:page performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (dictionary) {
                
                NSDictionary *xmlResponse = [[[[dictionary objectForKey:@"GetAssetsInBasketResponse"] objectForKey:@"GetAssetsInBasketResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
                
                NSLog(@"EMAM EBinViewController : Response for Get Assets In Basket : %@",xmlResponse);
                
                totalNumberOfAssets = [[[dictionary objectForKey:@"TotalNoOfAssests"] objectForKey:@"text"] intValue];
                
                id assetList = [xmlResponse objectForKey:@"Table"];
                if (! assetList) {
                    self.eBinAssets = nil;
                    totalNumberOfAssets = 0;
                    NSLog(@"EMAM EBinViewController : No AssetList Found...");
                }
                if ([assetList isKindOfClass:[NSArray class]]) {
                    self.eBinAssets = assetList;
                } else if ([assetList isKindOfClass:[NSDictionary class]]) {
                    self.eBinAssets = [NSArray arrayWithObject:assetList];
                }
                [_eBinCollecnView reloadData];
                _eBinCollecnView.hidden = NO;
                [self updatePageLbl];
                
            }
            else{
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    }
   
}


#pragma mark

- (void)imageForURL:(NSString *)strURL completionBlock:(void(^)(UIImage *image))block {
    
    NSString *folderPath = [LIBRARY_FOLDER stringByAppendingPathComponent:@"Thumbnails"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *error = nil;
        BOOL sucesss = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (!sucesss) {
            NSLog(@"EMAM AssetGridCell Exception : fail to create intermediate directory : %@",[error description]);
        }
    }
    
    UIImage *image = nil;
    NSString *filePath = [folderPath stringByAppendingPathComponent:[strURL lastPathComponent]];
    if (filePath) {
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    
    if (image) {
        block(image);
    }
    else {
        NSURL *imageURL = [NSURL URLWithString:strURL];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                       ^{
                           NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                           UIImage *image = [UIImage imageWithData:imageData];
                           if(image) {
                               [imageData writeToFile:filePath atomically:YES];
                               block(image);
                           }
                       });
        imageURL = nil;
    }
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.eBinAssets count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"LibraryCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UIButton *ebinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ebinBtn.tag=(indexPath.row+110);
    [ebinBtn setTitle:@"Remove eBin" forState:UIControlStateNormal];
    ebinBtn.titleLabel.textColor=[UIColor whiteColor];
    ebinBtn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
    ebinBtn.backgroundColor=[UIColor darkGrayColor];
    ebinBtn.frame = CGRectMake(79.0, 85.0, 86.0, 17.0);
    [ebinBtn addTarget:self action:@selector(addToEbinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:ebinBtn];
    
    
    
    
    
    UILabel *assetNameLbl = (UILabel *)[cell viewWithTag:101];
    assetNameLbl.text=@"";
    UILabel *assetSizeLbl = (UILabel *)[cell viewWithTag:102];
    assetSizeLbl.text=@"";
    UILabel *assetDateLbl = (UILabel *)[cell viewWithTag:103];
    assetDateLbl.text=@"";
    UILabel *objLabelFileType = (UILabel *)[cell viewWithTag:106];
    assetDateLbl.text=@"";
    if (objLabelFileType) {
        [objLabelFileType removeFromSuperview];
    }
    
    UIActivityIndicatorView *actOld = (UIActivityIndicatorView *)[cell viewWithTag:107];
    if (actOld) {
        [actOld removeFromSuperview];
    }
    
    UIImageView *assetThumbnail = (UIImageView *)[cell viewWithTag:104];
    assetThumbnail.image=nil;
    UIImageView *assetTypeImg = (UIImageView *)[cell viewWithTag:105];
    assetTypeImg.image=nil;

    
    
    
    assetNameLbl.text=[[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"TITLE"] objectForKey:@"text"];
    assetSizeLbl.text=[[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"SIZE"] objectForKey:@"text"];
    assetDateLbl.text=[[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"INGESTED_ON"] objectForKey:@"text"];

    
    
    
    
    UIActivityIndicatorView *act= [[UIActivityIndicatorView alloc] init];
    act.tag=107;
    act.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhite;
    act.center=assetThumbnail.center;
    [assetThumbnail addSubview:act];
    [act startAnimating];
    
    
    
    
    NSString *strPath = [[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
    
    NSLog(@"strPath:%@",[self.eBinAssets objectAtIndex:indexPath.row]);
    NSLog(@"strPath:%@",strPath);
    if (strPath) {
        [self imageForURL:strPath completionBlock:^(UIImage *image) {
            [assetThumbnail setImage:image];
            [act stopAnimating];
        }];
    }else {
        assetThumbnail.image=nil;
        [act stopAnimating];
        NSString *typeName = [[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"TYPE_NAME"] objectForKey:@"text"];
        if ([typeName isEqualToString:@"Audio"]) {
            
            assetThumbnail.image=[UIImage imageNamed:@"audio_asset.jpg"];
            
        }
        else  if ([typeName isEqualToString:@"Other Files"]) {
            assetThumbnail.image=nil;
            NSString *fileType = [[[self.eBinAssets objectAtIndex:indexPath.row] objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
            NSString *fileName = [NSString stringWithFormat:@"%@_asset.jpg",fileType];
            UIImage *image = [UIImage imageNamed:fileName];
            if (image) {
                [assetThumbnail setImage:image];
            }
            else {
                NSLog(@"EMAM AssetGridCell : No Image Found For Type : %@",fileType);
                UILabel *objLabelFileType = [[UILabel alloc] init];
                objLabelFileType.tag=106;
                objLabelFileType.numberOfLines = 1;
                objLabelFileType.textAlignment = NSTextAlignmentCenter;
                objLabelFileType.font = [UIFont fontWithName:APPBOLDFONTNAME size:18.0];
                
                [objLabelFileType setText:[[NSString stringWithFormat:@"%@ File",fileType] uppercaseString]];
                objLabelFileType.textColor = [UIColor whiteColor];
                objLabelFileType.backgroundColor = [UIColor clearColor];
                objLabelFileType.frame = CGRectMake(0,((assetThumbnail.frame.size.height/2) - 10), 170,20);
                [assetThumbnail addSubview:objLabelFileType];
                objLabelFileType=nil;
            }
        }
    }
    return cell;
    
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AssetViewController *assetVieweController = [[AssetViewController alloc] init];
    assetVieweController.asset = [self.eBinAssets objectAtIndex:indexPath.row];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:assetVieweController];
    navigationController.navigationBarHidden=NO;
    
    [self presentViewController:navigationController animated:YES completion:nil];
    
    
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex  {
    if(alertView.tag == 2030)   {
        if (buttonIndex == 0) { //YES
            [self removeAllAssetsFromUserBasket];
        }
    }
}

- (void)addToEbinButtonClicked:(id)sender   {
    
    UIButton *btn=(UIButton *)sender;
    NSLog(@"Del Tag : %d",(btn.tag-110));
    NSLog(@"commentId: %@",btn.titleLabel.text);
    
    NSDictionary *asset = [self.eBinAssets objectAtIndex:btn.tag-110];
    
        
        [self removeFromBasketAsset:asset];
    
    
}
- (void)removeFromBasketAsset:(NSDictionary *)asset {
    
    
    int assetId = [[[asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = [[[asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
    
    AssetService *assetService = [AssetService sharedInstance];
    [assetService removeFromUserBasketAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSString *responseString = [[[dictionary objectForKey:@"UpdateUserBasketResponse"] objectForKey:@"UpdateUserBasketResult"] objectForKey:@"text"];
        NSLog(@"Remove Asset from User Basket Response : %@",responseString);
        
        [self getAssetsInBasketAtPage:currentPage];
        [_eBinCollecnView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLib" object:nil];
        
        
        
        
    }];
}



- (void)removeAllAssetsFromUserBasket {
    
    NSLog(@"EMAM EBinViewController : Remove All Assets In Basket");
    
    
    
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:hud];
    hud.labelText = @"Loading...";
    
    [hud showAnimated:YES whileExecutingBlock:^{
        
        AssetService *assetService = [AssetService sharedInstance];
        [assetService removeAllItemFromUserBasketPerformRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (dictionary) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLib" object:nil];
                
                NSString *response = [[[dictionary objectForKey:@"UpdateUserBasketResponse"] objectForKey:@"UpdateUserBasketResult"] objectForKey:@"text"];
                
            
                NSLog(@"EMAM EBinViewController : Response for Remove All Assets In Basket : %@",response);
                [self getAssetsInBasketAtPage:currentPage];
                [_eBinCollecnView reloadData];
                _eBinCollecnView.hidden = NO;
                
                
            }
            else{
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
            }
            
        }];
        
    } completionBlock:^{
        [hud removeFromSuperview];
        
    }];
    
    
}

- (NSMutableDictionary *)eSendDetails {
    NSMutableDictionary *eSendInfo = [NSMutableDictionary dictionaryWithCapacity:20];
    // Set the default values.
    NSString *reelName = [NSString stringWithFormat:@"Message-%@",[Utilities getUUID]];
    [eSendInfo setValue:reelName forKey:@"ReelName"];
    [eSendInfo setValue:@"HTML" forKey:@"EmailType"];
    [eSendInfo setValue:[NSNumber numberWithInt:0] forKey:@"AccessLimit"];
    
    NSString *assetIds = [[[self.eBinAssets valueForKey:@"ID"] valueForKey:@"text"] componentsJoinedByString:@","];
    [eSendInfo setValue:assetIds forKey:@"AssetIds"];
    NSString *assetVersionIds = [[[self.eBinAssets valueForKey:@"VERSION_ID"] valueForKey:@"text"] componentsJoinedByString:@","];
    [eSendInfo setValue:assetVersionIds forKey:@"AssetVersionIds"];
    
    NSLog(@"assetIds : %@ \n assetVersionIds : %@",assetIds,assetVersionIds);
    //    NSString *virtualPath = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
    //    NSString *uuid = [[[[Datastore sharedStore] currentPreviewInfo] objectForKey:@"UUID"] objectForKey:@"text"];
    //    [eSendInfo setValue:[NSString stringWithFormat:@"%@/%@_1.jpg",virtualPath,uuid] forKey:@"ThumbNailUrl"];
    
    NSString *clientLogoImagePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"ClientLogoImagePath"];
    if (! clientLogoImagePath) {
        clientLogoImagePath = @"";
    }
    [eSendInfo setValue:clientLogoImagePath forKey:@"LogoPath"];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    dateComponents = nil;
    
    [eSendInfo setValue:[Utilities stringFromDate:newDate formatString:@"yyyy-MM-ddThh:mm:ss"] forKey:@"ExipryDate"];
    
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"Downloadable"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ReportTracking"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ForwardOption"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"SendCopy"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"AllowComments"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"ClearBasket"];
    [eSendInfo setValue:[NSNumber numberWithBool:NO] forKey:@"UnsubscribeLink"];
    [eSendInfo setValue:[NSNumber numberWithInt:6] forKey:@"EmailThemeId"];
    [eSendInfo setValue:[NSNumber numberWithInt:6] forKey:@"PreviewThemeId"];
    
    return eSendInfo;
}



- (void)showAdvanceEsendOptions {
    AdvancedESendView *advancedEsendViewController = [[AdvancedESendView alloc] init];
    advancedEsendViewController.showSendOption = YES;
    advancedEsendViewController.eSendInfo = [self eSendDetails];
    advancedEsendViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:advancedEsendViewController];
    navigationController.navigationBarHidden=YES;
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)doneTapped:(id)sender {
    
 
  [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)deleteAllTapped:(id)sender {
    
    [Utilities showAlert:APPNAME message:@"Are you sure you want to remove all assets?" delegateObject:self viewTag:2030 otherButtonTitle:@"Ok"];
}

- (IBAction)downldTapped:(id)sender {
}

- (IBAction)eSendTapped:(id)sender {
    
    [self showAdvanceEsendOptions];
}
- (IBAction)upTapped:(id)sender {
    
    if (currentPage<totalPage) {
        currentPage= currentPage+1;
        [self getAssetsInBasketAtPage:currentPage];
    }
}

- (IBAction)downTapped:(id)sender {
    
    if (currentPage>1) {
        currentPage= currentPage-1;
        [self getAssetsInBasketAtPage:currentPage];
    }
}
@end

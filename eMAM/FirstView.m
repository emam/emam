//
//  FirstView.m
//  eMAM
//
//  Created by Chindu Paul on 15/02/17.
//  Copyright © 2017 Empress Cybernetic Systems. All rights reserved.
//

#import "FirstView.h"
#import "Login.h"

@interface FirstView ()

@end

@implementation FirstView

- (void)viewDidLoad {
    [super viewDidLoad];
    
     Login *viewController=[[Login alloc]initWithNibName:@"Login" bundle:nil];
    
    [self presentViewController:viewController animated:NO completion:nil];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

/**************************************************************************************
 *  File Name      : DeliveryViewController.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 19-01-2013
 *  Copyright (C) 2013 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "DeliveryViewController.h"

#import "AdvancedAssetService.h"

@interface DeliveryViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *deliveryProfiles;
@property (nonatomic, strong) NSMutableArray *selectedDeliveryProfiles;

@end

@implementation DeliveryViewController

@synthesize isEDL = _isEDL;
@synthesize tableView = _tableView;
@synthesize deliveryProfiles = _deliveryProfiles;
@synthesize deliveryContents = _deliveryContents;
@synthesize selectedDeliveryProfiles = _selectedDeliveryProfiles;

#pragma mark -

- (NSMutableArray *)selectedDeliveryProfiles    {
    if (!_selectedDeliveryProfiles) {
        _selectedDeliveryProfiles = [[NSMutableArray alloc] init];
    }
    return _selectedDeliveryProfiles;
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil    {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self performSelector:@selector(initView)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)initView    {
    self.title = @"Select Delivery Profiles";
    [self customizeNavigationBar];
    
    //tableview for display filters
    CGRect  tableViewFrame;
    if (ISIPAD) {
        tableViewFrame      =   CGRectMake(0, 0, self.view.frame.size.width, (self.view.frame.size.height));
    }
    else    {
        tableViewFrame      =   CGRectMake(0, 0, self.view.frame.size.width, (self.view.frame.size.height));
    }
    
    self.tableView = [[UITableView alloc] initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    self.tableView.opaque = YES;
	self.tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.tableView setDataSource:(id)self];
	[self.tableView setDelegate:(id)self];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
    
    if (self.isEDL) {
        [self getAllEDLProfiles];
    } else {
        [self getAllDeliveryProfiles];
    }
}

- (void)refreshView {
    if ([self.deliveryProfiles count] == 0) {
        [Utilities showAlert:@"No Delivery Profiles found."];
        [self dismissView];
    } else  {
        [self.tableView reloadData];
    }
}

- (void)dismissView {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)customizeNavigationBar  {
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonClicked:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)cancelButtonClicked:(id)sender  {
    [self dismissView];
}

- (void)doneButtonClicked:(id)sender  {
    if ([self.selectedDeliveryProfiles count] > 0) {
        [self addToDeliverQueueAllowDuplicate:NO];
    } else {
        [Utilities showAlert:@"Select Profile to Delivery."];
    }
}

#pragma mark -

- (void)getAllDeliveryProfiles {
   
    
    int assetId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_ID"] objectForKey:@"text"] intValue];
    int versionId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService getAllDeliveryProfilesForAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id response = [[[[[dictionary objectForKey:@"GetAllDeliveryProfilesResponse"] objectForKey:@"GetAllDeliveryProfilesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
        NSLog(@"EMAM DeliveryViewController : Fetch Delivery Profiles Response : %@",response);
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            response = [NSArray arrayWithObjects:response, nil];
        }
        
        self.deliveryProfiles = response;
        NSLog(@"EMAM DeliveryViewController : Fetch Delivery Profiles : %d",[[self deliveryProfiles] count]);
        [self refreshView];
    }];
}

- (void)getAllEDLProfiles {
    
    int assetId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_ID"] objectForKey:@"text"] intValue];
    int versionId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService getAllSendEDLProfilesForAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        id response = [[[[[dictionary objectForKey:@"GetAllDeliveryProfilesResponse"] objectForKey:@"GetAllDeliveryProfilesResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"] objectForKey:@"Table"];
        //NSLog(@"EMAM DeliveryViewController : eSend Delivery Profiles Response : %@",response);
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            response = [NSArray arrayWithObjects:response, nil];
        }
        
        self.deliveryProfiles = response;
        NSLog(@"EMAM DeliveryViewController : Fetch EDL Delivery Profiles : %d",[[self deliveryProfiles] count]);
        [self refreshView];
    }];
}

- (void)addToDeliverQueueAllowDuplicate:(BOOL)allow   {
    
    int assetId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_ID"] objectForKey:@"text"] intValue];
    int versionId = [[[[Datastore sharedStore].currentPreviewInfo objectForKey:@"ASSET_VERSION"] objectForKey:@"text"] intValue];
    NSString *addedDate = [Utilities getUTCFormateDate:[NSDate date]] ;
    
    NSLog(@"EMAM DeliveryViewController : Add Delivery Profiles Count : %d",[[self selectedDeliveryProfiles] count]);
    NSLog(@"EMAM DeliveryViewController : EDL : %@ Allow Duplicate : %@",(self.isEDL ? @"YES" : @"NO"),(allow ? @"YES" : @"NO"));
    
    AdvancedAssetService *assetService = [AdvancedAssetService sharedInstance];
    [assetService UpdateDeliveryQueueDetailsForAssetId:assetId versionId:versionId addedDate:addedDate markers:self.deliveryContents profiles:self.selectedDeliveryProfiles allowDuplicate:allow isEDL:self.isEDL performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSLog(@"EMAM DeliveryViewController : Add to Delivey Queue Response : %@",dictionary);
        NSString * resultId =[ [dictionary valueForKey:@"ResponseId"]valueForKey:@"text"];
        NSLog(@"EMAM DeliveryViewController : Add to Delevery Queue Response Id : %@ Message : %@",resultId,[[dictionary objectForKey:@"ResponseMessage"] valueForKey:@"text"]);
//        if (resultId == 0) {
//            [Utilities showAlert:@"Unknown response from server, Please contact the developer."];
//        } else if (resultId > 0 && resultId < 10) {
//            [Utilities showAlert:[dictionary objectForKey:@"ResultString"]];
//            [self dismissView];
//        } else if(resultId == 10)  {
//                [Utilities showAlert:APPNAME message:[dictionary objectForKey:@"ResultString"] delegateObject:self viewTag:1001 otherButtonTitle:@"YES" secondButtonTitle:@"NO"];;
//        }
        
        NSString * resultString = [[dictionary valueForKey:@"ResponseStatus"] valueForKey:@"text"];
        
        
        NSLog(@"EMAM DeliveryViewController : Add to Delevery Queue Response Id : %@ Message : %@",resultString,[dictionary objectForKey:@"ResponseMessage"]);
        //        if (resultId == 0) {
 
        
        if ([resultString isEqualToString:@"Success"]) {
            [Utilities showAlert:@"Clips queued successfully for delivery."];
        }else{
            [Utilities showAlert:@"Unknown response from server, Please contact the developer."];
        }
        
        [self dismissView];
        
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex  {
    if(alertView.tag == 1001)   {
        if (buttonIndex == 0) { //YES
            [self addToDeliverQueueAllowDuplicate:YES];
        } else if (buttonIndex == 1) { // NO
            [self dismissView];
        }
    }
}

#pragma mark - UITableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableview {
    // Return the number of sections.
	int icount = 1;
    return icount;
}

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	int icount = [self.deliveryProfiles count];
    return icount;
}

- (CGFloat) tableView: (UITableView *) tableview heightForRowAtIndexPath: (NSIndexPath *) indexPath	{
	//	Return the height of cells in tableView
    CGFloat height = 44.0;
	return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell =nil;
    
    @try    {
        static NSString *CellIdentifier = @"DeliveryCell";
        
        cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        // Configure the cell...
        cell.accessoryType = UITableViewCellAccessoryNone;
        NSDictionary *deliveryProfile = [self.deliveryProfiles objectAtIndex:indexPath.row];
        cell.textLabel.text = [[deliveryProfile objectForKey:@"DELIVERYPROFILENAME"] objectForKey:@"text"];
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;
        
        if ([self.selectedDeliveryProfiles containsObject:deliveryProfile]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"EMAM DeliveryViewController : Exception for cellForAtIndexPath in DeliveryViewController : %@",[exception description]);
    }
    @finally    {
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Navigation logic may go here. Create and push another view controller.
    UITableViewCell *cell = [tableview cellForRowAtIndexPath:indexPath];
    
    NSDictionary *deliveryProfile = [self.deliveryProfiles objectAtIndex:indexPath.row];
    
    int index=indexPath.row;
    
    NSLog(@"deliveryProfile index:%d",index);
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        self.selectedDeliveryProfiles =[[NSMutableArray alloc] initWithObjects:deliveryProfile, nil];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        [self.selectedDeliveryProfiles removeObjectAtIndex:index];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self.tableView reloadData];
   
    NSLog(@"electedDeliveryProfiles:%@",self.selectedDeliveryProfiles);
    /*
    
    NSUInteger index = [self.selectedDeliveryProfiles indexOfObject:deliveryProfile];
    
    
    
    NSLog(@"deliveryProfile:%@",deliveryProfile);
    
    if (index != NSNotFound) {
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            [self.selectedDeliveryProfiles addObject:deliveryProfile];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            [self.selectedDeliveryProfiles removeObjectAtIndex:index];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
     
     
     */
}

@end

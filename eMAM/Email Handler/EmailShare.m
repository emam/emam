/**************************************************************************************
 *  File Name      : EmailShare.m
 *  Project Name   : <>
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 26/08/2011
 *  Copyright (C) 2011 naveenshan01@gmail.com. All Rights Reserved.
 ***************************************************************************************/

#import "EmailShare.h"

@implementation EmailShare

@synthesize mailSubject;
@synthesize messageBody;
@synthesize toRecipients;
@synthesize ccRecipients;
@synthesize bccRecipients;
@synthesize attachmentList;

#pragma mark -
//To get top ViewControoler
- (UIViewController *)getTopViewController  {
    @try {
        UIViewController *topViewController;
        // Try to find the root view controller programmically
        
        // Find the top window (that is not an alert view or other window)
        UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
        if (topWindow.windowLevel != UIWindowLevelNormal)   {
            
            NSArray *windows = [[UIApplication sharedApplication] windows];
            for(topWindow in windows)   {
                if (topWindow.windowLevel == UIWindowLevelNormal)
                    break;
            }
        }
        
        UIView *rootView = [[topWindow subviews] objectAtIndex:0];	
        id nextResponder = [rootView nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]]) {

            topViewController = nextResponder;
        } else if ([nextResponder isKindOfClass:[UINavigationController class]])    {
            topViewController = nextResponder;
        }    else
            NSLog(@"EmailViewController : Could not find a root view controller. May have some un-wanted subviews in application window");
        

        // Wrap the view in a nav controller if not already
        if ([topViewController respondsToSelector:@selector(presentModalViewController:animated:)])    {
        }
        else    {
            NSLog(@"EmailViewController : Could not find a root view controller. .");
        }
        
        return topViewController;
    }
    @catch (NSException *exception) {
        NSLog(@"EmailViewController : Exception : in getTopViewController : %@ ",[exception description]);
    }
    @finally {
        
    }  
    return nil;
}

#pragma mark Mail

- (BOOL)canSendMail {
    return [MFMailComposeViewController canSendMail];
}

//To send mail
- (void)sendMail    {
    if([MFMailComposeViewController canSendMail])   {
        MFMailComposeViewController *mailComposeViewController=[[MFMailComposeViewController alloc] init];
        mailComposeViewController.mailComposeDelegate=self;
         
        [mailComposeViewController setSubject:self.mailSubject];
        [mailComposeViewController setToRecipients:self.toRecipients];
        [mailComposeViewController setCcRecipients:self.ccRecipients];
        [mailComposeViewController setBccRecipients:self.bccRecipients];
        [mailComposeViewController setMessageBody:self.messageBody isHTML:YES];
        
        for(NSValue *value in attachmentList)   {
            struct Attachment attach ;
            [value getValue:&attach];
            
            if (attach.path)    {
                NSData *data=[NSData dataWithContentsOfFile:attach.path];
                [mailComposeViewController addAttachmentData:data mimeType:attach.attachmentMimeType fileName:attach.attachmentFilename];
            }
            else    {
                NSLog(@"EmailViewController : Fail to find attachment file details : %@ Path : %@",attach.attachmentFilename,attach.path);
            }
        }
        
        UIViewController *topViewController = [self getTopViewController];
        
        mailComposeViewController.navigationBar.barStyle = UIBarStyleBlack;
        [mailComposeViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [mailComposeViewController setModalPresentationStyle:UIModalPresentationFullScreen];
         
        [topViewController presentModalViewController:mailComposeViewController animated:YES];
        [mailComposeViewController release];
        mailComposeViewController = nil;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPNAME message:@"Mail account is not configured in the device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error        {
    NSString *message ;
    // Notifies users about errors associated with the interface
    switch (result)                {
            
        case MFMailComposeResultCancelled:
            message = @"Message Deleted";
            break;
        case MFMailComposeResultSaved:
            message = @"Message Saved";
            break;
        case MFMailComposeResultSent:
            message = @"Message Sent";
            break;
        case MFMailComposeResultFailed:
            message = @"Failed";
            break;
        default:
            message = @"Not sent";
            
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPNAME message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
    alert = nil;

    UIViewController *topViewController = [self getTopViewController];
    
    controller.delegate = nil;
    self.mailSubject=nil;
    self.messageBody=nil;
    self.toRecipients=nil;
    self.ccRecipients=nil;
    self.bccRecipients=nil;
    self.attachmentList=nil;
    
    [topViewController dismissModalViewControllerAnimated:YES];
}

#pragma mark memory management

- (void)dealloc {
    self.mailSubject=nil;
    self.messageBody=nil;
    self.toRecipients=nil;
    self.ccRecipients=nil;
    self.bccRecipients=nil;
    self.attachmentList=nil;
 
    [super dealloc];
}

@end

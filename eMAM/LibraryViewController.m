//
//  LibraryViewController.m
//  eMAM
//
//  Created by APPLE on 28/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//
//MARK: - Final
#import "LibraryViewController.h"
#import "AssetService.h"
#import "OfflineDownloads.h"
#import "UserService.h"

@interface LibraryViewController ()

@end

@implementation LibraryViewController
int currentPage;
int totalPage;
@synthesize assetsArray, IngestPicker, ingestContainer, libraryCollectionView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)setToDefaultValueForAdvanceSearch   {
    NSLog(@"EMAM LibraryViewController : Set Default Search Options...");
    
    [Datastore sharedStore].searchString = @"";
    
    [Datastore sharedStore].advancedSearchOption = @"Contain all these words";
    
    [Datastore sharedStore].needFullTextSearch = YES;
    [Datastore sharedStore].includeSubCategoriesInSearch = YES;
    
    [[Datastore sharedStore] setAdvancedSearchInOptions:[NSMutableArray arrayWithObjects:@"Title",@"Ingested by",@"Closed Caption",@"Asset ID",@"Description",@"Asset tags",@"Embedded Metadata",@"UUID",@"Author",@"Comments",@"Custom Metadata", nil]];
    [[Datastore sharedStore] setAdvancedRefineSearchOptions:[NSMutableArray arrayWithObjects:@"Video",@"Audio",@"Image",@"Other Files", nil]];
    [[Datastore sharedStore] setAdvancedSearchAssetStates:[NSMutableArray arrayWithObjects:@"Online Storage",@"Archived", nil]];
    [[Datastore sharedStore] setAdvancedSearchApprovalStates:[NSMutableArray arrayWithObjects:@"All", nil]];
    if (! [Datastore sharedStore].advancedSearchRating) {
        [Datastore sharedStore].advancedSearchRating = 0;
    }
}


- (void)setDefaultValues    {
    // Set Default Values
    [[Datastore sharedStore] setAssetsPerPage:40];
    [Datastore sharedStore].totalAssets = 1;
    [Datastore sharedStore].sortOption = @"Ingested On";
    [Datastore sharedStore].sortOrder = @"DESC";
    
    // To clear Search results
    [[Datastore sharedStore] clearAllFilters];
    // To reset Search Options.
    [self setToDefaultValueForAdvanceSearch];
    // To clear search filters
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"InSearchFilter"];
       NSLog(@"EMAM LibraryViewController : Set Default Values...");
}


-(void)updatePageLbl{
    int totalResults = [Datastore sharedStore].totalAssets;
    totalPage = ((totalResults / [Datastore sharedStore].assetsPerPage) + 1);
    _pgNoLbl.text=[NSString stringWithFormat:@"Page %d of %d",currentPage,totalPage];
    _pgNoLbl.hidden=NO;
}

#pragma mark - Data Handlers

- (void)getAssetsAtPage:(int)page    {
    
    NSLog(@"LibraryViewController : Load Assets for Page : %d",page);
    
    
    if (page>0) {
        [self.view addSubview:hud];
        hud.labelText = @"Loading..";
        
        [hud show:YES];
        AssetService *assetService = [AssetService sharedInstance];
        
        [assetService getAssetsAtPage:page performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
            
            if (error) {
                [hud removeFromSuperview];
                NSLog(@"EMAM LibraryViewController : Load Assets for Page Failed : %@",[error description]);
                if (error.code == 400) {
                    //[self authenticationFailed];
                }
                [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
                return;
                
            }else{
                
                // NSLog(@"LibraryViewController : Assets :%@",dictionary);
                assetsArray=[[NSMutableArray alloc] initWithArray:[[Datastore sharedStore] assets]];
            }
            [hud removeFromSuperview];
            [libraryCollectionView reloadData];
            [self updatePageLbl];
            
        }];
        
    }
    
}

#pragma mark

- (void)imageForURL:(NSString *)strURL completionBlock:(void(^)(UIImage *image))block {
    
    NSString *folderPath = [LIBRARY_FOLDER stringByAppendingPathComponent:@"Thumbnails"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *error = nil;
        BOOL sucesss = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (!sucesss) {
            NSLog(@"EMAM AssetGridCell Exception : fail to create intermediate directory : %@",[error description]);
        }
    }
    
    UIImage *image = nil;
    NSString *filePath = [folderPath stringByAppendingPathComponent:[strURL lastPathComponent]];
    if (filePath) {
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    
    if (image) {
        block(image);
    }
    else {
        NSURL *imageURL = [NSURL URLWithString:strURL];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                       ^{
                           NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                           UIImage *image = [UIImage imageWithData:imageData];
                           if(image) {
                               [imageData writeToFile:filePath atomically:YES];
                               block(image);
                           }
                       });
        imageURL = nil;
    }
}

-(void)updateLibrary{
    [self getUserBasket];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.SubmitButton.layer.cornerRadius = 5;
    self.SubmitButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.SubmitButton.layer.borderWidth = 1;
    self.CancelButton.layer.cornerRadius = 5;
    self.CancelButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.CancelButton.layer.borderWidth = 1;
    self.containerScroll.contentSize = CGSizeMake(0, 1000);
    
    [self.IngestPicker registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ingest"];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    isPlay = NO;
    is_Video = NO;
    ContentUrl = [NSURL URLWithString:@""];
    self.moviewPlayer.hidden = true;
    self.ingestImageView.hidden = true;
    
    ingestRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [ingestRecognizer setNumberOfTapsRequired:1];
    [ingestRecognizer setNumberOfTouchesRequired:1];
    
    self.view.backgroundColor=UIColorFromRGB(0x585858);
    libraryCollectionView.backgroundColor=UIColorFromRGB(0x3B3B3B);
    _sortBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _searchFilterBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _eBinBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _advSearchBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _logOutBtn.titleLabel.textColor=UIColorFromRGB(0xCCCCCC);
    _navBar.tintColor=UIColorFromRGB(0x585858);
    _pgNoLbl.textColor=UIColorFromRGB(0xCCCCCC);
    _pgNoLbl.hidden=YES;
    
    percentageUploaded = 0;
    self.ingestImageView.layer.borderWidth = 1;
    self.ingestImageView.layer.borderColor = [[UIColor blackColor]CGColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutTapped:) name:@"Authentication Expired" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLibrary) name:@"UpdateLib" object:nil];
    assetsArray = [[NSMutableArray alloc] init];
    currentPage=1;
    UINib *libCellNib = [UINib nibWithNibName:@"LibraryCollectionCell" bundle:nil];
    [libraryCollectionView registerNib:libCellNib forCellWithReuseIdentifier:@"LibraryCell"];
    [self setDefaultValues];
    [self getUserBasket];
    [self getAssetsAtPage:currentPage];
    
    isPickerShown = NO;
    IngestIdArray = [[NSMutableArray alloc]init];
    [UIView animateWithDuration:0.50 animations:^{
        self.pickerContainer.hidden = YES;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
    }];
    
    UserService *userService = [UserService sharedInstance];
    [userService getIngestProfileId:0 unitId:0 ingestProfile:0 performRequestWithHandler:^(NSDictionary *dictionary, NSError *error) {
        NSLog(@"%@", dictionary[@"GetUnitIngestProfilesResponse"][@"GetUnitIngestProfilesResult"][@"IngestProfiles"][@"diffgr:diffgram"][@"NewDataSet"][@"Table"]);
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        resultDict = dictionary[@"GetUnitIngestProfilesResponse"][@"GetUnitIngestProfilesResult"][@"IngestProfiles"][@"diffgr:diffgram"][@"NewDataSet"][@"Table"];
        IngestIdDict = [[NSMutableDictionary alloc]init];
        if (resultDict.count == 36) {
            IngestIdDict = dictionary[@"GetUnitIngestProfilesResponse"][@"GetUnitIngestProfilesResult"][@"IngestProfiles"][@"diffgr:diffgram"][@"NewDataSet"][@"Table"];
            
            NSMutableArray*ingestFilter=   [[NSMutableArray alloc] init];
            
            if (![IngestIdDict[@"XCodeType"][@"text"] isEqualToString: @"3"]) {
                [ingestFilter addObject:IngestIdDict];
            }
            if ([IngestIdDict[@"Is_Default_Profile"][@"text"] isEqualToString: @"true"]) {
                ingestId = IngestIdDict[@"ProfileID"][@"text"];
                [self.ingestIdText setText: IngestIdDict[@"profile_name1"][@"text"]];
            }
            
            if (ingestId == nil) {
                ingestId = IngestIdDict[@"ProfileID"][@"text"];
                [self.ingestIdText setText: IngestIdDict[@"profile_name1"][@"text"]];
            }
            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:KSHAREKEY];
            [shared setValue: ingestFilter forKey:@"eMAMIngestArray"];
            [shared synchronize];
            IngestIdArray = ingestFilter;
        }else {
            IngestIdArray = dictionary[@"GetUnitIngestProfilesResponse"][@"GetUnitIngestProfilesResult"][@"IngestProfiles"][@"diffgr:diffgram"][@"NewDataSet"][@"Table"];
            NSMutableArray*ingestFilter=   [[NSMutableArray alloc] init];
            
            for (NSInteger index = (IngestIdArray.count - 1); index >= 0; index--) {
                //            NSLog(@"IngestIdArray %@ :", IngestIdArray[index][@"profile_name1"][@"text"]);
                if (![IngestIdArray[index][@"XCodeType"][@"text"] isEqualToString: @"3"]) {
                    [ingestFilter addObject:IngestIdArray[index]];
                }
                if ([IngestIdArray[index][@"Is_Default_Profile"][@"text"] isEqualToString: @"true"]) {
                    ingestId = IngestIdArray[index][@"ProfileID"][@"text"];
                    [self.ingestIdText setText: IngestIdArray[index][@"profile_name1"][@"text"]];
                }
            }
            
            if (ingestId == nil) {
                ingestId = IngestIdArray[0][@"ProfileID"][@"text"];
                [self.ingestIdText setText: IngestIdArray[0][@"profile_name1"][@"text"]];
            }
            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:KSHAREKEY];
            [shared setValue: ingestFilter forKey:@"eMAMIngestArray"];
            [shared synchronize];
            IngestIdArray = ingestFilter;
        }
        
//        NSLog(@"%@", IngestIdArray);
    }];

//    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setLeftPadding) userInfo:nil repeats: NO];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [assetsArray count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"LibraryCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary *asset = [assetsArray objectAtIndex:indexPath.row];
    
    
    
    UIButton *ebinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ebinBtn.tag=(indexPath.row+110);
    [ebinBtn setTitle:@"Add to eBin" forState:UIControlStateNormal];
    ebinBtn.titleLabel.textColor=[UIColor whiteColor];
    ebinBtn.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    ebinBtn.backgroundColor=[UIColor darkGrayColor];
    ebinBtn.frame = CGRectMake(67.0, 85.0, 99.0, 17.0);
    NSMutableArray *basketArray=[[NSMutableArray alloc] initWithArray:[Datastore sharedStore].basketAssets];
    for(int i=0; i < [basketArray count]; i++) {
        
        NSString *assetId= [[asset objectForKey:@"ID"] objectForKey:@"text"];
        if ([assetId isEqualToString:[[[basketArray objectAtIndex:i] objectForKey:@"AssetId"] objectForKey:@"text"]]) {
            [ebinBtn setTitle:@"Remove from eBin" forState:UIControlStateNormal];
        }
        
        
    }
    [ebinBtn addTarget:self action:@selector(addToEbinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:ebinBtn];
    
    UILabel *assetNameLbl = (UILabel *)[cell viewWithTag:101];
    assetNameLbl.text=@"";
    UILabel *assetSizeLbl = (UILabel *)[cell viewWithTag:102];
    assetSizeLbl.text=@"";
    UILabel *assetDateLbl = (UILabel *)[cell viewWithTag:103];
    assetDateLbl.text=@"";
    UILabel *objLabelFileType = (UILabel *)[cell viewWithTag:106];
    assetDateLbl.text=@"";
    if (objLabelFileType) {
        [objLabelFileType removeFromSuperview];
    }
    
    UIActivityIndicatorView *actOld = (UIActivityIndicatorView *)[cell viewWithTag:107];
    if (actOld) {
        [actOld removeFromSuperview];
    }
    
    UIImageView *assetThumbnail = (UIImageView *)[cell viewWithTag:104];
    assetThumbnail.image=nil;
    UIImageView *assetTypeImg = (UIImageView *)[cell viewWithTag:105];
    assetTypeImg.image=nil;
    
    assetNameLbl.text=[[asset objectForKey:@"TITLE"] objectForKey:@"text"];
    assetSizeLbl.text=[[asset objectForKey:@"SIZE"] objectForKey:@"text"];
    assetDateLbl.text=[[asset objectForKey:@"INGESTED_ON"] objectForKey:@"text"];
    
    
    UIActivityIndicatorView *act= [[UIActivityIndicatorView alloc] init];
    act.tag=107;
    act.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhite;
    act.center=assetThumbnail.center;
    [assetThumbnail addSubview:act];
    [act startAnimating];

    
    
    
    NSString *strPath = [[asset objectForKey:@"VIRTUAL_PATH"] objectForKey:@"text"];
    if (strPath) {
        [self imageForURL:strPath completionBlock:^(UIImage *image) {
            [assetThumbnail setImage:image];
            [act stopAnimating];
        }];
    }else {
        assetThumbnail.image=nil;
        [act stopAnimating];
        NSString *typeName = [[asset objectForKey:@"TYPE_NAME"] objectForKey:@"text"];
        if ([typeName isEqualToString:@"Audio"]) {
            
            assetThumbnail.image=[UIImage imageNamed:@"audio_asset.jpg"];
            
        }
        else  if ([typeName isEqualToString:@"Other Files"]) {
            assetThumbnail.image=nil;
            NSString *fileType = [[asset objectForKey:@"ORIGINAL_EXTENTION"] objectForKey:@"text"];
            NSString *fileName = [NSString stringWithFormat:@"%@_asset.jpg",fileType];
            UIImage *image = [UIImage imageNamed:fileName];
            if (image) {
                [assetThumbnail setImage:image];
            }
            else {
                NSLog(@"EMAM AssetGridCell : No Image Found For Type : %@",fileType);
                UILabel *objLabelFileType = [[UILabel alloc] init];
                objLabelFileType.tag=106;
                objLabelFileType.numberOfLines = 1;
                objLabelFileType.textAlignment = NSTextAlignmentCenter;
                objLabelFileType.font = [UIFont fontWithName:APPBOLDFONTNAME size:18.0];
                
                [objLabelFileType setText:[[NSString stringWithFormat:@"%@ File",fileType] uppercaseString]];
                objLabelFileType.textColor = [UIColor whiteColor];
                objLabelFileType.backgroundColor = [UIColor clearColor];
                objLabelFileType.frame = CGRectMake(0,((assetThumbnail.frame.size.height/2) - 10), 170,20);
                [assetThumbnail addSubview:objLabelFileType];
                objLabelFileType=nil;
            }
        }
    }

    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AssetViewController *assetVieweController = [[AssetViewController alloc] init];
    assetVieweController.asset = [assetsArray objectAtIndex:indexPath.row];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:assetVieweController];
    navigationController.navigationBarHidden=NO;
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

// MARK: - Actions
- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
}
- (IBAction)ShowPicker:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = YES;
        self.pickerContainer.hidden = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 200);
        
    }];
    [self.IngestPicker reloadData];
}

-(void) dismissKeyboard {
    [self.view endEditing:true];
}
- (IBAction)DismissPicker:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
        self.pickerContainer.hidden = YES;
    }];
}

- (IBAction)cancelClicked:(id)sender {
    [self.view endEditing:YES];
    if (is_Video == YES) {
        isPlay = NO;
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
        [self.videoController stop];
    }
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
    self.authorText.text = userName;
    self.descriptionText.text = @"";
    self.titleText.text = @"";
    self.ingestContainer.hidden = true;
    self.libraryCollectionView.hidden = false;
    [self.libraryCollectionView reloadData];
    self.ingestImageView.image = NULL;
    ContentUrl = [NSURL URLWithString:@""];
    [self.videoController.view removeFromSuperview];

}


- (IBAction)uploadIngestImages:(id)sender {
    [self.view endEditing:YES];
    if (is_Video == YES) {
        isPlay = NO;
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
        [self.videoController stop];
    }
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
    NSString *message;
    if ([[self.titleText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Title Cannot be Empty";
    }else if ([[self.authorText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Author Cannot be Empty";
    }if ([[self.ingestIdText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  isEqualToString: @""]) {
        message = @"Ingest id Cannot be Empty";
    }
    
    if (message != nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                        message: message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else {
        [self postIngest];
    }
}

- (IBAction)Play:(id)sender {
    if (isPlay == NO) {
        if (ContentUrl.absoluteString.length != 0) {
            [self.playButton setBackgroundImage: [UIImage imageNamed:@"Pause.png"] forState: UIControlStateNormal];
            [self.videoController play];
            isPlay = YES;
        }
    }else {
        [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
        [self.videoController pause];
        isPlay = NO;
    }
    
}
- (IBAction)Stop:(id)sender {
    isPlay = NO;
    [self.playButton setBackgroundImage: [UIImage imageNamed:@"Play.png"] forState: UIControlStateNormal];
    [self.videoController stop];
}

- (IBAction)IngestClicked:(id)sender {
//    [self setLeftPadding];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:imagePickerController.sourceType];
    
    imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:imagePickerController animated:YES completion:nil];
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
    self.authorText.text = userName;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"info : %@", info[@"UIImagePickerControllerMediaType"]);
    self.libraryCollectionView.hidden = true;
    self.ingestContainer.hidden = false;
    if ([info[@"UIImagePickerControllerMediaType"]  isEqual: @"public.movie"]) {
        is_Video = YES;
        self.moviewPlayer.hidden = false;
        self.ingestImageView.hidden = true;
        NSURL *movieUrl = info[UIImagePickerControllerMediaURL];
        ContentUrl = movieUrl;
        
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL: movieUrl];
      [self.videoController setScalingMode:MPMovieScalingModeFill];
        [self.videoController.view setFrame: CGRectMake (0, 0, self.moviewPlayer.frame.size.width, self.moviewPlayer.frame.size.height - 45)];
        self.videoController.view.userInteractionEnabled = NO;
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [self.moviewPlayer addSubview:self.videoController.view];
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }else {
        is_Video = NO;
        self.moviewPlayer.hidden = true;
        self.ingestImageView.hidden = false;
        self.ingestImageView.image = info[@"UIImagePickerControllerOriginalImage"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: UploadImagename];
        NSData* data = UIImagePNGRepresentation(self.ingestImageView.image);
        [data writeToFile:path atomically:YES];
        
    }
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"%@", refURL);
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset) {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSLog(@"ImageRep Filename : %@", [imageRep filename]);
        
        NSString *imageName = [imageRep filename];
        
        if (imageName == @"" || imageName == nil) {
            if (is_Video == YES){
                imageName = [[self randomStringWithLength:6] stringByAppendingString:@".MP4"];
            }else {
                imageName = [[self randomStringWithLength:6] stringByAppendingString:@".JPG"];
            }
            
        }
        NSLog(@"Image name : %@", UploadImagename);
        NSArray *getImageFormat = [imageName componentsSeparatedByString: @"."];
        UploadImagename = getImageFormat[0];
        uploadimageFormat = [@"." stringByAppendingString:getImageFormat[getImageFormat.count-1]];
        
        NSLog(@"Image Format : %@", uploadimageFormat);
        self.titleText.text = UploadImagename;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    [picker dismissViewControllerAnimated:true completion:^{
        
    }];
}

-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}
- (void)showStatus {
    if (percentageUploaded < 70) {
        percentageUploaded = percentageUploaded + 20;
        [self.view addSubview:hud];
        [hud show:YES];
        hud.labelText = @"Loading..";
    }else {
        if (is_UploadCompleted != 2) {
            if (is_UploadCompleted == 1) {
                hud.labelText = [NSString stringWithFormat:@"File Upload Sucess.."];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message: @"File uploaded successfully.."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else {
                hud.labelText = [NSString stringWithFormat:@"File Upload Failed.."];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message: @"File Upload Failed.."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
            NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
            self.authorText.text = userName;
            self.descriptionText.text = @"";
            self.titleText.text = @"";
            self.ingestContainer.hidden = true;
            self.libraryCollectionView.hidden = false;
            [self.libraryCollectionView reloadData];
            self.ingestImageView.image = NULL;
            ContentUrl = [NSURL URLWithString:@""];
            [self.videoController.view removeFromSuperview];
            percentageUploaded = 0;
            [UploadTimer invalidate];
            [hud removeFromSuperview];
        }else {
            hud.labelText = @"Loading..";
        }
    }
}
- (void)postIngest {
    is_UploadCompleted = 2;
    
    if (is_Video == true) {
        UploadTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showStatus) userInfo:nil repeats:YES];
    }else {
        [self.view addSubview:hud];
        [hud show:YES];
    }
    
    NSString *boundary = @"eMAMFormBoundary";
    NSData *imageData;
    if (is_Video == YES) {
        imageData = [NSData dataWithContentsOfURL:ContentUrl];
    }else {
        imageData = UIImageJPEGRepresentation(self.ingestImageView.image, 1.0);
    }
    
    NSString *imageName = [[self.titleText.text stringByAppendingString:uploadimageFormat] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    NSString *hostAddress;
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
    NSString *licenceKey = [[NSUserDefaults standardUserDefaults] valueForKey:KSERVERLICENSEKEY];
    NSString *RealRoot = [[NSUserDefaults standardUserDefaults] objectForKey:KROOTSERVER];
    NSString *root = [RealRoot substringToIndex:[RealRoot length]-1];
    NSString *port = [[NSUserDefaults standardUserDefaults] objectForKey:KSERVERPORT];
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:KENCRYPTEDPASSWORD];
    NSString *contentFormat;
    
    if ([uploadimageFormat.lowercaseString isEqualToString:@".png"]) {
        contentFormat = @"image/png";
    }else if ([uploadimageFormat.lowercaseString isEqualToString:@".jpg"]) {
        contentFormat = @"image/jpg";
    }else if ([uploadimageFormat.lowercaseString isEqualToString:@"jpeg"]) {
        contentFormat = @"image/jpeg";
    }else {
        contentFormat = @"application/octet-stream";
    }
    
    if ([port isEqualToString:@""]) {
        hostAddress = [NSString stringWithFormat:@"%@", root];
    }else {
        hostAddress = [NSString stringWithFormat:@"%@:%@", root, port];
    }
    
    NSString * urlString = [NSString stringWithFormat:@"%@/eMAMUploadManager/EMAMUploader", hostAddress];
    
    NSString *jsonString = [NSString stringWithFormat: @"{\"FileName\":\"%@\",\"UserId\":\"%@\",\"Password\":\"%@\",\"LicenseKey\":\"%@\",\"CategoryIds\":\"\",\"Tags\":\"\",\"ProjectIds\":\"\",\"Title\":\"%@\",\"Description\":\"%@\",\"Author\":\"%@\",\"IngestProfileId\":\"%@\",\"MetadatasetId\":\"-1\",\"AppDetails\":\"12:appversion 3.8\",\"CustomMetaData\":[],\"GatewayURL\":\"%@/emamgateway/emamservice.asmx\",\"UnitId\":\"0\",\"UploadType\":\"asset\"}", imageName, userName, password, licenceKey, imageName, self.descriptionText.text, self.authorText.text, ingestId, root];
    
    NSLog(@"%@", jsonString);
    
    NSData  *responseData = nil;
    NSURL *url=[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    responseData = [NSMutableData data] ;
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"multipart/form-data;boundary=eMAMFormBoundary" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"Connection" forHTTPHeaderField:@"Keep-Alive"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"metadata"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=%@\r\n", UploadImagename, imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *ImageContentType = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",contentFormat];
        [body appendData:[ImageContentType dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSLog(@"%@", request);
            NSLog(@"%@", data);
            NSLog(@"%@", response);
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Server Response : %@", responseString);
        
        NSString * message;
        if ([responseString isEqualToString:@"1"]) {
            if (is_Video == YES) {
                message = @"File uploaded successfully..";
            }else {
                message = @"File uploaded successfully..";
            }
        }else if ([responseString isEqualToString:@""]){
            message = @"Upload Failed";
        }else {
            message = responseString;
        }
        
        if(data.length > 0) {
            NSLog(@"%@", message);
            is_UploadCompleted = 1;
            if (is_Video == false) {
                NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:KUSERNAME];
                self.authorText.text = userName;
                self.descriptionText.text = @"";
                self.titleText.text = @"";
                self.ingestContainer.hidden = true;
                self.libraryCollectionView.hidden = false;
                [self.libraryCollectionView reloadData];
                self.ingestImageView.image = NULL;
                ContentUrl = [NSURL URLWithString:@""];
                [self.videoController.view removeFromSuperview];
                percentageUploaded = 0;
                [UploadTimer invalidate];
                [hud removeFromSuperview];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message: message
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
            
//            [self updateLibrary];
            
        }else {
            NSLog(@"%@", message);
            is_UploadCompleted = 0;
            if (is_Video == false) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message: message
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            [hud removeFromSuperview];
        }
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (self.ingestImageView.image == nil) {
        self.ingestContainer.hidden = true;
        self.libraryCollectionView.hidden = false;
        [self.libraryCollectionView reloadData];
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

// MARK: - TextView Delegates
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString: @"\n"]) {
        [textView resignFirstResponder];
        [self.containerScroll setContentOffset:CGPointMake(0, 0)];
        return false;
    }
    return true;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self.view addGestureRecognizer:ingestRecognizer];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer:recognizer];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.containerScroll setContentOffset:CGPointMake(0, 0)];

    return true;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view addGestureRecognizer:ingestRecognizer];
    if (isPickerShown == YES) {
        [UIView animateWithDuration:0.50 animations:^{
            isPickerShown = NO;
            self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
            self.pickerContainer.hidden = YES;
        }];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [self.view removeGestureRecognizer:ingestRecognizer];
    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer:recognizer];
    }
}

// MARK: - TableView Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return IngestIdArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ingest";
    UITableViewCell *cell = [IngestPicker dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font = [cell.textLabel.font fontWithSize:14];
    cell.textLabel.text = IngestIdArray[indexPath.row][@"profile_name1"][@"text"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    ingestId = IngestIdArray[indexPath.row][@"ProfileID"][@"text"];
    [self.ingestIdText setText: IngestIdArray[indexPath.row][@"profile_name1"][@"text"]];
    
    [UIView animateWithDuration:0.50 animations:^{
        isPickerShown = NO;
        self.pickerContainer.frame = CGRectMake(self.showPickerButton.frame.origin.x, self.showPickerButton.frame.origin.y, self.showPickerButton.frame.size.width, 0);
        self.pickerContainer.hidden = YES;
    }];
    
}

// MARK: - Other
- (void)addToEbinButtonClicked:(id)sender   {
    
    UIButton *btn=(UIButton *)sender;
    NSLog(@"Del Tag : %d",(btn.tag-110));
    NSLog(@"commentId: %@",btn.titleLabel.text);
    
    NSDictionary *asset = [assetsArray objectAtIndex:btn.tag-110];
    
    if ([btn.titleLabel.text isEqualToString:@"Add to eBin"]) {
        [self addToBasketAsset:asset];
        
    }else if ([btn.titleLabel.text isEqualToString:@"Remove from eBin"]) {
        
        [self removeFromBasketAsset:asset];
    }
    
    
}

- (void)getUserBasket{
    
    
    AssetService *assetService = [AssetService sharedInstance];
    [assetService getUserBasket:0 performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        NSLog(@"EMAM EBinViewController : dictionary : %@",dictionary);
        
        NSDictionary *xmlResponse = [[[[dictionary objectForKey:@"GetUserBasketResponse"] objectForKey:@"GetUserBasketResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
        
        NSLog(@"EMAM EBinViewController : Response for Get Assets In Basket : %@",xmlResponse);
        
        [self getAssetsAtPage:currentPage];
        
    }];
}




- (void)removeFromBasketAsset:(NSDictionary *)asset {
    
    
    int assetId = [[[asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = [[[asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
    
    AssetService *assetService = [AssetService sharedInstance];
    [assetService removeFromUserBasketAssetId:assetId versionId:versionId performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSString *responseString = [[[dictionary objectForKey:@"UpdateUserBasketResponse"] objectForKey:@"UpdateUserBasketResult"] objectForKey:@"text"];
        NSLog(@"Remove Asset from User Basket Response : %@",responseString);
        
        [self getUserBasket];
        [libraryCollectionView reloadData];
        [Utilities showAlert:APPNAME message:@"Asset removed from eBin successfully" delegateObject:nil];
        
        
        
    }];
}



- (void)addToBasketAsset:(NSDictionary *)asset {
    
    int assetId = [[[asset objectForKey:@"ID"] objectForKey:@"text"] intValue];
    int versionId = [[[asset objectForKey:@"VERSION_ID"] objectForKey:@"text"] intValue];
    
    AssetService *assetService = [AssetService sharedInstance];
    [assetService addToUserBasketAssetId:assetId versionId:versionId  performRequestWithHandler:^(NSDictionary *dictionary, NSError *error){
        
        
        if (error) {
            [Utilities showAlert:APPNAME message:error.localizedDescription delegateObject:nil];
            return;
        }
        
        NSString *responseString = [[[dictionary objectForKey:@"UpdateUserBasketResponse"] objectForKey:@"UpdateUserBasketResult"] objectForKey:@"text"];
        NSLog(@"Add Asset to User Basket Response : %@",responseString);
        if ([responseString boolValue]) {
            [self getUserBasket];
            [libraryCollectionView reloadData];
            [Utilities showAlert:APPNAME message:@"Asset added to eBin successfully”" delegateObject:nil];
        } else  {
            [Utilities showAlert:APPNAME message:@"Fail to Add Asset to Basket..." delegateObject:nil];
        }
        
        
    }];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logOutTapped:(id)sender {
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:KSHAREKEY];
    [shared setObject: @"False" forKey:@"eMAMLoggedIn"];
    [shared synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sortTapped:(id)sender {
    
    UIView *button = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        button = sender;
    } else {
        button = [sender view];
    }
    if (popoverController) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController = nil;
    }
    SortOptionsView *sortOptionsView = [[SortOptionsView alloc] init];
    sortOptionsView.viewType = SortOptions;
    sortOptionsView.delegate = (id)self;
    UINavigationController *sortNavigationController = [[UINavigationController alloc] initWithRootViewController:sortOptionsView];
    sortNavigationController.navigationBarHidden=YES;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:sortNavigationController];
    popoverController.backgroundColor=[UIColor lightGrayColor];
    [popoverController presentPopoverFromRect:button.frame inView:self.navigationController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}


#pragma mark - Sort Options ViewController Delegate

- (void) SortOptionsViewOptionChanged:(NSString *)option ViewType:(ViewType)type    {
    if (popoverController) {
        [popoverController dismissPopoverAnimated:NO];
        popoverController = nil;
    }
    [self getAssetsAtPage:currentPage];
}

#pragma mark - SearchFilter ViewController Delegate

- (void) searchFilterAssetDidFiltered    {
    currentPage = 1;
    NSLog(@"EMAM LibraryViewController : SearchFilter AssetDidFiltered...");
    [self getAssetsAtPage:currentPage];
}


#pragma mark -  AdvancedSearchViewController Delegate

- (void)clearSearchResults  {
    NSLog(@"EMAM LibraryViewController : Clear Search Results...");
    [_assetSearchBar resignFirstResponder];
    [Datastore sharedStore].isInSearch = NO;
    [self getAssetsAtPage:currentPage];
}

- (void)filteredTheDisplayWithSearchOptions {
    NSLog(@"EMAM LibraryViewController : Perform Asset Search...");
    [_assetSearchBar resignFirstResponder];
    [Datastore sharedStore].isInSearch = YES;
    currentPage = 1;
    [self getAssetsAtPage:currentPage];
}

#pragma mark - UISearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchbar {
    [searchbar resignFirstResponder];
    if ([searchbar.text length] != 0) {
        [Datastore sharedStore].searchString = searchbar.text;
        [Datastore sharedStore].isInAdvancedSearch = NO;
        [self filteredTheDisplayWithSearchOptions];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Clear the current result when the user clear the query
    if ([searchText length] == 0) {
        [Datastore sharedStore].searchString = nil;
        [self clearSearchResults];
    }
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return  YES;
}


- (IBAction)eBinTapped:(id)sender {
    
    EBinViewController *ebinViewController = [[EBinViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ebinViewController];
    navigationController.navigationBarHidden=YES;
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

- (IBAction)advancedSearchedTapped:(id)sender {
    
    UIView *button = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        button = (UIButton *)sender;
    }
    if (popoverControllerAdvSearch) {
        [popoverControllerAdvSearch dismissPopoverAnimated:YES];
        popoverControllerAdvSearch = nil;
    }
    
    if (advSearchFilterController) {
        advSearchFilterController = nil;
    }
    advSearchFilterController = [[AdvancedSearchView alloc] init];
    advSearchFilterController.currentType = Normal;
    advSearchFilterController.delegate = (id)self;
    advSearchNavigationController = [[UINavigationController alloc] initWithRootViewController:advSearchFilterController];
    advSearchNavigationController.navigationBar.barTintColor=[UIColor lightGrayColor];
    popoverControllerAdvSearch = [[UIPopoverController alloc] initWithContentViewController:advSearchNavigationController];
    popoverControllerAdvSearch.backgroundColor=[UIColor lightGrayColor];
    popoverControllerAdvSearch.delegate=self;
    
    [popoverControllerAdvSearch presentPopoverFromRect:[(UIButton *)sender frame]
                                                inView:self.view
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:YES];
    
    
    
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    popoverControllerAdvSearch=nil;
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (popoverControllerAdvSearch) {
        
        [popoverControllerAdvSearch dismissPopoverAnimated:YES];
        
    }
    
    if (popoverControllerownloads) {
        
        [popoverControllerownloads dismissPopoverAnimated:YES];
        
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
//        [self.moviewPlayer setFrame:CGRectMake(203, 0, 788, 316)];
        [self.videoController.view setFrame: CGRectMake (0, 0, self.moviewPlayer.frame.size.width, self.moviewPlayer.frame.size.height - 45)];
    }else {
//        [self.moviewPlayer setFrame:CGRectMake(0, 0, 738, 316)];
        [self.videoController.view setFrame: CGRectMake (0, 0, self.moviewPlayer.frame.size.width, self.moviewPlayer.frame.size.height - 45)];
    }
    
    //    if (popoverControllerAdvSearch) {
    //
    //        popoverControllerAdvSearch = [[UIPopoverController alloc] initWithContentViewController:advSearchNavigationController];
    //        popoverControllerAdvSearch.backgroundColor=[UIColor lightGrayColor];
    //        [popoverControllerAdvSearch presentPopoverFromRect:_advSearchBtn.frame inView:self.navigationController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //    }
    
    
    
}



- (IBAction)downTapped:(id)sender {
    
    if (currentPage>1) {
        currentPage= currentPage-1;
        [self getAssetsAtPage:currentPage];
    }
}

- (IBAction)upTapped:(id)sender {
    
    if (currentPage<totalPage) {
        currentPage= currentPage+1;
        [self getAssetsAtPage:currentPage];
    }
}
- (IBAction)searchFilterTapped:(id)sender {
    
    UIView *button = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        button = sender;
    } else {
        button = [sender view];
    }
    if (popoverController) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController = nil;
    }
    
    if (searchFilterController) {
        searchFilterController = nil;
    }
    searchFilterController = [[SearchFilterView alloc] init];
    searchFilterController.filterType = InitialFilters;
    searchFilterController.delegate = (id)self;
    searchFilterController.categoryId = 0;
    UINavigationController *searchNavigationController = [[UINavigationController alloc] initWithRootViewController:searchFilterController];
    searchNavigationController.navigationBarHidden=NO;
    searchNavigationController.navigationBar.barTintColor=[UIColor lightGrayColor];
    popoverController = [[UIPopoverController alloc] initWithContentViewController:searchNavigationController];
    popoverController.backgroundColor=[UIColor lightGrayColor];
    [popoverController presentPopoverFromRect:button.frame inView:self.navigationController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (IBAction)downloadsPopUpTapped:(id)sender {
    
    UIView *button = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        button = (UIButton *)sender;
    }
    if (popoverControllerownloads) {
        [popoverControllerownloads dismissPopoverAnimated:YES];
        popoverControllerownloads = nil;
    }
    
    OfflineDownloads *downloads = [[OfflineDownloads alloc] init];
    downloads.title=@"Ongoing Downloads";
    UINavigationController *downloadsNav = [[UINavigationController alloc] initWithRootViewController:downloads];
    downloadsNav.navigationBar.barTintColor=[UIColor lightGrayColor];
    popoverControllerownloads = [[UIPopoverController alloc] initWithContentViewController:downloadsNav];
    popoverControllerownloads.backgroundColor=[UIColor lightGrayColor];
    popoverControllerownloads.delegate=self;
    
    [popoverControllerownloads presentPopoverFromRect:[(UIButton *)sender frame]
                                               inView:self.view
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
    
    
}


@end

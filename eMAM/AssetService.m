//
//  AssetService.m
//  eMAM
//
//  Created by APPLE on 20/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import "AssetService.h"

@implementation AssetService

+ (AssetService *)sharedInstance {
    static AssetService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}



- (NSString *)postBodyForPage:(int)page {
    
    NSString *orderBy = [Utilities orderByOptionValue];
    NSString *sortOrder = [Datastore sharedStore].sortOrder;
    int categoryId = [Datastore sharedStore].parentCategoryId;
    int subCategoryId = [Datastore sharedStore].subCategoryId;
    int projectId = [Datastore sharedStore].projectId;
    int assetTypeId = [Datastore sharedStore].assetTypeId;
    int assetMinSize  = [Datastore sharedStore].assetMinSize;
    int assetmaxSize = [Datastore sharedStore].assetMaxSize;
    int assetsPerPage = [Datastore sharedStore].assetsPerPage;
    int totalAssets = [Datastore sharedStore].totalAssets;
    
    NSString *postBody = @"<GetSearchAssets xmlns=\"http://tempuri.org/\">\
    <objSearch>\
    <RefineCategory>0</RefineCategory>\
    <RefineStateID>0</RefineStateID>\
    <RefineIsArchived>false</RefineIsArchived>\
    <RefineRating>0.0</RefineRating>\
    <RefineIsBooleanSearch>false</RefineIsBooleanSearch>\
    <ProjectID>%d</ProjectID>\
    <IsSorting>false</IsSorting>\
    <IsSearchClicked>false</IsSearchClicked>\
    <CurrentPage>%d</CurrentPage>\
    <NoOfAssetPerPage>%d</NoOfAssetPerPage>\
    <IsAdvance>false</IsAdvance>\
    <SubAssetType>%d</SubAssetType>\
    <SubProjectID>0</SubProjectID>\
    <SubCategory>%d</SubCategory>\
    <Category>%d</Category>\
    <StateID>0</StateID>\
    <UserID>0</UserID>\
    <RoleID>0</RoleID>\
    <ClientID>0</ClientID>\
    <OrderBy>%@</OrderBy>\
    <Order>%@</Order>\
    <IsArchived>false</IsArchived>\
    <Rating>0.0</Rating>\
    <IsBooleanSearch>false</IsBooleanSearch>\
    <MINASSETSIZE>%d</MINASSETSIZE>\
    <MAXASSETSIZE>%d</MAXASSETSIZE>\
    <MobilePlatformID>0</MobilePlatformID>\
    <FolderId>0</FolderId>\
    <IsOnline>false</IsOnline>\
    <IsFullTextSearch>false</IsFullTextSearch>\
    <SearchOption>0</SearchOption>\
    <IncludeSubcategories>false</IncludeSubcategories>\
    </objSearch>\
    <TotalNoOfAssests>%d</TotalNoOfAssests>\
    </GetSearchAssets>";
    
    postBody = [NSString stringWithFormat:postBody,projectId,page,assetsPerPage,assetTypeId,subCategoryId,categoryId,orderBy,sortOrder,assetMinSize,assetmaxSize,totalAssets];
    
    orderBy = nil;
    sortOrder = nil;
    
    return postBody;
}

- (NSString *)basicSearchPostBodyForPage:(int)page {
    
    NSString *searchString = [Datastore sharedStore].searchString;
    NSString *orderBy = [Utilities orderByOptionValue];
    NSString *sortOrder = [Datastore sharedStore].sortOrder;
    int categoryId = [Datastore sharedStore].parentCategoryId;
    int subCategoryId = [Datastore sharedStore].subCategoryId;
    int projectId = [Datastore sharedStore].projectId;
    int assetTypeId = [Datastore sharedStore].assetTypeId;
    int assetMinSize  = [Datastore sharedStore].assetMinSize;
    int assetmaxSize = [Datastore sharedStore].assetMaxSize;
    int assetsPerPage = [Datastore sharedStore].assetsPerPage;
    int totalAssets = [Datastore sharedStore].totalAssets;
    
    NSString *postBody = @"<GetSearchAssets xmlns=\"http://tempuri.org/\">\
    <objSearch>\
    <RefineCategory>0</RefineCategory>\
    <RefineStateID>0</RefineStateID>\
    <RefineIsArchived>false</RefineIsArchived>\
    <RefineRating>0.0</RefineRating>\
    <RefineIsBooleanSearch>false</RefineIsBooleanSearch>\
    <ProjectID>%d</ProjectID>\
    <IsSorting>false</IsSorting>\
    <IsSearchClicked>false</IsSearchClicked>\
    <Searchphrase>%@</Searchphrase>\
    <CurrentPage>%d</CurrentPage>\
    <NoOfAssetPerPage>%d</NoOfAssetPerPage>\
    <IsAdvance>false</IsAdvance>\
    <SubAssetType>%d</SubAssetType>\
    <SubProjectID>0</SubProjectID>\
    <SubCategory>%d</SubCategory>\
    <Category>%d</Category>\
    <StateID>0</StateID>\
    <UserID>0</UserID>\
    <RoleID>0</RoleID>\
    <ClientID>0</ClientID>\
    <OrderBy>%@</OrderBy>\
    <Order>%@</Order>\
    <IsArchived>false</IsArchived>\
    <Rating>0.0</Rating>\
    <IsBooleanSearch>false</IsBooleanSearch>\
    <MINASSETSIZE>%d</MINASSETSIZE>\
    <MAXASSETSIZE>%d</MAXASSETSIZE>\
    <MobilePlatformID>0</MobilePlatformID>\
    <FolderId>0</FolderId>\
    <IsOnline>false</IsOnline>\
    <IsFullTextSearch>false</IsFullTextSearch>\
    <SearchOption>0</SearchOption>\
    <IncludeSubcategories>false</IncludeSubcategories>\
    </objSearch>\
    <TotalNoOfAssests>%d</TotalNoOfAssests>\
    </GetSearchAssets>";
    
    postBody = [NSString stringWithFormat:postBody,projectId,searchString,page,assetsPerPage,assetTypeId,subCategoryId,categoryId,orderBy,sortOrder,assetMinSize,assetmaxSize,totalAssets];
    
    orderBy = nil;
    sortOrder = nil;
    
    return postBody;
}

- (NSString *)advancedSearchPostBodyForPage:(int)page {
    
    NSString *searchString = [Datastore sharedStore].searchString;
    NSString *orderBy = [Utilities orderByOptionValue];
    NSString *sortOrder = [Datastore sharedStore].sortOrder;
    
    int assetsPerPage = [Datastore sharedStore].assetsPerPage;
    int totalAssets = [Datastore sharedStore].totalAssets;
    
    NSString *searchOption = @"0";
    if ([[Datastore sharedStore].advancedRefineSearchOptions count] > 0) {
        for(NSString *option in [Datastore sharedStore].advancedRefineSearchOptions)    {
            if ([option isEqualToString:@"Video"]) {
                searchOption = [NSString stringWithFormat:@"%@,1",searchOption];
            } else if ([option isEqualToString:@"Audio"]) {
                searchOption = [NSString stringWithFormat:@"%@,2",searchOption];
            } else if ([option isEqualToString:@"Image"]) {
                searchOption = [NSString stringWithFormat:@"%@,3",searchOption];
            } else if ([option isEqualToString:@"Other Files"]) {
                searchOption = [NSString stringWithFormat:@"%@,4",searchOption];
            }
        }
        // remove first '0,'
        searchOption = [searchOption substringFromIndex:2];
    }
    
    int advancedSearchOption = 0;
    if ([[Datastore sharedStore].advancedSearchOption isEqualToString:@"Exact Match"]) {
        advancedSearchOption = 1;
    } else if ([[Datastore sharedStore].advancedSearchOption isEqualToString:@"Contains any of these words"]) {
        advancedSearchOption = 2;
    } else if ([[Datastore sharedStore].advancedSearchOption isEqualToString:@"Contain all these words"]) {
        advancedSearchOption = 3;
    } else if ([[Datastore sharedStore].advancedSearchOption isEqualToString:@"Contains this word"]) {
        advancedSearchOption = 4;
    }
    
    int advancedSearchCategoryId = [Datastore sharedStore].advancedSearchCategoryId;
    
    NSString *isArchived = ([[Datastore sharedStore].advancedSearchAssetStates containsObject:@"Archived"] ? @"true" : @"false");
    NSString *isOnline = ([[Datastore sharedStore].advancedSearchAssetStates containsObject:@"Online Storage"] ? @"true" : @"false");
    
    NSString *approvalState = @"0";
    if ( [[Datastore sharedStore].advancedSearchApprovalStates count] > 0) {
        for(NSString *state in [Datastore sharedStore].advancedSearchApprovalStates)    {
            if ([state isEqualToString:@"All"]) {
                approvalState = [NSString stringWithFormat:@"%@,0",approvalState];
                break;
            } else if ([state isEqualToString:@"Approved"]) {
                approvalState = [NSString stringWithFormat:@"%@,1",approvalState];
            } else if ([state isEqualToString:@"Rejected"]) {
                approvalState = [NSString stringWithFormat:@"%@,2",approvalState];
            } else if ([state isEqualToString:@"Pending for approval"]) {
                approvalState = [NSString stringWithFormat:@"%@,3",approvalState];
            }
        }
        // remove first '0,'
        approvalState = [approvalState substringFromIndex:2];
    }
    
    float rating = 0.0;
    if ([Datastore sharedStore].advancedSearchRating == 0) {
        // All is selected.
        rating = -1;
    } else {
        rating = [Datastore sharedStore].advancedSearchRating;
    }
    
    NSString *searchIn = @"0";
    if ([[Datastore sharedStore].advancedSearchInOptions count] > 0) {
        for(NSString *option in [Datastore sharedStore].advancedSearchInOptions)    {
            if ([option isEqualToString:@"Title"]) {
                searchIn = [NSString stringWithFormat:@"%@,1",searchIn];
            } else if ([option isEqualToString:@"Description"]) {
                searchIn = [NSString stringWithFormat:@"%@,2",searchIn];
            } else if ([option isEqualToString:@"Author"]) {
                searchIn = [NSString stringWithFormat:@"%@,3",searchIn];
            } else if ([option isEqualToString:@"Ingested by"]) {
                searchIn = [NSString stringWithFormat:@"%@,4",searchIn];
            } else if ([option isEqualToString:@"Asset tags"]) {
                searchIn = [NSString stringWithFormat:@"%@,5",searchIn];
            } else if ([option isEqualToString:@"Comments"]) {
                searchIn = [NSString stringWithFormat:@"%@,6",searchIn];
            } else if ([option isEqualToString:@"Closed Caption"]) {
                searchIn = [NSString stringWithFormat:@"%@,7",searchIn];
            } else if ([option isEqualToString:@"Embedded Metadata"]) {
                searchIn = [NSString stringWithFormat:@"%@,8",searchIn];
            } else if ([option isEqualToString:@"Custom Metadata"]) {
                searchIn = [NSString stringWithFormat:@"%@,9",searchIn];
            } else if ([option isEqualToString:@"Asset ID"]) {
                searchIn = [NSString stringWithFormat:@"%@,A",searchIn];
            } else if ([option isEqualToString:@"UUID"]) {
                searchIn = [NSString stringWithFormat:@"%@,B",searchIn];
            }
        }
        // remove first '0,'
        searchIn = [searchIn substringFromIndex:2];
    }
    
    NSString *isFullTextSearch = (([Datastore sharedStore].needFullTextSearch) ? @"true" : @"false");
    
    NSString *includeSubCategories = (([Datastore sharedStore].includeSubCategoriesInSearch) ? @"true" : @"false");
    
    NSString *postBody = @"<GetSearchAssets xmlns=\"http://tempuri.org/\">\
    <objSearch>\
    <SearchOption>%d</SearchOption>\
    <IsFullTextSearch>%@</IsFullTextSearch>\
    <SearchIn>%@</SearchIn>\
    <AssetType>%@</AssetType>\
    <Category>%d</Category>\
    <IncludeSubcategories>%@</IncludeSubcategories>\
    <IsOnline>%@</IsOnline>\
    <IsArchived>%@</IsArchived>\
    <ApprovalState>%@</ApprovalState>\
    <Rating>%.2f</Rating>\
    <IsAdvance>true</IsAdvance>\
    <Searchphrase>%@</Searchphrase>\
    <Order>%@</Order>\
    <OrderBy>%@</OrderBy>\
    <CurrentPage>%d</CurrentPage>\
    <NoOfAssetPerPage>%d</NoOfAssetPerPage>\
    </objSearch>\
    <TotalNoOfAssests>%d</TotalNoOfAssests>\
    </GetSearchAssets>";
    
    postBody = [NSString stringWithFormat:postBody,advancedSearchOption,isFullTextSearch,searchIn,searchOption,advancedSearchCategoryId,includeSubCategories,isOnline,isArchived,approvalState,rating,searchString,sortOrder,orderBy,page,assetsPerPage,totalAssets];
    
    orderBy = nil;
    sortOrder = nil;
    
    NSLog(@"Asset Service : Advance Search Postbody : %@",postBody);
    
    return postBody;
}





#pragma mark - Asset Services

- (void)getUserBasket:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetUserBasket xmlns=\"http://tempuri.org/\">\
                          </GetUserBasket>"];
    NSString *soapAction = @"http://tempuri.org/GetUserBasket";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:^(NSDictionary *dictionary, NSError *error){
        if (!error) {
            NSError *parseError = [self processBasketResponse:dictionary];
            handler(dictionary,parseError);
        }
        handler(dictionary,error);
    }];
    
}

- (NSError *)processBasketResponse:(NSDictionary *)dictionary {
    BOOL authenticationExpired = YES;
    dictionary = [dictionary objectForKey:@"GetUserBasketResponse"];
    
    
    
    // NSLog(@"Asset Service : dictionary : %@",dictionary);
    
    
    
    
    NSDictionary *xmlResponse = [[[dictionary objectForKey:@"GetUserBasketResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
    
    
    NSLog(@"Asset Service : xmlResponse : %@",xmlResponse);
    
    
    id assetList = [xmlResponse objectForKey:@"Table"];
    if (! assetList) {
        NSLog(@"Asset Service : No AssetList Found...");
    }
    if ([assetList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setBasketAssets:assetList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:assetList forKey:@"BasketAssetList"];
    } else if ([assetList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setBasketAssets:[NSArray arrayWithObject:assetList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:assetList] forKey:@"BasketAssetList"];
    }
    
    return nil;
}


- (NSError *)processResponse:(NSDictionary *)dictionary {
    BOOL authenticationExpired = YES;
    dictionary = [dictionary objectForKey:@"GetSearchAssetsResponse"];
    
    
    
   // NSLog(@"Asset Service : dictionary : %@",dictionary);
    
    int totalNumberOfAssets = [[[dictionary objectForKey:@"TotalNoOfAssests"] objectForKey:@"text"] intValue];
    [[Datastore sharedStore] setTotalAssets:totalNumberOfAssets];
    
    NSDictionary *xmlResponse = [[[dictionary objectForKey:@"GetSearchAssetsResult"] objectForKey:@"diffgr:diffgram"] objectForKey:@"NewDataSet"];
    
    
    //NSLog(@"Asset Service : xmlResponse : %@",xmlResponse);
    
    
    id assetList = [xmlResponse objectForKey:@"Table"];
    if (! assetList) {
        NSLog(@"Asset Service : No AssetList Found...");
    }
    if ([assetList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssets:assetList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:assetList forKey:@"AssetList"];
    } else if ([assetList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssets:[NSArray arrayWithObject:assetList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:assetList] forKey:@"AssetList"];
    }
    
    id projectList = [xmlResponse objectForKey:@"Table1"];
    if (! projectList) {
        NSLog(@"Asset Service : No ProjectList Found...");
    }
    if ([projectList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setProjects:projectList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:projectList forKey:@"ProjectList"];
    } else if ([projectList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setProjects:[NSArray arrayWithObject:projectList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:projectList] forKey:@"ProjectList"];
    }
    
    id categoryList = [xmlResponse objectForKey:@"Table2"];
    if (! categoryList) {
        NSLog(@"Asset Service : No CategoryList Found...");
    }
    if ([categoryList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setCategories:categoryList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:categoryList forKey:@"CategoryList"];
    } else if ([categoryList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setCategories:[NSArray arrayWithObject:categoryList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:categoryList] forKey:@"CategoryList"];
    }
    
    id assetTypeList = [xmlResponse objectForKey:@"Table3"];
    if (! assetTypeList) {
        NSLog(@"Asset Service : No AssetTypeList Found...");
    }
    if ([assetTypeList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssetTypes:assetTypeList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:assetTypeList forKey:@"AssetTypeList"];
    } else if ([assetTypeList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssetTypes:[NSArray arrayWithObject:assetTypeList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:assetTypeList] forKey:@"AssetTypeList"];
    }
    
    id assetSizeList = [xmlResponse objectForKey:@"Table4"];
    if (! assetSizeList) {
        NSLog(@"Asset Service : No AssetSizeList Found...");
    }
    if ([assetSizeList isKindOfClass:[NSArray class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssetSizes:assetSizeList];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:assetSizeList forKey:@"AssetSizeList"];
    } else if ([assetSizeList isKindOfClass:[NSDictionary class]]) {
        authenticationExpired = NO;
        [[Datastore sharedStore] setAssetSizes:[NSArray arrayWithObject:assetSizeList]];
        // for temp
        [[NSUserDefaults standardUserDefaults] setValue:[NSArray arrayWithObject:assetSizeList] forKey:@"AssetSizeList"];
    }
    
    if (authenticationExpired && ![Datastore sharedStore].isInSearch) {
        NSLog(@"Asset Service : Found Response Missing : Mapped as Authentication Fail...");
        
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:@"Authentication Expired" forKey:NSLocalizedDescriptionKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Authentication Expired" object:nil];
        NSError *error = [NSError errorWithDomain:@"com.emam.ios" code:400 userInfo:userInfo];
        return error;
    }
    
    return nil;
}



- (void)getAssetsAtPage:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = nil;
    if ([Datastore sharedStore].isInAdvancedSearch   && [Datastore sharedStore].searchString) {
        // Advanced Search
        soapBody = [self advancedSearchPostBodyForPage:page];
    } else if ([Datastore sharedStore].isInSearch  && [Datastore sharedStore].searchString) {
        // Basic Search
        soapBody = [self basicSearchPostBodyForPage:page];
    } else {
        // Normal Assets
        soapBody = [self postBodyForPage:page];
    }
    
    NSLog(@"Asset Service : PostBody : %@",soapBody);
    
    NSString *soapAction = @"http://tempuri.org/GetSearchAssets";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:^(NSDictionary *dictionary, NSError *error){
        if (!error) {
            
            //NSLog(@"Assets :%@",dictionary);
            NSError *parseError = [self processResponse:dictionary];
            handler(dictionary,parseError);
        }
        handler(dictionary,error);
    }];
    
}

- (void)getAllCategoriesForSearchPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetCategories xmlns=\"http://tempuri.org/\">\
                          <CheckAssignCategoryPermission>false</CheckAssignCategoryPermission>\
                          </GetCategories>"];
    NSString *soapAction = @"http://tempuri.org/GetCategories";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)addToUserBasketAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = [NSString stringWithFormat:@"<UpdateUserBasket xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <Asset_Version_Id>%d</Asset_Version_Id>\
                          <IsDeleted>false</IsDeleted>\
                          </UpdateUserBasket>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/UpdateUserBasket";

    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)removeFromUserBasketAssetId:(int)assetId versionId:(int)versionId performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = [NSString stringWithFormat:@"<UpdateUserBasket xmlns=\"http://tempuri.org/\">\
                          <AssetId>%d</AssetId>\
                          <Asset_Version_Id>%d</Asset_Version_Id>\
                          <IsDeleted>true</IsDeleted>\
                          </UpdateUserBasket>",assetId,versionId];
    NSString *soapAction = @"http://tempuri.org/UpdateUserBasket";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)removeAllItemFromUserBasketPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    NSString *soapBody = [NSString stringWithFormat:@"<UpdateUserBasket xmlns=\"http://tempuri.org/\">\
                          <AssetId>0</AssetId>\
                          <Asset_Version_Id>0</Asset_Version_Id>\
                          <IsDeleted>true</IsDeleted>\
                          </UpdateUserBasket>"];
    NSString *soapAction = @"http://tempuri.org/UpdateUserBasket";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}

- (void)getAssetsInBasketAtPage:(int)page performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler{
    
    
    NSString *soapBody = [NSString stringWithFormat:@"<GetAssetsInBasket xmlns=\"http://tempuri.org/\">\
                          <objCriteria>\
                          <ProjectID>0</ProjectID>\
                          <REFINEPROJECTID>0</REFINEPROJECTID>\
                          <CategoryID>0</CategoryID>\
                          <REFINECATEGORYID>0</REFINECATEGORYID>\
                          <AssetTypeID>0</AssetTypeID>\
                          <PageNo>%d</PageNo>\
                          <AssetsPerPage>%d</AssetsPerPage>\
                          <OrderBy>ReelSortOrder</OrderBy>\
                          <MIN_ASSET_SIZE>0</MIN_ASSET_SIZE>\
                          <MAX_ASSET_SIZE>0</MAX_ASSET_SIZE>\
                          <MobilePlatformID>0</MobilePlatformID>\
                          </objCriteria>\
                          </GetAssetsInBasket>",page,10];
    
    NSString *soapAction = @"http://tempuri.org/GetAssetsInBasket";
    
    [[self class] sendRequestASI:[[self class] soapPostRequestWithSoapAction:soapAction soapBody:soapBody parameters:nil] completionHandler:handler];
    
}




@end

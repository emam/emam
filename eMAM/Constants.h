//
//  Constants.h
//  eMAM
//
//  Created by APPLE on 31/07/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#ifndef eMAM_Constants_h
#define eMAM_Constants_h

#pragma mark - General Constants

#define APPNAME @"eMAM"
#define KEncryptionKey @"!$@$%^%&"
#define APPFONTNAME @"Helvetica"
#define APPBOLDFONTNAME @"Helvetica-Bold"
#define ISIPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? YES : NO
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define LIBRARY_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/EMAM"]
#define ROOTSERVERNAME [[NSUserDefaults standardUserDefaults] valueForKey:KROOTSERVER]

#pragma mark ------

#define BEGIN_BLOCK do {
#define END_BLOCK   } while(0);
#define KOPERATIONSTATUS @"OperationStatus"


#pragma mark - User Constants

#define KUSERNAME @"UserName"
#define KPASSWORD @"Password"
#define KUSERID @"UserId"
#define KUSERFIRSTNAME @"UserFirstName"
#define KUSEREMAILID @"UserEmailId"

#pragma mark - Server Constants

#define KROOTSERVER @"rootServerAddress"
#define KENCRYPTEDPASSWORD @"encryptedPassword"
#define KSERVERLICENSEKEY @"serverLicenseKey"
#define KSERVERPORT @"serverPort"
#define KSERVICEGATEWAY @"ServiceGateWay"
#define KANALYTICSGATEWAY @"AnalyticsGateWay"
#define KSHAREKEY @"group.com.empress.emamipad"



#endif

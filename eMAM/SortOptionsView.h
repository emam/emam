//
//  SortOptionsView.h
//  eMAM
//
//  Created by APPLE on 18/09/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum  {
    SortOptions = 1,
    AssetOptions = 2
}ViewType;

@protocol SortOptionsViewDelegate <NSObject>

@optional
- (void) SortOptionsViewOptionChanged:(NSString *)option ViewType:(ViewType)type;

@end

@class SortOptionsDelegate;

@interface SortOptionsView : UIViewController{
    
    NSArray *options;
}
@property (nonatomic, assign) ViewType viewType;
@property (nonatomic, retain) id<SortOptionsViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *sortTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegCtrl;
@property (nonatomic, retain) NSArray *options;
- (IBAction)segmentControlIndexChanged:(id)sender;

@end

//
//  UserService.h
//  eMAM
//
//  Created by APPLE on 06/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseWebRequest.h"
#import "AFHTTPRequestOperation.h"
#import "XMLReader.h"

@interface UserService : BaseWebRequest

+ (UserService *)sharedInstance;

#pragma mark - Pinig Server

- (void)pingToServer:(NSString *)urlString performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - User Methods

- (void)authenticateUser:(NSString *)username password:(NSString *)password performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler ;
- (void)passwordReset:(NSString *)username performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler ;
- (void)getUserStatusPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;
- (void)getUserFeaturesperformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;
- (void)getIngestProfileId:(int)userId unitId:(int)unitId ingestProfile:(int)profileID performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

#pragma mark - Client Logo

- (void)getClientLogoPerformRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;


@end

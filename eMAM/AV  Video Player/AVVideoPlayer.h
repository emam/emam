//
//  AVVideoPlayer.h
//  AVPlayerSample
//
//  Created by Naveen Shan on 11/3/12.
//  Copyright (c) 2012 Naveen Shan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "Subclip.h"

@class AVPlayer;
@class Subclip;

@interface AVVideoPlayer : UIView

@property (nonatomic, copy) NSURL* URL;
@property (nonatomic, assign) float frameRate;
@property (nonatomic, strong) NSMutableArray *videoMarkers;
@property (nonatomic, strong) NSMutableArray *videoSubclips;

@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

@property (nonatomic, assign)  BOOL needShowMarker;
@property (nonatomic, assign)  BOOL needShowSubclip;

+ (AVVideoPlayer *)sharedInstance;
+ (void)destruct;

- (void)play:(id)sender;
- (void)pause:(id)sender;

- (void)showFullScreenOption:(BOOL)show;

- (CMTime)playerItemDuration;
+ (NSString *)formattedStringForDuration:(CMTime)time;
+ (NSString *)formattedStringForDurationSubClip:(CMTime)time  ;

#pragma mark Marker Control

- (void)addNewMarker;
- (void)addAllMarkers;
- (void)removeAllMarkers;

#pragma mark Subclip Control

- (CMTime)newSetInTime;
- (CMTime)fileSetOutTime;
- (void)addSubclip:(Subclip *)subclip;
- (void)addNewSubClipWithSetInTime:(CMTime)setInTime;
- (void)addNewSubClipWithSetInDefaultTime:(CMTime)setInTime;



- (void)editSubClipWithSetInTime:(CMTime )setInTime  index:(int)indexToReplace;
- (void)removeAllSubclips;

@end

/**************************************************************************************
 *  File Name      : PreviewView.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 14-12-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "PreviewView.h"

@interface UILabel (Allignment)

-(void)alignWithText;
@end

@implementation UILabel (Allignment)

-(void)alignWithText    {
    //current label frame,
    //to ensure the label frame is not alter above this value.
	CGSize labelSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
	//to find size of label that need to include its text.
	CGSize thelabelSize = [self.text sizeWithFont:self.font constrainedToSize:labelSize lineBreakMode:self.lineBreakMode];
    //set the label with new frame.
	self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, labelSize.width, thelabelSize.height);
}

@end

#pragma mark -


typedef enum    {
    TitleLabelTag           = 3030,
    AddedDateLabelTag       = 3031,
    AddedDateValueLabelTag  = 3032,
    DescriptionLabelTag     = 3033,
    TagLabelTag             = 3034,
    TagValueLabelTag        = 3035,
    CategoriesLabelTag      = 3036,
    CategoriesValueLabelTag = 3037,
    ProjectsLabelTag        = 3038,
    ProjectsValueLabelTag   = 3039,
} PreviewViewSubViewTag;

@interface PreviewView ()

@property (nonatomic, assign) int requiredHeight;
@property (nonatomic, strong) UIScrollView *contentView;

@end

#pragma mark -

@implementation PreviewView
// Public
@synthesize versionInfo = _versionInfo;
// Private
@synthesize requiredHeight = _requiredHeight;
@synthesize contentView = _contentView;

#pragma mark -

- (id)initWithFrame:(CGRect)frame   {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

#pragma mark - Override Setters

- (void)setVersionInfo:(NSDictionary *)versionInfo  {
    _versionInfo = versionInfo;
    
    NSString *text = nil;
    // Title Label
    UILabel *titleLabel = (UILabel *)[self.contentView viewWithTag:TitleLabelTag];
    text = [[_versionInfo objectForKey:@"TITLE"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        [titleLabel setText:text];
    } else {
        [titleLabel setText:@"Unknown Asset"];
    }
    
    // Date Added Labels
    UILabel *addedDateValueLabel = (UILabel *)[self.contentView viewWithTag:AddedDateValueLabelTag];
    text = [[_versionInfo objectForKey:@"INGESTED_ON"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        addedDateValueLabel.hidden = NO;
        [addedDateValueLabel setText:text];
    }
    
    // Description Labels
    UILabel *descriptionLabel = (UILabel *)[self.contentView viewWithTag:DescriptionLabelTag];
    text = [[_versionInfo objectForKey:@"ASSET_DESC"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        descriptionLabel.hidden = NO;
        [descriptionLabel setText:text];
    }
    
    // Tags Labels
    UILabel *tagValueLabel = (UILabel *)[self.contentView viewWithTag:TagValueLabelTag];
    text = [[_versionInfo objectForKey:@"Asset_Tags"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        tagValueLabel.hidden = NO;
        [tagValueLabel setText:text];
    }
    
    // Categories Labels
    UILabel *categoriesValueLabel = (UILabel *)[self.contentView viewWithTag:CategoriesValueLabelTag];
    text = [[_versionInfo objectForKey:@"Asset_Categories"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        categoriesValueLabel.hidden = NO;
        [categoriesValueLabel setText:text];
    }
    
    // Projects Labels
    UILabel *projectsValueLabel = (UILabel *)[self.contentView viewWithTag:ProjectsValueLabelTag];
    text = [[_versionInfo objectForKey:@"Asset_In_Projects"] objectForKey:@"text"];
    if (text && [text length] > 0) {
        projectsValueLabel.hidden = NO;
        [projectsValueLabel setText:text];
    }
    
    [self alignViews];
}

- (void)layoutSubviews  {
    [super layoutSubviews];
    self.contentView.frame = self.bounds;
    self.contentView.contentSize = self.bounds.size;
    if (self.bounds.size.height < self.requiredHeight) {
        self.contentView.contentSize = CGSizeMake(self.contentView.contentSize.width, self.requiredHeight);
    }
    [self alignViews];
}

#pragma mark -

- (void)initView    {
    self.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    self.contentView = [[UIScrollView alloc] initWithFrame:[self bounds]];
    [self addSubview:self.contentView];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.tag = TitleLabelTag;
    titleLabel.numberOfLines = 1;
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    
//    titleLabel.textColor = [UIColor colorWithRed:0.6 green:0.078 blue:0.019 alpha:1.0];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:30.0];
    [self.contentView addSubview:titleLabel];
 
    UILabel *addedDateLabel = [[UILabel alloc] init];
    addedDateLabel.tag = AddedDateLabelTag;
    addedDateLabel.numberOfLines = 1;
    addedDateLabel.lineBreakMode = UILineBreakModeWordWrap;
    addedDateLabel.text = @"Date Added      :";
    addedDateLabel.textColor = [UIColor blackColor];
    addedDateLabel.backgroundColor = [UIColor clearColor];
    addedDateLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
    [self.contentView addSubview:addedDateLabel];
    
    UILabel *addedDateValueLabel = [[UILabel alloc] init];
    addedDateValueLabel.tag = AddedDateValueLabelTag;
    addedDateValueLabel.numberOfLines = 1;
    addedDateValueLabel.hidden = YES;
    addedDateValueLabel.lineBreakMode = UILineBreakModeWordWrap;
    addedDateValueLabel.textColor = [UIColor blackColor];
    addedDateValueLabel.backgroundColor = [UIColor clearColor];
    addedDateValueLabel.font = [UIFont fontWithName:APPFONTNAME size:20.0];
    [self.contentView addSubview:addedDateValueLabel];
    
    UILabel *descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.tag = DescriptionLabelTag;
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.hidden = YES;
    descriptionLabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionLabel.textColor = [UIColor blackColor];
    descriptionLabel.backgroundColor = [UIColor clearColor];
    descriptionLabel.font = [UIFont fontWithName:APPFONTNAME size:20.0];
    [self.contentView addSubview:descriptionLabel];
    
    UILabel *tagLabel = [[UILabel alloc] init];
    tagLabel.tag = TagLabelTag;
    tagLabel.numberOfLines = 1;
    tagLabel.lineBreakMode = UILineBreakModeWordWrap;
    tagLabel.text = @"Tags                 :";
    tagLabel.textColor = [UIColor blackColor];
    tagLabel.backgroundColor = [UIColor clearColor];
    tagLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
    [self.contentView addSubview:tagLabel];
    
    UILabel *tagValueLabel = [[UILabel alloc] init];
    tagValueLabel.tag = TagValueLabelTag;
    tagValueLabel.numberOfLines = 1;
    tagValueLabel.hidden = YES;
    tagValueLabel.lineBreakMode = UILineBreakModeWordWrap;
    tagValueLabel.textColor = [UIColor blackColor];
    tagValueLabel.backgroundColor = [UIColor clearColor];
    tagValueLabel.font = [UIFont fontWithName:APPFONTNAME size:20.0];
    [self.contentView addSubview:tagValueLabel];
    
    UILabel *categoriesLabel = [[UILabel alloc] init];
    categoriesLabel.tag = CategoriesLabelTag;
    categoriesLabel.numberOfLines = 1;
    categoriesLabel.lineBreakMode = UILineBreakModeWordWrap;
    categoriesLabel.text = @"Categories       :";
    categoriesLabel.textColor = [UIColor blackColor];
    categoriesLabel.backgroundColor = [UIColor clearColor];
    categoriesLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
    [self.contentView addSubview:categoriesLabel];
    
    UILabel *categoriesValueLabel = [[UILabel alloc] init];
    categoriesValueLabel.tag = CategoriesValueLabelTag;
    categoriesValueLabel.numberOfLines = 1;
    categoriesValueLabel.hidden = YES;
    categoriesValueLabel.lineBreakMode = UILineBreakModeWordWrap;
    categoriesValueLabel.textColor = [UIColor blackColor];
    categoriesValueLabel.backgroundColor = [UIColor clearColor];
    categoriesValueLabel.font = [UIFont fontWithName:APPFONTNAME size:20.0];
    [self.contentView addSubview:categoriesValueLabel];
    
    UILabel *projectsLabel = [[UILabel alloc] init];
    projectsLabel.tag = ProjectsLabelTag;
    projectsLabel.numberOfLines = 1;
    projectsLabel.lineBreakMode = UILineBreakModeWordWrap;
    projectsLabel.text = @"Projects           :";
    projectsLabel.textColor = [UIColor blackColor];
    projectsLabel.backgroundColor = [UIColor clearColor];
    projectsLabel.font = [UIFont fontWithName:APPBOLDFONTNAME size:15.0];
    [self.contentView addSubview:projectsLabel];
    
    UILabel *projectsValueLabel = [[UILabel alloc] init];
    projectsValueLabel.tag = ProjectsValueLabelTag;
    projectsValueLabel.numberOfLines = 1;
    projectsValueLabel.hidden = YES;
    projectsValueLabel.lineBreakMode = UILineBreakModeWordWrap;
    projectsValueLabel.textColor = [UIColor blackColor];
    projectsValueLabel.backgroundColor = [UIColor clearColor];
    projectsValueLabel.font = [UIFont fontWithName:APPFONTNAME size:20.0];
    [self.contentView addSubview:projectsValueLabel];
}

- (void)alignViews  {
    int gap = 10;
    int padding = 20;
    int maxHeight = 150;
    int constWidth = 150;
    int valueLabelXPosition = (padding + constWidth + gap);
    int valueLabelMaxWidth = (self.frame.size.width - (padding + constWidth + gap + padding));
    int yPosition = padding;
    
    // Title Label
    UILabel *titleLabel = (UILabel *)[self.contentView viewWithTag:TitleLabelTag];
    titleLabel.frame = CGRectMake(padding, yPosition, (self.frame.size.width - (2 * padding)), 40);
    
    yPosition += (titleLabel.frame.size.height + gap);
    // Date Added Labels
    UILabel *addedDateLabel = (UILabel *)[self.contentView viewWithTag:AddedDateLabelTag];
    addedDateLabel.frame = CGRectMake(padding, yPosition, constWidth, 20);
    
    UILabel *addedDateValueLabel = (UILabel *)[self.contentView viewWithTag:AddedDateValueLabelTag];
    addedDateValueLabel.frame = CGRectMake(valueLabelXPosition, yPosition, valueLabelMaxWidth, 20);
    
    yPosition += ((addedDateValueLabel.hidden) ? (addedDateLabel.frame.size.height + gap) :
                  (addedDateValueLabel.frame.size.height + gap));
    // Description Labels
    UILabel *descriptionLabel = (UILabel *)[self.contentView viewWithTag:DescriptionLabelTag];
    descriptionLabel.frame = CGRectMake(padding, yPosition, (self.frame.size.width - (2 * padding)), maxHeight);
    [descriptionLabel alignWithText];
    
    yPosition += ((descriptionLabel.hidden) ? (gap) :
                  (descriptionLabel.frame.size.height + gap));
    // Tags Labels
    UILabel *tagLabel = (UILabel *)[self.contentView viewWithTag:TagLabelTag];
    tagLabel.frame = CGRectMake(padding, yPosition, constWidth, 20);
    
    UILabel *tagValueLabel = (UILabel *)[self.contentView viewWithTag:TagValueLabelTag];
    tagValueLabel.frame = CGRectMake(valueLabelXPosition, yPosition, valueLabelMaxWidth, maxHeight);
    [tagValueLabel alignWithText];
    
    yPosition += ((tagValueLabel.hidden) ? (tagLabel.frame.size.height + gap) :
                  (tagValueLabel.frame.size.height + gap));
    // Categories Labels
    UILabel *categoriesLabel = (UILabel *)[self.contentView viewWithTag:CategoriesLabelTag];
    categoriesLabel.frame = CGRectMake(padding, yPosition, constWidth, 20);
    
    UILabel *categoriesValueLabel = (UILabel *)[self.contentView viewWithTag:CategoriesValueLabelTag];
    categoriesValueLabel.frame = CGRectMake(valueLabelXPosition, yPosition, valueLabelMaxWidth, maxHeight);
    [categoriesValueLabel alignWithText];
    
    yPosition += ((categoriesValueLabel.hidden) ? (categoriesLabel.frame.size.height + gap) :
                  (categoriesValueLabel.frame.size.height + gap));
    // Projects Labels
    UILabel *projectsLabel = (UILabel *)[self.contentView viewWithTag:ProjectsLabelTag];
    projectsLabel.frame = CGRectMake(padding, yPosition, constWidth, 20);
    
    UILabel *projectsValueLabel = (UILabel *)[self.contentView viewWithTag:ProjectsValueLabelTag];
    projectsValueLabel.frame = CGRectMake(valueLabelXPosition, yPosition, valueLabelMaxWidth, maxHeight);
    [projectsValueLabel alignWithText];
    
    yPosition += ((projectsValueLabel.hidden) ? (projectsLabel.frame.size.height + gap) :
                  (projectsValueLabel.frame.size.height));
    
    self.requiredHeight = (yPosition + padding);
}

@end

//
//  LeftPaddingLabel.m
//  EMAM
//
//  Created by Naveen Shan on 01/05/13.
//  Copyright (c) 2012 Naveen Shan. All rights reserved.
//

#import "LeftPaddingLabel.h"

@implementation LeftPaddingLabel

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 40, 0, 5};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end

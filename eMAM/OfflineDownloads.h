//
//  OfflineDownloads.h
//  eMAM
//
//  Created by Dony George on 20/03/15.
//  Copyright (c) 2015 Empress Cybernetic Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfflineDownloads : UIViewController{
    NSMutableArray *currentDownloads;
}

@property (weak, nonatomic) IBOutlet UITableView *downloadsTable;
@end

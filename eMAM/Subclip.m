/**************************************************************************************
 *  File Name      : Subclip.m
 *  Project Name   : EMAM
 *  Description    : N/A
 *  Version        : 1.0
 *  Created by     : Naveen Shan
 *  Created on     : 02-01-2012
 *  Copyright (C) 2012 Empress Cyber. All Rights Reserved.
 ***************************************************************************************/

#import "Subclip.h"

@implementation Subclip

@synthesize subclipId = _subclipId;
@synthesize clipName = _clipName;
@synthesize setInTimeString = _setInTimeString;
@synthesize setOutTimeString = _setOutTimeString;
@synthesize description = _description;

@synthesize setInTime = _setInTime;
@synthesize setOutTime = _setOutTime;
@synthesize subclipView = _subclipView;

#pragma mark -

- (id)initWithDictionary:(NSDictionary *)dictionary  {
    self = [super init];
    if (self) {
        self.subclipId = [[dictionary objectForKey:@"id"] objectForKey:@"text"];
        self.clipName = [[dictionary objectForKey:@"Clip_Name"] objectForKey:@"text"];
        self.description = [[dictionary objectForKey:@"Description"] objectForKey:@"text"];
        self.setInTimeString = [[dictionary objectForKey:@"SetInTime"] objectForKey:@"text"];
        self.setOutTimeString = [[dictionary objectForKey:@"SetOutTime"] objectForKey:@"text"];
        [self setCMTimes];
    }
    return self;
}




- (void)setCMTimes   {
    NSLog(@"EMAM Subclip View : SetInTimeString : %@",self.setInTimeString);
    self.setInTime = [self cmTimeForString:self.setInTimeString];
    NSLog(@"EMAM Subclip View : SetOutTimeString : %@",self.setOutTimeString);
    self.setOutTime = [self cmTimeForString:self.setOutTimeString];
}

- (CMTime)cmTimeForString:(NSString *)string    {
    CMTime cmTime;
    
    
    NSString *firstSection = [string substringWithRange:NSMakeRange(0,2)];
    NSString *secondSection = [string substringWithRange:NSMakeRange(2,2)];
    NSString *thirdSection = [string substringWithRange:NSMakeRange(4,2)];
    NSString *fourthSection = [string substringWithRange:NSMakeRange(6,2)];
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:firstSection];
    [array addObject:secondSection];
    [array addObject:thirdSection];
    [array addObject:fourthSection];
    
    NSArray *components = [[NSArray alloc] initWithArray:array];
    
    
    
   //  NSArray *components = [self.timeCode componentsSeparatedByString:@":"];
    
    if ([components count] >= 4) {
        float hourInSeconds = [[components objectAtIndex:0] intValue] * 60 * 60;
        float minitueInSeconds = [[components objectAtIndex:1] intValue] * 60;
        float seconds = [[components objectAtIndex:2] intValue];
        
        NSString *frameString = [NSString stringWithFormat:@".%@",[components objectAtIndex:3]];
        float timeFrameInSeconds = [frameString floatValue] * [AVVideoPlayer sharedInstance].frameRate;
        float totalSeconds = hourInSeconds + minitueInSeconds + seconds;
        NSString *floatString = [NSString stringWithFormat:@"%.0f.%.0f",totalSeconds,timeFrameInSeconds];
        totalSeconds = [floatString floatValue];
        
        cmTime = CMTimeMakeWithSeconds(totalSeconds, NSEC_PER_SEC);
        CMTimeShow(cmTime);
    }
    
    return cmTime;
}


@end

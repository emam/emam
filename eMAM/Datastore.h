//
//  Datastore.h
//  eMAM
//
//  Created by APPLE on 08/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    Video_Assets_Support,
    Audio_Assets_Support,
    Image_Assets_Support,
    Document_Assets_Support,
    Projects,
    FTP_Ingest,
    Email_Ingest,
    Tape_Ingest,
    Advance_Search,
    Custom_Metadata,
    Annotations,
    Proxy_Editing,
    Rating_Feature,
    Internal_Approval,
    External_Approval,
    Asset_Categorization,
    Archive,
    Delivery_Via_Web_Download,
    Delivery_To_FTP,
    Delivery_To_Email,
    Delivery_To_Tape,
    Delivery_To_DVD,
    Report_By_Asset_History,
    Report_By_Asset_Type,
    Report_By_User,
    Live_Dashboard,
    Site_Analytics,
    Assets_Export_Service,
    History_Logs_Export_Service,
    RSS_Feed,
    Synchronization,
    Key_Frame_Extraction,
    Web_Upload,
    Order,
    Delivery_To_Network_Folder,
    Delivery_To_iTunes,
    Delivery_To_Joost,
    FCP_EXPORT,
    eBIN_Feature,
    ASSET_PER_PAGE,
    SORT_Feature,
    THEMES,
    SEARCH,
    CATEGORIES,
    SEARCH_FILTER,
    MERGE,
    FLIP_FACTORY,
    RHOZET,
    EMBED_CODE,
    RegistrationForm,
    AmazonS3Storage,
    Marker_Feature,
    eSEND_Feature
} UserFeature;

@interface Datastore : NSObject


#pragma mark

@property (nonatomic, assign) int userRoleID;
@property (nonatomic, retain) NSMutableArray *userFeatures;


@property (nonatomic, assign) int totalAssets;
@property (nonatomic, assign) int assetsPerPage;
@property (nonatomic, retain) NSString *sortOption;
@property (nonatomic, retain) NSString *sortOrder;

@property (nonatomic, retain) NSArray *assets;
@property (nonatomic, retain) NSArray *basketAssets;
@property (nonatomic, retain) NSArray *projects;
@property (nonatomic, retain) NSArray *categories;
@property (nonatomic, retain) NSArray *assetTypes;
@property (nonatomic, retain) NSArray *assetSizes;

@property (nonatomic, assign) int parentCategoryId;
@property (nonatomic, assign) int subCategoryId;
@property (nonatomic, assign) int projectId;
@property (nonatomic, assign) int subProjectId;

@property (nonatomic, assign) int assetTypeId;
@property (nonatomic, assign) int assetMinSize;
@property (nonatomic, assign) int assetMaxSize;

// for search filtering
@property (nonatomic, retain) NSString *assetTypeSearchFilter;
@property (nonatomic, retain) NSString *categorySearchFilter;
@property (nonatomic, retain) NSString *projectSearchFilter;
@property (nonatomic, retain) NSString *assetSizeSearchFilter;

// for advance search
@property (nonatomic, assign) BOOL isInSearch;
@property (nonatomic, assign) BOOL isInAdvancedSearch;
@property (nonatomic, retain) NSString *searchString;

@property (nonatomic, assign) NSString *advancedSearchOption;
@property (nonatomic, assign) BOOL needFullTextSearch;

@property (nonatomic, retain) NSMutableArray *advancedSearchInOptions;
@property (nonatomic, retain) NSMutableArray *advancedRefineSearchOptions;
@property (nonatomic, retain) NSMutableArray *advancedSearchApprovalStates;
@property (nonatomic, retain) NSMutableArray *advancedSearchAssetStates;

@property (nonatomic, assign) int advancedSearchCategoryId;
@property (nonatomic, assign) BOOL includeSubCategoriesInSearch;

@property (nonatomic, assign) float advancedSearchRating;

@property (nonatomic, retain) NSArray *searchCategories;

// for Advanced Asset Service
@property (nonatomic, retain) NSDictionary *currentPreviewInfo;
@property (nonatomic, retain) NSArray *videoFrameRates;


@property (nonatomic, retain) NSString *frameDuration;




#pragma mark - Data access & Clear

+ (Datastore *)sharedStore;
- (void)clearAllFilters;


#pragma mark - User Feature Methods

- (BOOL)setUserFeature:(NSString *)userFeature;
- (BOOL)allowUserFeature:(UserFeature)userFeature;

#pragma mark

- (NSArray *)categoriesWithID:(int)categoryId;
- (NSMutableArray *)subCategoriesWithParentID:(int)categoryId;

#pragma mark

- (NSArray *)searchCategoriesWithID:(int)categoryId;
- (NSMutableArray *)subSearchCategoriesWithParentID:(int)categoryId;


@end

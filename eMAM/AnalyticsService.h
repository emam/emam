//
//  AnalyticsService.h
//  eMAM
//
//  Created by APPLE on 22/08/14.
//  Copyright (c) 2014 Empress Cybernetic Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseWebRequest.h"
#import "AFHTTPRequestOperation.h"
#import "XMLReader.h"

@interface AnalyticsService : BaseWebRequest

+ (AnalyticsService *)sharedInstance;

#pragma mark - Analytics

- (void)updateHitCountForFeature:(NSString *)featureCode performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

- (void)updateLoggedInFlag:(int)loggedInFlag forUser:(NSString *)userEmail performRequestWithHandler:(void (^)(NSDictionary *dictionary, NSError *error))handler;

@end
